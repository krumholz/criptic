.. highlight:: rest

Parameter files
===============

Criptic simulations require a parameter file to specify various run time options. Such input files are usually named ``criptic.in``, but need not be. A criptic parameter file is a plain text file conisting of a series of lines formatted as::

  keyword    value

or::

  keyword    value1 value2 value3 ...

Some keywords require a single value, some require exactly three values, and some can take any number of values; some values are expected to be integers, some real numbers, and some can be strings. Details are given below. In the parameter file, any part of a line after a ``#`` symbol is treated as a comment and ignored. All keywords listed below are required unless a default value is given, or unless stated otherwise. Keyword lines can appear in any order.

Keywords can be broken down into a few categories:

* :ref:`ssec-global-parameters`
* :ref:`ssec-geometry-parameters`
* :ref:`ssec-integrator-parameters`
* :ref:`ssec-tree-parameters`
* :ref:`ssec-loss-parameters`
* :ref:`ssec-output-parameters`
* :ref:`ssec-other-parameters`

At the start of a run, criptic parses the parameter file and stores the results in the `ParmParser <doxyhtml/classcriptic_1_1_parm_parser.html>`_, which is then passed to the various problem setup routines (see :doc:`probsetup`). This allows users to access the contents of the parameter file during problem setup. See :ref:`ssec-parmparser` for details on how to use the `ParmParser <doxyhtml/classcriptic_1_1_parm_parser.html>`_.
  
.. _ssec-global-parameters:

Global control parameters
-------------------------

These parameters control the overall behavior of a simulation. They are:

* ``verbosity`` (default value ``1``): integer specifying level of verbosity in the output; a value of 0 means run silently, a value of 1 prints out minimal status information as the simulation runs, and 2 and 3 print out progressively more information; higher verbosity levels, particularly level 3, are intended mainly for debugging
* ``max_time`` (default value ``infinity``): maximum amount of simulation time for which to run
* ``max_step`` (default value ``infinity``): the maximum number of time steps to run
* ``max_dt_increase`` (default value ``1.1``): maximum increase in time step from one step to the next
* ``min_dt`` (default value ``0.0``): minimum time step; if the time step falls below this value, the simulation terminates
* ``dt_init``: initial time step
    
.. _ssec-geometry-parameters:

Geometry parameters
-------------------

These control the geometry of the problem domain. They are:

* ``geometry.lo_type`` (default value ``open open open``): three values giving the boundary condition on the low side of the problem domain in each cardinal direction. Allowed values are ``open`` (domain goes off to infinity), ``absorbing`` (any cosmic ray packet the crosses the boundary is deleted), ``reflecting`` (any packet that reaches the boundary reflects off it), and ``periodic`` (the domain is periodic). The values for each of the three cardinal directions need not be the same.
* ``geometry.hi_type`` (default value ``open open open``): same as ``geometry.lo_type``, but for the high side of the problem domain. If particular dimension is set to ``periodic`` in ``geometry.lo_type``, the corresponding dimension must also be set to ``periodic`` in ``geometry.hi_type``.
* ``geometry.prob_lo``: three real numbers giving the location of the lower left corner of the problem domain. This parameter is ignored for any dimension where ``geometry.lo_type`` is ``open``, and if all three dimensions of ``geometry.lo_type`` are ``open``, then this parameter is not required; otherwise it is mandatory.
* ``geometry.prob_hi``: same as ``geometry.prob_lo``, but for the high side of the domain.

.. _ssec-integrator-parameters:

Integrator parameters
---------------------

These keywords control how cosmic ray packet positions and other properties are integrated. The alllowed keywords are:

* ``integrator.packet_rate``: a real number giving the rate, in units of s\ :sup:`-1`, at which sources inject new packets. This rate is summed over all sources. This keyword is required in any simulation that includes sources, but is not used otherwise.
* ``integrator.packet_dt_min`` (default value ``0``): minimum time step to allow during individual packet advances; if the time step for any packet falls below this, the code aborts with an error message.
* ``integrator.qSamp`` (default value ``-1``): index describing how packets are sampled in momentum. The number of packets injected will be distributed as :math:`dn/dp \propto p^{\mathrm{qSamp}}`. Note that this parameter is different than the field ``q`` for cosmic ray sources (see :ref:`ssec-init-sources`). The ``q`` value for a source describes the actual distribution of cosmic ray momenta produced by that source, while ``qSamp`` describes how criptic distributes packets to sample that distribution; if ``q`` and ``qSamp`` are different (as they usually are), criptic will assign momentum-dependent weights to the packets injected.
* ``integrator.equal_sampling`` (default value ``1``): integer describing how different cosmic ray particle types are sampled. If set to a non-zero value, then equal numbers of packets are assigned to every particle species produced by sources in the domain. If set to zero, then a different weighting must be specified by ``integrator.sampling_ratio``.
* ``integrator.sampling_ratio`` (default value ``1.0 1.0 1.0``): list of one real number per cosmic ray particle type, which specifies the relative numbers of cosmic ray packets assigned to sample each species. The first number gives the weight of protons, the second the weight of electrons, and the third the weight of positrons. Thus for example a value ``1.0 2.0 3.0`` indicates that, for every proton packet injected by sources, there should be two electron packets and three positron packets injected. This parameter is ignored if ``integrator.equal_sampling`` is set to a non-zero value.
* ``integrator.c_min_wgt`` (default value ``0.5``): a real number between 0 and 1 that controls how the new global (as opposed to individual cosmic ray packet) time step is estimated. In each time step, criptic records both the minimum time step :math:`dt_{\mathrm{min}}` used to advance any packet and the geometric mean time step :math:`dt_{\mathrm{mean}}` of the time steps used to advance all the packets. The next time step is set to :math:`dt = dt_{\mathrm{min}}^{\mathrm{c\_min\_wgt}} dt_{\mathrm{meam}}^{1-\mathrm{c\_min\_wgt}}`. Note that the global time step is only meaningful in non-linear problems that use field quantities (see :doc:`propagation`), since it dictates the frequency with which the field quantities are recomputed; in other problems this parameter is ignored.
* ``integrator.c_step`` (default value ``0.25``): a positive real number controlling the individual time steps on which cosmic ray packets advance. Time steps are proportional to ``c_step``; see the  `criptic method paper <https://ui.adsabs.harvard.edu/abs/2022arXiv220713838K/abstract>`_ for a full description of how ``c_step`` is used to set the time step for each process.
* ``integrator.err_tol`` (default value ``1.0e-4``): a positive real number given the tolerance for integration of cosmic ray packets along magnetic field lines. Relative errors in tracing trajectories along field lines will be smaller than this value.
* ``integrator.w_frac_min`` (default value ``1.0e-3``): a positive real number specifying the condition under which a packet's weight is reduce so much that it is removed from the simulation. If catastrophic loss processes to cause packet's weight ``w`` (see `CRPacket <Doxygen/html/classcriptic_1_1_c_r_packet.html>`_) divided by its initial weight at injection ``wInj`` to fall below this value, the packet is deleted.
* ``integrator.p_min`` (default value ``0.0``): a real number giving a momentum, in units of GeV :math:`c^{-1}`, below which packets are deleted. Packets whose momenta fall below ``p_min`` are removed from the simulation.
* ``integrator.T_min`` (default value ``1e-3``): a real number giving a kinetic energy, in units of GeV, below which packets are deleted. Packets whose kinetic energies fall below ``T_min`` are removed from the simulation.

.. _ssec-tree-parameters:
  
Tree parameters
---------------

These parameters control the structure of the kd-tree used to distribute packets across MPI ranks, and to evaluate field quantities (see :doc:`propagation`). Some of these parameters have no effect in a calculation that does not use MPI, or that does not use field quantities. The parameters are:

* ``tree.extra_global_levels`` (default value ``0`` if the number of MPI ranks is a power of 2, or ``2`` otherwise): a non-negative integer giving the number of extra levels in the global kd-tree. In order to distribute packets across ranks in an MPI calculation, criptic builds a global kd-tree containing all packets on all ranks. The number of levels in this tree is given by :math:`N_\mathrm{lev} = \lceil \log_2 N_\mathrm{rank}\rceil + \mathrm{tree.extra\_global\_levels}`, where :math:`N_\mathrm{rank}` is the number of MPI ranks used in the computation; the number of leaves in the global tree is therefore :math:`2^{N_\mathrm{lev}}`. Note that, if :math:`N_\mathrm{rank}` is a power of 2 and ``tree.extra_global_levels`` is 0, this results in the global tree having exactly one leaf per MPI rank. Increasing this parameter, yielding more leaves per MPI rank, can lead to better load balancing in MPI computations, at the cost of somewhat more overhead in constructing the tree.
* ``tree.partition_samples_per_rank`` (default value ``256``): integer :math:`\geq 4` giving the number of samples per MPI rank used to estimate the median packet position when constructing the global tree.
* ``tree.n_ngb_eff`` (default value ``1024``): a non-negative integer giving the effective number of neighbors to use when computing the bandwidth matrix for kernel density estimation.
* ``tree.kde_tol`` (default value ``0.1``): a positive real number specifying the fractional error tolerance to be used when computing kernel density estimates of field quantities.
* ``tree.source_dither`` (see below for default value): a positive real number giving a size scale over which to dither the positions of sources when injecting cosmic ray packets; this dithering is required to avoid having the bandwidth tensor be exactly 0 in a situation where all the packets within the kernel are injected at the position of a single source. In a calculation where ``geometry.lo_type`` and ``geometry.hi_type`` are not open in at least one dimension (see :ref:`ssec-geometry-parameters`), this parameter takes on a default value of :math:`10^{-6}` times the size of the smallest dimension of the problem domain. In a calculation where the geometric boundaries are open or semi-open in all directions, this parameter has no default value, and must be set explicitly in any calculation that uses field quantities and includes sources.

.. _ssec-loss-parameters:
  
Loss parameters
---------------

Loss parameters determine the behavior of loss mechanisms, and the corresponding treatment of secondary packet production. By default all loss mechanisms are turned on; they are off only if explicitly turned off by a keyword. Keywords in this category are:

* ``losses.disable_all`` (default value ``0``): if set to a non-zero value, all loss mechanisms are turned off
* ``losses.enable_brem`` (no default value, but not required): if set to zero bremsstrahlung losses are turned off, and if set to a non-zero value, bremsstrahlung losses are turned on; this keyword overrides ``losses.disable_all``, so for example one can do a calculation including only bremsstrahlung losses by setting ``losses.disable_all  1`` and ``losses.enable_brem  1``
* ``losses.enable_coulomb`` (no default value, but not required): same as ``losses.enable_brem``, but for Coulomb losses
* ``losses.enable_IC`` (no default value, but not required): same as ``losses.enable_brem``, but for inverse Compton losses
* ``losses.enable_ionization`` (no default value, but not required): same as ``losses.enable_brem``, but for ionization losses
* ``losses.enable_nuc_inelastic`` (no default value, but not required): same as ``losses.enable_brem``, but for nuclear inelastic losses
* ``losses.enable_positron_annihilation`` (no default value, but not required): same as ``losses.enable_brem``, but for positron annihilation losses
* ``losses.enable_synchrotron`` (no default value, but not required): same as ``losses.enable_brem``, but for synchrotron losses
* ``losses.f_sec`` (default value :math:`1/\ln 10`): the :math:`f_\mathrm{sec}` parameter as defined in the `criptic method paper <https://ui.adsabs.harvard.edu/abs/2022arXiv220713838K/abstract>`_, which controls the number of secondary packet samples generated per primary packet loss

.. _ssec-output-parameters:

Output parameters
-----------------

These parameters control the contents and production of checkpoint files. For more details on checkpoint files, see :doc:`output`. Some keywords in this category control when checkpoint files are written and what they are called:

* ``output.chk_name`` (default value ``criptic_``): the base name for checkpoint files; if ``output.chk_name`` is set to ``criptic_``, then checkpoint files will be named ``criptic_00000.hdf5``, ``criptic_00001.hdf5``, ``criptic_00002.hdf5``, and so forth
* ``output.chk_int`` (default value ``0``): if this keyword is set to a positive integer, criptic write a checkpoint every ``output.chk_int`` time steps
* ``output.chk_dt`` (default value ``0.0``): if this keyword is set to a positive real number, critpic will write a checkpoint every time the calculation advances in time by an amount ``output.chk_dt``

Some control what contents are included in output files:
  
* ``output.deleted_packets`` (default value ``1``): if this set to a non-zero integer, checkpoint files include the properties of any packets deleted since the last checkpoint was written
* ``output.field_quantities`` (default value ``1``): if this is set to a non-zero integer, checkpoint files include field quantities (see :ref:`ssec-field-quantities`); however, regardless of the value of this parameter, field quantities are only included in checkpoints if they are required by the cosmic ray propagation model
* ``output.loss_rates`` (default value ``1``): if this is set to a non-zero integer, checkpoint files include the continuous and catastrophic loss rates for each packet by all loss processes
* ``output.photon_emission`` (default value ``1``): if this is set to a non-zero integer, checkpoint files include the total photon emission rate at each photon energy (see ``output.photon_energy`` and ``output.photon_energy_span``, below), summed over all packets, due to each loss mechanism and for each cosmic ray particle type; however, regardless of the value of this parameter, photon emission is included only if photon energies are specified
* ``output.photon_emission_full`` (default value ``0``): same as ``output.photon_emission``, with the difference that, if this keyword is set to a non-zero integer, criptic writes out the photon emission rate produced by *each* packet (as opposed to the sum of all packets by type, as for ``output.photon_emission``) via each loss mechanism at each photon energy; *warning*: the output produced by setting this option to true is likely to be very large
* ``output.ionization`` (default value ``1``): if this is set to a non-zero integer, checkpoint files include the ionization rates of H, He, and H\ :sub:`2` produced by each packet

Finally, the last set of output keywords control the energies / frequencies at which criptic writes photon emission. These energies can be specified in two ways. One is via the keyword
  
* ``output.photon_energies`` (no default value, but not required): list of real numbers specifying the photon energies at which to compute the specific luminosity

By default the units used in ``output.photon_energies`` are GeV, but the user can override this by adding the keyword

* ``output.photon_energies_units`` (default value ``GeV``): the units used in ``output.photon_energies``; allowed values are ``Hz``, ``kHz``, ``MHz``, ``GHz``, ``eV``, ``keV``, ``MeV``, and ``GeV``, and if one of the frequency options is selected (``Hz``, ``kHz``, ``MHz``, ``GHz``), then ``output.photon_energies`` is interpreted as specifying the frequency rather than the energy

The other way of specifying photon energies is via keywords of the form:

* ``output.photon_energy_span_N`` where ``N`` is ``1``, ``2``, ``3``, etc. (no default value, but not required): this keyword must be followed by two real numbers and an integer. The first real number is the mimimum energy, the second is the maximum energy, and the integer is the number of energies in between the minimum and maximum at which to compute output photon emission; energies are uniformly spaced in log. Thus for example if a parameter file contains the line ``output.photon_energy_span_1  1 1000 50``, it will compute photon emission at 50 energies uniformly spaced in logarithm from 1 GeV to 1000 GeV.

By default ``output.photon_energy_span_N`` is assumed to use units of GeV, but this can be overridden by setting ``output.photon_energy_span_N_unit`` (where again ``N`` can be ``1``, ``2``, ``3``, etc.), exactly as with ``output.photon_energies_units``. Note that there can be arbitrarily many ``output.photon_energy_span_N`` lines, as long as the value of ``N`` increases by 1 each, i.e., a parameter file is allowed to contain ``output.photon_energy_span_1``, ``output.photon_energy_span_2``, ``output.photon_energy_span_3``, etc. A file can also combine ``output.photon_energy_span_N`` statements with ``output.photon_energies`` statements.

.. _ssec-other-parameters:

Gas, cosmic ray propagation, and problem parameters
---------------------------------------------------

Parameter file entries can also be used to control the `Gas <doxyhtml/classcriptic_1_1gas_1_1_gas.html>`_ and `Propagation <doxyhtml/classcriptic_1_1propagation_1_1_propagation.html>`_ classes, and to control aspects of the initial conditions. There are no general keywords of this form; exactly which parameters can be controlled at run time depend on the details of the background gas state, propagation model, and problem setup. However, all pre-defined classes and test problems provided in the criptic repository follow the naming convention that keywords of the form ``gas.XXX`` are inputs to the `Gas <doxyhtml/classcriptic_1_1gas_1_1_gas.html>`_ class, keywords of the form ``cr.XXX`` are inputs to the `Propagation <doxyhtml/classcriptic_1_1propagation_1_1_propagation.html>`_ class, and keywords of the form ``prob.XXX`` are inputs to be used in the ``Prob.cpp`` routines (see :doc:`probsetup`).

.. _ssec-parmparser:

The ParmParser
--------------

At the start of each run, criptic parses the input file and stores the results in the `ParmParser <doxyhtml/classcriptic_1_1_parm_parser.html>`_ class. In turn, this object is passed to various other routines during initialization; users implementing their own gas descriptors (:doc:`gas`), cosmic ray propagation models (:doc:`propagation`), and writing problem setup files (:doc:`probsetup`) have access to the ParmParser, and can use it to read parameter file keywords. The ParmParser defines two methods to access keywords. The method::

  void get(const std::string& name, T& val) const;

sets ``val`` to the value of the keyword ``name``; the type ``T`` can be an integer, a Real, a string, or a std::vector of integer, Real, or string, and the type to return will be deduced from the type of argument passed. If the requested keyword was not included in the input file, or the value assigned to the keyword cannot be converted to the requested type (e.g., ``val`` is an integer, but the parameter file specifies a value of ``3.5`` for that keyword) the ParmParser throws an error and halts the run.

The method::

  bool query(const std::string &name, T& val) const;

operates identically to ``get``, except that it does not throw an error if the keyword is not found; instead, it returns ``true`` if the keyword was found, and ``false`` if not. If the keyword is not found, ``val`` is unaltered.

  



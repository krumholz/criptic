.. highlight:: rest

Output files
============

Criptic writes its output in checkpoint files; on startup it write one output file, with the extension ``00000.hdf5``, containing the initial conditions. It also write an output file when a simulation completes due to reaching the maximum simulation time or maximum number of steps. Users can also write output at intermediate time. The conditions for writing output are controlled by the :ref:`ssec-output-parameters` in the parameter file.

Checkpoint files contain, at a minimum, the position and and properties of every cosmic ray packet and every source at the time the checkpoint is written. Depending on the :ref:`ssec-output-parameters` in the output file, they may contain other information as well.

Output files are written `HDF5 <https://www.hdfgroup.org/solutions/hdf5/>`_ format. The easiest way to examine them is to use :ref:`ssec-cripticpy`, criptic's Python helper library. However, any software capable of parsing HDF5 files should be able to read them, and the :ref:`ssec-output-contents` are listed below.

.. _ssec-cripticpy:

cripticpy
---------

Criptic comes with a Python helper library, cripticpy, to process checkpoints and perform related calculations. See :doc:`cripticpy` for full documentation of all functions in the library.

To read a checkpoint file into memory using Python, do::

  from cripticpy import readchk
  data = readchk('CHKNAME')

where ``CHKNAME`` is the name of the checkpoint file to be read. This will return a Python dict containing the full contents of the output file. A full list of the entries in the dict is provided in the docstring for ``readchk``, and in the :doc:`cripticpy`. Note that, if `astropy <https://www.astropy.org/>`_ is installed, all dimensional quantities read from the checkpoint will be astropy quantity objects, with dimensions correctly attached.


.. _ssec-output-contents:

Full contents of checkpoint files
---------------------------------

Checkpoint files contain a series of HDF5 groups. These are:

* CRPacket: contains the positions and properties of cosmic ray packets
* CRSource: contains the positions and properties of cosmic ray sources
* CRPacketDel: contains the positions and properties of cosmic ray packets that have been deleted since the last checkpoint was written, along with the time at which they were deleted
* FieldQty: contains the field quantities computed for each cosmic ray packet
* Losses: contains the loss rates for each cosmic ray packet
* PhotonEmission: contains the specific luminosities due to all loss mechanisms
* Ionization: contains the packet ionization rates

Of these, the CRPacket group is always present in any checkpoint file, and the CRSource group is always present if there are any sources in the simulation. The remaining groups are optional, and whether they are provided or not is controlled by the :ref:`ssec-output-parameters`.

The contents of the groups are as follows:

* CRPacket:

  * Attributes:
  
    * t: current simulation time
    * dt: next time step
    * step: current step number
    * uniqueID: current value of the unique ID counter
    * units: unit system used in the simulation -- 0 for Gaussian/CGS, 1 for SI
    * ParticleNames: names of cosmic ray particle types
    * LossMechanisms: names of loss mechanisms used in the simulation
    * MPICounts: number of packets on each MPI rank
      
  * Data sets:
    
    * pos: positions of cosmic ray packets
    * data: the contents of the `CRPacket <doxyhtml/classcriptic_1_1_c_r_packet.html>`_ class.

* CRSource:

  * Attributes:

    * MPICounts: number of sources on each MPI rank

  * Data sets:
    
    * pos: positions of cosmic ray sources
    * data: the contents of the `CRSource <doxyhtml/classcriptic_1_1_c_r_source.html>`_ class.
  
* CRPacketDel:
  
  * Data sets:
    
    * pos: positions of cosmic ray packets at the time they were deleted
    * data: the contents of the `CRPacket <doxyhtml/classcriptic_1_1_c_r_packet.html>`_ class at the time each packet was deleted
    * tdel: time at which each packet was deleted

* FieldQty:

  * Attributes:

    * names: names of field quantities

  * Data sets:

    * qty: value of the field quantities for each packet
    * grad: values of the gradients of field quantities for each packet

* Losses:

  * Data sets:

    * lossRate: catastrophic loss rate for each packet by each loss mechanism
    * dpdt: continuous loss rate for each packet by each loss mechanism

* PhotonEmission:

  * Attributes:

    * photEn: photon energies at which photon output is computed, in units of GeV

  * Data sets:

    * dLdE: specific luminosity of each packet, due to each loss mechanism, at each photon energy, in units of erg s\ :sup:`-1` GeV\ :sup:`-1` (or J s\ :sup:`-1` GeV\ :sup:`-1` if the code is using SI units)
    * dLdEsummary: specific luminosity due to each loss mechanism, for each cosmic particle type (i.e., summed over all particles of that type), at each photon energy, in units of erg s\ :sup:`-1` GeV\ :sup:`-1` (or J s\ :sup:`-1` GeV\ :sup:`-1` if the code is using SI units)

* Ionization:

  * Attributes:

    * TargetNames: names of targets whose ionization rates are computed

  * Data sets:

    * ionRate: ionization rate produced by each packet for each target species, in units of s\ :sup:`-1`
    
Not all of these fields will be present in every checkpoint file; which ones exist is determined by the :ref:`ssec-output-parameters`.

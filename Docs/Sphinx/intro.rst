.. highlight:: rest

Introduction to criptic
=======================

This is a document is intended as a user's guide for the criptic cosmic ray simulation package. A full description of the equations solved and the physical approximations invovled is provided in the `criptic method paper <https://ui.adsabs.harvard.edu/abs/2022arXiv220713838K/abstract>`_, and full documentation of all criptic classes and methods is provided in the `doxygen <doxyhtml/index.html>`_ documentation.

A short summary is that criptic uses an Itô calculus approach to solve the Fokker-Planck equation describing the spatial and momentum distribution of a collection of population of cosmic rays, including high-accuracy treatments of the most important microphysical loss processes that cosmic rays undergo when interacting with interstellar matter. It also predicts the observable radiation and (indirectly) observable ionization rate that the cosmic rays produce through these interactions. It can therefore be used both to compute the properties the cosmic rays themselves and those of the photons they produce.

A key feature of criptic is that it is completely agnostic and highly flexible when it comes to both the model that describes how cosmic rays propagate, and the properties of the background gas and magnetic fields through which they do so. While a set of "standard" propagation models and gas backgrounds are provided, it is expected that for most applications users will define their own. Explanations for how to do so are provided below.

The remainder of this document describes the basic steps in setting up and running a criptic simulation. For the most common case of a simulation to track the pitch-angle averaged distribution function, these are:

#. Write the problem setup file that describes the initial conditions of your simulation -- see :doc:`probsetup`
#. Write the gas descriptor file that describes the background gas through with the cosmic rays propagate, or, alternatively, choose one of the standard pre-defined gas descriptors -- see :doc:`gas`
#. Write the cosmic ray propagation descriptor file that describes your preferred model for how cosmic rays propagate, or, alternatively, choose one of the standard pre-defined cosmic ray propagation descriptors -- see :doc:`propagation`
#. Compile the code for your problem -- see :doc:`building`
#. Write the parameter file that provides the run-time parameters for the simulation -- see :doc:`parameters`
#. Run the simulation -- see :doc:`running`
#. Examine and analyze the output -- see :doc:`output`

Criptic can also simulate the evolution of a pitch angle-dependent distribution. Doing so involves some changes in the procedure for setting up and running a simulation -- see :doc:`pitchangle`.

Before diving into these documents, it is also helpful to review the brief note on :doc:`units`.
   
Finally, for those who are interested in adding capabilities to criptic or otherwise carrying out new development, :doc:`developer` provides a brief code to the structure of the code, and :doc:`tests` describes the standard test suite that can be used to validate code modifications.

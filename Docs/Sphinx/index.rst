.. criptic documentation master file, created by
   sphinx-quickstart on Sun Jul 24 23:16:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to criptic's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting
   license
   intro
   units
   probsetup
   gas
   propagation
   building
   parameters
   running
   output
   pitchangle
   developer
   tests
   doxygen
   cripticpy

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

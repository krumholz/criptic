.. highlight:: rest

Building criptic
================

Language standard
-----------------

Criptic is written using the C++17 standard, and requires a C++ compiler that supports this standard. Full functionality also reqiures a compiler that supports OpenMP threading. For more information on compilation options, see :ref:`ssec-compilation-flags`.

Dependencies
------------

Criptic depends on two packages that are not included in the repository:

* The `GNU scientific library <http://www.gnu.org/software/gsl/>`_ (v2.0 or later)
* An `HDF5 libary <https://www.hdfgroup.org/solutions/hdf5/>`_. Note that parallel HDF5 is not required, only serial.
  
These must be installed prior to building criptic, and the associated header and compiled object files must either be placed in standard locations, or the locations must be specified a system-specific Makefile (see :ref:`ssec-makefiles`).

In addition, if the code is going to be compiled in MPI mode (see :ref:`ssec-compilation-flags`), an MPI implementaton and MPI compiler are required.

.. _ssec-compilation-flags:

Compiling and compilation flags
-------------------------------

The minimal command to compile criptic is::

  make PROB=PROB_NAME

where ``PROB_NAME`` is the name of the sub-directory of ``Src/Prob/`` that contains the problem setup files. The instructions to write such a directory are provided in :doc:`probsetup`. This command will compile the code at high optimization, with both OpenMP threading and MPI parallelism enabled. The compilation command will produce an executable called ``criptic`` in the directory ``Src/Prob/PROB_NAME``.

You can control the compilation using the flags ``MPI``, ``OPENMP``, and ``DEBUG``. Setting the flag ``MPI=NO`` will disable MPI, setting ``OPENMP=NO`` will disable OpenMP threading, and setting ``DEBUG=TRUE`` will compile the code in debug mode; debug mode also causes the compiled executable to be called ``criptic_dbg`` rather than ``criptic``. Thus for example the make statement::

  make PROB=PROB_NAME OPENMP=NO DEBUG=TRUE

while cause a version of criptic to be compiled in debug mode with OpenMP threading disabled and MPI enabled, with the compiled executable called ``criptic_dbg`` and placed in ``Src/Prob/PROB_NAME``.

Finally, doing::

  make PROB=PROB_NAME clean

will delete all object and executable files. Separate sets of object files are kept for every combination of MPI, OpenMP, and debug mode on and off, so the command above will only delete the object files for the default settings, i.e., MPI and OpenMP on, debugging off. To clean the object files for other versions include the appropriate ``MPI``, ``OPENMP``, and ``DEBUG`` flags; alternatively,::

  make PROB=PROB_NAME allclean

will delete all combinations of object files for the specified problem.


.. _ssec-makefiles:

Machine-specific makefiles
--------------------------

Criptic comes with generic Makefiles that should be sufficient to build the code in most unix-like environments. However, you may want to modify these to specify particular compilers, or if you have installed the GSL or HDF5 header or compiled library files in locations that are not in the standard compilation search path. The mechanism for doing this is providing a machine-specific makefile.

Creating a machine-specific makefile involves two steps. First, in the directory ``Src/Makefiles`` create a file with a name of the form ``Make.mach.NAME_OF_MY_MACHINE``. It is probably easiest to create this file by copying one of the generic makefiles, like ``Make.mach.linux-gnu``, and modifying that, since the options in these are pre-defined. In your machine-specific makefile, you can set the following options:

* ``MACH_CXX``: your C++ compiler
* ``MACH_MPICXX``: your MPI-enabled C++ compiler
* ``MACH_CSTDFLAG``: what flag to use to specify that the compiler should use the C++17 standard
* ``MACH_CXXLIB``: linker flag required (if any) to link to the C++ standard template library
* ``MACH_CXXOPTFLAGS``: compiler flags to use when compiling in optimized mode
* ``MACH_LDOPTFLAGS``: compiler flags to use when linking in optimized mode
* ``MACH_CXXDEBFLAGS``: compiler flags to use when compiling in debug mode
* ``MACH_LDDEBUGLAGS``: compiler flags to use when linking in debug mode
* ``MACH_CXX_OMP_FLAG``: compiler flag required to turn on OpenMP
* ``MACH_LD_OMP_FLAG``: compiler flag required to link an OpenMP program
* ``HDR_PATH``: colon-separated list of paths to directories containing header files
* ``LIB_PATH``: colon-separated list of paths to directories containing library files

Second, edit the file ``Src/Makefile`` to add automatic detection of your machine based on the output of ``uname -n``. At the start of this file, you will find a block of code of the form::

  ifeq ($(UNAMEN), avatar)
     include Makefiles/Make.mach.avatar
  else ifeq ($(UNAMEN), morticia.anu.edu.au)
     include Makefiles/Make.mach.morticia
  else ifneq (,$(findstring gadi, $(UNAMEN)))
     include Makefiles/Make.mach.gadi
  else...

If running ``uname -n`` on your machine results in a machine name of the form ``mymachine.myuniversity.edu``, then add a block in the if statement above of the form::

  else ifeq ($(UNAMEN), mymachine.myuniversity.edu)
     include Makefiles/Make.mach.NAME_OF_MY_MACHINE

This will automatically include your custom makefile when compiling.

Alternately, if you do not want to alter ``Src/Makefile``, you can manually specify your machine name when compiling by adding the option ``MACHINE=XXX`` to your ``make`` command, which will cause the problem-specific makefile ``Make.mach.XXX`` in ``Src/Makefiles`` to be selected.

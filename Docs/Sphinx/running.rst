.. highlight:: rest

Running a criptic simulation
----------------------------

Once criptic is compiled (:doc:`building`) and the parameter file is written (:doc:`parameters`), running the code is straightforward::

  criptic criptic.in

where ``criptic.in`` is the name of the input parameter file. This will run the code on a single MPI rank. To run an MPI job using multiple ranks, use ``mpirun`` or ``mpiexec`` as usual::

  mpirun -np NUM_PROCESSES criptic criptic.in

The commands above will start a new run from time 0. It is also possible to restart a criptic run from a checkpoint file. To do so, do::

  criptic --restart CHKFILE criptic.in

or::

  mpirun -np NUM_PROCESSES criptic --restart CHKFILE criptic.in

where ``CHKFILE`` is the name of the checkpoint file from which to restart. Note that there is no requirement when restarting to use the same number of MPI ranks as were used previously. Checkpoint files can be read by any number of MPI ranks.

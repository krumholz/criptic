.. highlight::rest

.. _sec-pitch-angle:

Simulating pitch angle evolution
================================

By default criptic solve the Fokker-Planck equation for the pitch angle-averaged cosmic ray distribution function, as described in the `criptic method paper <https://ui.adsabs.harvard.edu/abs/2022MNRAS.517.1355K/abstract>`_. However, it is also possible to use criptic to simulate the evolution of the pitch angle-dependent distribution function. The remainder of this page provides:

* A description of the :ref:`ssec-pitch-angle-equation` in this case
* :ref:`ssec-pitch-angle-setup`
* :ref:`ssec-pitch-angle-propagation`
* :ref:`ssec-pitch-angle-building`
* :ref:`ssec-pitch-angle-output`

.. _ssec-pitch-angle-equation:
  
Physical model and equations solved
-----------------------------------

In criptic's default mode, the distribution function is assumed to be close to isotropic, while the anisotropy is treated as a small perturbation that gives rise to streaming. In this approximation, the equation that criptic solves is:

.. math::

   \frac{\partial \tilde{f}}{\partial t} & = &
   \frac{\partial^2}{\partial x_i\partial x_j} \left(K_{ij} \tilde{f}\right) 
   + 
   \frac{\partial^2}{\partial p^2}\left(K_{\rm pp} \tilde{f} \right)
   \nonumber \\
   & & {} - \frac{\partial}{\partial x_i} \left\{\left[\frac{\partial K_{ij}}{\partial x_j} + v_i + \left(w + \frac{p}{3} \frac{\partial w}{\partial p}\right)\hat{t}_i\right] \tilde{f}\right\} 
   \nonumber \\
   & & - \frac{\partial}{\partial p} \left\{\left[
   \frac{\partial K_{\rm pp}}{\partial p} + 2\frac{K_{\rm pp}}{p} - \dot{p}_{\rm cts}
   \right.\right.
   \nonumber \\
   & & \qquad\quad \left.\left. {} -
   \frac{p}{3}\left(
   \frac{\partial v_i}{\partial x_i} + \frac{dw}{dx_i} \hat{t}_i + w \frac{d\hat{t}_i}{dx_i}
   \right)
   \right] \tilde{f}\right\}
   \nonumber \\
   & & {} - L \tilde{f} + \tilde{S}.

Here :math:`\tilde{f}` is the pitch angle averaged distribution function as a function of position :math:`\mathbf{x}` and magnitude of momentum :math:`p`, :math:`\mathsf{K}` is the spatial diffusion tensor, :math:`K_{pp}` is the momentum diffusion coefficient, :math:`\mathbf{v}` is the background gas velocity, :math:`w` is the cosmic ray streaming speed, :math:`\hat{\mathbf{t}}` is a unit vector parallel to the local magnetic field, :math:`\dot{p}_\mathrm{cts}` is the rate of momentum loss by continuous processes, :math:`L` is the catastrophic loss rate, and :math:`S` is the rate of cosmic ray injection by sources.
   
However, it is also possible to use criptic to simulate systems in which the pitch angle distribution is not assumed isotropized. In this case, criptic solves an alternative Fokker-Plank equation,

.. math::

   \frac{\partial \tilde{f}}{\partial t} & = &
   \frac{\partial^2}{\partial x_i\partial x_j} \left(K_{ij} \tilde{f}\right) 
   + 
   \frac{\partial^2}{\partial p^2}\left(K_{\rm pp} \tilde{f} \right)
   + \frac{\partial}{\partial \mu} \left[\left(1-\mu^2\right)
     K_{\mu\mu} \frac{\partial\tilde{f}}{\partial\mu} \right]
   \nonumber \\
   & & {} - \frac{\partial}{\partial x_i} \left\{\left[\frac{\partial K_{ij}}{\partial x_j} + v_i + v_\mathrm{CR} \hat{t}_i \right] \tilde{f}\right\} 
   \nonumber \\
   & & - \frac{\partial}{\partial p} \left\{\left[
   \frac{\partial K_{\rm pp}}{\partial p} + 2\frac{K_{\rm pp}}{p} - \dot{p}_{\rm cts} -
   \frac{p}{3} \frac{\partial v_i}{\partial x_i} 
   \right] \tilde{f}\right\}
   \nonumber \\
   & & {} - L \tilde{f} + \tilde{S}.

Here :math:`\tilde{f}` represents the distribution function in position, momentum, and cosine of pitch angle :math:`\mu`, :math:`K_{\mu\mu}` is the pitch angle diffusion coefficient, and :math:`v_\mathrm{CR}` represents the velocity of a CR particle of momentum :math:`p`.

There are two significant change from the default version of the Fokker-Plank equation: (1) all the terms involving streaming, :math:`w`, have been removed, because streaming is not a meaningful concept if the pitch angle distribution cannot be approximated as isotropic plus a small perturbation; (2) new terms have appeared describing the evolution of the distribution in pitch angle, and how cosmic rays with a particular pitch angle move along field lines due to their microphysical velocity.


.. _ssec-pitch-angle-setup:

Problem setup for pitch angle-dependent simulations
---------------------------------------------------

In a simulation that evolves the CR pitch angle distribution, setting up the initial conditions follows the same basic structure as for a conventional simulation, as described in :doc:`probsetup`. The only difference is that cosmic ray packets and source have somewhat different fields for pitch angle-dependent simulations; which fields are present are controlled by compile-time parameters, as described in :ref:`ssec-pitch-angle-building`.

For a problem that is following the pitch angle distribution, the `CRPacket <doxyhtml/classcriptic_1_1_c_r_packet.html>`_ class has an additional field:

* ``mu``: the cosine of the pitch angle of the packet; must be in the range :math:`-1` to :math:`1`

The ``initPackets`` routine must set ``mu`` for every packet it initializes as well as all of the other required fields described in :ref:`ssec-init-packets`.

Similarly, for a problem that follows the pitch angle distribution, the `CRSource <doxyhtml/classcriptic_1_1_c_r_source.html>`_ class has two additional fields:

* ``mu0``: the minimum cosine pitch angle of injected packets
* ``mu1``: the maximum cosine pitch angle of injected packets

Sources inject packets with a uniform distribution in cosine pitch angle from ``mu0`` to ``mu1``. Users must initialize ``mu0`` and ``mu1`` for every source that is created in the ``initSources`` routine, in addition to the other mandatory fields described in :ref:`ssec-init-sources`.


.. _ssec-pitch-angle-propagation:

Propagation models for pitch angle-dependent simulations
--------------------------------------------------------

Simulations that follow the pitch angle distribution use the same basic setup as described in :doc:`propagation` to describe the CR propagation parameters (i.e., the various diffusion coefficients, etc.). The only difference is that the `PropagationData <doxyhtml/structcriptic_1_1propagation_1_1_propagation_data.html>`_ struct has different members for pitch angle-dependent simulations, reflecting the changes in terms present in the Fokker-Planck equation for this case -- see :ref:`ssec-pitch-angle-equation`.

In pitch angle-dependent simulations this struct has the additional two fields:

* ``kMu``: the pitch angle diffusion coefficient :math:`K_{\mu\mu}` in astrophysical units (i.e.,  s\ :sup:`-1`)
* ``dkMudMu``: the derivative of the diffusion coefficient with respect to :math:`\mu`, i.e., :math:`dK_{\mu\mu}/d\mu`, in astrophysical units (i.e.,  s\ :sup:`-1`)

In addition, because streaming is undefined for a pitch angle-dependent calculation, the fields ``vStr`` and ``vStrGrad`` are *not* present.


.. _ssec-pitch-angle-building:

Building and running criptic for pitch angle-dependent simulations
------------------------------------------------------------------

Switching criptic from solving the default Fokker-Planck equation to the pitch angle-dependent one given in  :ref:`ssec-pitch-angle-equation` is handled by providing the compile time definition ``TRACK_PITCH_ANGLE``. The easiest way to accomplish this is to add the line::

  #define TRACK_PITCH_ANDLE

to the ``Definitions.H`` file in the problem directory being compiled -- see :ref:`ssec-definitions-H`. The :ref:`test-pitchanglediff` test problem directory, in ``Src/Prob/Test/PitchAngleDiff``, provides an example of this approach. Setting this definition will also modify the  `CRPacket <doxyhtml/classcriptic_1_1_c_r_packet.html>`_, `CRSource <doxyhtml/classcriptic_1_1_c_r_source.html>`_, and `PropagationData <doxyhtml/structcriptic_1_1propagation_1_1_propagation_data.html>`_ classes as described in :ref:`ssec-pitch-angle-setup` and :ref:`ssec-pitch-angle-propagation`.

Once criptic is build with this option, running a simulation is exactly the same as in the standard case described in :doc:`running`.


.. _ssec-pitch-angle-output:

Output files for pitch angle-dependent simulations
--------------------------------------------------

Simulations with pitch angle-dependent evolution write output files in the same basic format as simulations run in default mode, as described in :doc:`output`. The only difference is that the contents of the ``CRPacket``, ``CRSource``, and ``CRPacketDel`` data sets are modified to reflect the changes in the  `CRPacket <doxyhtml/classcriptic_1_1_c_r_packet.html>`_ and `CRSource <doxyhtml/classcriptic_1_1_c_r_source.html>`_ classes described in :ref:`ssec-pitch-angle-setup` -- that is, the data describing CR packets have an additional field ``mu``, and the data describing CR sources have additional fields ``mu0`` and ``mu1``. The :ref:`ssec-cripticpy` library will automatically detect if these additional fields are present in output files, and will import the data to python when reading criptic output files where they are.

.. highlight:: rest

Getting criptic
===============

The criptic source code is hosted on `bitbucket <https://bitbucket.org/krumholz/criptic/>`_. The code can be downloaded using git::

  git clone https://bitbucket.org/krumholz/criptic.git

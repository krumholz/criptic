.. highlight:: rest

Code layout for developers
==========================

The criptic source code is found in the ``Src`` directory, which contains the ``Definitions.H`` file that sets default compile-time parameters, the ``Makefile`` for building the code, and the main program, ``main.cpp``. The remainder of the code is divided into subdirectories:

* ``Src/Build`` contains object and dependency files created automatically during the build process
* ``Src/Core`` contains the core classes
* ``Src/Gas`` contains the gas descriptor machinery
* ``Src/IO`` contains the code that manages parsing parameter files and managing checkpoints
* ``Src/Losses`` contains implementations of all the loss processes
* ``Src/MPI`` contains all the code related to MPI parallelism
* ``Src/Makefiles`` contains machine-specific makefiles
* ``Src/Prob`` contains all problem setup files
  
  * ``Src/Prob/Test`` contains the standard test suite problems

* ``Src/Propagation`` contains the cosmic ray propagation model machinery
* ``Src/Utils`` contains utilities used throughout the code
* ``Src/pcg-cpp`` contains the `PGC random number generator <https://www.pcg-random.org/index.html>`_ source code

In addition to these source directories, and the documentation directories found in ``Docs``, the criptic repository includes the directory ``Misc``. This contains `Mathematica <https://www.wolfram.com/mathematica/>`_ notebooks used to tabulate the various numerical functions that criptic uses to calculate microphysical processes.

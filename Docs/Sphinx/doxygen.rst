.. highlight:: rest

Full documentation of criptic
-----------------------------

See `doxygen documentation <doxyhtml/index.html>`_ for a full listing of every criptic class, function, and source file.

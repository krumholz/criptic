.. highlight:: rest

Unit systems in criptic
=======================

Unfortunately cosmic ray astrophysics is a field where multiple, conflicting unit systems are common, and where the natural unit systems for some quantities (e.g., cosmic ray particle masses and energies) are very different from tose for other quantities (e.g., astrophysical size and distance scales). For this reason, it is important to be clear on unit conventions. The conventions used by criptic, and throughout this document, are as follows:

* Unless stated otherwise, all *astrophysical* quantities (e.g., positions of cosmic ray packets, densities and velocities of interstellar gas, interstellar magnetic fields) are in CGS/Gaussian units. Thus for example magnetic fields are in Gauss, densities are in g cm\ :sup:`-3`, etc. Alternately, if users prefer, they can compile the code using SI units (see :ref:`ssec-definitions-H`), in which case these quantities are in SI units.
* Unless stated otherwise, cosmic ray properties referenced in :doc:`parameters` are expected to be in a unit system where energy is measured in GeV and momentum in GeV/:math:`c`.
* Internally, criptic uses a unit system for cosmic rays where masses are measured in units of the proton mass :math:`m_p` and velocities are measured in units of :math:`c`. Thus for example if a cosmic ray packet has a momentum :math:`p = 10`, this should be interpreted as it having a momentum of :math:`10m_p c \approx 9.3827\mbox{ GeV}/c`. Any time a user is directly setting cosmic ray properties within the code, for example when :doc:`probsetup`, this is the unit system they should use. Checkpoint files also use this internal unit system, though the checkpoint reader provides automatic unit conversions via the `astropy <https://www.astropy.org/>`_ units module -- see :doc:`output` for details.

To make unit conversions within the code easier, the `constants <doxyhtml/namespacecriptic_1_1constants.html>`_ and `units <doxyhtml/namespacecriptic_1_1units.html>`_ namespaces in the criptic code define the values of various physical constants in CGS/Gaussian (or SI) units, conversions between these units and particle physics units such as GeV, respectively.

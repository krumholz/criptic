.. highlight:: rest

Full documentation of cripticpy
===============================

.. automodule:: cripticpy
   :members:

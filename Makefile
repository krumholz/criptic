# Makefile for criptic

# Specify which problem to build
PROB ?= NONE

# Set MPI or non-MPI mode
MPI ?= ENABLE_MPI

# Set openMP or non-openMP mode
OPENMP ?= ENABLE_OPENMP

# Specify to build in debug or non-debug mode
DEBUG ?= FALSE

# Optionally set location of build directories
BUILDLOC =

# Optionally set machine name to pick machine-specific makefile
MACHINE =

# Optional extra compilation flags
EXTRA = 

# List of tests
TESTS := $(foreach test,$(wildcard Src/Prob/Test/*),$(notdir $(test)))

# Phony targets
.PHONY: all debug criptic criptic-debug clean allclean alltests docs

all: criptic

debug: criptic-debug

criptic: | Src/Prob/$(PROB)
ifeq ($(PROB), NONE)
	$(error Problem to build must be specified by setting PROB!)
endif
	@echo "*** Compiling $(PROB) ***"
	cd Src && $(MAKE) MPI=$(MPI) OPENMP=$(OPENMP) \
            MACHINE=$(MACHINE) PROB=$(PROB) DEBUG=$(DEBUG) \
            BUILDLOC=$(BUILDLOC) EXTRA=$(EXTRA)

criptic-debug: | Src/Prob/$(PROB)
ifeq ($(PROB), NONE)
	$(error Problem to build must be specified by setting PROB!)
endif
	make criptic MPI=$(MPI) OPENMP=$(OPENMP) \
            MACHINE=$(MACHINE) PROB=$(PROB) DEBUG=TRUE \
            BUILDLOC=$(BUILDDIR) EXTRA=$(EXTRA)

clean:
ifeq ($(PROB), NONE)
	$(error Problem to clean must be specified by setting PROB!)
endif
	cd Src && $(MAKE) clean MPI=$(MPI) OPENMP=$(OPENMP) \
            MACHINE=$(MACHINE) PROB=$(PROB)

allclean:
	cd Src && $(MAKE) allclean

tests:
	$(foreach test, $(TESTS), \
	     $(MAKE) MPI=$(MPI) OPENMP=$(OPENMP) \
             MACHINE=$(MACHINE) DEBUG=$(DEBUG) \
             EXTRA=$(EXTRA) PROB=Test/$(test);)

Src/Prob/$(PROB):
	$(error Problem directory Prob/Src/$(PROB) not found!)

docs:
	sphinx-build -b html Docs/Sphinx Docs/html

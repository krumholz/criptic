"""
A little utility for computing the percentile values on bin counts.
"""

from scipy.special import gammainc
from scipy.optimize import root_scalar
import numpy as np

# The objective function to be minimized
def _obj(nexpect, p, nobs):
    return gammainc(nobs+1, nexpect) - p

# The main function
def pctnbin(p, nobs):
    """
    Given a number of observed object nobs, compute the pth percentile
    value for the posterior probability distribution of the expected
    number of objects, assuming a Poisson likelihood function and a
    flat prior on the expectation value.

    Parameters:
       p : float or arraylike
          The percentile value(s) to return
       nobs : int
          Number of observed objects

    Returns:
       pct_nex : float or arraylike
          The pth percentile value of the expectation value
    """
    if hasattr(p, '__iter__'):
        return np.array(
            [ root_scalar(_obj, args=(p_,nobs), x0=nobs, x1=nobs+1).root
              for p_ in p])
    else:
        return root_scalar(_obj, args=(p,nobs), x0=nobs, x1=nobs+1).root


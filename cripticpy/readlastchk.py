"""
Routine to read the last CRIPTIC checkpoint in a directory
"""

from .readchk import readchk
from glob import glob
import os.path as osp
import numpy as np

def readlastchk(dirname=".", chkname="criptic_",
                units=True, ke=True, ndot=True,
                sort=False, meta=False, sources_only=False):
    """
    Read the most recent CRIPTIC checkpoint in a specified directory

    Parameters
       dirname : string
          The name of the directory in which to find the checkpoint
       chkname : string
          Base name for checkpoint files; checkpoint files are files
          with names of the form basenameNNNNN.hdf5, where NNNNN is
          the checkpoint number
       units : bool
          If True, all quantities are returned as astropy.Quantity
          objects with appropriate units; if False data are returned as
          flat numpy arrays.
       ke : bool
          If True, energies will be computed for all momentum
          quantities; only allowed if units is also True
       ndot : bool
          If True, compute number of CRs injected per unit time for
          each source
       sort : bool
          If True, all packets, deleted packets, and sources will be
          sorted in order of unique ID
       meta : bool
          If True, only the metadata are read and returned
       sources_only : bool
          If True, only sources are returned, not packets

    Returns
       out : dict
          a dict containing the following entries (some optional, may
          or may not be included depending on contents of the
          checkpoint file being read and the keywords provided)

          "t" : float or astropy.Quantity
             time of output
          "dt" : float or astropy.Quantity
             size of next time step
          "step" : int
             next step number
          "uid" : int
             value of the unique ID counter
          "part_names" : list of string
             names of particles that correspond to each particle type
          "packets" : dict
             a dict containing information on packets, with the following
             entries:

             "x" : ndarray or astropy.Quantity, size (n,3)
                positions of all packets
             "ptype" : ndarray, size (n)
                particle type numbers of all packets
             "uid" : ndarray, size (n)
                unique IDs of all packets
             "source" : ndarray, size (n)
                unique IDs of sources of all packets
             "p" : ndarray or astropy.Quantity, size (n)
                momenta of all packets
             "w" : ndarray, size (n)
                statistical weights of all packets
             "tinj" : ndarray or astropy.Quantity, size (n)
                times at which each packet was injected
             "wgtinj" : ndarray or astropy.Quantity, size (n)
                statistical weights of each packet at injection
             "gr" : ndarray or astropy.Quantity, size (n)
                grammage traversed by each packet
             "sec" : ndarray, size (n)
                boolean that is true if packet is a secondary, false if not
             "lossmech" : list of string
                names of loss mechanisms
             "T" : ndarray or astropy.Quantity, size (n)
                CR packet kinetic energies
             "pres" : ndarray or astropy.Quantity, size(n)
                CR pressure divided by k_B at position of each packet;
                pressures only include packets with gyroradii >= the
                gyroradius of this packet
             "presGrad" : ndarray or astropy.Quantity, size (n,3)
                gradient of pres
             "eden" : ndarray or astropy.Quantity, size(n)
                CR energy density at position of each packet, including only
                CRs with gyroradii >= the gyroradius of this packet
             "edenGrad" : ndarray or astropy.Quantity, size(n,3)
                gradient of eden
             "nden" : ndarray or astropy.Quantity, size(n)
                CR number density at position of each packet, including only
                CRs with gyroradii >= the gyroradius of this packet
             "ndenGrad" : ndarray or astropy.Quantity, size(n,3)
                gradient of nden

          delpackets : dict
             a dict containing information on packets that were deleted
             since the last checkpoint, with the following entries:

             "x" : ndarray or astropy.Quantity, size (m,3)
                positions of all packets
             "ptype" : ndarray, size (m)
                particle types of all packets
             "uid" : ndarray, size (m)
                unique IDs of all packets
             "source" : ndarray, size (m)
                unique IDs of sources of all packets
             "p" : ndarray or astropy.Quantity, size (m)
                momenta of all packets
             "w" : ndarray, size (m)
                statistical weights of all packets
             "tinj" : ndarray or astropy.Quantity, size (m)
                times at which each packet was injected
             "wgtinj" : ndarray, size (m)
                statistical weights of each packet at injection
             "gr" : ndarray or astropy.Quantity, size (m)
                grammage traversed by each packet
             "T" : ndarray or astropy.Quantity, size (n)
                CR packet kinetic energies

          sources : dict
             a dict containing information on sources, with the following
             entries:

             "x" : ndarray or astropy.Quantity, size (i,3)
                positions of all sources
             "ptype" : ndarray, size(i)
                particle types produced by sources
             "uid" : ndarray, size (i)
                unique IDs of all sources
             "p0" : ndarray or astropy.Quantity, size (i)
                lower limit of momentum of packets injected by each source
             "p1" : ndarray or astropy.Quantity, size (i)
                upper limit of momentum of packets injected by each source
             "q" : ndarray, size (i)
                index of source momentum distribution
             "coef" : ndarray or astropy.Quantity, size (i)
                coefficient of source momentum distribution
             "ndot" : ndarray or astropy.Quantity, size (i)
                number of CRs injected per unit time by source

         losses : dict
             a dict containing information on losses, with the
             following entries:

             "lossrate" : ndarray or astropy.Quantity, size(n,m)
                rate of catastrophic loss for all loss processes
             "dpdt" : ndarray or astropy.Quantity, size(n,m)
                rate of continuous momentum loss for all loss processes

         photons : dict
            a dict containing information on photon emission, with the
            following entries:

            "en" : ndarray or astropy.Quantity, size(j)
               energies at which photon emission is calculated
            "freq" : ndarray or astropy.Quantity, size(j)
               frequencies at which photon emission is calculated;
               equal to en / h
            "dLdE" : ndarray or astropy.Quantity, size(n,m,j)
               array giving the specific luminosity dL/dE (units of
               erg s^-1 GeV^-1 or J s^-1 GeV^-1 if criptic was run in
               MKS mode) of each of the n packets produced via each of
               the m loss mechanisms at each of the j energies
            "dLdE_sum" : ndarray or astropy.Quantity, size(u,v,m,j)
               array giving the specific luminosity dL/dE (units of
               erg s^-1 GeV^-1 or J s^-1 GeV^-1 if criptic was run in
               MKS mode) produced by different mechanisms and
               particle populations; the index u runs from 0 to number
               of distinct particle types, and gives emission produced
               by that type; the index v is 0 (for primary particles)
               or 1 (for secondary particles); the index m runs over
               all the emission mechanisms (in the same orders as
               lossmech); finally, the index j runs over the j
               energies in en

          ionization : dict
             a dict containing information on ionization rates, with
             the following entries:

             "targets" : list of string
                names of the ionization targets included in the file
             "rate" : ndarray or astropy.Quantity, size(n,m)
                array of size number of packets x number of ionization
                targets, giving the ionization rate (units of 1/time)
                for each packet for each target species
    """

    # Get list of checkpoint files
    chkfiles = glob(osp.join(dirname,
                             chkname+"[0-9][0-9][0-9][0-9][0-9].hdf5"))

    # Get maximum checkpoint number
    chknums = [ int(c[-10:-5]) for c in chkfiles ]
    chknum = "{:05d}".format(np.amax(chknums))

    # Read the checkpoint and return
    return readchk(osp.join(dirname, chkname + chknum + ".hdf5"),
                   units=units, ke=ke, ndot=ndot, sort=sort,
                   meta=meta, sources_only=sources_only)

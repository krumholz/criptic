"""
This is a little utility function that makes 2d PDF plots with
flanking histograms.
"""

import numpy as np
from matplotlib import rcParams
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.gridspec as gridspec
from scipy.stats import binned_statistic_2d

def pdfplot(x, y,
            aspect = 'auto',
            cmap = rcParams["image.cmap"],
            cbar = True,
            cbar_nticks = None,
            log = True,
            nxbin = 50,
            nybin = 50,
            scat_alpha=1.0,
            thresh = None,
            threshcontour = True,
            threshfrac = 0.99,
            w = None,
            xhistlim = None,
            xlabel = None,
            xlim = None,
            yhistlim = None,
            ylabel = None,
            ylim = None,
            zmax = None,
            zscale = 'total'):
    """
    Makes a 2D heat map plus scatter plot with flanking histograms.

    Parameters:
       x : array, shape (N,)
          x coordinates of points
       y : array, shape (N,)
          y coordinates of points
       aspect : 'auto' | 'equal' | num
          aspect ratio of the central panel; effects are identical to
          calling set_aspect on the central panel axes
       cmap : str | Colormap
          color map for central heatmap
       cbar : bool
          if True, a color bar will be added to the plot
       cbar_nticks : int
          number of tick marks on the color bar
       log : bool
          if True, use a logarithmic scale for the heat map and histograms
       nxbin : int or arraylike, shape (N,)
          number of bins in x direction
       nybin : int or arraylike, shape (N,)
          number of bins in y direction
       scat_alpha : float
          alpha value for scatter plot points; set to zero to suppress
          plotting of scatter points
       thresh : float
          threshhold below which to hide the density histogram and
          show individual scatter points; default is to use threshfrac
          instead
       threshcontour : bool
          if True, draw a contour at the boundary between the heat
          mapped region and the scatter point region
       threshfrac : float
          fraction of the sum of the weights at which to switch from
          showing the data as a heatmap to showing individual scatter
          points
       w : array, shape (N,)
          weights of individual points; if None, all points have weight 1
       xhistlim : arraylike, shape (2,)
          limits on the histogram in the x direction
       xlabel : string
          labels for x dimension
       xlim : arraylike, shape (2,)
          plotting limits in x direction
       yhistlim : arraylike, shape (2,)
          limits on the histogram in the x direction
       ylabel : string
          labels for y dimension
       ylim : arraylike, shape (2,)
          plotting limits in y direction
       zmax : float
          maximum value for 2D density PDF plots; default is maximum
    value in the heat map
       zscale : 'total' | 'max' | 'frac' | 'density' | 'normed'
          method used to scale the bins in the central PDF and
          flanking histograms; 'total' means that the quantity shown
          is the unscaled sum of the weights of all points in that
          bin; 'max' means that bins are normalized so the value in
          the largest bin is unity; 'frac' means that the value shown
          is the fraction of the weight in each bin; 'density' means
          that the bins are normalized so that the quantity shown is
          the sum of weights divided by the size / area of the bin;
          'normed' means that bins are normalized so that the area
          integral is unity

    Returns
       axcen : matplotlib.Axis
          callback to the axis containing the central panel
       axtop : matplotlib.Axis
          callback to the axis containing the top flanking panel
       axright : matplotlib.Axis
          callback to the axis containing the right flanking panel
    """

    # Define the plotting grid
    if xlim is None:
        xlim = [np.amin(x), np.amax(x)]
    if ylim is None:
        ylim = [np.amin(y), np.amax(y)]
    xgrd = np.linspace(xlim[0], xlim[1], nxbin+1)
    ygrd = np.linspace(ylim[0], ylim[1], nybin+1)
    xgrd_h = 0.5*(xgrd[1:]+xgrd[:-1])
    ygrd_h = 0.5*(ygrd[1:]+ygrd[:-1])
    xx, yy = np.meshgrid(xgrd_h, ygrd_h)

    # Set the weights if not specified
    if w is None:
        w = np.ones(x.shape)

    # Get 2D histogram; note that we have to handle the case of
    # inverted axis limits with care, because binned_statistic_2d
    # doesn't natively support them
    xlim1 = np.sort(xlim)
    ylim1 = np.sort(ylim)
    binsum, xe, ye, binidx \
        = binned_statistic_2d(x, y, w,
                              statistic='sum',
                              bins=[nxbin, nybin],
                              range = [[float(xlim1[0]), float(xlim1[1])],
                                       [float(ylim1[0]), float(ylim1[1])]],
                              expand_binnumbers=True)
    if xlim[0] > xlim[1]:
        binsum = binsum[::-1, :]
        xe = xe[::-1]
        binidx[0,:] = nxbin+1 - binidx[0,:]
    if ylim[0] > ylim[1]:
        binsum = binsum[:, ::-1]
        ye = ye[::-1]
        binidx[1,:] = nybin+1 - binidx[1,:]        

    # Set z
    if zscale == 'total':
        z = binsum
    elif zscale == 'max':
        z = binsum / np.amax(binsum)
    elif zscale == 'frac':
        z = binsum / np.sum(w)
    elif zscale == 'density':
        z = binsum / np.abs((xe[1]-xe[0])*(ye[1]-ye[0]))
    elif zscale == 'normed':
        z = binsum / np.abs((xe[1]-xe[0])*(ye[1]-ye[0])) / np.sum(w)
    else:
        raise ValueError("unknown zscale parameter: "+str(zscale))

    # Set minima and maxima for 2D plot
    if zmax is None:
        if zscale == 'max':
            zmax = 1.0
        else:
            zmax = np.amax(z)
    if thresh is not None:
        zmin = thresh
    else:
        zsort = np.sort(z, axis=None)
        csum = np.cumsum(zsort)
        csum = csum/csum[-1]
        zmin = zsort[np.argmax(csum > 1.0-threshfrac)]
    if log:
        zmin = np.log10(zmin)
        zmax = np.log10(zmax)

    # Take log if requested
    if log:
        if np.amax(z) <= 0.0:
            raise ValueError("cannot use log scale: no positive z values")
        z[z <= 0] = np.amin(z[z > 0])   # Mask negative values
        z = np.log10(z)

    # Get indices of individual points to show
    flag = np.logical_and.reduce((binidx[0,:] > 0,
                                  binidx[1,:] > 0,
                                  binidx[0,:] <= binsum.shape[0],
                                  binidx[1,:] <= binsum.shape[1]))
    scatteridx = np.zeros(len(x), dtype=bool)
    scatteridx[flag] \
        = z[binidx[0,flag]-1, binidx[1,flag]-1] < zmin
        
    # Set up plot
    gs = gridspec.GridSpec(4, 4)
    axcen = plt.subplot(gs[1:, :-1])

    # Plot contour at threshhold if requested
    if threshcontour:
        axcen.contour(xx, yy, np.transpose(z), levels=[zmin],
                      colors='k',
                      linestyles='-')
    
    # Plot scatter points outside contour
    if scat_alpha > 0:
        axcen.scatter(x[scatteridx],
                      y[scatteridx],
                      color='k',
                      s=5,
                      alpha=scat_alpha,
                      edgecolor='none')

    # Plot density map
    img = axcen.imshow(np.transpose(z),
                       origin='lower', aspect=aspect,
                       vmin=zmin, vmax=zmax, cmap=cmap,
                       extent=[xlim[0], xlim[1], ylim[0], ylim[1]])
    
    # Set plot range
    axcen.set_xlim(xlim)
    axcen.set_ylim(ylim)

    # Add labels
    if xlabel is not None:
        axcen.set_xlabel(xlabel)
    if ylabel is not None:
        axcen.set_ylabel(ylabel)

    # Get 1D histograms; again, handle inverted axes with care
    histx, xe \
        = np.histogram(x, weights=w, bins=nxbin, range=xlim1)
    histy, ye \
        = np.histogram(y, weights=w, bins=nybin, range=ylim1)
    if xlim[0] > xlim[1]:
        histx = histx[::-1]
        xe = xe[::-1]
    if ylim[0] > ylim[1]:
        histy = histy[::-1]
        ye = ye[::-1]

    # Scale histograms
    if zscale == 'total':
        pass
    elif zscale == 'max':
        histx = histx / float(np.amax(histx))
        histy = histy / float(np.amax(histy))
    elif zscale == 'frac':
        histx = histx / np.sum(w)
        histy = histy / np.sum(w)
    elif zscale == 'density':
        histx = histx / np.abs(xe[1]-xe[0])
        histy = histy / np.abs(ye[1]-ye[0])
    elif zscale == 'normed':
        histx = histx / np.abs(xe[1]-xe[0]) / np.sum(w)
        histy = histy / np.abs(ye[1]-ye[0]) / np.sum(w)

    # Add flanking histograms
    if zscale == 'max':
        label = 'Scaled PDF'
    elif zscale == 'count':
        label = r'N'
    elif zscale == 'frac':
        label = 'Fraction'
    elif zscale == 'density':
        label = 'Density'
    elif zscale == 'normed':
        label = 'PDF'
    axtop = plt.subplot(gs[0, :-1])
    axtop.bar(xgrd[:-1], histx, xgrd[1]-xgrd[0],
              align='edge',
              facecolor='C0', edgecolor='black')
    axtop.set_xlim(xlim)
    if xhistlim is not None:
        axtop.set_ylim(xhistlim)
    if log:
        axtop.set_yscale('log')
    axtop.set_xticklabels([])
    axtop.set_ylabel(label)
    axright = plt.subplot(gs[1:, -1])
    axright.barh(ygrd[:-1], histy, ygrd[1]-ygrd[0], 
                 align='edge',
                 facecolor='C0',
                 edgecolor='black')
    axright.set_ylim(ylim)
    if yhistlim is not None:
        axright.set_xlim(yhistlim)
    if log:
        axright.set_xscale('log')
    axright.set_yticklabels([])
    axright.set_xlabel(label)

    # Add colorbar
    if colorbar:
        if zscale == 'max':
            if log:
                label = 'log Scaled PDF'
            else:
                label = 'Scaled PDF'
        elif zscale == 'count':
            if log:
                label = 'log N'
            else:
                label = 'N'
        elif zscale == 'frac':
            if log:
                label = 'log fraction'
            else:
                label = 'fraction'
        elif zscale == 'density':
            if log:
                label = 'log density'
            else:
                label = 'density'
        elif zscale == 'normed':
            if log:
                label = 'log PDF'
            else:
                label = 'PDF'
        axcbar = plt.subplot(gs[0,-1])
        cbar = plt.colorbar(img, ax=axcbar, orientation='vertical',
                            fraction=0.8,
                            aspect=10,
                            label=label)
        if nticks is not None:
            cbar.set_ticks(np.linspace(zmin, zmax, nticks))
        axcbar.remove()

    # Return handles to axes
    return (axcen, axtop, axright)

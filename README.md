### Welcome to criptic ###

Criptic is the Cosmic Ray Interstellar Propagation Tool using Itô Calculus. Criptic is software to simulate the propagation of cosmic rays through the interstellar medium, while at the same time predicting the radiative signatures that these cosmic rays produce. The code is parallelized using a hybrid OpenMP + MPI model. The equations solved and numerical methods used are described in [Krumholz, Crocker, & Sampson (2022, MNRAS, 517, 1355)](https://ui.adsabs.harvard.edu/abs/2022MNRAS.517.1355K/abstract).

### Getting started ###

A user's guide to criptic, which provides instructions on how to use the code, is available at <https://criptic.readthedocs.io/>. This documentation is also included in this repository. Once you have cloned the repository, do *make docs* in the main directory to build the documentation (requires [sphinx](https://www.sphinx-doc.org/en/master/) and [doxygen](https://doxygen.nl/)); the documentation can then be accessed through *Docs/html/index.html*.

### Dependencies ###

Criptic requires the following external libraries:

* [HDF5](https://www.hdfgroup.org/solutions/hdf5/)
* [GNU scientific library](https://www.gnu.org/software/gsl/)

It also uses the [PCG](https://www.pcg-random.org/index.html) random number generator, but this is included with the repository and does not require separate installation.

In addition, criptic comes with a python helper library, cripticpy, to parse checkpoint files. This requires

* [h5py](https://www.h5py.org/)
* [numpy](https://numpy.org/)
* [matplotlib](https://matplotlib.org/)
* [astropy](https://www.astropy.org/) (not strictly required, but provides greatly enhanced functionality)

### Acknowledgements ###

In addition to [Mark Krumholz](https://www.mso.anu.edu.au/~krumholz/), [Roland Crocker](https://researchers.anu.edu.au/researchers/crocker-r), and [Matt Sampson](https://www.astrosampson.com/), the co-authors of the criptic method paper, the following people contributed:

* [James Beattie](https://astrojames.github.io/) helped derive analytic solutions for some of the test problems
* [Christoph Federrath](https://www.mso.anu.edu.au/~chfeder/) wrote the FLASH checkpoint reader

### License ###

Criptic is distributed under the terms of the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html) version 3.0. The text of the license is included in the main directory of the repository as GPL-3.0.txt.

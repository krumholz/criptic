#include "CartesianGrid.H"

using namespace std;
using namespace criptic;
using namespace gas;

/////////////////////////////////////////////////////
// Constructors
/////////////////////////////////////////////////////
CartesianGrid::CartesianGrid(const ParmParser& pp,
			     const Geometry& geom_,
			     const IdxType nBB_,
			     const int nGhost_) :
  geom(geom_),
  nBB(nBB_),
  nGhost(nGhost_) {

  // Safety check: make sure boundary conditions are not open
  if (geom.isAnyOpen())
    MPIUtil::haltRun("CartesianGrid: cannot use open boundary "
		     "conditions with CartesianGrid", errBadInputFile);

  // Read size of grid
  vector<int> nGrid_;
  pp.get("gas.n_grid", nGrid_);
  for (IdxType i = 0; i < 3; i++) nGrid[i] = nGrid_[i];

  // Set total number of components
  nComp = nf + 2*nBB;
  
  // Compute grid size
  dx = (geom.xHi() - geom.xLo()) / nGrid;

  // Allocate memory
  data.resize( (nGrid[0] + 2*nGhost) *
	       (nGrid[1] + 2*nGhost) *
	       (nGrid[2] + 2*nGhost) *
	       nComp);
}


CartesianGrid::CartesianGrid(const IdxVec& nGrid_,
			     const Geometry& geom_,
			     const IdxType nBB_,
			     const int nGhost_) :
  geom(geom_),
  nGrid(nGrid_),
  nBB(nBB_),
  nGhost(nGhost_) {

  // Safety check: make sure boundary conditions are not open
  if (geom.isAnyOpen())
    MPIUtil::haltRun("CartesianGrid: cannot use open boundary "
		     "conditions with CartesianGrid", errBadInputFile);

  // Set total number of components
  nComp = nf + 2*nBB;
  
  // Compute grid size
  dx = (geom.xHi() - geom.xLo()) / nGrid;

  // Allocate memory
  data.resize( (nGrid[0] + 2*nGhost) *
	       (nGrid[1] + 2*nGhost) *
	       (nGrid[2] + 2*nGhost) *
	       nComp);
}


/////////////////////////////////////////////////////
// Boundary condition filler
/////////////////////////////////////////////////////

// Method to apply boundary conditions to compute ghost zone
// properties. The basic rule is:
// absorbing boundaries --> all quantities linearly extrapolated
//                          across buondary
// reflecting boundaries --> all quantities except the component of
//                           vectors normal to surface linearly
//                           extrapolated; vector component normal to
//                           surface is reflected
// periodic boundaries --> just to periodic fill
void CartesianGrid::applyBC() {

  // Only one MPI rank does this
  
  if (!MPIUtil::IOProc) return;
  
  // Safety check
  if (nGrid.min() < 2)
    MPIUtil::haltRun("CartesianGrid: applyBC requires grid "
		     "size >= 2 cells in every dimension",
		     errBadInputFile);

  // Lo x face
  for (IdxType j=0; j<nGrid[1]; j++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bLo()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}
      }
    }
  }
      
  // Hi x face
  for (IdxType j=0; j<nGrid[1]; j++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bHi()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }
  }

  // Lo y face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bLo()[1]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
	      (*this)(i,1,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,-1,k,n) = -(*this)(i,0,k,n);
	    else
	      (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
		(*this)(i,1,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,-1,k,n) = (*this)(i,nGrid[1]-1,k,n);
	    break;
	  }
	}
      }
    }
  }
      
  // Hi y face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType k=0; k<nGrid[2]; k++) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bHi()[1]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
	      (*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,nGrid[1],k,n) = -(*this)(i,nGrid[1]-1,k,n);
	    else
	      (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
		(*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,nGrid[1],k,n) = (*this)(i,0,k,n);
	    break;
	  }
	}
      }
    }
  }

  // Lo z face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType j=0; j<nGrid[1]; j++) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bLo()[2]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,j,-1,n) = 2 * (*this)(i,j,0,n) -
	      (*this)(i,j,1,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vzIdx || n == BzIdx)
	      (*this)(i,j,-1,n) = -(*this)(i,j,0,n);
	    else
	      (*this)(i,j,-1,n) = 2 * (*this)(i,j,0,n) -
		(*this)(i,j,1,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,j,-1,n) = (*this)(i,j,nGrid[2]-1,n);
	    break;
	  }
	}
      }
    }
  }
      
  // Hi z face
  for (IdxType i=0; i<nGrid[0]; i++) {
    for (IdxType j=0; j<nGrid[1]; j++) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bHi()[2]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,j,nGrid[2],n) = 2 * (*this)(i,j,nGrid[2]-1,n) -
	      (*this)(i,j,nGrid[2]-2,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vzIdx || n == BzIdx)
	      (*this)(i,j,nGrid[2],n) = -(*this)(i,j,nGrid[2]-1,n);
	    else
	      (*this)(i,j,nGrid[2],n) = 2 * (*this)(i,j,nGrid[2]-1,n) -
		(*this)(i,j,nGrid[2]-2,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,j,nGrid[2],n) = (*this)(i,j,0,n);
	    break;
	  }
	}
      }
    }
  }

  // xy edges -- these are the four corners of every xy slice
  for (IdxType k=0; k<nGrid[2]; k++) {

    // (-1,-1,*) and (-1,nGrid[1],*) edges
    for (SignedIdxType j=-1; j<=(SignedIdxType) nGrid[1]; j+=nGrid[1]+1) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bLo()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}
      }
    }

    // (nGrid[0],-1,*) and (nGrid[0],nGrid[1],*) edges
    for (SignedIdxType j=-1; j<=(SignedIdxType) nGrid[1]; j+=nGrid[1]+1) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bHi()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }      
  }

  // xz edges -- these are the four corners of every xz slice
  for (IdxType j=0; j<nGrid[1]; j++) {

    // (-1,*,-1) and (-1,*,nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bLo()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}
      }
    }

    // (nGrid[0],*,-1) and (nGrid[0],*,nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bHi()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }      
  }

  // yz edges -- these are the four corners of every yz slice
  for (IdxType i=0; i<nGrid[0]; i++) {

    // (*,-1,-1) and (*,-1,nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bLo()[1]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
	      (*this)(i,1,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,-1,k,n) = -(*this)(i,0,k,n);
	    else
	      (*this)(i,-1,k,n) = 2 * (*this)(i,0,k,n) -
		(*this)(i,1,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,-1,k,n) = (*this)(i,nGrid[1]-1,k,n);
	    break;
	  }
	}
      }
    }

    // (*,nGrid[1],-1) and (*,nGrid[1],nGrid[2]) edges
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bHi()[1]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
	      (*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vyIdx || n == ByIdx)
	      (*this)(i,nGrid[1],k,n) = -(*this)(i,nGrid[1]-1,k,n);
	    else
	      (*this)(i,nGrid[1],k,n) = 2 * (*this)(i,nGrid[1]-1,k,n) -
		(*this)(i,nGrid[1]-2,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(i,nGrid[1],k,n) = (*this)(i,0,k,n);
	    break;
	  }
	}
      }
    }
  }

  // Six corners
  for (SignedIdxType j=-1; j<=(SignedIdxType) nGrid[1]; j+=nGrid[1]+1) {
    for (SignedIdxType k=-1; k<=(SignedIdxType) nGrid[2]; k+=nGrid[2]+1) {

      // (-1,-1,-1), (-1,nGrid[1],-1), (-1,-1,nGrid[2]), and
      // (-1,nGrid[1],nGrid[2]) corners
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bLo()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
	      (*this)(1,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(-1,j,k,n) = -(*this)(0,j,k,n);
	    else
	      (*this)(-1,j,k,n) = 2 * (*this)(0,j,k,n) -
		(*this)(1,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(-1,j,k,n) = (*this)(nGrid[0]-1,j,k,n);
	    break;
	  }
	}	
      }

      // (nGrid[0],-1,-1), (nGrid[0],nGrid[1],-1),
      // (nGrid[0],-1,nGrid[2]), and (nGrid[0],nGrid[1],nGrid[2]) corners
      for (IdxType n=0; n<nComp; n++) {
	switch (geom.bHi()[0]) {
	case Geometry::openBC:
	  {
	    MPIUtil::haltRun("GasModelCartesian: cannot use open "
			     "boundary conditions on Cartesian grid",
			     errBadGridSpecification);
	    break;
	  }
	case Geometry::absorbingBC:
	  {
	    (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
	      (*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::reflectingBC:
#ifdef TRACK_PITCH_ANGLE
	case Geometry::reflectingPitchAngleBC:
#endif
	  {
	    if (n == vxIdx || n == BxIdx)
	      (*this)(nGrid[0],j,k,n) = -(*this)(nGrid[0]-1,j,k,n);
	    else
	      (*this)(nGrid[0],j,k,n) = 2 * (*this)(nGrid[0]-1,j,k,n) -
		(*this)(nGrid[0]-2,j,k,n);
	    break;
	  }
	case Geometry::periodicBC:
	  {
	    (*this)(nGrid[0],j,k,n) = (*this)(0,j,k,n);
	    break;
	  }
	}
      }
    }
  } 
}

/////////////////////////////////////////////////////
// Functions to implement the required Gas interface
/////////////////////////////////////////////////////

inline
void CartesianGrid::frame(const RealVec &x,
			  const Real t,
			  RealVec& v,
			  TNBBasis& tnb) const {

  // Convert position to cell coordinates
  SIdxVec iv;
  RealVec f;
  posToIdx(x, iv, f);

  // Interpolate velocity to target point
  v = interpVec(iv, f, vxIdx);

  // Interpolate magnetic field and magnetic field gradient to target point
  RealVec B = interpVec(iv, f, BxIdx);
  RealTensor2 gradB = interpGradVector(iv, f, BxIdx);

  // Compute TNB basis vectors from B and gradB
  tnb = TNBVectors(B, gradB);
}

inline
GasData CartesianGrid::gasData(const RealVec& x,
			       const Real t) const {
  
  // Convert position to cell coordinates
  SIdxVec iv;
  RealVec f;
  posToIdx(x, iv, f);

  // Interpolate all components to target position
  vector<Real> dat = interp(iv, f);

  // Fill in scalar data
  GasData gd;
  gd.dx = dx.min();
  gd.den = dat[denIdx];
  gd.ionDen = dat[ionDenIdx];
  gd.v[0] = dat[vxIdx];
  gd.v[1] = dat[vyIdx];
  gd.v[2] = dat[vzIdx];
  gd.B[0] = dat[BxIdx];
  gd.B[1] = dat[ByIdx];
  gd.B[2] = dat[BzIdx];
  gd.xH0 = dat[xH0Idx];
  gd.xHp = dat[xHpIdx];
  gd.xHe0 = dat[xHe0Idx];
  gd.xHep = dat[xHepIdx];
  gd.xHep2 = dat[xHep2Idx];
  gd.xe = dat[xeIdx];
  gd.Z = dat[ZIdx];
  gd.TBB.resize(nBB);
  gd.WBB.resize(nBB);
  for (IdxType i = 0; i < nBB; i++) {
    gd.TBB[i] = dat[nf + 2*i];
    gd.WBB[i] = dat[nf + 2*i+1];
  }

  // Compute required gradients
  gd.denGrad = interpGradScalar(iv, f, denIdx);
  gd.ionDenGrad = interpGradScalar(iv, f, ionDenIdx);
  gd.vGrad = interpGradVector(iv, f, vxIdx);
  gd.BGrad = interpGradVector(iv, f, BxIdx);

  // Return
  return gd;
}

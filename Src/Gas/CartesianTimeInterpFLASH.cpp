#include "CartesianTimeInterpFLASH.H"
#include "FLASH/FlashGG.h"
#include <cmath>
#include <filesystem>
#include <regex>
#include <sstream>

using namespace std;
using namespace criptic;
using namespace gas;
using namespace std::filesystem;

CartesianTimeInterpFLASH::
CartesianTimeInterpFLASH(const ParmParser& pp, const Geometry& geom_):
  CartesianTimeInterp(pp, geom_, 0)
{
  // Set verbosity level
  verbosity = 0;
  pp.query("verbosity", verbosity);

  // Read scalings for chi, rho, and cs from input file
  chi = rhoScale = vScale = 1.0;
  pp.query("gas.chi", chi);
  pp.query("gas.rho0", rhoScale);
  pp.query("gas.v0", vScale);

  // Read the directory name nad template file name used for the snapshots
  string dirName, tempName;
  pp.query("gas.slice_dir", dirName);
  pp.get("gas.slice_filename", tempName);
  regex reg(tempName);

  // Find all the file names that match the template; these are files
  // in the direcetory dirName with names that are regex matches to
  // slice_filename
  for (auto const& file : directory_iterator(dirName)) {
    if (!file.is_regular_file()) continue;          // Skip non-files
    string fname = file.path().filename().string(); // Extract file name
    if (regex_match(fname, reg))                    // Check for match
      snapFiles.push_back(file.path().string());
  }

  // Sort the files
  sort(snapFiles.begin(), snapFiles.end());

  // Make sure that number of files matches up with times
  if (snapFiles.size() != tSlice.size()) {
    stringstream ss;
    ss << "CartesianTimeInterpFLASH: found inconsistent number of files: "
       << tSlice.size() << " times given, but "
       << snapFiles.size() << " slice files found"
       << endl;
    MPIUtil::haltRun(ss.str(), errBadInputFile);
  }
  
  // If told to skip first n files, remove them from the list, and
  // reset times of remaining files so first file is at t = 0.
  int skip_n = 0;
  if (pp.query("gas.skip_n_files", skip_n)) {
    if ((IdxType) skip_n > snapFiles.size()) {
      stringstream ss;
      ss << "CartesianTimeInterpFLASH: told to skip "
	 << skip_n << " files, but only "
	 << snapFiles.size() << " slice files found"
	 << endl;
      MPIUtil::haltRun(ss.str(), errBadInputFile);
    }
    snapFiles.erase(snapFiles.begin(),snapFiles.begin() + skip_n);
    tSlice.erase(tSlice.begin(),tSlice.begin() + skip_n);
    Real dt = tSlice[0];
    for (IdxType i = 0; i < tSlice.size(); i++) tSlice[i] -= dt;
  }

  // If told to skip every nth file, delete those from the list
  int skip = 0;
  if (pp.query("gas.slice_skip", skip)) {
    if (skip >= 2) {
      IdxType ptr1 = 0, ptr2 = 0;
      while (ptr2 < snapFiles.size()) {
        snapFiles[ptr1] = snapFiles[ptr2];
        tSlice[ptr1] = tSlice[ptr2];
        ptr1 += 1;
        ptr2 += skip;
      }
      snapFiles.resize(ptr1);
      tSlice.resize(ptr1);
    }
  }
}


void CartesianTimeInterpFLASH::loadData(const IdxType idx, CartesianGrid *g)
{
  // Create a FLASH gg (general grid) object corresponding to the
  // slice being loaded
  FlashGG gg = FlashGG(snapFiles[idx]);

  // Set up arguments to FLASH reader
  vector<int> nGrid = { (int) g->gridSize()[0],
                        (int) g->gridSize()[1],
                        (int) g->gridSize()[2] };
  vector< vector<double> > bounds =
    { {-0.5, 0.5}, {-0.5, 0.5}, {-0.5, 0.5} };

  // Load flash data
  float *Grid_Block_dens =
    gg.GetUniformGrid(nGrid, bounds, "dens", false, true);
  float *Grid_Block_velx =
    gg.GetUniformGrid(nGrid, bounds, "velx", false, true);
  float *Grid_Block_vely =
    gg.GetUniformGrid(nGrid, bounds, "vely", false, true);
  float *Grid_Block_velz =
    gg.GetUniformGrid(nGrid, bounds, "velz", false, true);
  float *Grid_Block_magx =
    gg.GetUniformGrid(nGrid, bounds, "magx", false, true);
  float *Grid_Block_magy =
    gg.GetUniformGrid(nGrid, bounds, "magy", false, true);
  float *Grid_Block_magz =
    gg.GetUniformGrid(nGrid, bounds, "magz", false, true);

  // Copy data to Cartesian grid object
  for (IdxType i = 0; i < g->gridSize()[0]; i++) {
    for (IdxType j = 0; j < g->gridSize()[1]; j++) {
      for (IdxType k = 0; k < g->gridSize()[2]; k++) {

        // Index in FLASH data
        IdxType idx = i + g->gridSize()[0] * (j + g->gridSize()[1] * k);

        // Copy from FLASH data
        (*g)(i,j,k,CartesianGrid::denIdx) =
          Grid_Block_dens[idx] * rhoScale;
        (*g)(i,j,k,CartesianGrid::ionDenIdx) =
          Grid_Block_dens[idx] * rhoScale * chi;
        (*g)(i,j,k,CartesianGrid::vxIdx) = Grid_Block_velx[idx] * vScale;
        (*g)(i,j,k,CartesianGrid::vyIdx) = Grid_Block_vely[idx] * vScale;
        (*g)(i,j,k,CartesianGrid::vzIdx) = Grid_Block_velz[idx] * vScale;
        (*g)(i,j,k,CartesianGrid::BxIdx) =
          Grid_Block_magx[idx] * vScale * sqrt(rhoScale);
        (*g)(i,j,k,CartesianGrid::ByIdx) =
          Grid_Block_magy[idx] * vScale * sqrt(rhoScale);
        (*g)(i,j,k,CartesianGrid::BzIdx) =
          Grid_Block_magz[idx] * vScale * sqrt(rhoScale);

        // Assign defaults to remaining quantities
        (*g)(i,j,k,CartesianGrid::xH0Idx) = 1;
        (*g)(i,j,k,CartesianGrid::xHpIdx) = chi;
        (*g)(i,j,k,CartesianGrid::xHe0Idx) = 0.0955;
        (*g)(i,j,k,CartesianGrid::xHepIdx) = 0;
        (*g)(i,j,k,CartesianGrid::xHep2Idx) = 0;
        (*g)(i,j,k,CartesianGrid::xeIdx) = chi;
        (*g)(i,j,k,CartesianGrid::ZIdx) = 0.0199;

      } // k
    } // j
  } // i

  // Free memory
  delete [] Grid_Block_dens;
  delete [] Grid_Block_velx;
  delete [] Grid_Block_vely;
  delete [] Grid_Block_velz;
  delete [] Grid_Block_magx;
  delete [] Grid_Block_magy;
  delete [] Grid_Block_magz;

}


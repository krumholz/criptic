#include "CartesianTimeInterp.H"
#include <algorithm>

using namespace std;
using namespace criptic;
using namespace gas;

CartesianTimeInterp::CartesianTimeInterp(const ParmParser& pp,
					 const Geometry& geom_,
					 const IdxType nBB,
					 const int nGhost) {
  
  // Save verbosity
  verbosity = 1;
  pp.query("verbosity", verbosity);
  
  // Read the time slices available; this can be specified either by
  // just giving the time between slices and the total number
  // available, or by giving a list of all times available
  if (!pp.query("gas.times", tSlice)) {
    Real dt;
    pp.get("gas.dt", dt);
    int nSlice;
    pp.get("gas.nslice", nSlice);
    if (nSlice < 2)
      MPIUtil::haltRun("CartesianTimeInterp: need >= 2 slice times",
		       errBadInputFile);
    tSlice.resize(nSlice);
    for (int i=0; i < nSlice; i++) tSlice[i] = i*dt;
  }

  // Make sure the first time slice is time zero, and that times are
  // monotoncally increasing
  if (tSlice[0] != 0.0)
    MPIUtil::haltRun("CartesianTimeInterp: first time slice must be at t = 0",
		     errBadInputFile);
  for (IdxType i = 0; i < tSlice.size()-1; i++) {
    if (tSlice[i+1] <= tSlice[i])
      MPIUtil::haltRun("CartesianTimeInterp: slice times must be "
		       "monotonically increasing",
		       errBadInputFile);
  }

  // Make sure initial time step is less than time of first slice; it
  // is an error if not
  Real dt = 0.0;
  pp.query("dt_init", dt);
  if (dt >= tSlice[1])
    MPIUtil::haltRun("CartesianTimeInterp: initial simulation "
		     "time step must be < time of second slice",
		     errBadInputFile);

  // Construct old and new gas data objects
  gasOld = new CartesianGrid(pp, geom_, nBB, nGhost);
  gasNew = new CartesianGrid(pp, geom_, nBB, nGhost);

  // Flag that no data have been loaded yet
  dataLoaded = false;
}


CartesianTimeInterp::~CartesianTimeInterp() {
  delete gasOld;
  delete gasNew;
}


void CartesianTimeInterp::updateState(const Real t,
				      Real& tNext) {

  // Safety check
  if (t >= tSlice.back())
    MPIUtil::haltRun("CartesianTimeInterp: simulation time has "
		     "exceeded time of last time slice",
		     errBadInputFile);

  // Handle special case of the first time this method is called, at
  // which point no data are loaded and tPtr does not yet point to the
  // right location.
  if (!dataLoaded) {

    // Move pointer so tSlice[tPtr] <= t < tSlice[tPtr+1]
    tPtr = 0;
    while (tSlice[tPtr+1] < t) tPtr++;

    // Load old and new data and set boundary conditions
    if (verbosity > 1 && MPIUtil::IOProc) {
      cout << "CartesianTimeInterp: loading slice at t = " << tSlice[tPtr]
	   << endl;
    }
    loadData(tPtr, gasOld);
    if (verbosity > 1 && MPIUtil::IOProc) {
      cout << "CartesianTimeInterp: loading slice at t = " << tSlice[tPtr+1]
	   << endl;
    }
    tPtr++;
    loadData(tPtr, gasNew);
    gasOld->applyBC();
    gasNew->applyBC();

    // Flag that data are now loaded
    dataLoaded = true;

    // Return
    return;
  }

  // General case on second and subsequent calls to this method: check
  // if we have hit the time when we need to load a new snapshot, or
  // if we need to reduce the time step to hit the next load time
  if (t == tSlice[tPtr]) {
    
    swap(gasOld, gasNew);    // Swap old and new data
    tPtr++;                  // Increment pointer
    if (verbosity > 1 && MPIUtil::IOProc) {
      cout << "CartesianTimeInterp: loading slice at t = " << tSlice[tPtr]
	   << endl;
    }
    loadData(tPtr, gasNew);  // Load new data
    gasNew->applyBC();       // Apply boundary conditions
    
  } else if (tNext > tSlice[tPtr]) {
    
    tNext = tSlice[tPtr];    // Throttle time step

  }  
}

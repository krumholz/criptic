// This sets up the Terzan 5 problem generator.

#include "Terzan5Prob.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
                                 const criptic::Geometry& geom,
                                 criptic::RngThread& rng) {
  return new criptic::Terzan5Prob(pp, geom, rng);
}

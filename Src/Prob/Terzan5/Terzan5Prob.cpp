// This implements the setup and calculations for the Terzan 5
// simulation. The setup is that a source is placed at the origin
// which injects cosmic rays over a specified range in pitch angle,
// and with a specified initial momentum. The CRs then diffuse in
// pitch angle while also undergoing synchrotron losses.

#include <array>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include "Terzan5Prob.H"
#include "../../MPI/MPIUtil.H"
#include "../../Losses/InverseCompton.H"
#include "../../Utils/Constants.H"
#include "../../Utils/PartTypes.H"
#include "../../Utils/SR.H"
#include "../../Utils/Units.H"
#include "hdf5.h"
#ifdef _OPENMP
#   include "omp.h"
#endif

using namespace criptic;
using namespace std;

// Constructor; this reads some information about outputs from the
// input deck
Terzan5Prob::Terzan5Prob(const ParmParser& pp_,
			 const Geometry& geom_,
			 RngThread& rng_) :
  Prob(pp_, geom_, rng_)
{
	// Read physical parameters from input file; enforce that B is in
	// the x direction
	RealVec B;
	Real kMu;
	pp.get("gas.magField", B);
	pp.get("cr.kMu0", kMu);
	pp.get("prob.muinj", muinj);
	if (!pp.query("prob.rStandOff", rSO)) rSO = 0.0;
	if (B[1] != 0.0 || B[2] != 0.0)
		MPIUtil::haltRun("Terzan5Prob: magnetic field must lie in x direction",
			errBadInputFile);

	// Set reference CR momentum p0 so ratio of loss time to
	// isotropization time at p = p0 is 1
	Real UB = B.mag2() / (8*M_PI);
	p0 = 3 * constants::me_c * constants::me_c * kMu /
		(4 * constants::sigmaT * UB * constants::mp_c);
}

// Set up the source
void Terzan5Prob::initSources(vector<RealVec>& x,
			      vector<CRSource>& sources) {

	// Only one MPI rank does this
  	if (MPIUtil::IOProc) {
  
    	// Create sources
		for (auto muinj_ : muinj) {
    		RealVec r(rSO, 0, 0);
    		CRSource src;
    		src.type = partTypes::electron;
    		src.p0 = src.p1 = p0;
    		src.mu0 = muinj_;
    		src.mu1 = 1.0;
			src.q = 0.0; // Doesn't matter, just to silence compiler warning
    		src.setLum(1.0);  // Arbitrary value, we re-scale later

    		// Push onto output vectors
    		x.push_back(r);
    		sources.push_back(src);
		}
  	}
}
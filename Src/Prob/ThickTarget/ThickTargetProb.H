/**
 * @file ThickTargetProb.H
 * @brief Custom problem class used for the thick target test
 *
 * @details
 * This is a specialization of the Prob interface class to set up the
 * thick target test problem.
 *
 * @author Mark Krumholz
 */

#ifndef _THICKTARGET_PROB_H_
#define _THICKTARGET_PROB_H_

#include "../Prob.H"

namespace criptic {

  /**
   * @class ThickTargetProb
   * @brief Prob class for the variable diffusion test problem
   * @details
   * This class is derived from the general Prob class, and sets up
   * the variable diffusion test problem.
   */
  class ThickTargetProb : public Prob {

  public:
    
    /**
     * @brief Constructor; this just invokes the parent constructor
     * @param pp The object containing the input deck
     * @param geom The problem geometry object
     * @param rng The random number generator
     */
    ThickTargetProb(const ParmParser& pp_,
		  const Geometry& geom_,
		  RngThread& rng_) : Prob(pp_, geom_, rng_)
    { }

    /**
     * @brief Set up CR propagation class
     * @return An object of class Propagation describing CR propagation
     * @details
     * This routine sets up the propagation coefficients for the
     * variable diffusion test problem.
     */
    virtual propagation::Propagation *initProp();

    /**
     * @brief Set up initial sources for the thick target problem
     * @param x Positions of initial CR sources
     * @param sources Properties of initial CR sources
     * @details
     * Sets up the initial CR source distribution for the variable
     * thick target test problem.
     */
    virtual void initSources(std::vector<RealVec>& x,
			     std::vector<CRSource>& sources);
  
  };
}

#endif

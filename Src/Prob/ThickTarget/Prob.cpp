// This sets up the variable diffusion test problem, by returning
// the problem generator class for it.

#include "ThickTargetProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::ThickTargetProb(pp, geom, rng);
}

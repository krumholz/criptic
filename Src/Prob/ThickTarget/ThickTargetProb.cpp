// This file provides a problem setup for the thick target test. As
// with the loss rates test, this isn't a test with a known exact
// solution, just a test against a tabulated solution computed using
// an earlier version of the code.

#include "ThickTargetProb.H"
#include "../../MPI/MPIUtil.H"
#include "../../Propagation/NoPropagation.H"
#include "../../Utils/Constants.H"
#include "../../Utils/PartTypes.H"
#include "../../Utils/SR.H"
#include "../../Utils/Units.H"

using namespace criptic;
using namespace std;

// Set up the CR propagation model; for this test, we set up a model
// where all propagation is off
propagation::Propagation* ThickTargetProb::initProp() {
  return new propagation::NoPropagation();
}

// Set up sources; we place two at the origin, one that makes protons
// and one that makes electrons
void ThickTargetProb::initSources(vector<RealVec>& x,
				  vector<CRSource>& sources) {
  
  // Only one MPI rank does this
  if (MPIUtil::IOProc) {

    // Get source properties from input deck
    Real L, T0, T1, q, epRatio;
    pp.get("prob.L", L);
    pp.get("prob.T0", T0);
    pp.get("prob.T1", T1);
    pp.get("prob.q", q);
    pp.get("prob.e_p_ratio", epRatio);

    // Create sources
    x.push_back(RealVec(0,0,0));
    x.push_back(RealVec(0,0,0));
    sources.resize(2);
    sources[0].type = partTypes::proton;
    sources[1].type = partTypes::electron;
    sources[0].p0 = sr::p_from_T(partTypes::proton,
				 T0 * units::GeV / constants::mp_c2);
    sources[1].p0 = sr::p_from_T(partTypes::electron,
				 T0 * units::GeV / constants::mp_c2);
    sources[0].p1 = sr::p_from_T(partTypes::proton,
				 T1 * units::GeV / constants::mp_c2);
    sources[1].p1 = sr::p_from_T(partTypes::electron,
				 T1 * units::GeV / constants::mp_c2);
    sources[0].q = sources[1].q = q;
    sources[0].setLum(L);
    sources[1].setLum(epRatio * L);
  }
}

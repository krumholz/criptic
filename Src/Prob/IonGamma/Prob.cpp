// This file provides the problem setup for the IonGamma problem,
// published at Krumholz, Crocker, & Offner, 2023, MNRAS, 520, 5126

#include "IonGammaProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
                                 const criptic::Geometry& geom,
                                 criptic::RngThread& rng) {
  return new criptic::IonGammaProb(pp, geom, rng);
}


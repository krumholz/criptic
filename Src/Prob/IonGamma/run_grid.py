"""
This script runs a grid of criptic models for sources of different
particle types with different initial CR energies in the thick target
regime.
"""

import numpy as np
import astropy.units as u
from astropy.constants import c, m_p
import os
import os.path as osp
import subprocess

# Location of criptic executable
criptic_exe = '/home/krumholz/Projects/criptic/Src/Prob/IonGamma/criptic'

# Location of template parameter file
template_param = '/home/krumholz/Projects/criptic/Src/Prob/IonGamma/criptic_m7.0.in'

# Directory into which to write output
outdir = '/avatar/krumholz/IonGamma'

# List of particle types
part_types = [ 0,   # Protons
               1 ]  # Electrons

# List of energies
mPi0 = 134.9758 * u.MeV/c**2   # pi^0 mass
Tpi_th = 2 * mPi0 * c**2 + (mPi0 * c**2)**2 / \
         (2 * m_p * c**2)      # pion production threshold energy
logT = [
    np.linspace(np.log10(Tpi_th.to(u.MeV).value), 9, 51), # Proton energies
    np.linspace(2, 9, 51) ] # Electron energies

# Number of outputs per run
nout = 10

# Loop over particle types and energies
for pt, logTpart in zip(part_types, logT):
    for logT_ in logTpart:

        # String for this run
        rundir = outdir
        if pt == 0: rundir = osp.join(rundir, "proton")
        elif pt == 1: rundir = osp.join(rundir, "electron")
        rundir += "_{:05.3f}".format(logT_)

        # Create directory for this run if it does not already exist
        try:
            os.mkdir(rundir)
        except:
            pass

        # Check if we already have output in this directory; if so,
        # skip it
        if osp.exists(osp.join(rundir,
                               'criptic_{:05d}.hdf5'.format(nout))):
            continue

        # If we are here, read the template input file and write to an
        # input file in the target directory, changing the lines we
        # want to change
        fpin = open(template_param, 'r')
        template_out = osp.join(rundir, 'criptic.in')
        fpout = open(template_out, 'w')
        for line in fpin:
            if line.startswith('prob.T'):
                fpout.write('prob.T    {:e}  # Energy in GeV\n'.
                            format(10.**(logT_-3)))
            elif line.startswith('prob.ptype'):
                fpout.write('prob.ptype  {:d}  # Particle type\n'.
                            format(pt))
            else:
                fpout.write(line)
        fpin.close()
        fpout.write('output.chk_name   {:s}\n'.
                    format(osp.join(rundir, 'criptic_')))
        fpout.close()

        # Run criptic
        cmdstr = "{:s} {:s} >& {:s}".format(
            criptic_exe,
            template_out,
            osp.join(rundir, 'criptic.log'))
        print(cmdstr)
        subprocess.run(cmdstr, shell=True)

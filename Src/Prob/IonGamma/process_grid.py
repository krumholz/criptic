"""
This script reads the grid of criptic models produced by run_grid and
extracts the key results on ionization and gamma-ray production.
"""

import numpy as np
import astropy.units as u
from cripticpy import readchk, parseinput
import os
import os.path as osp
from glob import glob

# Base name and directory location of the runs to process
run_basename = "IonGamma"
run_dir = "/avatar/krumholz/"

# Base name of the summary files to write
summary_file = "IonGammaSamp"

# Extensions to expect
comp = [ "HI", "H2" ]
fsync = [ "_m6.5", "_m7.0", "_m7.5" ]

# Maximum checkpoint number expected in each directory
ncheck = 20

# Number of energies in output
nenergy = 101

# Loop over all combinations of background composition and fsync
for comp_ in comp:
    for fsync_ in fsync:

        # Build name of output file
        outname = summary_file + comp_ + fsync_ + ".npz"

        # First read the set of energies and particle types in the existing
        # summary file, if it exists; we will skip over runs that are already
        # stored
        if osp.isfile(outname):
            with np.load(outname) as data:
                pt = data['pt']
                logT = data['logT']
                ionrate = data['ionrate']
                ionrate_sec = data['ionrate_sec']
                dLdE = data['dLdE']
                skipruns = []
                for pt_, logT_ in zip(pt, logT):
                    rundir = osp.join(run_dir, run_basename + comp_ + fsync_)
                    if pt_ == 0: rundir = osp.join(rundir, "proton")
                    elif pt_ == 1: rundir = osp.join(rundir, "electron")
                    rundir += "_{:05.3f}".format(logT_)
                    skipruns.append(rundir)
        else:
            pt = np.zeros(0, dtype='int')
            logT = np.zeros(0)
            ionrate = np.zeros((0,ncheck))
            ionrate_sec = np.zeros((0,ncheck))
            dLdE = np.zeros((0,ncheck,nenergy))
            skipruns = []

        # Now read the list of available runs, and remove any that are to be
        # skipped
        dirlist = glob(osp.join(run_dir, run_basename + comp_ + fsync_,
                                'proton_*')) + \
                  glob(osp.join(run_dir, run_basename + comp_ + fsync_,
                                'electron_*'))
        runlist = [ r for r in dirlist if r not in skipruns ]

        # Now begin processing
        for run in runlist:

            # First check that we have the final checkpoint for this run; if
            # not, the run is incomplete and we skip it
            chkname = 'criptic_{:05d}.hdf5'.format(ncheck)
            if not osp.isfile(osp.join(run, chkname)):
                print("Skipping "+run)
                continue
            else:
                print("Processing "+run)

            # If we are here, loop over the available checkpoints
            ionrate_ = []
            ionrate_sec_ = []
            dLdE_ = []
            for i in range(1, ncheck+1):

                # Read data
                chkname = 'criptic_{:05d}.hdf5'.format(i)
                data = readchk(osp.join(run, chkname))
                param = parseinput(osp.join(run, 'criptic.in'))

                # Determine particle type and kinetic energy
                logT_ = np.log10(param['prob.T']) + 3  # Convert GeV to MeV
                pt_ = param['prob.ptype']

                # Grab ionization rates for H or H2, both total and
                # secondaries only
                if comp_ == "HI":
                    idx = data['ionization']['targets'].index('H')
                else:
                    idx = data['ionization']['targets'].index('H2')
                idxsec = np.where(data['packets']['sec'])[0]
                ionrate_.append( np.sum(
                    data['ionization']['rate'][:,idx].to(1/u.s).value))
                ionrate_sec_.append( np.sum(
                    data['ionization']['rate'][idxsec,idx].to(1/u.s).value))
        
                # Grab gamma-ray luminosity
                dLdE_.append( np.sum(
                    data['photons']['dLdE_sum'].value, axis=(0,1,2) ) )

            # Consolidate data into output arrays
            logT = np.append(logT, logT_)
            pt = np.append(pt, pt_)
            ionrate = np.vstack((ionrate, np.array(ionrate_)))
            ionrate_sec = np.vstack((ionrate_sec, np.array(ionrate_sec_)))
            dLdE = np.vstack((dLdE,
                              np.array(dLdE_).reshape(1,ncheck,nenergy)))

            # Write output
            np.savez(outname, pt=pt, logT=logT,
                     ionrate=ionrate,
                     ionrate_sec=ionrate_sec,
                     dLdE=dLdE)
    

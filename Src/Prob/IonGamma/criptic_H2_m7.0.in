# criptic input deck

######## Global run controls ########

dt_init      1e12                    # Initial time step
max_time     1e14                    # Maximum step number
verbosity    1                       # Verbosity levels

######## Problem geometry ########

geometry.lo_type    absorbing absorbing absorbing   # Bounary type in (x,y,z)
geometry.hi_type    absorbing absorbing absorbing   # Bounary type in (x,y,z)
geometry.prob_lo    -3.09e18 -3.09e18 -3.09e18      # Problem domain lo
geometry.prob_hi    3.09e18 3.09e18 3.09e18         # Problem domain hi

######## Output controls ########

output.chk_dt                5.0e12
output.photon_energy_span_1  0.1 1e4 101 # Write photons on grid of energies
output.photon_energy_span_1_unit GeV      # from 0.1 GeV - 10 TeV

######## Integrator controls ########

integrator.packet_rate  2.0e-7
integrator.w_frac_min   1e-6
integrator.T_min        1e-6
integrator.c_step	0.05

######## Loss controls ########

losses.f_sec            0.2

######## Parameters for the background gas ########

# Note that the choices below correspond to nH = 10^3 H cm^-3, f_sync
# = f_IC = 10^-7

gas.density     2.3598e-21           # Total density
gas.ionDensity  0.0                  # Ionized gas density
gas.velocity    0.0 0.0 0.0          # Gas velocity
gas.comp        molecular            # Composition
gas.TBB         2.73 40.0            # Blackbody field temperatures
gas.WBB         1.0 0.03066          # Blackbody dilution factors
gas.magField    1.221e-4 0.0 0.0     # Magnetic field strength

######## Problem setup parameters ########

prob.T          0.1                # CR energy in GeV
prob.ptype      0                    # Particle type


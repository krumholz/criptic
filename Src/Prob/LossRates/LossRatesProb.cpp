// Implementation of the LossRatesProb class.  This isn't really
// testing against an analytic solution; the problem just computes the
// loss rates for CR packets of all particle types at a wide range of
// energies from all mechanisms. 

#include "LossRatesProb.H"
#include "../../Gas/CartesianGrid.H"
#include "../../MPI/MPIUtil.H"
#include "../../Propagation/NoPropagation.H"
#include "../../Utils/Constants.H"
#include "../../Utils/PartTypes.H"
#include "../../Utils/SR.H"
#include "../../Utils/Units.H"

using namespace criptic;
using namespace std;

// Set up initial gas state
gas::Gas* LossRatesProb::initGas() {

  // Allocate grid
  IdxVec nGrid(6,6,6);
  gas::CartesianGrid *grid = new gas::CartesianGrid(nGrid, geom, 2);

  // Define the properties of the three regions
  Real nH[3]   = { 0.001  , 1.0    , 1000.0   };   // H nucleon number density
  Real chi[3]  = { 1.0    , 0.01   , 1.0e-6   };   // Ion mass fraction
  Real B[3]    = { 1.0e-6 , 7.0e-6 , 2.0e-5   };   // Magnetic field strength
  Real xH0[3]  = { 0.0    , 0.99   ,    0.0   };   // H^0 abundance
  Real xHp[3]  = { 1.0    , 0.01   ,    0.0   };   // H^+ abundance
  Real xHe0[3] = { 0.0    , 0.078  ,    0.079 };   // He^0 abundance
  Real xHep[3] = { 0.079  , 0.001  ,    0.0   };   // He^+ abundance
  Real xe[3]   = { 1.079  , 0.011  , 1.0e-7   };   // e^- abundance
  Real TBB[3]  = { 5000.  , 5000.  ,   10.0   };   // Blackbody temp
  Real WBB[3]  = { 1.0e-13, 1.0e-13,    0.01  };   // Dilution factor
  
  // Assign quantities
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 6; j++) {
      for (int k = 0; k < 6; k++) {

	// Density
	(*grid)(2*i,j,k,gas::CartesianGrid::denIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::denIdx) =
	  nH[i] * 1.4 * constants::mH;

	// Ion density
	(*grid)(2*i,j,k,gas::CartesianGrid::ionDenIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::ionDenIdx) =
	  nH[i] * 1.4 * constants::mH * chi[0];

	// Velocity
	(*grid)(2*i,j,k,gas::CartesianGrid::vxIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::vxIdx) =
	  (*grid)(2*i,j,k,gas::CartesianGrid::vyIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::vyIdx) =
	  (*grid)(2*i,j,k,gas::CartesianGrid::vzIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::vzIdx) = 0.0;

	// Magnetic field
	(*grid)(2*i,j,k,gas::CartesianGrid::BxIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::BxIdx) = B[i];
	(*grid)(2*i,j,k,gas::CartesianGrid::ByIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::ByIdx) =
	  (*grid)(2*i,j,k,gas::CartesianGrid::BzIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::BzIdx) = 0.0;
	
	// x(H^0)
	(*grid)(2*i,j,k,gas::CartesianGrid::xH0Idx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::xH0Idx) = xH0[i];
	
	// x(H^+)
	(*grid)(2*i,j,k,gas::CartesianGrid::xHpIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::xHpIdx) = xHp[i];
    
	// x(He^0)
	(*grid)(2*i,j,k,gas::CartesianGrid::xHe0Idx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::xHe0Idx) = xHe0[i];
    
	// x(He^+)
	(*grid)(2*i,j,k,gas::CartesianGrid::xHepIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::xHepIdx) = xHep[i];
    
	// x(He^2+)
	(*grid)(2*i,j,k,gas::CartesianGrid::xHep2Idx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::xHep2Idx) = 0.0;
	
	// x(e^-)
	(*grid)(2*i,j,k,gas::CartesianGrid::xeIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::xeIdx) = xe[i];
	
	// Total metal mass
	(*grid)(2*i,j,k,gas::CartesianGrid::ZIdx) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::ZIdx) = 0.0199;

	// CMB temperature and dilution
	(*grid)(2*i,j,k,gas::CartesianGrid::nf) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::nf) = 2.73;
	(*grid)(2*i,j,k,gas::CartesianGrid::nf+1) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::nf+1) = 1.0;

	// Non-CMB contribution
	(*grid)(2*i,j,k,gas::CartesianGrid::nf+2) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::nf+2) = TBB[i];
	(*grid)(2*i,j,k,gas::CartesianGrid::nf+3) =
	  (*grid)(2*i+1,j,k,gas::CartesianGrid::nf+3) = WBB[i];
      }
    }
  }

  // Fill ghost cells
  grid->applyBC();
    
  return grid;
}


// Set up the CR propagation model; for this test, we set up a model
// where all propagation is off
propagation::Propagation* LossRatesProb::initProp() {
  return new propagation::NoPropagation();
}


// Initialize CR packets
void LossRatesProb::initPackets(vector<RealVec>& x,
				vector<CRPacket>& packets) {

  // Only IO processor does this
  if (!MPIUtil::IOProc) return;

  // Read energy range
  Real Tmin, Tmax;
  int nPart;
  pp.get("prob.T_min", Tmin);
  pp.get("prob.T_max", Tmax);
  pp.get("prob.n_part", nPart);

  // Read problem domain
  RealVec xLo, xHi;
  pp.get("geometry.prob_lo", xLo);
  pp.get("geometry.prob_hi", xHi);
  RealVec xreg[3];
  xreg[0] = (2./3.) * xLo;
  xreg[1] = zeroVec;
  xreg[2] = (2./3.) * xHi;

  // Create packets
  for (int j = 0; j < 3; j++) {
    for (int i = 0; i < partTypes::nPartType; i++) {
      for (int n = 0; n < nPart; n++) {

	// Place in appropriate zone
	x.push_back(xreg[j]);

	// Set packet properties
	CRPacket pkt;
	pkt.type = static_cast<partTypes::pType>(i);
	pkt.src = CRPacket::noSrc;
	pkt.tInj = 0.0;
	pkt.wInj = pkt.w = 1.0;
	pkt.gr = 0.0;
	pkt.p = sr::p_from_T(pkt.type,
			     Tmin * units::GeV / constants::mp_c2 *
			     pow(Tmax/Tmin, n/(nPart-1.0)) );

	// Add packet
	packets.push_back(pkt);
      }
    }
  }
}

"""
Make a plot showing the rates for various loss processes.
"""

from cripticpy import readchk, parseinput
from cripticpy.sr import parttypes
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u

# Read the data
data = readchk('criptic_00000.hdf5')

# Read input energy range
params = parseinput('criptic.in')
Tmin = params['prob.T_min'] * u.GeV
Tmax = params['prob.T_max'] * u.GeV

# Set gas densities in different regions
nH = np.array([1e-3, 1, 1e3])*u.cm**-3
nHlab = [r'H$^+$', r'H~\textsc{i}', r'H$_2$']

# Get loss timescales for continuous and catastrophic processes in Myr^-1
rcat = data['losses']['lossrate'].to(1/u.Myr)
rcts = np.transpose(
    np.transpose(data['losses']['dpdt']) / data['packets']['p']). \
    to(1/u.Myr)

# Set floor values to suppress warnings later on about taking the log
# of zero
rcat[rcat < 1e-30/u.Myr] = 1e-30/u.Myr
rcts[rcts < 1e-30/u.Myr] = 1e-30/u.Myr

# Get indices corresponding to different physical regions and
# different particle types
idxr = [data['packets']['x'][:,0] < 0,
        data['packets']['x'][:,0] == 0,
        data['packets']['x'][:,0] > 0]
ptype = [0, 2]
idxt = [data['packets']['ptype'] == p for p in ptype]

# Make plot
plt.figure(1, figsize=(7,3.5))
plt.clf()
plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=12)
xlim = [-3, 6]
ylim = [-8, 5]
for i in range(len(idxr)):
    for j in range(len(idxt)):
        plt.subplot(len(idxt),len(idxr),len(idxr)*j+i+1)
        idx = np.logical_and(idxr[i], idxt[j])
        T = data['packets']['T'][idx].to(u.GeV)
        for m in range(len(data['lossmech'])):
            plt.plot(np.log10(T/u.GeV),
                     np.log10(rcat[idx,m]*u.Myr),
                     'C{:d}-.'.format(m),
                     lw=2)
            plt.plot(np.log10(T/u.GeV),
                     np.log10(rcts[idx,m]*u.Myr),
                     'C{:d}'.format(m),
                     lw=2)
        plt.xlim(xlim)
        plt.ylim(ylim)
        if i != 0:
            plt.gca().set_yticklabels('')
        #elif j == 1:
        #    plt.ylabel(r'$\log \, L$ [Myr$^{-1}$]')
        if j != len(idxt)-1:
            plt.gca().set_xticklabels('')
        else:
            if i == 2:
                plt.gca().set_xticks([-3,0,3,6])
            else:
                plt.gca().set_xticks([-3,0,3])
            if i == 1:
                plt.xlabel(r'$\log T$ [GeV]')
        if j == 0:
            plt.title(nHlab[i])
            
# Add legend
nm = len(data['lossmech'])
plt.subplot(len(idxt),len(idxr),len(idxr))
for m in range(nm):
    plt.plot([-1,-1], [-1,-1], 'C{:d}'.format(m),
             label=data['lossmech'][m],
             lw=2)
plt.legend(prop={"size" : 10}, bbox_to_anchor=(1.2, 1.05))
plt.subplot(len(idxt),len(idxr),len(idxt)*len(idxr))
plt.plot([-1,-1], [-1,-1], 'k', label='Continuous', lw=2)
plt.plot([-1,-1], [-1,-1], 'k-.', label='Catastrophic', lw=2)
plt.legend(prop={"size" : 10}, bbox_to_anchor=(1.2, 0.8))

# Add particle labels
for i in range(len(idxt)):
    plt.subplot(len(idxt),len(idxr),(i+1)*len(idxr))
    plt.twinx()
    plt.gca().set_yticks([])
    plt.ylabel(parttypes[ptype[i]]['symbol'])

# Add common y axis label
ax = plt.gcf().add_subplot(111, frameon=False)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
ax.set_ylabel(r'$\log \, L$ [Myr$^{-1}$]')

# Adjust spacing
plt.subplots_adjust(right=0.72, left=0.1, hspace=0.12, wspace=0,
                    bottom=0.15, top=0.92)

# Save
plt.savefig('LossRates.pdf')
plt.savefig('LossRates.png')

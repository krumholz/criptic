"""
Script to run the criptic test suite
"""

import argparse
import os
import subprocess
import sys

# Parse command line
parser = argparse.ArgumentParser(
    description="Script to run the criptic test suite")
parser.add_argument("--noplots",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-l", "--list",
                    help="just list available tests and exit; do not "
                    "run tests",
                    default=False, action="store_true")
parser.add_argument("--norun",
                    help="run the analysis script only, do not run "
                    "the simulations; results are only meaningful if "
                    "the simulations have already been run",
                    default=False, action="store_true")
parser.add_argument("-t", "--tests", nargs="*")
args = parser.parse_args()

# Grab list of all available tests
testdir = os.path.dirname(os.path.abspath(__file__))
dirlist = os.listdir(testdir)
tests_avail = [ d for d in dirlist
                if os.path.isdir(os.path.join(testdir, d)) ]
tests_avail.sort()
if args.list:
    print("Available tests are: ")
    for l in tests_avail:
        print("   " + l)
    exit(0)

# Set list of tests to run
if args.tests is None:
    tests_to_run = tests_avail
else:
    tests_to_run = []
    for t in args.tests:
        if t in tests_avail:
            tests_to_run.append(t)
        else:
            raise ValueError("unknown test "+t)

# Loop over tests
for t in tests_to_run:

    # Print test name
    print("Running test " + t + "...")

    # Change to test directory
    rundir = os.path.join(testdir, t)
    os.chdir(rundir)
    
    # Run test, being sure to export the LD_LIBRARY_PATH if it is set,
    # since for some reason subprocess seems not to export that
    # automatically
    cmd = "./criptic criptic.in"
    if "LD_LIBRARY_PATH" in os.environ:
        cmd = "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:" + \
            os.environ["LD_LIBRARY_PATH"] + \
            "; " + cmd
    if not args.norun:
        if not args.verbose:
            cmd = cmd + " >& test.log"
            subprocess.run(cmd, shell=True)
        else:
            popen = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                     universal_newlines=True,
                                     shell=True)
            for line in iter(popen.stdout.readline, ""):
                print(line.strip())
            popen.stdout.close()
            popen.wait()

    # Now run the analysis script and capture the result
    cmd = sys.executable + " analyze.py"
    if args.verbose:
        cmd = cmd + " --verbose"
    res = subprocess.run(cmd, shell=True, capture_output=True,
                         text=True)
    
    # Print test result
    if not args.verbose:
        res = res.stdout.split('\n')[0]
        print("    " + res)
    else:
        for r in res.stdout.split('\n'):
            print("   " + r)

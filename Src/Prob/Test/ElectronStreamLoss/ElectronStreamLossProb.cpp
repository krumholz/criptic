// This file implements the problem setup for the electron streaming
// loss test problem.

#include "ElectronStreamLossProb.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Constants.H"
#include "../../../Utils/PartTypes.H"
#include "../../../Utils/SR.H"
#include "../../../Utils/Units.H"

using namespace criptic;
using namespace std;

void
ElectronStreamLossProb::initSources(vector<RealVec>& x,
				    vector<CRSource>& sources) {
  
  // Only one MPI rank does this
  if (MPIUtil::IOProc) {
  
    // Get source luminosities and energies from input deck
    Real L, p0, p1, pidx;
    pp.get("prob.L", L);
    pp.get("prob.p0", p0);
    pp.get("prob.p1", p1);
    pp.get("prob.pidx", pidx);

    // Create source
    x.resize(1);
    sources.resize(1);
    sources[0].type = partTypes::electron;
    sources[0].p0 = p0 * units::GeV / constants::c / constants::mp_c;
    sources[0].p1 = p1 * units::GeV / constants::c / constants::mp_c;
    sources[0].q = pidx;
    sources[0].setLum(L);
  }
}

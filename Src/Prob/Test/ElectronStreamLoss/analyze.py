"""
Analysis script for electron transport with loss test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import astropy.constants as const
import os.path as osp
from cripticpy import readchk, readlastchk, parseinput, pctnbin

# Name of the problem this script is for
prob_name = "ElectronStreamLoss"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-kn", "--include_kn",
                    help="include Klein-Nishina regime tests on plots",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
p0 = params['prob.p0'] * u.GeV / const.c
p1 = params['prob.p1'] * u.GeV / const.c
q = params['prob.pidx']
L = params['prob.L'] * u.erg/u.s
B = params['gas.magField'][0]
TBB = params['gas.TBB']
WBB = params['gas.WBB']
vstr = params['cr.vStr0']
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'
    
# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
    if args.include_kn:
        data_kn = readchk(osp.join(args.dir, chkname+"kn_" +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)
    if args.include_kn:
        data_kn = readlastchk(args.dir, chkname+"kn_")

# Compute dimensionless position of all packets
aR = 4 * const.sigma_sb / const.c
UR = WBB * aR * TBB**4
UB = ((B**2 / (8.0 * np.pi)).to(u.G**2)).value * \
     u.erg/u.cm**3 # Fancy footwork because astropy doesn't correctly
                   # implement cgs units for magnetism
tloss = 3 * const.m_e * const.c / (4 * const.sigma_T * (UR + UB))
x = data['packets']['x'][:,0]
xi = (x / (vstr * tloss)).to('')
if args.include_kn:
    xi_kn = (data_kn['packets']['x'][:,0] / (vstr * tloss)).to('')

# Compute packet dimensionless momenta
gamma = (data['packets']['p'] / (const.m_e * const.c)).to('')
gamma0 = (p0  / (const.m_e * const.c)).to('')
gamma1 = (p1  / (const.m_e * const.c)).to('')

# Compute normalization of momentum distribution, \dot{n}_0 / p_0
coef = data['sources']['coef'][0] * (const.m_e/const.m_p)**q / \
       (const.m_p * const.c)

# Exact solution for integrated momentum distribution
def dndlg_ex(gamma):
    xi_max = np.minimum(data['t']/tloss, 1/gamma - 1/gamma1)
    return coef * const.m_e * const.c * tloss * \
        gamma**q / (q+1) * ((1 - xi_max*gamma)**(-q-1) - 1)

# Exact solution for local momentum distribution
def dndlgdx_ex(gamma, xi):
    fac = 1 - xi*gamma
    fac[fac < 0] = 1e-30  # Avoid divide by zero
    return coef / vstr * const.m_e * const.c * \
        gamma * fac**(-2) * (gamma / fac)**q

# Get total momentum distribution
nbin = 30
hist, edge = np.histogram( np.array(np.log(gamma)), bins=nbin,
                           range = np.array([np.log(gamma0),
                                             np.log(gamma1)]),
                           weights = data['packets']['w'])    

# Compute distribution function
cen = 0.5 * (edge[1:] + edge[:-1])
dlg = edge[1] - edge[0]
dndlg = hist/dlg

# Repeat for K-N regime data
if args.include_kn:
    gamma_kn = (data_kn['packets']['p'] / (const.m_e * const.c)).to('')
    hist_kn, edge = np.histogram( np.array(np.log(gamma_kn)), bins=nbin,
                                  range = np.array([np.log(gamma0),
                                                    np.log(gamma1)]),
                                  weights = data_kn['packets']['w'])    
    dndlg_kn = hist_kn/dlg

# Compute L1 error, omitting final bin where the exact solution goes
# to zero within the bin and thus we can't approximate the function as
# constant
err = np.abs(np.log10(dndlg[:-1] / dndlg_ex(np.exp(cen[:-1]))))
l1err = float(np.mean(err))

# Values of xi at which to evaluate local momentum distribution
xi_target = np.array([10.**-5, 10.**-4.5, 10.**-4, 10.**-3.5, 10.**-3])
xi_tol = 0.2

# Loop over values of xi
dndlgdx = []
if args.include_kn: dndlgdx_kn = []
l1err_loc = []
for i, xi_t in enumerate(xi_target):

    # Get limits on xi and find indices of packets within these limits
    xi_min = xi_t / (1 + xi_tol)
    xi_max = min(xi_t * (1 + xi_tol), data['t']/tloss)
    idx = np.logical_and(xi > xi_min, xi < xi_max)
    dx = vstr * tloss * (xi_max - xi_min)

    # Compute histogram of packets in log(gamma)
    hist, edge = np.histogram( np.array(np.log(gamma[idx])), bins=nbin,
                               range = np.array([np.log(gamma0),
                                                 np.log(gamma1)]),
                               weights = data['packets']['w'][idx])
    
    # Compute distribution function
    cen = 0.5 * (edge[1:] + edge[:-1])
    dlg = edge[1] - edge[0]
    dndlgdx.append(hist/dlg/dx)

    # Repeat for KN data
    if args.include_kn:
        idx_kn = np.logical_and(xi_kn > xi_min, xi_kn < xi_max)
        hist_kn, edge \
            = np.histogram( np.array(np.log(gamma_kn[idx_kn])), bins=nbin,
                            range = np.array([np.log(gamma0),
                                              np.log(gamma1)]),
                            weights = data_kn['packets']['w'][idx_kn])
        dndlgdx_kn.append(hist_kn/dlg/dx)

    # Compute L1 error, omitting final bin where the exact solution goes
    # to zero within the bin and thus we can't approximate the function as
    # constant
    gamma_max = gamma1 / (1 + xi_t * gamma1)
    idx = np.where(np.exp(cen) < gamma_max)
    err = np.abs(np.log10(dndlgdx[-1][idx] /
                          dndlgdx_ex(np.exp(cen[idx]), xi_t)))
    l1err_loc.append(float(np.mean(err)))

# Print test result
if args.verbose:
    print("L1 error (integrated) = " + str(l1err))
    print("L1 error (x bins) = " + str(l1err_loc))
if l1err < 0.05 and np.amax(l1err_loc) < 0.05:
    print("PASS")
else:
    print("FAIL")
    
# Plot if requested
if not args.testonly:

    # Set font
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    
    # Prepare plot
    plt.figure(1, figsize=(4,4.5))
    plt.clf()
    plt.subplot(2,1,1)

    # Plot integrated momentum distribution
    g = np.logspace(np.log10(float(gamma0)),
                    np.log10(float(gamma1)),
                    1000)
    plt.plot(g, g * dndlg_ex(g), 'C0', label='Exact')
    plt.plot(np.exp(cen), np.exp(cen) * dndlg, 'o', mfc='C0', mec='k',
             label='Sim')
    if args.include_kn:
        plt.plot(np.exp(cen), np.exp(cen) * dndlg_kn, 'o', mfc='none',
                 mec='C0', alpha=0.5, label='Sim-KN')
    
    # Add injection spectrum
    plt.plot(g, coef * const.m_e * const.c * data['t'] * g**(q+2),
             'k--', alpha=0.5,
             label='Injected')

    # Adjust plot and add labels
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim([20,2e6])
    plt.legend(prop={"size" : 10})
    plt.ylabel(r'$\gamma^2 (dn_\mathrm{CR}/d\gamma)$')
    plt.gca().set_xticklabels('')

    # Plot local momentum distributions
    plt.subplot(2,1,2)
    p = []
    lab = []
    for i, xi_t in enumerate(xi_target):
        plt.plot(np.exp(cen), np.exp(cen) * dndlgdx[i].to(1/u.cm),
                 'o', mfc='C{:d}'.format(i), mec='k',
                 label=r'${:4.1f}$'.format(np.log10(xi_t)))
        plt.plot(g, g * dndlgdx_ex(g, xi_t).to(1/u.cm),
                 'C{:d}'.format(i))
        if args.include_kn:
            plt.plot(np.exp(cen), np.exp(cen) * dndlgdx_kn[i].to(1/u.cm),
                     'o', mec='C{:d}'.format(i), mfc='none', alpha=0.5)
            
    
    # Add injection spectrum
    dndlgdx_inj = coef / vstr * const.m_e * const.c * g * g**q
    plt.plot(g, g * dndlgdx_inj.to(1/u.cm), 'k--', alpha=0.5)

    # Add legend
    leg = plt.legend(prop={"size" : 10},
                     title=r'$\log\,\xi$',
                     ncol=3, loc='upper right')
    plt.setp(leg.get_title(),fontsize=10)

    # Adjust plot and add labels
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim([20,2e6])
    plt.ylim([2e34,2e37])
    plt.ylabel(r'$\gamma^2 (d^2 n_\mathrm{CR}/d\gamma\,dx)$ [cm$^{-1}$]')
    plt.xlabel(r'$\gamma$')

    # Adjust overall spacing
    plt.subplots_adjust(left=0.18, top=0.95, right=0.95, hspace=0)

    # Save
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

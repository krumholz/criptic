// This file provides a problem setup for the electron streaming with
// losses test.

#include "ElectronStreamLossProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::ElectronStreamLossProb(pp, geom, rng);
}

// This file implements the problem setup for the AnisoDiffProb class,
// which sets up the anisotropic diffusion test problem. The problem
// setup is as follows:
// 1. One or more point sources of CRs are placed at the origin; the
//    sources are monochromatic (i.e., all the CRs they inject have
//    the same energy), and have specified total luminosities.
// 2. The background gas is uniform, and is characterised by a
//    constant magnetic field.
// 3. The CR propagation model is a simple energy-dependent powerlaw
//    in both the parallel and perpendicular directions, i.e., we have
//    kappa_par = kappa_par0 * (T/T0)^p, where T is the kinetic energy
//    of the CR, and kappa_par0, T0, and p are user-defined parameters
//    taken from the input deck. Perpendicular diffusion is defined in
//    an analogous way.
// 4. The are no CR packets present in the volume initially.

#include "AnisoDiffProb.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Units.H"

using namespace criptic;
using namespace std;

void
AnisoDiffProb::initSources(vector<RealVec>& x,
			   vector<CRSource>& sources) {

  // Only one MPI rank does this
  if (MPIUtil::IOProc) {
  
    // Get source luminosities and energies from input deck
    vector<Real> LSrc, TSrc;
    pp.get("prob.L", LSrc);
    pp.get("prob.T", TSrc);
    if (LSrc.size() != TSrc.size()) {
      MPIUtil::haltRun("Prob: inconsistent number of sources found"
		       " in input file!",
		       errBadInputFile);
    }

    // Create sources
    IdxType n = LSrc.size();
    x.resize(n);
    sources.resize(n);
    for (IdxType i=0; i<n; i++) {
      sources[i].type = partTypes::proton;
      sources[i].p0 = sources[i].p1 =
	sr::p_from_T(partTypes::proton,
		     TSrc[i] * units::GeV / constants::mp_c2);
      sources[i].setLum(LSrc[i]);
    }
  }
}

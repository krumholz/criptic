// This sets up the anisotropic diffusion test problem, by returning
// the problem generator class for it.

#include "AnisoDiffProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::AnisoDiffProb(pp, geom, rng);
}

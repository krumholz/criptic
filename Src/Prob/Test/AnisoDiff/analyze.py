"""
Analysis script for anisotropic diffusion test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import astropy.units as u
import astropy.constants as const
from astropy.units import Quantity
import os.path as osp
from scipy.special import erf
from cripticpy import readchk, readlastchk, heatscatter, parseinput, pctnbin

# Name of the problem this script is for
prob_name = "AnisoDiff"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()


# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
kPar0 = params['cr.kPar0']
kPerp0 = params['cr.kPerp0']
kParIdx = params['cr.kParIdx']
kPerpIdx = params['cr.kPerpIdx']
Lsrc = params['prob.L'] * u.erg/u.s
Tsrc = params['prob.T'] * u.GeV
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)

# Compute dimensionless radius for all packets
kRatio = kPar0 / kPerp0
r = (np.sqrt(data['packets']['x'][:,0]**2 / kRatio +
             data['packets']['x'][:,1]**2 +
             data['packets']['x'][:,2]**2)).to(u.cm)

# Sum packet energy in radial bins for each source, and normalise by
# bin volume to get energy density
U = []
Uup = []
Ulo = []
rbin = []
rc = []
for Ts in Tsrc:
    idx = np.abs((data['packets']['T'] - Ts) / Ts) < 1.0e-6
    w = data['packets']['w'][idx][0]
    T = data['packets']['T'][idx][0]
    count, edges = np.histogram(r[idx], bins=50)
    clo = []
    chi = []
    for c in count:
        chi.append(pctnbin(0.95, c))
        if c > 0:
            clo.append(pctnbin(0.05, c))
        else:
            clo.append(0.0)
    Tsum = count *  w * T.to(u.erg)
    Tup = np.array(chi) * w * T.to(u.erg)
    Tlo = np.array(clo) * w * T.to(u.erg)
    if type(edges) is not Quantity:
        edges = edges * u.cm   # Catch if astropy does not support np.histogram
    rbin.append(edges)
    rc.append(0.5 * (edges[1:] + edges[:-1]))
    U.append(Tsum / (4./3. * np.pi * np.sqrt(kRatio) *
                      (edges[1:]**3 - edges[:-1]**3)))
    Uup.append(Tup / (4./3. * np.pi * np.sqrt(kRatio) *
                      (edges[1:]**3 - edges[:-1]**3)))
    Ulo.append(Tlo / (4./3. * np.pi * np.sqrt(kRatio) *
                      (edges[1:]**3 - edges[:-1]**3)))

# Generate corresponding analytic solutions; note that the analytic
# solution below is the analytic integral for the mean energy density
# in a finite-sized bin
Uth = []
for L, T, r_ in zip(Lsrc, Tsrc, rbin):
    idx = np.abs((data['packets']['T'] - T) / T) < 1.0e-6
    p = data['packets']['p'][idx][0]    # Get momentum
    kappa = kPerp0 * (p / (const.m_p * const.c))**kPerpIdx
    r0 = r_[:-1]
    r1 = r_[1:]
    rk = np.sqrt(kappa * data['t'])
    x0 = r0 / (2. * rk)
    x1 = r1 / (2. * rk)
    U_ = 3 * L / \
         (8. * np.pi * np.sqrt(kRatio) * kappa * (r1**3 - r0**3)) * \
         (r1**2 - r0**2 -
          2.0 * rk / np.pi**0.5 * (
              r1 * np.exp(-x1**2) - r0 * np.exp(-x0**2)) -
          (r1**2 - 2*rk**2) * erf(x1) + (r0**2 - 2*rk**2) * erf(x0) )
    Uth.append(U_)

# Compute the L1 norm of the error and print result
l1err = []
relerr = []
for L_, U_, Uth_, rb_ in zip(Lsrc, U, Uth, rbin):
    l1err.append(
        np.sum(
            np.abs((U_ - Uth_) * 4./3. * np.sqrt(kRatio) * np.pi *
                   (rb_[1:]**3 - rb_[:-1]**3)))
        )
    relerr.append( l1err[-1] / (L*data['t']) )
if args.verbose:
    print("L1 errors at T = " + str(Tsrc) + " = " + str(np.array(relerr)))
if np.amax(relerr) < 2e-2:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    # Plot radial distribution
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    for i in range(len(Tsrc)):
        plt.plot(rc[i].to(u.pc), Uth[i].to(u.eV/u.cm**3),
                 'C{:d}'.format(i))
        idx = Ulo[i] == 0*u.eV/u.cm**3
        Utmp = U[i].to(u.eV/u.cm**3).value
        Uerr = np.zeros((2,Utmp.size))
        Uerr[0,:] = 1*(Utmp - Ulo[i].to(u.eV/u.cm**3).value)
        Uerr[1,:] = Uup[i].to(u.eV/u.cm**3).value - Utmp
        plt.errorbar(rc[i][::2].to(u.pc).value, Utmp[::2], yerr=Uerr[:,::2],
                     fmt='o', ecolor='C{:d}'.format(i),
                     mfc='C{:d}'.format(i),
                     mec='k',
                     label='{:d} GeV (sim, 90\% CI)'.format(
                         int(np.round(Tsrc[i].to(u.GeV) / u.GeV))))
    plt.plot([-1], [-1], 'k', label='Exact')
    plt.yscale('log')
    plt.legend(prop={"size" : 10})
    plt.xlabel(r"$r' = \sqrt{x^2/\chi + y^2 + z^2}$ [pc]")
    plt.ylabel(r'$U_{\mathrm{CR}}$ [eV cm$^{-3}$]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1."+args.imgformat))

    # Make projection plot at lowest energy
    idx = np.abs((data['packets']['T'] - Tsrc[0]) / Tsrc[0]) < 1.0e-6
    x = data['packets']['x'][idx]
    w = data['packets']['w'][idx]
    T = data['packets']['T'][idx]
    plt.figure(2, figsize=(4,3))
    plt.clf()
    heatscatter(x[:,0].to(u.pc), x[:,1].to(u.pc), w=w*T,
                xlim = np.array([-1,1])*20*u.pc,
                ylim = np.array([-1,1])*20*u.pc,
                vmin = 0.1*u.eV*u.pc/u.cm**3,
                vmax = 100*u.eV*u.pc/u.cm**3,
                nxbin = 80,
                nybin = 80)
    plt.xlabel(r'$x$ [pc]')
    plt.ylabel(r'$y$ [pc]')
    norm = colors.Normalize(vmin=-1, vmax=2)
    plt.colorbar(cm.ScalarMappable(norm=norm),
                 label=r'$\log \int U_\mathrm{CR}\, dz$ [eV cm$^{-3}$ pc]')
    plt.subplots_adjust(bottom=0.18, left=0.18, top=0.95)
    plt.savefig(osp.join(args.dir, prob_name+"2."+args.imgformat))

// This file implements the problem setup for the number density
// computation test problem.

#include "NdenCompProb.H"
#include "PropPowerlawForce.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Units.H"

using namespace criptic;
using namespace std;

// Initialize with custom CR propagation class
propagation::Propagation *
NdenCompProb::initProp() {
  return new propagation::PropPowerlawForce(pp);
}

// Initialize source at origin with properties specified in the input
// deck
void
NdenCompProb::initSources(vector<RealVec>& x,
			  vector<CRSource>& sources) {

  // Only one MPI rank does this
  if (MPIUtil::IOProc) {

    // Get source luminosity and momentum range
    Real L, p0, p1, q;
    pp.get("prob.L", L);
    pp.get("prob.p0", p0);
    pp.get("prob.p1", p1);
    pp.get("prob.q", q);

    // Create source
    x.push_back(zeroVec);
    sources.resize(1);
    sources[0].type = partTypes::proton;
    sources[0].p0 = (p0 * units::GeV / constants::c) / constants::mp_c;
    sources[0].p1 = (p1 * units::GeV / constants::c) / constants::mp_c;
    sources[0].q = q;
    sources[0].setLum(L);
  }
}

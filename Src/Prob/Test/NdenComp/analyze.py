"""
Analysis script for number density computation test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import os.path as osp
import astropy.units as u
from astropy.constants import m_p, c
from scipy.special import erfc
from cripticpy import readchk, readlastchk, parseinput

# Name of the problem this script is for
prob_name = "NdenComp"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
p0 = params['prob.p0'] * u.GeV/c
p1 = params['prob.p1'] * u.GeV/c
q = params['prob.q']
k = params['cr.kPar0']
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)

# Extract CR data we need
p = data['packets']['p']
w = np.copy(data['packets']['w'])
r = np.sqrt(data['packets']['x'][:,0]**2 +
            data['packets']['x'][:,1]**2 +
            data['packets']['x'][:,2]**2)
nden = data['packets']['nden']

# Extract CR production rate
ndot = data['sources']['ndot'][0]

# Get exact solutions
def nden_ex(r, p, t):
    fac = ((p1.to(u.kg * u.m/u.s)**(q+1) -
            p.to(u.kg * u.m/u.s)**(q+1)) / (p1**(q+1) - p0**(q+1))).to('') 
    return ndot * fac / (4 * np.pi * r * k) * \
        erfc(r / (2 * np.sqrt(k * t)))
ndenex = nden_ex(r, p, data['t'])

# Put packets in bins of exact number density
lnden_bins = np.linspace(-16,-6,41)
idx = np.digitize(np.log10(ndenex*u.cm**3), lnden_bins)
pct = [5,25,50,75,95]
lnden_pct = np.zeros((lnden_bins.size-1, len(pct)))
for i in range(lnden_bins.size-1):
    if np.sum(idx == i+1) > 0:
        lnden_pct[i,:] = np.percentile(np.log10(nden[idx == i+1]*u.cm**3),
                                       pct)
lnden_ctr = 0.5*(lnden_bins[1:] + lnden_bins[:-1])

# Put packets in bins of radius and momentum
rdiff = np.sqrt(k * data['t'])
rbfac = np.array([0.2, 0.5, 1, 2, 3.5])
rb = rbfac * rdiff
lnpbins = np.linspace(0,3,25)
ln_pct = np.zeros((rb.size, lnpbins.size-1, 5))
for i in range(rb.size):
    idx = np.abs(r/rb[i] - 1) < 0.05
    nd = nden[idx]
    ptmp = p[idx]
    idx1 = np.digitize(np.log10(ptmp*c/u.GeV), lnpbins)
    for j in range(lnpbins.size-1):
        nd1 = nd[idx1 == j+1]
        if nd1.size > 0:
            ln_pct[i,j,:] = \
                np.percentile(np.log10(nd1 * u.cm**3),
                              pct)
            
# Compute L1 error; defining the error is a bit tricky here; we define
# the error error as the integral of the absolute logarithmic difference
# between the exact and simulation-averaged values of log n from log n
# = -13 to -8, normalized by the size of the 50% CI interval averagd
# over this range. To pass, we also require that the mean width of the
# 50% CI not be larger than a threshold value
lnlim = [-13,-8]
idx = np.logical_and(lnden_ctr > lnlim[0], lnden_ctr < lnlim[1])
binsize = lnden_bins[1] - lnden_bins[0]
l1err = np.mean(np.abs(lnden_pct[idx,2] - lnden_ctr[idx])) * binsize
pctintegral = np.sum(lnden_pct[idx,3] - lnden_pct[idx,1]) * binsize
relerr = l1err / pctintegral
pctrange = pctintegral / (lnlim[1] - lnlim[0])
if args.verbose:
    print("L1 relative error = {:f}, mean 50% CI range = {:f} dex".
          format(relerr, pctrange))
if relerr < 0.01 and pctrange < 0.2:
    print("PASS")
else:
    print("FAIL")

# Make plots
if not args.testonly:
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)

    # First figure: spectra at different radii
    plt.figure(1, figsize=(4,3))
    plt.clf()
    lnpgrid = np.linspace(0,3,500)
    lnpctr = 0.5 * (lnpbins[1:] + lnpbins[:-1])
    handles = []
    labels = []
    for i in range(rb.size):
        line, = plt.plot(lnpctr,
                         ln_pct[i,:,2] + lnpctr, 'C{:d}'.format(i))
        handles.append(line)
        labels.append(r'${{{:4.1f}}}$'.format(rbfac[i]))
        plt.fill_between(lnpctr,
                         ln_pct[i,:,1] + lnpctr,
                         ln_pct[i,:,3] + lnpctr,
                         color='C{:d}'.format(i),
                         alpha=0.5)
        plt.fill_between(lnpctr,
                         ln_pct[i,:,0] + lnpctr,
                         ln_pct[i,:,1] + lnpctr,
                         color='C{:d}'.format(i),
                         alpha=0.25)
        plt.fill_between(lnpctr,
                         ln_pct[i,:,3] + lnpctr,
                         ln_pct[i,:,4] + lnpctr,
                         color='C{:d}'.format(i),
                         alpha=0.25)
        plt.plot(lnpgrid,
                 np.log10(10**lnpgrid *
                          (nden_ex(rb[i],
                                   10.**lnpgrid*u.GeV/c,
                                   data['t'])*u.cm**3) + 1e-100),
                 'C{:d}--'.format(i))
    plt.xlim([0,3])
    plt.ylim([-14.5,-4.5])
    leg = plt.legend(handles, labels, ncol=3, loc='lower left',
                     prop={"size" : 10}, title=r"$r/r_\mathrm{{diff}}$")
    plt.plot([0,0], [0,0], 'k--', label='Exact')
    plt.plot([0,0], [0,0], 'k', label='Sim (median)')
    plt.fill_between([0,0], [0,0], [0,0], color='k', alpha=0.5,
                     label='Sim (50\% CI)')
    plt.fill_between([0,0], [0,0], [0,0], color='k', alpha=0.25,
                     label='Sim (90\% CI)')
    plt.legend(loc='upper right', ncol=2, prop={"size" : 10})
    plt.gca().add_artist(leg)
    plt.xlabel(r'$\log\,p$ [GeV/$c$]')
    plt.ylabel(r'$\log\,p\, n_\mathrm{CR}(>p)$ [cm$^{-3}$ GeV/$c$]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

    # Second figure: exact verus approximate number densities
    plt.figure(2, figsize=(4,3))
    plt.clf()
    plt.plot(lnden_ctr, lnden_ctr, 'k--', label='1 : 1')
    plt.plot(lnden_ctr, lnden_pct[:,2], 'C0', label='Median')
    plt.fill_between(lnden_ctr, lnden_pct[:,1], lnden_pct[:,2],
                     color='C0', alpha=0.5,
                     label="50\% CI")
    plt.fill_between(lnden_ctr, lnden_pct[:,2], lnden_pct[:,3],
                     color='C0', alpha=0.5)
    plt.fill_between(lnden_ctr, lnden_pct[:,0], lnden_pct[:,1],
                     color='C1', alpha=0.5,
                     label="90\% CI")
    plt.fill_between(lnden_ctr, lnden_pct[:,3], lnden_pct[:,4],
                     color='C1', alpha=0.5)
    plt.xlim([lnden_bins[0], lnden_bins[-1]])
    plt.ylim([lnden_bins[0], lnden_bins[-1]])
    plt.legend(prop={"size" : 10})
    plt.xlabel(r'$\log\, n_\mathrm{CR}(>p)$ exact [cm$^{-3}$]')
    plt.ylabel(r'$\log\, n_\mathrm{CR}(>p)$ sim [cm$^{-3}$]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"2." + args.imgformat))


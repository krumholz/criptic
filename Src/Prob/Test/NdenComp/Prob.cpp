// This sets up the number density computation test problem, by
// returning the problem generator class for it.

#include "NdenCompProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::NdenCompProb(pp, geom, rng);
}

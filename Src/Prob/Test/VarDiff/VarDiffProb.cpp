// This file provides a problem setup for the time- and
// spatially-variable diffusion test problem. The problem setup is as
// follows:
// 1. The background gas is uniform, and is characterised by a
//    constant magnetic field.
// 2. The CR propagation model is a custom model in which the
//    diffusion coefficient is a powerlaw function of space and time,
//    of the form kappa = kappa0 (r/r0)^p (t/t0)^q; diffusion is
//    isotropic, i.e., k_Par = k_Perp.
// 3. The are no CR sources in the volume.
// 4. CR packets are initially distributed following a similarity
//    solution,
//    U(r,t) \propto [3 / 4 pi Gamma( (p-5)/(p-2) )] *
//                   (2-p)^[ 6/(p-2) ] *
//                   [ (q+1) / (kappa0 t0) ]^[ 3/(2-p) ] *
//                   r0^[ 3p/(2-p) ] *
//                   (t/t0)^[ 3 (q+1)/(p-2) ] *
//                   exp[ -(q+1) / (p-2)^2 * r0^2 / (kappa0 t0) *
//                         (r/r0)^(2-p) (t/t0)^(-q-1) ],
//    where the constant of proportionality is set by the total energy
//    of CR packets in the volume, and the packets are initially
//    distributed in radius following the solution evaluated for t = t0.
//
// The test is to verify that the packets continue to follow the
// similarity solution at later times.

#include "PropVarDiff.H"
#include "VarDiffProb.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Constants.H"
#include "../../../Utils/PartTypes.H"
#include "../../../Utils/SR.H"
#include "../../../Utils/Units.H"
#include <gsl/gsl_roots.h>
#include <gsl/gsl_sf.h>

using namespace criptic;
using namespace std;

// Helper function used to compute intial packet radii; see
// initPackets for explanation of what this function does
inline Real SQR(Real x) { return x*x; }
typedef struct gsl_pars_type {
  Real r0; Real t0; Real kappa0; Real p; Real q; Real f;
} gsl_pars;
Real gsl_func(Real r_over_r0, void *pars) {
  gsl_pars *gpars = (gsl_pars *) pars;
  const Real r0 = gpars->r0;
  const Real t0 = gpars->t0;
  const Real kappa0 = gpars->kappa0;
  const Real p = gpars->p;
  const Real q = gpars->q;
  const Real f = gpars->f;
  const Real y = (q + 1.0) / SQR(p-2.0) *
    SQR(r0) / (kappa0 * t0) * pow( r_over_r0, 2-p );
  return 1 + 3 * gsl_sf_gamma_inc(-3.0/(p-2.0), y) /
    ((p-2) * gsl_sf_gamma( (p-5)/(p-2) )) - f;
}  

// Set up the CR propagation model; this test uses a custom model
propagation::Propagation *
VarDiffProb::initProp() {
  return new propagation::PropVarDiff(pp);
}

// Set up the initial packet distribution, following the similarity
// solution. This cannot be done analytically for general values of p
// and q, but we can exploit the result that, at t = t0, the fraction
// of packets at radius < r can be calculated analytically. It is
//    f(<r) = 1 + 3 Gamma[ 3/(2-p), y ] / [ (p-2) Gamma[ (p-5)/(p-2) ]
// where
//    y = [ (q+1)/(p-2)^2 ] [r0^2 / (kappa0 t0)] (r/r0)^(2-p).
// We can therefore draw a value f from a uniform distribution and
// numerically invert the equation f(<r) = f to get r values
// distributed as we want. The routine implements this strategy.
void
VarDiffProb::initPackets(vector<RealVec>& x,
			 vector<CRPacket>& packets) {
  // Only create packets on the IO processor; they will be
  // redistributed to other processors automatically
  if (MPIUtil::IOProc) {

    // Read problem parameters
    Real kappa0, r0, t0, p, q, eCR, eCRTot;
    int np;
    pp.get("prob.n_packet", np);
    pp.get("cr.kappa0", kappa0);
    pp.get("cr.r0", r0);
    pp.get("cr.t0", t0);
    pp.get("cr.kappa_r_idx", p);
    pp.get("cr.kappa_t_idx", q);
    pp.get("prob.e_cr", eCR);
    pp.get("prob.e_cr_tot", eCRTot);

    // Safety check on parameters
    if (t0 <= 0 || kappa0 <= 0 || r0 <= 0 || p >= 2 || q <= -1) {
      MPIUtil::haltRun("Prob: variable diffusion test requires "
		       "kappa0 > 0, r0 > 0, t0 > 0, kappa_r_idx < 2, "
		       "kappa_t_idx > -1",
		       errBadInputFile);
    }

    // Resize arrays to correct size
    x.resize(np);
    packets.resize(np);

    // Start parallel region
#ifdef _OPENMP
#pragma omp parallel
#endif
    {

      // Every thread sets up its own GSL infrastructure
      gsl_function F;
      gsl_pars pars = { r0, t0, kappa0, p, q, 0.0 };
      F.function = &gsl_func;
      F.params = &pars;
      gsl_root_fsolver *s = gsl_root_fsolver_alloc(gsl_root_fsolver_brent);
      
      // Loop over packets
#ifdef _OPENMP
#pragma omp for
#endif
      for (int i=0; i<np; i++) {

	// Generate random deviate and store in GSL structure
	pars.f = rng.uniform();

	// Numerically solve to get r
	gsl_root_fsolver_set(s, &F, 0.0, 1000.0);
	Real r = 0.0;
	int iter, status;
	const int maxiter = 1000;
	for (iter = 0, status = GSL_CONTINUE;
	     iter < maxiter && status == GSL_CONTINUE;
	     iter++) {
	  status = gsl_root_fsolver_iterate(s);
	  Real ylo = gsl_root_fsolver_x_lower(s);
	  Real yhi = gsl_root_fsolver_x_upper(s);
	  status = gsl_root_test_interval(ylo, yhi, 1e-6, 1e-6);
	  if (status == GSL_SUCCESS) r = r0 * gsl_root_fsolver_root(s);
	}
	if (status != GSL_SUCCESS || iter == maxiter)
	  MPIUtil::haltRun("initPackets: unable to converge packet "
			   "radius calculation!", errUnspecified);

	// Pick mu and phi
	Real mu = rng.uniform(-1,1);
	Real phi = rng.uniform(0, 2*M_PI);

	// Set x, y, z coordinates
	x[i][0] = r * sqrt(1-SQR(mu)) * cos(phi);
	x[i][1] = r * sqrt(1-SQR(mu)) * sin(phi);
	x[i][2] = r * mu;

	// Set packet properties
	packets[i].type = partTypes::proton;
	packets[i].src = -1;
	packets[i].tInj = 0.0;
	packets[i].p = sr::p_from_T(packets[i].type,
				    eCR * units::GeV / constants::mp_c2);
	packets[i].w = packets[i].wInj = eCRTot / (eCR * units::GeV * np);
	packets[i].gr = 0.0;
      }

      // Free memory
      gsl_root_fsolver_free(s);
    }
  }
}

// Implementation of the PropVarDiff class

#include "PropVarDiff.H"
#include <cmath>

using namespace criptic;
using namespace criptic::propagation;

// Constructor
PropVarDiff::PropVarDiff(const ParmParser& pp) {

  // Read input file parameters
  pp.get("cr.kappa0", kappa0);
  pp.get("cr.r0", r0);
  pp.get("cr.t0", t0);
  pp.get("cr.kappa_r_idx", p);
  pp.get("cr.kappa_t_idx", q);

}

// Propagation coefficient calculation
inline PropagationData
PropVarDiff::operator()(const RealVec& x,
			const Real t,
			const gas::GasData& gd,
			const CRPacket& packet,
			const FieldQty& qty,
			const FieldQtyGrad& qtyGrad) const {

  // Fill propagation data; remember that simulation starts at time t0
  // at t = 0, so time that enters the calculation here is simulation
  // time + t0
  PropagationData pd;
  pd.kPar = kappa0 * std::pow(x.mag()/r0, p) * std::pow(t/t0 + 1, q);
  pd.kPerp = pd.kPar;
  pd.kPP = 0.0;
  pd.vStr = 0.0;
  pd.kParGrad = p * pd.kPar * x/x.mag2(); // grad(kappa) = p kappa r / |r|^2
  pd.kPerpGrad = pd.kParGrad;
  pd.vStrGrad = 0.0;
  pd.dvStr_dp = 0.0;
  pd.dkPP_dp = 0.0;

  // Return
  return pd;
}

			      

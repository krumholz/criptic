"""
Analysis script for variable diffusion test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import astropy.constants as const
from astropy.units import Quantity
import os.path as osp
from scipy.special import gamma, gammaincc
from scipy.optimize import root_scalar
from cripticpy import readchk, readlastchk, pctnbin, parseinput

# Name of the problem this script is for
prob_name = "VarDiff"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
kappa0 = params['cr.kappa0'] * u.cm**2 / u.s
t0 = params['cr.t0'] * u.s
r0 = params['cr.r0'] * u.cm
p = params['cr.kappa_r_idx']
q = params['cr.kappa_t_idx']
etot = params['prob.e_cr_tot'] * u.erg
eta = r0**2 / (kappa0 * t0)
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read starting and final checkpoints
data0 = readchk(osp.join(args.dir, chkname+'00000.hdf5'))
if args.chk >= 0:
    data1 = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data1 = readlastchk(args.dir, chkname)

# Get packet radii
rad0 = np.sqrt(data0['packets']['x'][:,0]**2 +
               data0['packets']['x'][:,1]**2 +
               data0['packets']['x'][:,2]**2)
rad1 = np.sqrt(data1['packets']['x'][:,0]**2 +
               data1['packets']['x'][:,1]**2 +
               data1['packets']['x'][:,2]**2)
                                              
# Get packet energy density in radial bins, along with confidence intervals
# t = 0 snapshot; note that we have to be a little careful here with
# units, because astropy units only started working correctly with
# numpy histogram in a fairly recent version, and we therefore want to
# handle the case where it doesn't.
c0, e0 = np.histogram(rad0, bins=30)
if not (type(e0) is Quantity):
    e0 = e0 * rad0.unit
clo0 = np.array([ pctnbin(0.05, c) for c in c0 ])
chi0 = np.array([ pctnbin(0.95, c) for c in c0 ])
clim0 = np.stack((clo0, chi0))
T0 = c0 * data0['packets']['w'][0] * data0['packets']['T'][0]
Tlim0 = clim0 * data0['packets']['w'][0] * data0['packets']['T'][0]
rc0 = 0.5 * (e0[1:] + e0[:-1])
V0 = 4./3. * np.pi * (e0[1:]**3 - e0[:-1]**3)
U0 = T0 / V0
Ulim0 = Tlim0 / V0
Uerr0 = np.abs(Ulim0 - U0)

# final snapshot
c1, e1 = np.histogram(rad1, bins=30)
if not (type(e1) is Quantity):
    e1 = e1 * rad0.unit
clo1 = np.array([ pctnbin(0.05, c) for c in c1 ])
chi1 = np.array([ pctnbin(0.95, c) for c in c1 ])
clim1 = np.stack((clo1, chi1))
T1 = c1 * data1['packets']['w'][0] * data1['packets']['T'][0]
Tlim1 = clim1 * data1['packets']['w'][0] * data1['packets']['T'][0]
rc1 = 0.5 * (e1[1:] + e1[:-1])
V1 = 4./3. * np.pi * (e1[1:]**3 - e1[:-1]**3)
U1 = T1 / V1
Ulim1 = Tlim1 / V1
Uerr1 = np.abs(Ulim1 - U1)

# Exact solution in bins
def gammau(a, z):
    # Upper inconcomplete gamma function
    return gamma(a) * gammaincc(a, z)
def Uex(r, t, bin=True):
    if bin:
        V = 4./3. * np.pi * (r[1:]**3 - r[:-1]**3)
        norm = etot / V
        f = (q+1)/(p-2)**2 * eta * (1+t/t0)**(-q-1)
        return norm * (
            gammau( 3/(2-p), (f * (r[:-1]/r0)**(2-p)).to('').value ) -
            gammau( 3/(2-p), (f * (r[1:]/r0)**(2-p)).to('').value ) ) / \
            gamma( 3/(2-p) )
    else:
        return 3 / ( 4*np.pi*gamma((p-5)/(p-2)) ) * (2-p)**(6/(p-2)) * \
            ((q+1)*eta)**(3/(2-p)) * r0**-3 * (1+t/t0)**(3*(q+1)/(p-2)) * \
            np.exp( -(q+1)*eta / (p-2)**2 * (r/r0)**(2-p) * (1+t/t0)**(-q-1) )

# Generate exact solutions at time 0 and final time
Uex0 = Uex(e0, data0['t'])
Uex1 = Uex(e1, data1['t'])

# Compute L1 norm of error
l1err = np.sum(np.abs(Uex1 - U1) * 4/3 * np.pi * (e1[1:]**3 - e1[:-1]**3)) \
    / etot

# Determine pass / fail
if args.verbose:
    print("L1 error = " + str(float(l1err)))
if l1err < 0.02:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    plt.errorbar(rc0.to(u.pc).value, U0.to(u.eV/u.cm**3).value,
                 yerr=Uerr0.to(u.eV/u.cm**3).value,
                 fmt='o', ecolor='C0', mfc='C0', mec='k',
                 label='Initial (sim, 90\% CI)')
    plt.plot(rc0.to(u.pc), Uex(e0, data0['t']).to(u.eV/u.cm**3), 'C0')
    plt.errorbar(rc1.to(u.pc).value, U1.to(u.eV/u.cm**3).value,
                 yerr=Uerr1.to(u.eV/u.cm**3).value,
                 fmt='o', ecolor='C1', mfc='C1', mec='k',
                 label='Final (sim, 90\% CI)')
    plt.plot(rc1.to(u.pc), Uex(e1, data1['t']).to(u.eV/u.cm**3), 'C1')
    plt.xlim([0,np.ceil(rc1[-1].to(u.pc).value)])
    plt.ylim([1e-4, 2e4])
    plt.yscale('log')
    plt.plot([-1], [-1], 'k', label='Exact')
    plt.legend(prop={"size" : 10})
    plt.xlabel(r'$r$ [pc]')
    plt.ylabel(r'$U_{\mathrm{CR}}$ [eV cm$^{-3}$]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

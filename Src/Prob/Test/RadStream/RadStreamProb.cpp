// This file provides a problem setup for the radial streaming
// problem. The problem consists of a single source placed at the
// origin in a region with a split-monopole magnetic field. The source
// emits CRs at a constant rate, which stream outward at the ion
// Alfven speed. For a magnetic field of magnitude B_0 at radius r_0,
// and a fluid with total density rho_0 at radius r_0 that varies with
// radius as r^k, and constant ionization fraction chi, the ion Alfven
// speed at radius r is given by
// v_Ai = v_Ai0 (r / r_0)^(-2 - k/2),
// where
// v_Ai0 = B_0 / sqrt(4 pi chi rho_0) (in CGS units)
//       = B_0 / sqrt(mu_0 chi rho_0) (in MKS units)
// If the CR injection rate from the source is Ndot, and CR flux is
// conserved, then the CR number density at radius r should be
//    n = Ndot / (4 pi r^2 v_Ai)
//      = Ndot / (4 pi r0^2 v_Ai0) (r / r0)^(-k/2).
// Individual CR packet positions evolve as dr/dt = v_str, which gives
//    r = r0 * [ (3 + k/2) v0 t / r0 ]^[ 1/(3 + k/2) ]
// (for r >> initial radius). In addition, the CR packets should lose
// momentum due to the evolution of the streaming speed; specifically,
// we should have
//    dp/dt = -(p/3) div v_str ==> dp/dr = -p/3 (div v_str) / v_str.
// For v_str = v_Ai as above, we have
//    p = p_i (r/r_i)^(k/6),
// where pi is the initial momentum and r_i is the initial radius. The
// test is to verify that we recover these analytic results.

#include "RadStreamProb.H"
#include "../../../Gas/SplitMonopole.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Constants.H"
#include "../../../Utils/PartTypes.H"
#include "../../../Utils/SR.H"
#include "../../../Utils/Units.H"

using namespace criptic;
using namespace std;

// Set up the background gas -- in this case just by initializing a
// SplitMonopole object and returning it
gas::Gas*
RadStreamProb::initGas() {
  return new gas::SplitMonopole(pp);
}

void
RadStreamProb::initSources(vector<RealVec>& x,
			   vector<CRSource>& sources) {

  // Read from input file
  Real LSrc, TSrc;
  pp.get("prob.L", LSrc);
  pp.get("prob.T", TSrc);

  // Set source position
  x.resize(1);
  x[0] = zeroVec;

  // Set source properties
  sources.resize(1);
  sources[0].type = partTypes::proton;
  sources[0].p0 = sources[0].p1 =
    sr::p_from_T(partTypes::proton,
		 TSrc * units::GeV / constants::mp_c2);
  sources[0].setLum(LSrc);
}


// This sets up the radial streaming test problem, by returning
// the problem generator class for it.

#include "RadStreamProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::RadStreamProb(pp, geom, rng);
}

"""
Analysis script for radial streaming test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
from astropy.constants import c
import os.path as osp
from scipy.stats import binned_statistic
from cripticpy import readchk, readlastchk, parseinput, pT

# Name of the problem this script is for
prob_name = "RadStream"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Parse the input file and extract quantities we need; note that the
# calculation of the Alfven speed is brute-forced as far as units,
# because at the time of this writing, astropy does not properly
# handle Gaussian electromagnetic units
params = parseinput(osp.join(args.dir, 'criptic.in'))
params['prob.L'] = params['prob.L'] * u.erg/u.s
params['prob.T'] = params['prob.T'] * u.GeV
p0 = pT(params['prob.T'], 'proton')
r0 = (params['gas.r0'].to(u.cm)).value
B0 = (params['gas.B0'].to(u.G)).value
rho0 = (params['gas.rho0'].to(u.g/u.cm**3)).value
vAi0 = B0 / np.sqrt(4 * np.pi * rho0) * u.cm/u.s
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)

# Extract packet properties of interest
age = data['t'] - data['packets']['tinj']
r = np.sqrt(data['packets']['x'][:,0]**2 +
            data['packets']['x'][:,1]**2 +
            data['packets']['x'][:,2]**2)
p = data['packets']['p']

# Functions that give the exact solutions
def rex(t):
    r0 = params['gas.r0']
    k = params['gas.krho']
    q = 3 + k/2.0
    return r0 * ( q * vAi0 * t / r0 ) ** (1/q)
def pex(r):
    ri = params['tree.source_dither']
    k = params['gas.krho']
    return p0 * ( r / ri ) ** (k/6.0)

# Get prediction for exact radius and momentum
r0 = params['gas.r0']
k = params['gas.krho']
q = 3 + k/2.0
ri = params['tree.source_dither']
rexact = rex(age)
pexact = pex(rexact)

# Compute mean and standard deviation of the error
rerr = r/rexact - 1
rerr_mean = np.mean(rerr)
rerr_std = np.std(rerr)
perr = p/pexact - 1
perr_mean = np.mean(perr)
perr_std = np.std(perr)

# Print outcome; note we allow slightly larger values for the standard
# deviation in the error, to account for the fact that packets start
# at a range of radii due to the initial dither
if args.verbose:
    print("Radial position error = " + str(rerr_mean) +
          " (mean), " + str(rerr_std) + " (variance); momentum error = " +
          str(perr_mean) + " (mean), " + str(perr_std) + " (variance)")
if rerr_mean < 0.03 and perr_mean < 0.03 and \
   rerr_std < 0.2 and perr_std < 0.2:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:

    # Get bins of radius versus age
    rmean, tedge, idx \
        = binned_statistic(age.to(u.yr).value,
                           r.to(u.pc).value,
                           statistic='mean',
                           bins=50, range=[0, data['t'].to(u.yr).value])
    rstd, tedge, idx \
        = binned_statistic(age.to(u.yr).value,
                           r.to(u.pc).value,
                           statistic='std',
                           bins=50, range=[0, data['t'].to(u.yr).value])
    tc = 0.5 * (tedge[1:] + tedge[:-1])

    # Get bins of momentum versus radius, excluding inside flat region
    keep = r > params['gas.rflat']
    pmean, redge, idx \
        = binned_statistic(r[keep].to(u.pc).value,
                           (p[keep] / (u.GeV/c)).to(''),
                           statistic='mean',
                           bins=50,
                           range=[params['gas.rflat'].to(u.pc).value,
                                  rex(data['t']).to(u.pc).value])
    pstd, redge, idx \
        = binned_statistic(r[keep].to(u.pc).value,
                           (p[keep] / (u.GeV/c)).to(''),
                           statistic='std',
                           bins=50,
                           range=[params['gas.rflat'].to(u.pc).value,
                                  rex(data['t']).to(u.pc).value])
    rc = 0.5 * (redge[1:] + redge[:-1])

    # Plot radial positions versus exact
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    plt.errorbar(tc, rmean, yerr=rstd,
                 fmt='o', mec='k', mfc='C0',
                 label='Sim (90\% CI)')
    tgrid = np.linspace(0*u.yr, data['t'].to(u.yr), 500)
    plt.plot(tgrid, rex(tgrid).to(u.pc), color='k',
             label='Exact')
    plt.legend(prop={"size":10})
    plt.xlabel(r'$t_\mathrm{packet}$ [yr]')
    plt.ylabel(r'$r$ [pc]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

    # Plot momentum versus exact
    plt.figure(2, figsize=(4,3))
    plt.clf()
    plt.errorbar(rc, pmean, yerr=pstd,
                 fmt='o', mec='k', mfc='C0',
                 label='Sim (90\% CI)')
    rgrid = np.linspace(params['gas.rflat'].to(u.pc),
                        rex(data['t']).to(u.pc), 500)
    plt.plot(rgrid, (pex(rgrid) / (u.GeV/c)).to(''), color='k',
             label='Exact')
    plt.legend(prop={"size":10})
    plt.xlabel(r'$r$ [pc]')
    plt.ylabel(r'$p$ [GeV $c^{-1}$]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"2." + args.imgformat))

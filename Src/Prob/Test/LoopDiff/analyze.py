"""
Analysis script for loop diffusion test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import astropy.units as u
import astropy.constants as const
from astropy.units import Quantity
import os.path as osp
from cripticpy import readchk, readlastchk, heatscatter, parseinput, pctnbin

# Name of the problem this script is for
prob_name = "LoopDiff"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
kPar0 = params['cr.kPar0']
L = params['prob.L'] * u.erg/u.s
r0 = params['prob.r'] * u.cm
freqFac = params['gas.freq']
freq = freqFac * kPar0 / r0**2 
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)

# Get packet positions expressed in radial coordinates, centered on
# the center of the field loop at this time
xc = r0 * np.array([np.sin(2 * np.pi * u.rad * freq * data['t']), 0, 0])
x = data['packets']['x'] - xc
r = np.sqrt(x[:,0]**2 + x[:,1]**2)
theta = np.arccos(x[:,0] / r)
theta[x[:,1] < 0] = -theta[x[:,1] < 0]

# Bin packets in angle and get energy per unit angle
count, edges = np.histogram(theta.to(u.rad).value, bins=49,
                            range=[-np.pi, np.pi])
centers = 0.5 * (edges[1:] + edges[:-1])
clo = []
chi = []
for c in count:
    chi.append(pctnbin(0.95, c))
    if c > 0:
        clo.append(pctnbin(0.05, c))
    else:
        clo.append(0.0)
chi = np.array(chi)
clo = np.array(clo)
fac = data['packets']['w'][0] * data['packets']['T'][0].to(u.erg)
dtheta = edges[1] - edges[0]
U = count * fac / dtheta
Uup = chi * fac / dtheta
Ulo = clo * fac / dtheta
Uerr = np.zeros((2,U.size)) * u.erg
Uerr[0,:] = U - Ulo
Uerr[1,:] = Uup - U

# Compute exact solution
def Uex(t, theta, nmax=100):
    n = np.arange(1, nmax)
    fac = kPar0 * data['t'] / r0**2
    terms = np.cos(np.outer(theta, n)) / n**2 * \
        (np.exp(-n**2 * fac) - 1)
    termsum = np.sum(terms, axis=1)
    return L / np.pi * (t/2 - r0**2 / kPar0 * termsum)
Uex_ = Uex(data['t'], centers)

# Compute L1 error and determine if test passes
l1err = np.sum(dtheta * np.abs(U - Uex_)) / (L * data['t'])
if args.verbose:
    print("L1 error = " + str(float(l1err)) +
          ", scatter in radial distance = " +
          str(float(np.std(r)/r0)))
if l1err < 0.01 and np.std(r) / r0 < 1e-4:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    plt.plot(centers,
             Uex_ / (L*data['t']), color='k', label='Exact')
    plt.errorbar(centers,
                 U / (L*data['t']),
                 yerr=Uerr / (L*data['t']), fmt='o', ecolor='C0',
                 mfc='C0', mec='k', label='Sim (90\% CI)')
    plt.legend(prop={"size" : 10})
    plt.xlabel(r'$\phi$ [rad]')
    plt.ylabel(r'$(\mathcal{L}t)^{-1} \, dE_\mathrm{CR}/d\phi$'
               ' [rad$^{-1}$]')
    plt.xlim([-np.pi,np.pi])
    plt.ylim([0,0.3])
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

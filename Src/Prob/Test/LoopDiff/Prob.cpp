// This sets up the loop diffusion diffusion test problem, by returning
// the problem generator class for it.

#include "LoopDiffProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::LoopDiffProb(pp, geom, rng);
}


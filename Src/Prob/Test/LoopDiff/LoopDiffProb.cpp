// This file implements the problem setup for the loop diffusion test.

#include "LoopDiffGas.H"
#include "LoopDiffProb.H"

using namespace criptic;
using namespace std;

// Set up the background gas; this test uses a custom gas claass
gas::Gas* LoopDiffProb::initGas() {
  return new gas::LoopDiffGas(pp);
}

void LoopDiffProb::initSources(vector<RealVec>& x,
			       vector<CRSource>& sources) {

  // Only one MPI rank does this
  if (MPIUtil::IOProc) {
  
    // Get source luminosity and position
    Real L, r;
    pp.get("prob.L", L);
    pp.get("prob.r", r);

    // Get oscillation frequency and diffusion coefficient -- needed
    // to set initial source velocity
    Real freqFac, kPar0;
    pp.get("gas.freq", freqFac);
    pp.get("cr.kPar0", kPar0);
    Real freq = 2 * M_PI * freqFac * kPar0 / (r * r);

    // Create source
    x.push_back( RealVec(r, 0, 0) );
    sources.resize(1);
    sources[0].type = partTypes::proton;
    sources[0].p0 = sources[0].p1 = 1.0;
    sources[0].setLum(L);
    sources[0].v = RealVec( r * freq, 0, 0 );
    sources[0].a = 0.0;
  }
}

void LoopDiffProb::userWork(const Real t,
			    Real& dt,
			    gas::Gas& gasBG,
			    propagation::Propagation& prop,
			    CRTree& tree) {
  gas::LoopDiffGas& g = dynamic_cast<gas::LoopDiffGas&>(gasBG);
  tree.getSourcePos(0) = RealVec( g.amp, 0, 0 ) + g.xc(t);
  tree.getSourceData(0).v = g.vc(t);
  tree.getSourceData(0).a = g.ac(t);
}

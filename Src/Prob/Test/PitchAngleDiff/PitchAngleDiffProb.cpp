// This file provides a problem setup for the pitch angle diffusion
// test. The problem consists of one or more sources that inject
// packets with different initial momenta; the injected packets all
// have pitch angles mu = 1. The pitch angle distribution then evolves
// by diffusion. The exact solution is that, for a source with
// luminosity L after a time T, the pitch angle distribution is
//
// dE/dmu = L T / 2 +
//          L / k * sum_{l = 1}^{infty} (2l + 1) / (2 l (l+1)) P_l(mu)
//                              [1 - exp(-k l (l+1) T)]
//
// where k is the diffusion coefficient and P_l is the Legendre
// polynomial of order l.

#include "PitchAngleDiffProb.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Units.H"

using namespace criptic;
using namespace std;

void
PitchAngleDiffProb::initSources(vector<RealVec>& x,
				vector<CRSource>& sources) {

  // Only one MPI rank does this
  if (MPIUtil::IOProc) {
  
    // Get source luminosities and energies from input deck
    vector<Real> LSrc, TSrc;
    pp.get("prob.L", LSrc);
    pp.get("prob.T", TSrc);
    if (LSrc.size() != TSrc.size()) {
      MPIUtil::haltRun("Prob: inconsistent number of sources found"
		       " in input file!",
		       errBadInputFile);
    }

    // Create sources
    IdxType n = LSrc.size();
    x.resize(n);
    sources.resize(n);
    for (IdxType i=0; i<n; i++) {
      sources[i].type = partTypes::proton;
      sources[i].p0 = sources[i].p1 =
	sr::p_from_T(partTypes::proton,
		     TSrc[i] * units::GeV / constants::mp_c2);
      sources[i].mu0 = 1;
      sources[i].mu1 = 1;
      sources[i].setLum(LSrc[i]);
    }
  }
}

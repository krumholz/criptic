"""
Analysis script for pitch angle diffusion test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import os.path as osp
import astropy.units as u
import astropy.constants as const
from cripticpy import readchk, readlastchk, parseinput, pctnbin

# Name of the problem this script is for
prob_name = "PitchAngleDiff"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()


# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
kMu0 = params['cr.kMu0']
kMuIdx = params['cr.kMuIdx']
Lsrc = params['prob.L'] * u.erg/u.s
Tsrc = params['prob.T'] * u.GeV
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)

# Get energy density distribution dE/dmu for each packet energy
dEdmu = []
dEdmulo = []
dEdmuhi = []
for Ts in Tsrc:
    idx = np.abs((data['packets']['T'] - Ts) / Ts) < 1.0e-6
    w = data['packets']['w'][idx][0]
    T = data['packets']['T'][idx][0]
    count, edges = np.histogram(data['packets']['mu'][idx], bins=30,
                                range=[-1,1])
    muc = 0.5 * (edges[1:] + edges[:-1])
    dmu = edges[1] - edges[0]
    clo = []
    chi = []
    for c in count:
        chi.append(pctnbin(0.95, c))
        if c > 0:
            clo.append(pctnbin(0.05, c))
        else:
            clo.append(0.0)
    chi = np.array(chi)
    clo = np.array(clo)
    dEdmu.append( count * w * T.to(u.erg) / dmu)
    dEdmulo.append( clo * w * T.to(u.erg) / dmu)
    dEdmuhi.append( chi * w * T.to(u.erg) / dmu)
    
# Compute exact solutions
dEdmu_th = []
for L, T in zip(Lsrc, Tsrc):
    idx = np.abs((data['packets']['T'] - T) / T) < 1.0e-6
    p = data['packets']['p'][idx][0]    # Get momentum
    kMu = kMu0 * (p / (const.m_p * const.c))**kMuIdx
    l = np.arange(1,200)
    coef = np.append([0],
                     (2*l + 1) / (2 * l * (l+1)) * \
                     (1 - np.exp(-kMu * data['t'] * l * (l+1))))
    dEdmu_th.append( L * data['t'] / 2 +
                     L / kMu * np.polynomial.legendre.legval(muc, coef) )

# Compute the L1 norm of the error and print result
relerr = []
for L, dEdmu_, dEdmu_th_ in zip(Lsrc, dEdmu, dEdmu_th):
    relerr.append(np.sum( dEdmu_ - dEdmu_th_ ) * dmu / (L * data['t']))
if args.verbose:
    print("L1 errors at T = " + str(Tsrc) + " = " + str(np.array(relerr)))
if np.amax(relerr) < 2e-2:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    for i in range(len(Lsrc)):
        plt.plot(muc, dEdmu_th[i] / (L * data['t']), 'C'+str(i))
        dEdmu_err = np.zeros((2,dEdmu[i].size)) * u.erg
        dEdmu_err[0,:] = dEdmu[i] - dEdmulo[i]
        dEdmu_err[1,:] = dEdmuhi[i] - dEdmu[i]
        plt.errorbar(muc, dEdmu[i] / (L * data['t']),
                     yerr = dEdmu_err / (L * data['t']), fmt='o',
                     ecolor='C'+str(i),
                     mfc='C'+str(i),
                     mec='k',
                     label=r'Sim (90\% CI), $T={:d}$ GeV'.
                     format(int(Tsrc[i].to(u.GeV).value)))
    plt.plot([-10,-10], [-10,-10], 'k', label='Exact')
    plt.legend(loc='upper left', prop={"size" : 10})
    plt.xlim([-1,1])
    plt.ylim([0,2])
    plt.xlabel(r'$\mu$')
    plt.ylabel(r'$(\mathcal{L}t)^{-1} \, dE_\mathrm{CR}/d\mu$')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

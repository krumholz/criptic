// Implementation of the PropNonLinDiff class

#include <cmath>
#include <gsl/gsl_sf.h>
#include "PropNonLinDiff.H"
#include "../../../Utils/Constants.H"

using namespace std;
using namespace criptic;
using namespace criptic::propagation;

// Constructor
PropNonLinDiff::PropNonLinDiff(const ParmParser& pp) {

  // Read input parameters
  Real r0, t0, eCRTot;
  pp.get("prob.r0", r0);
  pp.get("prob.t0", t0);
  pp.get("cr.kappa_idx", q);
  pp.get("prob.e_cr_tot", eCRTot);

  // Convert total CR energy to code units
  eCRTot /= constants::mp_c2;

  // Deduce the scale CR energy density from r0
  U0 = eCRTot / pow(r0, 3) * pow(M_PI, -1.5) *
    gsl_sf_gamma(1.0/q + 2.5) /
    gsl_sf_gamma(1.0/q + 1);

  // Deduce kappa0 from t0
  kappa0 = r0*r0 * q / (2.0 * t0 * (3*q + 2));
}

// Propagation coefficient calculation
inline PropagationData
PropNonLinDiff::operator()(const RealVec& x,
			   const Real t,
			   const gas::GasData& gd,
			   const CRPacket& packet,
			   const FieldQty& qty,
			   const FieldQtyGrad& qtyGrad) const {

  // Fill propagation data
  PropagationData pd;
  pd.kPar = kappa0 * pow( qty[edenIdx]/U0, q );
  pd.kPerp = pd.kPar;
  pd.kPP = 0.0;
  pd.vStr = 0.0;
  pd.kParGrad = q * pd.kPar * qtyGrad[edenIdx] / qty[edenIdx];
  pd.kPerpGrad = pd.kParGrad;
  pd.vStrGrad = 0.0;
  pd.dvStr_dp = 0.0;
  pd.dkPP_dp = 0.0;

  // Return
  return pd;
}

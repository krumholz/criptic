"""
Analysis script for non-linear diffusion test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import astropy.constants as const
from astropy.units import Quantity
import os.path as osp
from scipy.special import gamma
from cripticpy import readchk, readlastchk, pctnbin, parseinput

# Name of the problem this script is for
prob_name = "NonLinDiff"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
etot = params['prob.e_cr_tot'] * u.erg
r0 = params['prob.r0'] * u.cm
t0 = params['prob.t0'] * u.s
q = params['cr.kappa_idx']
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Construct remaining parameters from the ones read
U0_ = etot / (np.sqrt(np.pi) * r0)**3 * gamma(1.0/q + 2.5) / gamma(1.0/q + 1)
kappa0_ = r0**2 * q / (2 * t0 * (3*q + 2))

# Read starting and final checkpoints
data0 = readchk(osp.join(args.dir, chkname+'00000.hdf5'))
if args.chk >= 0:
    data1 = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data1 = readlastchk(args.dir, chkname)

# Get packet radii
rad0 = np.sqrt(data0['packets']['x'][:,0]**2 +
               data0['packets']['x'][:,1]**2 +
               data0['packets']['x'][:,2]**2)
rad1 = np.sqrt(data1['packets']['x'][:,0]**2 +
               data1['packets']['x'][:,1]**2 +
               data1['packets']['x'][:,2]**2)

# Get packet energy density in radial bins, along with confidence intervals
# t = 0 snapshot; note that we have to be a little careful here with
# units, because astropy units only started working correctly with
# numpy histogram in a fairly recent version, and we therefore want to
# handle the case where it doesn't.
nbin = 30
c0, e0 = np.histogram(rad0, bins=nbin)
if not (type(e0) is Quantity):
    e0 = e0 * rad0.unit
clo0 = np.array([ pctnbin(0.05, c) for c in c0 ])
chi0 = np.array([ pctnbin(0.95, c) for c in c0 ])
clim0 = np.stack((clo0, chi0))
T0 = c0 * data0['packets']['w'][0] * data0['packets']['T'][0]
Tlim0 = clim0 * data0['packets']['w'][0] * data0['packets']['T'][0]
rc0 = 0.5 * (e0[1:] + e0[:-1])
V0 = 4./3. * np.pi * (e0[1:]**3 - e0[:-1]**3)
U0 = T0 / V0
Ulim0 = Tlim0 / V0
Uerr0 = np.abs(Ulim0 - U0)

# Repeat for final snapshot
c1, e1 = np.histogram(rad1, bins=nbin)
if not (type(e1) is Quantity):
    e1 = e1 * rad0.unit
clo1 = np.array([ pctnbin(0.16, c) for c in c1 ])
chi1 = np.array([ pctnbin(0.84, c) for c in c1 ])
clim1 = np.stack((clo1, chi1))
T1 = c1 * data1['packets']['w'][0] * data1['packets']['T'][0]
Tlim1 = clim1 * data1['packets']['w'][0] * data1['packets']['T'][0]
rc1 = 0.5 * (e1[1:] + e1[:-1])
V1 = 4./3. * np.pi * (e1[1:]**3 - e1[:-1]**3)
U1 = T1 / V1
Ulim1 = Tlim1 / V1
Uerr1 = np.abs(Ulim1 - U1)

# Function to give the exact solution
def r1(t):
    return r0 * (t/t0 + 1)**(1/(3*q+2))
def Uex(r, t):
    r1_ = np.atleast_1d(r1(t))
    fac = np.atleast_1d(1.0 - (r/r1_)**2)
    fac[r >= r1_] = 0.0
    fac[r < r1_] = fac[r < r1_]**(1/q)
    return U0_ * fac * (t/t0+1)**(-3 / (3*q+2))
def dUdrex(r, t):
    r1_ = np.atleast_1d(r1(t))
    fac = np.atleast_1d(1.0 - (r/r1_)**2)
    fac[r >= r1_] = 0.0
    fac[r < r1_] = fac[r < r1_]**(1/q-1)
    return -2 * U0_ * r / (q*r0**2) * fac * (t/t0+1)**(-3 / (3*q+2))


# Compute L1 norm of error and decide pass/fail
l1err = np.sum(np.abs(Uex(rc1, data1['t']) - U1)
               * 4/3 * np.pi * (e1[1:]**3 - e1[:-1]**3)) / etot
if args.verbose:
    print("L1 error = " + str(float(l1err)))
if l1err < 0.05:
    print("PASS")
else:
    print("FAIL")

# Plot if requested
if not args.testonly:

    # Get exact solutions to plot
    rex = np.linspace(0, r1(data1['t']), 500)
    UCR0_ex = Uex(rex, data0['t'])
    UCR_ex = Uex(rex, data1['t'])
    dedr_ex = dUdrex(rex, data0['t'])

    # Set font
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    
    # First plot: energy density compubed in bins vs exact solution
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.errorbar(rc0.to(u.pc).value, U0.to(u.eV/u.cm**3).value,
                 yerr=Uerr0.to(u.eV/u.cm**3).value,
                 fmt='o', ecolor='C0', mfc='C0', mec='k',
                 label='Initial (sim, 90\% CI)')
    plt.plot(rex.to(u.pc), UCR0_ex.to(u.eV/u.cm**3), 'C0')
    plt.errorbar(rc1.to(u.pc).value, U1.to(u.eV/u.cm**3).value,
                 yerr=Uerr1.to(u.eV/u.cm**3).value,
                 fmt='o', ecolor='C1', mfc='C1', mec='k',
                 label='Final (sim, 90\% CI)')
    plt.plot(rex.to(u.pc), UCR_ex.to(u.eV/u.cm**3), 'C1')
    plt.xlim([0,np.ceil(2*rc1[-1].to(u.pc).value)/2])
    plt.ylim([0.5, 2*U0_.to(u.eV/u.cm**3).value])
    plt.yscale('log')
    plt.plot([-1], [-1], 'k', label='Exact')
    plt.legend(prop={"size":10})
    plt.xlabel(r'$r$ [pc]')
    plt.ylabel(r'$U_{\mathrm{CR}}$ [eV cm$^{-3}$]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

    # Second plot: energy density and its gradient computed on the fly
    # by the KDE vs exact solution

    # Compute percentiles on simulations in bins
    eden = data0['packets']['eden'].to(u.eV/u.cm**3)
    dedr = (np.sum(data0['packets']['edenGrad'] *
                   data0['packets']['x'], axis=1) /
            rad0).to(u.eV/u.cm**3/u.pc)
    idx = np.digitize(rad0, e0)
    pct = [5,25,50,75,95]
    eden_pct = np.zeros((c0.size, 5))
    dedr_pct = np.zeros((c0.size, 5))
    for i in range(c0.size):
        eden_pct[i,:] = np.percentile(eden[idx == i+1], pct)
        dedr_pct[i,:] = np.percentile(dedr[idx == i+1], pct)

    # Plot
    plt.figure(2, figsize=(4,4))
    plt.clf()
    
    # Energy density
    plt.subplot(2,1,1)
    plt.plot([-1], [-1], 'k', label='Exact')
    plt.plot(rc0.to(u.pc).value, eden_pct[:,2],
             'C0', label='Sim (median)')
    plt.fill_between(rc0.to(u.pc).value,
                     eden_pct[:,1],
                     eden_pct[:,2],
                     color='C0', alpha=0.5,
                     label="50\% CI")
    plt.fill_between(rc0.to(u.pc).value,
                     eden_pct[:,2],
                     eden_pct[:,3],
                     color='C0', alpha=0.5)
    plt.fill_between(rc0.to(u.pc).value,
                     eden_pct[:,0],
                     eden_pct[:,1],
                     color='C1', alpha=0.5,
                     label="90\% CI")
    plt.fill_between(rc0.to(u.pc).value,
                     eden_pct[:,3],
                     eden_pct[:,4],
                     color='C1', alpha=0.5)
    plt.plot(rex.to(u.pc), UCR0_ex.to(u.eV/u.cm**3), 'k')
    plt.ylim([3, 2*U0_.to(u.eV/u.cm**3).value])
    plt.yscale('log')
    plt.xlim([0, r1(data0['t']).to(u.pc).value])
    plt.legend(prop={"size" : 10})
    plt.ylabel(r'$U_{\mathrm{CR}}$ [eV cm$^{-3}$]')
    plt.gca().set_xticklabels([])

    # Plot gradient of energy density
    plt.subplot(2,1,2)
    plt.plot(rc0.to(u.pc).value, dedr_pct[:,2],
             'C0', label='Sim')
    plt.fill_between(rc0.to(u.pc).value,
                     dedr_pct[:,1],
                     dedr_pct[:,2],
                     color='C0', alpha=0.5)
    plt.fill_between(rc0.to(u.pc).value,
                     dedr_pct[:,2],
                     dedr_pct[:,3],
                     color='C0', alpha=0.5)
    plt.fill_between(rc0.to(u.pc).value,
                     dedr_pct[:,0],
                     dedr_pct[:,1],
                     color='C1', alpha=0.5)
    plt.fill_between(rc0.to(u.pc).value,
                     dedr_pct[:,3],
                     dedr_pct[:,4],
                     color='C1', alpha=0.5)
    plt.plot(rex[rex < r1(data0['t'])].to(u.pc),
             dedr_ex[rex < r1(data0['t'])].to(u.eV/u.cm**3/u.pc), 'k')
    
    plt.xlim([0, r1(data0['t']).to(u.pc).value])
    plt.ylabel(r'$dU_{\mathrm{CR}}/dr$ [eV cm$^{-3}$ pc$^{-1}$]')
    plt.xlabel(r'$r$ [pc]')
    
    plt.subplots_adjust(top=0.95, right=0.95, hspace=0.07, left=0.2,
                        bottom=0.18)

    plt.savefig(osp.join(args.dir, prob_name+"2." + args.imgformat))


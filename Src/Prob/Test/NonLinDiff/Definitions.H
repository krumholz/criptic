/**
 * @file Definitions.H
 * @brief Problem-specific compile-time defines
 *
 * @details
 * This file contains problem-specific compile-time defines.
 *
 * @author Mark Krumholz
 */

// Number of rigidity bins to use in the CR tree -- since this problem
// is mono-energetic, we can just use one
#if (!defined(TREE_RIGIDITY_BINS))
/**
 * @def TREE_RIGIDITY_BINS
 * @brief Number of rigidity bins to use in the CR tree
 */
#   define TREE_RIGIDITY_BINS 1
#endif

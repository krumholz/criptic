// This file provides a problem setup for the non-linear diffusion
// test problem. The problem setup is as
// follows:
// 1. The background gas is uniform, and is characterised by a
//    constant magnetic field.
// 2. The CR propagation model is a custom model in which the
//    diffusion coefficient is isotropic, and is a powerlaw function
//    of the local CR energy density, given by kappa = kappa_0 (U /
//    U_0)^q, where kappa_0, U_0, and n are user-specified parameters.
// 3. The are no CR sources in the volume.
// 4. CR packets are initially distributed following an exact
//    similarity solution, whereby they are distributed in radius as
//    U \propto [1 - (r/r1)^2]^(1/q), where
//    r1 = r0 * (t/t0)^(1/(3*q+2)).
//    The quantities r0 and t0 as user-defined parameters, and
//    together they determine kappa0 via the relation
//    kappa0 = r0^2 * q / (2 * t0 * (3*q + 2))
//
// The simulation is initialized with a distribution corresponding to
// t = t0, and the test is to verify that it continues to follow the
// exact solution at later times.

#include "PropNonLinDiff.H"
#include "NonLinDiffProb.H"

using namespace criptic;
using namespace std;

// Initialize the propagation model
propagation::Propagation *NonLinDiffProb::initProp() {
  return new propagation::PropNonLinDiff(pp);
}

// Little convenience function
inline Real SQR(Real x) { return x*x; }

// Initialize the packet distribution
void NonLinDiffProb::initPackets(vector<RealVec>& x,
				 vector<CRPacket>& packets) {
  // Only create packets on the IO processor; they will be
  // redistributed to other processors automatically
  if (MPIUtil::IOProc) {

    // Read problem parameters
    Real r0, t0, q, eCRTot, eCR;
    int np;
    pp.get("prob.n_packet", np);
    pp.get("prob.r0", r0);
    pp.get("prob.t0", t0);
    pp.get("cr.kappa_idx", q);
    pp.get("prob.e_cr", eCR);
    pp.get("prob.e_cr_tot", eCRTot);

    // Safety check on parameters
    if (t0 <= 0 || r0 <= 0 || q <= -2./3.) {
      MPIUtil::haltRun("Prob: nonlinear diffusion test requires "
		       "r0 > 0, t0 > 0, kappa_dx > -2/3",
		       errBadInputFile);
    }

    // Resize arrays to correct size
    x.resize(np);
    packets.resize(np);

    // Loop over packets
    for (int i=0; i<np; i++) {

      // Pick radius from the analytic solution, which is distributed
      // as U_CR ~ [1 - (r/r0)^2]^(1/q) at time t = t0; we do this
      // with a rejection method, which is probably inefficient, but it
      // doesn't matter much since we're only doing it once
      Real r;
      do {
	r = r0 * pow(rng.uniform(), 1.0/3.0);
      } while (rng.uniform() > pow(1.0 - SQR(r/r0), 1.0/q));

      // Pick random angles
      Real mu = rng.uniform(-1,1);
      Real phi = rng.uniform(0, 2*M_PI);

      // Set position
      x[i][0] = r * sqrt(1.0 - SQR(mu)) * cos(phi);
      x[i][1] = r * sqrt(1.0 - SQR(mu)) * sin(phi);
      x[i][2] = r * mu;

      // Set packet properties
      packets[i].type = partTypes::proton;
      packets[i].src = -1;
      packets[i].tInj = 0.0;
      packets[i].p = sr::p_from_T(packets[i].type,
				  eCR * units::GeV / constants::mp_c2);
      packets[i].w = packets[i].wInj = eCRTot / (eCR * units::GeV * np);
      packets[i].gr = 0.0;

    }

  }
}

// This sets up the non-linear diffusion test problem, by returning
// the problem generator class for it.

#include "NonLinDiffProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::NonLinDiffProb(pp, geom, rng);
}

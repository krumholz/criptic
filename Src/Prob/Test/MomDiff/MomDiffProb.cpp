// This file provides a problem setup for the momentum diffusion test
// problem. In this problem, we place point source of CRs that injects
// those CRs at some specified initial momentum p0. The CRs do not
// diffuse in position, but they do diffuse in momentum, with a
// diffusion coefficient k_pp. The Green's function for an instantaneous
// population of CRs that start a momentum p0 at some time t after
// injection is
// G(p, t) = (p / 2 p0) (pi k_pp t)^(-1/2) *
//   [ exp( -(p - p0)^2 / 4 k_pp t ) -
//     exp( -(p + p0)^2 / 4 k_pp t ) ],
// and the corresponding density of CRs after injection has been going
// on for a time T is
// f(p) = (p / 2 k_pp p0) *
//    { 2 (k_pp T / pi)^(1/2) [ exp( -(p - p0)^2 / 4 k_pp T ) -
//                              exp( -(p + p0)^2 / 4 k_pp T ) ] +
//      (p + p0) Erfc[ (p + p0) / sqrt(4 k_pp T) ] +
//      |p - p0| [ Erf( |p - p0| / sqrt(4 k_pp T) ) - 1 ] }.
// The test is to verify that the code reproduces this analytic solution.


#include "MomDiffProb.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Units.H"

using namespace criptic;
using namespace std;

void
MomDiffProb::initSources(vector<RealVec>& x,
			 vector<CRSource>& sources) {

  // Only one MPI rank does this
  if (MPIUtil::IOProc) {

    // Get source luminosity and initial CR momentum
    Real L, p0;
    pp.get("prob.L", L);
    pp.get("prob.p0", p0);

    // Convert p0 from input units of GeV / c to code units of m_p * c
    p0 *= units::GeV / constants::mp_c2;

    // Set source position
    x.push_back(zeroVec);

    // Create source
    sources.resize(1);
    sources[0].type = partTypes::proton;
    sources[0].p0 = sources[0].p1 = p0;
    sources[0].setLum(L); 
  }
}


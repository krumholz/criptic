"""
Analysis script for loop diffusion test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
from astropy.constants import m_p, c
import os.path as osp
from scipy.special import erf, erfc
from cripticpy import readchk, readlastchk, pctnbin, parseinput

# Name of the problem this script is for
prob_name = "MomDiff"

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
kPP = params['cr.kPP0']
p0 = params['prob.p0'] * u.GeV / c
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)

# Grab momenta and bin them in momentum
p = data['packets']['p']
count, edges = np.histogram( (p / (u.GeV/c)).to(''),
                             bins=50)
centers = 0.5 * (edges[1:] + edges[:-1])
clo = []
chi = []
for count_ in count:
    chi.append(pctnbin(0.95, count_))
    if c > 0:
        clo.append(pctnbin(0.05, count_))
    else:
        clo.append(0.0)
chi = np.array(chi)
clo = np.array(clo)
dp = (edges[1] - edges[0]) * (u.GeV/c)
w = data['packets']['w'][0]
dfdp = count * w / dp
dfdpup = chi * w / dp
dfdplo = clo * w / dp
dfdperr = np.zeros((2,dfdp.size)) / (u.GeV/c)
dfdperr[0,:] = dfdp - dfdplo
dfdperr[1,:] = dfdpup - dfdp

# Exact solution
ndot = data['sources']['ndot'][0]
def dfdp_ex(p, t):
    fac = 4 * kPP * t
    return ndot * p / (2 * kPP * p0) * (
        np.sqrt(fac / np.pi) * (
            np.exp( -(p - p0)**2 / fac ) -
            np.exp( -(p + p0)**2 / fac ) ) +
        (p + p0) * erfc( (p + p0) / np.sqrt(fac) ) +
        np.abs(p - p0) * ( erf( np.abs(p - p0) / np.sqrt(fac) ) - 1 ) )
dfdp_exact = dfdp_ex(centers * u.GeV/c, data['t'])

# Compute L1 error and decide pass / fail
l1err = np.sum(dp * np.abs(dfdp - dfdp_exact)) / (ndot * data['t'])
if args.verbose:
    print("L1 error = " + str(float(l1err)))
if l1err < 0.02:
    print("PASS")
else:
    print("FAIL")

# Make plot if requested
if not args.testonly:
    # Plot radial distribution
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    pgrid = np.linspace(0,8,500) * u.GeV/c
    dfdp_grid = dfdp_ex(pgrid, data['t'])
    plt.plot((pgrid/(u.GeV/c)).to(''),
             (dfdp_grid*u.GeV/c).to(''), 'k', label='Exact')
    plt.errorbar(centers, (dfdp*u.GeV/c).to('').value,
                 yerr=(dfdperr*u.GeV/c).to('').value,
                 fmt='o', ecolor='C0', mfc='C0',
                 mec='k', label='Sim (90\% CI)')
    plt.yscale('log')
    plt.ylim([1e49,5e54])
    plt.xlim([0,8])
    plt.legend(prop={"size" : 10})
    plt.xlabel(r'$p$ [$\mathrm{GeV}/c$]')
    plt.ylabel(r'$dn_{\rm CR}/dp$ [$(\mathrm{GeV}/c)^{-1}$]')
    plt.subplots_adjust(top=0.95, right=0.95, bottom=0.18, left=0.2)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

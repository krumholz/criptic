// This sets up the anisotropic diffusion test problem, by returning
// the problem generator class for it.

#include "MomDiffProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::MomDiffProb(pp, geom, rng);
}

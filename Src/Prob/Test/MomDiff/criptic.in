# criptic input deck

######## Global run controls ########

dt_init      6.31152e10              # Initial time step = 2 kyr
max_time     3.15576e13              # Time at which to stop
	      			     # simulation = 1 Myr
verbosity    1                       # Verbosity level

######## Problem geometry ########

geometry.lo_type    open open open   # Open in x, y, z on low side
geometry.hi_type    open open open   # Open in x, y, z on high size

######## Integrator controls ########

integrator.packet_rate  1.0e-7       # Packets injected per unit time

######## Losses ########

losses.disable_all      1            # Disable all losses

######## Output controls ########

output.chk_dt  1.57788e13            # Time interval between checkpoints

######## Cosmic ray propagation model ########

# This input file uses the simple powerlaw parameterization, whereby
# kPar = kPar0 * (p / m_p c)^kParIdx, and similarly for all other quantities

cr.kPar0     0.0                     # Parallel diffusion
cr.kParIdx   0.0
cr.kPerp0    0.0                     # Perpendicular diffusion
cr.kPerpIdx  0.0
cr.kPP0      3.16881e-14             # Momentum diffusion -- this
	     			     # choice corresponds to a
				     # diffusion time t_diff = p^2 /
				     # kPP0 = 1 Myr for p = 1 GeV/c
cr.kPPIdx    0.0
cr.vStr0     0.0                     # Streaming speed
cr.vStrIdx   0.0

######## Parameters for the background gas ########

gas.density     2.34e-26             # Total density
gas.ionDensity  2.34e-26             # Ionized gas density
gas.velocity    0.0 0.0 0.0          # Gas velocity
gas.magField    3.0e-6 0.0 0.0       # Magnetic field strength
gas.dx          3.09e18              # Length scale

######## Problem setup parameters ########

prob.L           1.0e38              # Source luminosity in erg/s
prob.p0          1.0                 # Source momentum in GeV / c

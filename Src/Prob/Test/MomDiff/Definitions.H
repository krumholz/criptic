/**
 * @file Definitions.H
 * @brief Problem-specific compile-time defines
 *
 * @details
 * This file contains problem-specific compile-time defines. It is
 * intentionally empty for this probem, which does not require any
 * such definitions.
 *
 * @author Mark Krumholz
 */


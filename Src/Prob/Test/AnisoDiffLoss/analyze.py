"""
Analysis script for anisotropic diffusion with losses test problem
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import astropy.units as u
import astropy.constants as const
from astropy.units import Quantity
import os.path as osp
from scipy.special import erf, erfc
from scipy.integrate import quad
from cripticpy import readchk, readlastchk, heatscatter, parseinput, \
    pctnbin, Tp, vp

# Name of the problem this script is for
prob_name = "AnisoDiffLoss"

# Define some physical constants
mPi0 = 134.9758 * u.MeV / const.c**2      # pi^0 rest mass
Tpth = 2 * mPi0 * const.c**2 + (mPi0 * const.c**2)**2 / \
       (2 * const.m_p * const.c**2)  # Kinematic threshold for pi^0 creation

# Little helper function to compute the cross section for nuclear
# inelastic scattering
def sigma_nuc(T):
    mb = 1e-27 * u.cm**2   # 1 millibarn
    sigmaPP = (30.7 - 0.96 * np.log(T/Tpth) + 0.18 * np.log(T/Tpth)**2) * \
        (1.0 - (T/Tpth)**-1.9)**3 * mb
    sigmaPP[T <= Tpth] = 0.0
    T0 = 1e3 * u.GeV
    sigmaRpp = 79 * mb
    sigmaPP0 = (30.7 - 0.96 * np.log(T0/Tpth) + 0.18 * np.log(T0/Tpth)**2) * \
        (1.0 - (T0/Tpth)**-1.9)**3 * mb
    G = 1.0 + np.log( np.maximum(1.0, sigmaPP / sigmaPP0) )
    epsc = 1.2
    eps1 = 0.066
    epsNuc = epsc + eps1 * G * sigmaRpp / (sigmaPP + 1e-30*mb)
    sigma = sigmaPP * epsNuc
    return sigma

# Parse command line arguments
parser = argparse.ArgumentParser(
    description="Analysis script for the "+prob_name+" test")
parser.add_argument("--testonly",
                    help="report test results only, do not make plots",
                    default=False, action="store_true")
parser.add_argument("-c", "--chk",
                    help="number of checkpoint file to examine (default "
                    "is last checkpoint file in directory)",
                    default=-1, type=int)
parser.add_argument("-d", "--dir",
                    help="directory containing run to analyze",
                    default=".", type=str)
parser.add_argument("-v", "--verbose",
                    help="print verbose error results",
                    default=False, action="store_true")
parser.add_argument("-if", "--imgformat",
                    help="file format to use when saving images",
                    default="pdf")
args = parser.parse_args()

# Extract parameters we need from the input file
params = parseinput(osp.join(args.dir, 'criptic.in'))
kPar0 = params['cr.kPar0']
kPerp0 = params['cr.kPerp0']
kParIdx = params['cr.kParIdx']
kPerpIdx = params['cr.kPerpIdx']
L = params['prob.L'] * u.erg/u.s
p0 = params['prob.p0'] * u.GeV/const.c
p1 = params['prob.p1'] * u.GeV/const.c
pidx = params['prob.pidx']
rho = params['gas.density']
if 'output.chkname' in params:
    chkname = params['output.chkname']
else:
    chkname= 'criptic_'

# Read checkpoint
if args.chk >= 0:
    data = readchk(osp.join(args.dir, chkname +
                            "{:05d}".format(args.chk) + ".hdf5"))
else:
    data = readlastchk(args.dir, chkname)

# Compute diffusion radius for all packets
p = data['packets']['p']
K = kPerp0 * (p / (const.m_p * const.c))**kPerpIdx
rdiff = np.sqrt(K * data['t'])

# Compute loss time and dimensionless loss radius for all packets
nH = rho / (1.333 * const.m_p)
v = vp(p, data['packets']['ptype'])
T = data['packets']['T']
tloss = 1 / (nH * sigma_nuc(T) * v + 1.0 / (1e30 * u.Gyr))
rloss = np.sqrt(tloss * K)

# Compute effective radius for all packets
chi = kPar0 / kPerp0
r = (np.sqrt(data['packets']['x'][:,0]**2 / chi +
             data['packets']['x'][:,1]**2 +
             data['packets']['x'][:,2]**2)).to(u.cm)

# Set up momentum bins, and compute diffusion and loss radii for the
# central momentum of each bin; we will use these to set up the radial
# bins and construct the analytic solution
npbin = 30
logpbinedge = np.arange(npbin+1) * np.log(p1/p0) / npbin
pbinedge = p0 * np.exp(logpbinedge)
pbin = np.sqrt(pbinedge[1:] * pbinedge[:-1])
Tbin = Tp(pbin, data['packets']['ptype'][:pbin.size])
vbin = vp(pbin, data['packets']['ptype'][:pbin.size])
Kbin = kPerp0 * (pbin / (const.m_p * const.c))**kPerpIdx
rdiffbin = np.sqrt(Kbin * data['t'])
tlossbin = 1 / (nH * sigma_nuc(Tbin) * vbin + 1.0 / (1e30 * u.Gyr))
rlossbin = np.sqrt(tlossbin * Kbin)

# Bin the packets in momentum and r / rdiff
nxbin = 30
xedge = np.logspace(-1,1, nxbin+1)
xcen = np.sqrt(xedge[1:] * xedge[:-1])
redge = np.outer(xedge, rdiffbin)
rcen = np.outer(xcen, rdiffbin)
wbin, xe, ye = np.histogram2d((r/rdiff).to(''),
                              np.log(p/p0),
                              bins=[xedge, logpbinedge],
                              weights=data['packets']['w'])
w2bin, xe, ye = np.histogram2d((r/rdiff).to(''),
                               np.log(p/p0),
                               bins=[xedge, logpbinedge],
                               weights=data['packets']['w']**2)
neff = wbin**2 / (w2bin + 1e-30)

# Normalize sums so that we have d^2 n_cr / dr d ln p
dlnp = logpbinedge[1] - logpbinedge[0]
dist = wbin / dlnp / (4/3 * np.pi * np.sqrt(chi) *
                      (redge[1:,:]**3 - redge[:-1,:]**3))

# Get uncertainty on simulation result
dist_lim = np.zeros((2,)+dist.shape) / u.cm**3
for i in range(neff.shape[0]):
    for j in range(neff.shape[1]):
        pctlim = pctnbin([0.05, 0.95], float(neff[i,j]))
        dist_lim[0,i,j] = dist[i,j] * pctlim[0] / (neff[i,j] + 1e-30)
        dist_lim[1,i,j] = dist[i,j] * pctlim[1] / (neff[i,j] + 1e-30)

# Commpute the injection rate \dot{n} of CRs into each momentum bin
ndotbin = data['sources']['coef'][0] * \
    ((pbinedge[1:]**(pidx+1) - pbinedge[:-1]**(pidx+1)) / (pidx + 1) /
     (const.m_p * const.c)**(pidx+1)).to('')

# Compute the exact solution; note that we do this by numerically
# integrating the exact point solution over the bin, because otherwise
# the error in the innermost winds up being dominated by the error
# associated with evaluating the density at the bin center, rather
# than properly integrating.
def den_ex(r, rloss, rdiff):
    return 4 * np.pi * r * \
        ( np.exp( -r / rloss ) *
          erfc( r / (2 * rdiff) - rdiff / rloss ) +
          np.exp( r / rloss ) *
          erfc( r / (2 * rdiff) + rdiff / rloss ) )
dist_ex = np.zeros(rcen.shape) * u.cm**-3
dist_ex_noloss = np.zeros(rcen.shape) * u.cm**-3
for i in range(nxbin):
    for j in range(npbin):
        binint =  quad(den_ex,
                       redge[i,j].to(u.cm).value,
                       redge[i+1,j].to(u.cm).value,
                       args=(rlossbin[j].to(u.cm).value,
                             rdiffbin[j].to(u.cm).value))[0] * u.cm**2
        binavg = binint / (4./3. * np.pi *
                           (redge[i+1,j]**3 - redge[i,j]**3))
        dist_ex[i,j] = ndotbin[j] / (8 * np.pi * np.sqrt(chi) * dlnp *
                                      Kbin[j]) * binavg
        binint =  quad(den_ex,
                       redge[i,j].to(u.cm).value,
                       redge[i+1,j].to(u.cm).value,
                       args=(1e100,
                             rdiffbin[j].to(u.cm).value))[0] * u.cm**2
        binavg = binint / (4./3. * np.pi *
                           (redge[i+1,j]**3 - redge[i,j]**3))
        dist_ex_noloss[i,j] = ndotbin[j] / \
            (8 * np.pi * np.sqrt(chi) * dlnp * Kbin[j]) * binavg
        
# Compute the L1 error and print result
diffp = np.sum(4/3 * np.pi * np.sqrt(chi) *
               (redge[1:,:]**3 - redge[:-1,:]**3) *
               np.abs(dist - dist_ex),
               axis=0)   # Difference in each momentum bin
diff = np.sum(diffp * (pbin / (u.GeV / const.c))**(-pidx-1))
norm = np.sum(ndotbin * (pbin / (u.GeV / const.c))**(-pidx-1)) * \
    data['t']
L1err = diff / norm
if args.verbose:
    print("L1 error = {:f}".format(L1err))
if L1err < 5e-2:
    print("PASS")
else:
    print("FAIL")

# Make plots
if not args.testonly:
    
    # Plot radial distribution at sample momenta
    plt.figure(1, figsize=(4,3))
    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    npplot = 5
    for i in range(npplot):
        idx = int(i * (npbin-1) / (npplot-1))
        plt.plot(rcen[:,idx].to(u.pc),
                 dist_ex[:,idx].to(u.cm**-3)
                 * (pbin[idx]/(u.GeV/const.c)).to('').value**2,
                 'C{:d}'.format(i))
        plt.plot(rcen[:,idx].to(u.pc),
                 dist_ex_noloss[:,idx].to(u.cm**-3)
                 * (pbin[idx]/(u.GeV/const.c)).to('').value**2,
                 'C{:d}--'.format(i))
        plt.xscale('log')
        plt.yscale('log')
        dist_tmp = dist[::1,idx].to(u.cm**-3).value
        dist_err_tmp = np.zeros((2,dist_tmp.size))
        dist_err_tmp[0,:] = dist_tmp - dist_lim[0,::1,idx].to(u.cm**-3).value
        dist_err_tmp[1,:] = dist_lim[1,::1,idx].to(u.cm**-3).value - dist_tmp
        dist_tmp *= (pbin[idx]/(u.GeV/const.c)).to('').value**2
        dist_err_tmp *= (pbin[idx]/(u.GeV/const.c)).to('').value**2
        plt.errorbar(rcen[::1,idx].to(u.pc),
                     dist_tmp,
                     yerr=dist_err_tmp,
                     fmt='o', ecolor='C{:d}'.format(i),
                     mfc='C{:d}'.format(i),
                     mec='k',
                     label=r'$10^{{{:4.1f}\pm 0.1}}$'.format(
                         np.log10(pbin[idx]/(u.GeV/const.c))
                     ))
    leg = plt.legend(title = r"$p$ [GeV/$c$]",
                     fontsize=10,
                     title_fontsize=10,
                     ncol = 1)
    l1, = plt.plot([-1,-1], [-1,-1], "k")
    l2, = plt.plot([-1,-1], [-1,-1], "ko")
    l3, = plt.plot([-1,-1], [-1,-1], "k--")
    plt.legend([l1,l2,l3],
               ["Exact", "Sim (90\% CI)", "No $pp$"], loc='upper right',
               prop = {"size":10},
               ncol = 1)
    plt.gca().add_artist(leg)
    plt.xlabel(r"$r' = \sqrt{x^2/\chi + y^2 + z^2}$ [pc]")
    plt.ylabel(r"$p^3\, (dn_{\mathrm{CR}}/dp)$ [$(\mbox{GeV}/c)^2$ cm$^{-3}$]")
    plt.ylim([1e-17, 1e-7])
    plt.subplots_adjust(left=0.2, bottom=0.18, top=0.95, right=0.95)
    plt.savefig(osp.join(args.dir, prob_name+"1." + args.imgformat))

    # Plot projection
    idx = np.logical_and(data['packets']['p'] >= pbinedge[5],
                         data['packets']['p'] < pbinedge[6])
    x = data['packets']['x'][idx]
    w = data['packets']['w'][idx]
    T = data['packets']['T'][idx]
    dT = Tp(pbinedge[6], data['packets']['ptype'][0]) - \
        Tp(pbinedge[5], data['packets']['ptype'][0])
    plt.figure(2, figsize=(4,3))
    plt.clf()
    heatscatter(x[:,0].to(u.pc), x[:,1].to(u.pc), w=w*T/dT,
                xlim = np.array([-1,1]) * 0.5 * u.kpc,
                ylim = np.array([-1,1]) * 0.5 * u.kpc,
                vmin = 10.**-2.5*u.eV*u.pc/u.cm**3/u.GeV,
                vmax = 10*u.eV*u.pc/u.cm**3/u.GeV,
                nxbin = 80,
                nybin = 80)
    plt.xlabel(r'$x$ [pc]')
    plt.ylabel(r'$y$ [pc]')
    norm = colors.Normalize(vmin=-2.5, vmax=1)
    plt.colorbar(cm.ScalarMappable(norm=norm),
                 label=r'$\log \int \frac{dU_\mathrm{CR}}{dT}\, dz$ [eV pc cm$^{-3}$ GeV$^{-1}$]')
    plt.subplots_adjust(bottom=0.18, left=0.18, top=0.95)
    plt.savefig(osp.join(args.dir, prob_name+"2." + args.imgformat))


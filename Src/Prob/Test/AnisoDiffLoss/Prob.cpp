// This sets up the anisotropic diffusion test with loss problem, by
// returning the problem generator class for it.

#include "AnisoDiffLossProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
				 const criptic::Geometry& geom,
				 criptic::RngThread& rng) {
  return new criptic::AnisoDiffLossProb(pp, geom, rng);
}

// This file implements the problem setup for the AnisoDiffProb class,
// which sets up the anisotropic diffusion test problem. The problem
// setup is as follows:
// 1. One or more point sources of CRs are placed at the origin; the
//    sources are monochromatic (i.e., all the CRs they inject have
//    the same energy), and have specified total luminosities.
// 2. The background gas is uniform, and is characterised by a
//    constant magnetic field.
// 3. The CR propagation model is a simple energy-dependent powerlaw
//    in both the parallel and perpendicular directions, i.e., we have
//    kappa_par = kappa_par0 * (T/T0)^p, where T is the kinetic energy
//    of the CR, and kappa_par0, T0, and p are user-defined parameters
//    taken from the input deck. Perpendicular diffusion is defined in
//    an analogous way.
// 4. The are no CR packets present in the volume initially.

#include "AnisoDiffLossProb.H"
#include "../../../MPI/MPIUtil.H"
#include "../../../Utils/Units.H"

using namespace criptic;
using namespace std;

void
AnisoDiffLossProb::initSources(vector<RealVec>& x,
			       vector<CRSource>& sources) {

  // Only one MPI rank does this
  if (MPIUtil::IOProc) {
  
    // Get source luminosities and energies from input deck
    Real L, p0, p1, pidx;
    pp.get("prob.L", L);
    pp.get("prob.p0", p0);
    pp.get("prob.p1", p1);
    pp.get("prob.pidx", pidx);

    // Create source
    x.resize(1);
    sources.resize(1);
    sources[0].type = partTypes::proton;
    sources[0].p0 = p0 * units::GeV / constants::c / constants::mp_c;
    sources[0].p1 = p1  * units::GeV / constants::c / constants::mp_c;
    sources[0].q = pidx;
    sources[0].setLum(L);
  }
}


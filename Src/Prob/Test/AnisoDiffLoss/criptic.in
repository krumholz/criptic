# criptic input deck

######## Global run controls ########

dt_init      1.0e10                  # Initial time step
max_time     2.0e12                  # Time at which to stop simulation
verbosity    1                       # Verbosity levels

######## Problem geometry ########

geometry.lo_type    open open open   # Open in x, y, z on low side
geometry.hi_type    open open open   # Open in x, y, z on high size

######## Integrator controls ########

integrator.packet_rate  1.0e-6       # Packets injected per unit time

######## Losses ########

losses.disable_all           1       # Turn off all loss mechanisms
losses.enable_nuc_inelastic  1       # Turn inelastic nuclear scattering back on
losses.f_sec                 0       # Disable secondaries

######## Output controls ########

output.chk_dt  1.0e12                 # Time interval between checkpoints

######## Cosmic ray propagation model ########

# This input file uses the simple powerlaw parameterization, whereby
# kPar = kPar0 * (p / m_p c)^kParIdx, and similarly for all other quantities

cr.kPar0     4.0e28                  # Parallel diffusion
cr.kParIdx   0.5
cr.kPerp0    1.0e28                  # Perpendicular diffusion
cr.kPerpIdx  0.5
cr.kPP0      0.0                     # Momentum diffusion
cr.kPPIdx    0.0
cr.vStr0     0.0                     # Streaming speed
cr.vStrIdx   0.0

######## Parameters for the background gas ########

gas.density     2.34e-21             # Total density
gas.ionDensity  2.34e-27             # Ionized gas density
gas.velocity    0.0 0.0 0.0          # Gas velocity
gas.magField    3.0e-4 0.0 0.0       # Magnetic field strength
gas.dx          1.0e20               # Length scale
gas.comp        molecular            # Fully molecular composition

######## Problem setup parameters ########

prob.L           1.0e38              # Source luminosity in erg/s
prob.p0          0.1                 # Source minimum CR momentum in GeV/c
prob.p1          1e5                 # Source maximum CR momentum in GeV/c
prob.pidx        -2.2                # Source momentum injection index
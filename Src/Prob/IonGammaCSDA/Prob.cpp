// This file provides a problem setup for the IonGammaCSDA problem

#include "IonGammaCSDAProb.H"

criptic::Prob *criptic::initProb(const criptic::ParmParser& pp,
                                 const criptic::Geometry& geom,
                                 criptic::RngThread& rng) {
  return new criptic::IonGammaCSDAProb(pp, geom, rng);
}


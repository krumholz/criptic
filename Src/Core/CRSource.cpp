// Implementation of the CRSource class

#include "CRSource.H"
#ifdef _OPENMP
#   include <omp.h>
#endif

using namespace criptic;
using namespace std;

vector<CRPacket> CRSource::draw(const IdxType n,
				const Real t0,
				const Real t1,
				const Real qSamp,
				const RngThread& rng) const {

  // Allocate object to hold output
  vector<CRPacket> p(n);

  // Loop over packets to be drawn
  Real w = 0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:w)
#endif
  for (IdxType i=0; i<n; i++) {

    // Set CR packet properties
    p[i].type = type;
    p[i].src = uniqueID;
    p[i].tInj = rng.uniform(t0, t1);
    p[i].gr = 0.0;
#ifdef TRACK_PITCH_ANGLE
    p[i].mu = rng.uniform(mu0, mu1);
#endif

    // Set momentum; be careful to handle special case where p1 = p0
    if (p1 > p0) {
      Real y = rng.uniform();
      if (qSamp == -1.0) {
	p[i].p = p0 * exp(y * log(p1/p0));
      } else {
	Real p0q = pow(p0, qSamp+1);
	Real p1q = pow(p1, qSamp+1);
	p[i].p = pow(y*p0q + (1-y)*p1q, 1/(qSamp+1));
      }
    } else {
      p[i].p = p0;
    }

    // Set weight scaling
    p[i].w = pow(p[i].p, q-qSamp);
    w += p[i].w;
  }

  // Rescale weights to get correct total
  Real scale = nRate() * (t1 - t0) / w;
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (IdxType i=0; i<n; i++) {
    p[i].w *= scale;
    p[i].wInj = p[i].w;
  }  
  // Return
  return p;
}

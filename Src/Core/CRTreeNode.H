/**
 * @file CRTreeNode.H
 * @brief Data structures to describe nodes in the CR tree
 *
 * @details
 * This method defines data structures, and some associated bit masks,
 * to represent nodes of the CR tree.
 */

#ifndef _CRTREENODE_H_
#define _CRTREENODE_H_

#include "FieldQty.H"
#include "../Definitions.H"
#include "../Utils/RealBox.H"
#include "../Utils/RealTensor2.H"
#include "../Utils/Types.H"
#include "../Utils/Vec3.H"

namespace criptic {

  // Flag values for bitmasking indices. The binary representations
  // of these variables are:
  // rootNode      = 000000...
  // nullNode      = 111111...
  // extNode       = 100000...
  constexpr IdxType rootNode = 0;       /**< Index of root node */
  constexpr IdxType nullNode = maxIdx;  /**< Flag value for invalid node */
  constexpr IdxType nullPacket = maxIdx; /**< Flag for invalid packet */
  constexpr IdxType extNode = ~(nullNode >> 1); /**< Flag for node on another
						   MPI rank */

  /**
   * @brief Struct representing a node of the global KD tree
   * @details
   * This structure represents a node of the global KD tree, which
   * organizes the data across all MPI ranks, and is replicated on
   * each rank.
   */
  typedef struct {
    IdxType start;  /**< Index of first packet in the node */
    IdxType count;  /**< Number of packets in this node on this MPI rank */
    IdxType srcStart; /**< Index of first source in the node */
    IdxType srcCount; /**< Number of sources in this node on this MPI rank */
    int owner;      /**< Which MPI rank owns this node */
    int splitDim;   /**< Dimension along which node splits; a value
		       of -1 indicates the node is a leaf */
    Real splitVal;  /**< Value at which the node splits */
    RealBox box;    /**< Bounding box for node */
  } GlobalNode;

  /**
   * @brief Struct representing a node of the local KD tree
   * @details
   * This structure represents a node of the local KD tree, which
   * organizes the data within a single MPI rank.
   */
  typedef struct LocalNodeStruct_ {
    IdxType start;  /**< Index of first packet in the node */
    IdxType count;  /**< Number of packets in this node */
    IdxType lchild; /**< Index of left child */
    IdxType rchild; /**< Index of right child */
    IdxType parent; /**< Index of parent */
    IdxType order;  /**< Order of this leaf in a breadth-first
		       traversal of the tree; set to null for
		       non-leaf nodes */
    int splitDim;   /**< Dimension along which node splits; a value
		       of -1 indicates the node is a leaf */
    Real splitVal;  /**< Value at which the node splits */
    RealBox box;    /**< Bounding box for node */
    RealBox bnd;    /**< Tight bounding box for node */
    Real wSum;      /**< Sum of CR packet weights */
    Real w2Sum;     /**< Sum of squares of CR packet weights */
    RealVec cm;     /**< Box "center of mass" determined by
		       packet weights */  
    RealTensor2 cov;   /**< CR packet-weighted covariance matrix */
    FieldQty fQ[TREE_RIGIDITY_BINS]; /**< Total contribution of
					packets in a node to field
					quantities, counting only
					packets above specified
					rigidity values */
    /**
     * @brief Flag if this node refers to packets on an external rank
     * @returns True if this node refers to packets on an external rank
     * @details
     * This routine returns true if and only if the this node in the
     * tree has children, but those children reside on another MPI
     * rank and thus this node cannot be opened on the local rank.
     */
    constexpr bool ext() const {
      return (lchild != nullNode) && (lchild & extNode);
    }

    /**
     * @brief Rank that owns the children of an external node
     * @returns Rank that owns the children of an external node
     * @details
     * The results are only meaningful if called on a node for which
     * ext() returns true. Results are undefined if called on a node
     * for which ext() returns false. 
     */
    constexpr int rank() const {
      return (int) (lchild & (~extNode));
    }
    
  } LocalNode;

}

#endif
// _CRTREENODE_H_

// Implementation of the geometry class

#include <cmath>
#include "Geometry.H"
#include "../MPI/MPIUtil.H"

using namespace std;
using namespace criptic;

Geometry::Geometry(const ParmParser &pp) :
  bcLo(openBC),
  bcHi(openBC)
{

  // First get BC type on low side, from the geometry.lo keyword
  vector<string> bcLoStr;
  bool hasLoBC = pp.query("geometry.lo_type", bcLoStr);
  bool allLoOpen = true;
  if (hasLoBC) {
    if (bcLoStr.size() != 3)
      MPIUtil::haltRun("Geometry: expected three values for "
		       "geometry.lo_type", errBadInputFile);
    for (int i=0; i<3; i++) {
      if (bcLoStr[i] == "open")
	bcLo[i] = openBC;
      else if (bcLoStr[i] == "absorbing")
	bcLo[i] = absorbingBC;
      else if (bcLoStr[i] == "reflecting")
	bcLo[i] = reflectingBC;
      else if (bcLoStr[i] == "periodic")
	bcLo[i] = periodicBC;
#ifdef TRACK_PITCH_ANGLE
      else if (bcLoStr[i] == "reflectingPA")
	bcLo[i] = reflectingPitchAngleBC;
#endif
      else {
	string errMsg = "Geometry: unknown boundary condition type: " +
	  bcLoStr[i];
	MPIUtil::haltRun(errMsg, errBadInputFile);
      }
      if (bcLo[i] != openBC) allLoOpen = false;
    }
  }
  
  // Repeat on high side
  vector<string> bcHiStr;
  bool hasHiBC = pp.query("geometry.hi_type", bcHiStr);
  bool allHiOpen = true;
  if (!hasHiBC) {
    // If low BC is periodic in a given direction, default value of hi
    // BC in that direction is also periodic
    for (int i=0; i<3; i++)
      if (bcLo[i] == periodicBC) bcHi[i] = periodicBC;
  } else {
    if (bcHiStr.size() != 3)
      MPIUtil::haltRun("Geometry: expected three values for "
		       "geometry.hi_type", errBadInputFile);
    for (int i=0; i<3; i++) {
      if (bcHiStr[i] == "open")
	bcHi[i] = openBC;
      else if (bcHiStr[i] == "absorbing")
	bcHi[i] = absorbingBC;
      else if (bcHiStr[i] == "reflecting")
	bcHi[i] = reflectingBC;
      else if (bcHiStr[i] == "periodic")
	bcHi[i] = periodicBC;
#ifdef TRACK_PITCH_ANGLE
      else if (bcHiStr[i] == "reflectingPA")
	bcHi[i] = reflectingPitchAngleBC;
#endif
      else {
	string errMsg = "Geometry: unknown boundary condition type: " +
	  bcHiStr[i];
	MPIUtil::haltRun(errMsg, errBadInputFile);
      }
      if (bcHi[i] != openBC) allHiOpen = false;
    }
  }
  
  // Error check: make sure that if the low boundary condition says
  // periodic, the high one does too, and vice-versa
  for (int i=0; i<3; i++) {
    if ((bcLo[i] == periodicBC && bcHi[i] != periodicBC) ||
	(bcLo[i] != periodicBC && bcHi[i] == periodicBC)) {
      MPIUtil::haltRun("Geometry: if one of geometry.lo_type and "
		       "geometry.hi_type is periodic, coresponding "
		       "value on other face must be periodic as well",
		       errBadInputFile);
    }
  }

  // Set flags
  periodicity = (bcLo == periodicBC);
  hasPeriodicDim = periodicity.max();

  // Next check for size of domain; this is mandatory if boundary
  // condition is not open in any direction, and is allowed but
  // ignored in any direction where the boundary is open.
  bool hasProbLo = pp.query("geometry.prob_lo", box.lo);
  bool hasProbHi = pp.query("geometry.prob_hi", box.hi);
  if (!hasProbLo && !allLoOpen)
    MPIUtil::haltRun("Geometry: did not find required geometry.prob_lo "
		     "entry in input file", errBadInputFile);
  if (!hasProbHi && !allHiOpen)
    MPIUtil::haltRun("Geometry: did not find required geometry.prob_hi "
		     "entry in input file", errBadInputFile);

  // If domain is infinite or semi-infinite, fill in flag values
  for (int i=0; i<3; i++) {
    if (bcLo[i] == openBC) box.lo[i] = -maxReal;
    if (bcHi[i] == openBC) box.hi[i] = maxReal;
  }

  // Set domain size
  for (int i=0; i<3; i++) {
    if (bcLo[i] != openBC && bcHi[i] != openBC)
      probSize[i] = box.hi[i] - box.lo[i];
    else
      probSize[i] = maxReal;
    if (probSize[i] <= 0.0)
      MPIUtil::haltRun("Geometry: geometry.prob_hi values must be larger "
		       "than geometry.prob_lo values", errBadInputFile);
  }

}



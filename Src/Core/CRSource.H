/**
 * @file CRSource.H
 * @brief Class to define a source of CRs
 *
 * @details
 * This module defines a class to hold cosmic ray sources, and do
 * computations on them related to injection of CRs.
 */

#ifndef _CRSOURCE_H_
#define _CRSOURCE_H_

#include <cmath>
#include <gsl/gsl_sf.h>
#include "CRPacket.H"
#include "../Utils/Constants.H"
#include "../Utils/PartTypes.H"
#include "../Utils/RngThread.H"
#include "../Utils/SR.H"
#include "../Utils/Types.H"

namespace criptic {

  /**
   * @class CRSource
   * @brief A class to describe a source of CR packets
   * @details
   * This is a trivial class that holds data to describe a source of
   * CR packets. Sources produce packets at a rate per unit time per
   * unit momentum d^2 n / dp dt = k * p^q over a momentum range (p0,
   * p1).
   */
  class CRSource {

  public:

    // Data
    partTypes::pType type;   /**< type of particle */
    IdxType uniqueID;        /**< unique ID number */
    Real p0;                 /**< injected CR momentum lower limit */
    Real p1;                 /**< injected CR momentum upper limit */
    Real q;                  /**< powerlaw index on p distribution */
    Real k;                  /**< injection rate coefficient */
#ifdef TRACK_PITCH_ANGLE
    Real mu0;                /**< minimum pitch angle of injected CRs */
    Real mu1;                /**< maximum pitch angle of injected CRs */
#endif
    RealVec v;               /**< source velocity */
    RealVec a;               /**< source acceleration */

    /**
     * @brief Returns kinetic energy corresponding to minimum momentum
     * @return Kinetic energy corresponding to minimum momentum
     */
    Real T0() const {
      return sr::T_from_p(type, p0);
    }

    /**
     * @brief Returns kinetic energy corresponding to maximum momentum
     * @return Kinetic energy corresponding to maximum momentum
     */
    Real T1() const {
      return sr::T_from_p(type, p1);
    }

    /**
     * @brief Set the momentum lower limit by specifying the kinetic energy
     * @param T0 Kinetic energy of corresponding to the momentum lower limit
     */
    void set_T0(const Real T0) {
      p0 = sr::p_from_T(type, T0);
    }
    
    /**
     * @brief Set the momentum upper limit by specifying the kinetic energy
     * @param T1 Kinetic energy of corresponding to the momentum upper limit
     */
    void set_T1(const Real T1) {
      p1 = sr::p_from_T(type, T1);
    }

    /**
     * @brief Returns total energy corresponding to minimum momentum
     * @return Total energy corresponding to minimum momentum
     */
    Real E0() const {
      return sr::E_from_p(type, p0);
    }

    /**
     * @brief Returns total energy corresponding to maximum momentum
     * @return Total energy corresponding to maximum momentum
     */
    Real E1() const {
      return sr::E_from_p(type, p1);
    }

    /**
     * @brief Returns rigidity correspondning to minimum momentum
     * @return Rigidity corresponding to minimum momentum
     */
    Real R0() const {
      return p0 / partTypes::charge[type];
    }
    
    /**
     * @brief Returns rigidity correspondning to minimum momentum
     * @return Rigidity corresponding to minimum momentum
     */
    Real R1() const {
      return p1 / partTypes::charge[type];
    }
    
    /**
     * @brief Returns total CR injection rate per unit time
     * @return CR injection rate per unit time
     * @details
     * This function returns integral_{p0}^{p1} k p^q dp, which is the
     * total number of CR packets produced per unit time.
     */
    Real nRate() const {
      if (p1 <= p0) return k;
      else if (q == -1.0) return k * std::log(p1/p0);
      else return k * (std::pow(p1, q+1) - std::pow(p0, q+1)) / (q+1);
    }

    /**
     * @brief Returns total linear momentum injection rate per time
     * @return CR momentum injected per unit time
     * @details
     * This function returns integral_{p0}^{p1} p * k p^q dp, which is the
     * total linear momentum injected per unit time by the source.
     */
    Real pRate() const {
      if (p1 <= p0) return k * p0;
      else if (q == -2.0) return k * std::log(p1/p0);
      else return k * (std::pow(p1, q+2) - std::pow(p0, q+2)) / (q+2);
    }

    /**
     * @brief Returns total kinetic energy injection rate per time
     * @return Kinetic energy injection rate per unit time
     * @details
     * This function returns integral_{p0}^{p1} T(p) * k p^q dp, where
     * T(p) is the kinetic energy carried by a CR packet of momentum
     * p. Thus this quantity is the total kinetic energy carried by
     * the injected CRs per unit time.
     */
    Real TRate() const {
      if (p1 <= p0) return k * sr::T_from_p(type, p0);
      else {
	Real m = partTypes::mass[type];
	Real m2 = m*m;
	Real p02 = p0*p0;
	Real p12 = p1*p1;
	if (q == -2.0) {
	  return k * m * (1/p1 - 1/p0) +
	    std::sqrt(1 + m2/p02) - std::sqrt(1 + m2/p12) +
	    std::asinh(p1/m) - std::asinh(p0/m);
	} else if (q == 0.0) {
	  // Use series expansions to avoid underflows
	  Real p1fac = (m2/p12 > 1e-6) ?
	    std::log( p1 * (std::sqrt(1 + m2/p12) - 1) ) :
	    std::log( m2 / (2 * p1) - m2*m2 / (8 * p1 * p12));
	  Real p0fac = (m2/p12 > 1e-6) ?
	    std::log( p0 * (std::sqrt(1 + m2/p02) - 1) ) :
	    std::log( m2 / (2 * p0) - m2*m2 / (8 * p0 * p02));
	  return k * 0.5 * (p1 * std::sqrt(p12 + m2) -
			    p0 * std::sqrt(p02 + m2) -
			    2 * m * (p1 - p0) -
			    m2 * (p1fac - p0fac) );
	} else {
	  Real a = -1-q/2.0;
	  Real b = (1+q)/2.0;
	  Real term1 = m/(q+1) *
	    (std::pow(p0, q+1) - std::pow(p1, q+1));
	  Real term2 = std::pow(m, q+2) * gsl_sf_beta(a, b) *
	    ( gsl_sf_beta_inc( a, b, 1/(1+p02/m2) ) -
	      gsl_sf_beta_inc( a, b, 1/(1+p12/m2) ) );
	  return k * (term1 + term2);
	}
      }
    }

    /**
     * @brief Return mean CR momentum injected
     * @return Mean momentum of CRs this source produces
     * @details
     * This returns the mean momentum per CR injected. It is equal to
     * CRSource::pRate() / CRSource::nRate().
     */
    Real pBar() const {
      return pRate() / nRate();
    }

    /**
     * @brief Return mean CR kinetic energy injected
     * @return Mean kinetic energy of CRs this source produces
     * @details
     * This returns the mean kineticenergy per CR injected. It is equal to
     * CRSource::TRate() / CRSource::nRate().
     */
    Real TBar() const {
      return TRate() / nRate();
    }

    /**
     * @brief Return the statistical weight of this source
     * @param qSamp Sampling spectral index
     * @return Weight of this source
     * @details
     * This function returns the weight that should be assigned to
     * this source in a scheme where CR momentum is sampled such that
     * the number of packets in a particular momentum range p to p +
     * dp is proportional to p^qSamp.
     */
    Real wgt(const Real qSamp) const {
      if (p1 <= p0)
	return k / std::pow(p0, qSamp);
      else if (qSamp == -1.0)
	return k * std::log(p1/p0) / std::pow(p0, qSamp);
      else
	return k * (std::pow(p1, qSamp+1) - std::pow(p0, qSamp+1)) /
	  ((qSamp+1) * std::pow(p0, qSamp));
    }

    /**
     * @brief Set the rate coefficient to produce a specified luminosity
     * @param lum Luminosity to set source to
     * @param codeUnits Is lum in code units?
     * @details
     * This method changes the value of the rate coefficient k to a
     * value such that the kinetic luminosity of the source (the value
     * returned by CRSource::TRate()) is equal to the specified
     * luminosity. The luminosity can be specified either in code
     * units or in metric units (ergs / s or Watts depending on
     * whether the code was compiled with CGS or MKS units).
     */
    void setLum(const Real lum, const bool codeUnits = false) {
      k = 1.0;
      if (codeUnits) k = lum / TRate();
      else k = (lum / constants::mp_c2) / TRate();
    }

    /**
     * @brief Draw CR packets
     * @param n Number of packets to draw
     * @param t0 Start of time interval over which packets are drawn
     * @param t1 End of time interval over which packets are drawn
     * @param qSamp Momentum sampling powerlaw index
     * @param rng criptic::RngThread random number generator
     * @return A std::vector of n CR packets
     * @details
     * This method randomly generates n CR packets from the source
     * momentum distribution. The number of packets per unit momentum
     * will be distributed as dn/dp ~ p^qSamp, with the constant of
     * proportionality set to produce the target number n; the packet
     * weights will be set to reflect the true injection rate and
     * momentum distribuiton of the source.
     */
    std::vector<CRPacket> draw(const IdxType n,
			       const Real t0,
			       const Real t1,
			       const Real qSamp,
			       const RngThread& rng) const;
  };

}

#endif
// _CRSOURCE_H_

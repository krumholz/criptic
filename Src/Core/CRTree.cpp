#include "AdvancePacket.H"
#include "CRTree.H"
#include <algorithm>
#include <cassert>
#include <list>
#ifdef _OPENMP
#   include <omp.h>
#endif

using namespace std;
using namespace criptic;

// Trivial little function that says whether a number is a power of
// two
inline bool isPowerTwo(const IdxType i) {
  return (i != 0) && ((i & (i-1)) == 0);
}

///////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////

CRTree::CRTree(const ParmParser& pp,
	       const Geometry& geom_,
	       const gas::Gas& gasBG_,
	       const propagation::Propagation& prop_,
	       const Losses& loss_,
	       const RngThread& rng_) :
  geom(geom_),
  gasBG(gasBG_),
  prop(prop_),
  loss(loss_),
  rng(rng_)
#ifdef ENABLE_MPI
  , comm(this)
#endif
{
  // Initialize ID counter
  uniqueID = 0;

  // Extract info we need from input deck

  // Verbosity
  verbosity = 1;
  pp.query("verbosity", verbosity);

  // Do we need field quantities and their gradients
  needFQ = prop.fieldQtyNeed() >= needFieldQty;
  needFQG = prop.fieldQtyNeed() == needFieldQtyGrad;
  if (!needFQ) {
    qty.resize(1);
    h.resize(1);
  }
  if (!needFQG) qtyGrad.resize(1);

  // Number of "extra" levels in the top tree, beyond the minimum
  // needed to give at least 1 node to each rank -- defaults to zero
  // if the number of ranks is a power of 2, or 2 otherwise
  globTreeExtLev = isPowerTwo(MPIUtil::nRank) ? 0 : 2;
  pp.query("tree.extra_global_levels", globTreeExtLev);
  if (globTreeExtLev < 0)
    MPIUtil::haltRun("CRTree: tree.extra_global_levels must be >= 0",
		     errBadInputFile);
		   
  // Number of samples per rank used to partition the data in the
  // global tree
  partitionSamples = 256;
  pp.query("tree.partition_samples_per_rank", partitionSamples);
  if (partitionSamples < 4)
    MPIUtil::haltRun("CRTree: tree.partition_samples_per_rank must be >= 4",
		     errBadInputFile);

  // Minimum individual packet time step
  packetDtMin = 0.0;
  pp.query("integrator.packet_dt_min", packetDtMin);
  
  // CR momentum sampling index
  qSamp = -1.0;
  pp.query("integrator.qSamp", qSamp);

  // CR packet sampling rate
  packetRate = 0.0;
  pp.query("integrator.packet_rate", packetRate);

  // CR packet sampling strategy for multiple species
  equalSampling = 1;
  pp.query("integrator.equal_sampling", equalSampling);
  pp.query("integrator.sampling_ratio", samplingRatio);
  if (samplingRatio.size() > 0 &&
      samplingRatio.size() != (IdxType) partTypes::nPartType)
    MPIUtil::haltRun("CRTree: integrator.sampling_ratio must have one "
		     "entry per CR species",
		     errBadInputFile);

  // Time step controls
  cMinWgt = 0.5;
  pp.query("integrator.c_min_wgt", cMinWgt);
  if (cMinWgt < 0 || cMinWgt > 1)
    MPIUtil::haltRun("CRTree: integrator.c_min_wgt must be in [0,1]",
		     errBadInputFile);
  cStep = 0.25;
  pp.query("integrator.c_step", cStep);
  if (cStep <= 0)
    MPIUtil::haltRun("CRTree: integrator.c_step must be > 0",
		     errBadInputFile);

  // Error tolerance
  errTol = 1.0e-4;
  pp.query("integrator.err_tol", errTol);
  if (errTol <= 0)
    MPIUtil::haltRun("CRTree: integrator.err_tol must be > 0",
		     errBadInputFile);

  // Packet minimum weight, momentum, and energy; note that momentum
  // and energy are assumed to be units of GeV/c and GeV,
  // respectively, which are then converted to code units.
  wFracMin = 1.0e-3;  
  pp.query("integrator.w_frac_min", wFracMin);
  pMin = 0.0;
  pp.query("integrator.p_min", pMin);
  pMin *= units::GeV / constants::mp_c2;
  TMin = units::MeV / constants::mp_c2;
  pp.query("integrator.T_min", TMin);
  TMin *= units::GeV / constants::mp_c2;

  // Kernel density estimation parameters
  nNgbEff = 1024;
  pp.query("tree.n_ngb_eff", nNgbEff);
  kdeTol = 0.1;
  pp.query("tree.kde_tol", kdeTol);
  hDither = geom.xSize().min();
  if (hDither != maxReal) hDither /= 1e6;
  else hDither = 0.0;
  pp.query("tree.source_dither", hDither);

  // If field quantities or their gradients are used, check that KDE
  // tolerance must be non-negative
  if (kdeTol < 0 && needFQ)
    MPIUtil::haltRun("CRTree: tree.kde_tol must be >= 0",
		     errBadInputFile);

  // If field quantities are used, check that the source dither size
  // is positive
  if (hDither <= 0.0 && needFQ)
    MPIUtil::haltRun("CRTree: tree.source_dither must be set explicitly "
		     "to a positive value if propagation model uses "
		     "field quantity gradients and boundary conditions "
		     "are open",
		     errBadInputFile);
}


///////////////////////////////////////////////////////////////
// Methods to add packets and sources to the tree
///////////////////////////////////////////////////////////////

void CRTree::addPacketsAndSources(const vector<RealVec>& x_,
				  const vector<CRPacket>& pd_,
				  const vector<RealVec>& xSrc_,
				  const vector<CRSource>& src_) {

  // Safety assertions
  assert(x_.size() == pd_.size());
  assert(xSrc_.size() == src_.size());

  // Save old packet and source numbers
  IdxType npOld = xBuf.size();
  IdxType nsOld = xSrcBuf.size();

  // Copy data to buffers
  copy(x_.begin(), x_.end(), back_inserter(xBuf));
  copy(pd_.begin(), pd_.end(), back_inserter(pdBuf));
  copy(xSrc_.begin(), xSrc_.end(), back_inserter(xSrcBuf));
  copy(src_.begin(), src_.end(), back_inserter(srcBuf));

  // Resize quantity arrays, and add leaf assignment information
  if (needFQ) {
    qtyBuf.resize( qtyBuf.size() + x_.size() );
    hBuf.resize( hBuf.size() + x_.size() );
  }
  if (needFQG) qtyGradBuf.resize( qtyGradBuf.size() + x_.size() );

  // Synchronize uniqueID pointer across ranks; the way this works is
  // that every rank starts with the same unique ID pointer, and then
  // we do an AllToAll where every rank tells all the other ranks how
  // many packets and sources it is adding, then all ranks offset
  // their uniqueIDs by the number being added on lower ranks, e.g.,
  // if rank 0 is adding 50 packets, rank 1 is adding 100 packets, and
  // rank 2 is adding 200 packets, then rank 0 will increase its
  // uniqueID by 0, rank 1 will increase its uniqueID by 50, and rank
  // 2 will increase its uniqueID by 150.
#ifdef ENABLE_MPI
  vector<IdxType> nAdd(MPIUtil::nRank);
  nAdd[MPIUtil::myRank] = pd_.size();
  MPI_Alltoall(MPI_IN_PLACE, 1, MPIUtil::MPI_Idx, nAdd.data(), 1,
	       MPIUtil::MPI_Idx, MPI_COMM_WORLD);
  for (int i=0; i<MPIUtil::myRank; i++) uniqueID += nAdd[i];
#endif

  // Assign uniqueID's to the packets and sources we're adding
  for (IdxType i=npOld; i<pdBuf.size(); i++) {
    pdBuf[i].uniqueID = uniqueID;
    uniqueID++;
  }
  for (IdxType i=nsOld; i<srcBuf.size(); i++) {
    srcBuf[i].uniqueID = uniqueID;
    uniqueID++;
  }
}

void CRTree::addPackets(const vector<RealVec>& x_,
			const vector<CRPacket>& pd_) {

  vector<RealVec> xSrc_;
  vector<CRSource> src_;
  addPacketsAndSources(x_, pd_, xSrc_, src_);
}

void CRTree::addSources(const vector<RealVec>& xSrc_,
			const vector<CRSource>& src_) {
  vector<RealVec> x_;
  vector<CRPacket> pd_;
  addPacketsAndSources(x_, pd_, xSrc_, src_);
}


///////////////////////////////////////////////////////////////
// Methods to rebuild the tree
///////////////////////////////////////////////////////////////
void CRTree::rebuild() {
  
  // If verbose enough, print status
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: rebuilding tree..." << endl;

  // Step 1: merge the buffers of incoming packets and sources into
  // the main data holders and clear the incoming buffers
  copy(xBuf.begin(), xBuf.end(), back_inserter(x));
  copy(pdBuf.begin(), pdBuf.end(), back_inserter(pd));
  copy(xSrcBuf.begin(), xSrcBuf.end(), back_inserter(xSrc));
  copy(srcBuf.begin(), srcBuf.end(), back_inserter(src));
  xBuf.clear();
  pdBuf.clear();
  qtyBuf.clear();
  qtyGradBuf.clear();
  hBuf.clear();
  xSrcBuf.clear();
  srcBuf.clear();

  // Step 2: get the size of the bounding box on all packets and
  // sources across all MPI ranks
  setupDomain();

  // Step 3: build the global tree and assign parts of it to different
  // MPI ranks
  buildGlobalTree();

  // Step 4: exchange packets and sources so that each rank owns the
  // ones that fall within its leaves
#ifdef ENABLE_MPI
  exchange();
#endif

  // Steps 5 - 7: we can skip these if we don't need to compute field
  // quantities
  if (needFQ) {

    // Step 5: allocate storage to hold field quantities
    qty.resize(x.size());
    h.resize(x.size());
    if (needFQG) qtyGrad.resize(x.size());

    // Step 6: now that data have been partitioned, build the local
    // packet tree
    buildLocalTree();

    // Step 7: sum the weights and compute tight bounding boxes in the
    // local tree, exchanging data between MPI ranks if necessary
    sumTree();
    
  }

  // If verbose enough, print status
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: rebuilding complete" << endl;
}


void CRTree::setupDomain() {

  // Initialize limits on domain
  Real &domLoX = domLo[0];
  Real &domLoY = domLo[1];
  Real &domLoZ = domLo[2];
  Real &domHiX = domHi[0];
  Real &domHiY = domHi[1];
  Real &domHiZ = domHi[2];
  Real &logR0_ = logR0;
  Real &logR1_ = logR1;
  domLoX = domLoY = domLoZ = logR0_ = maxReal;
  domHiX = domHiY = domHiZ = logR1_ = -maxReal;

  // First compute bounding box in position and energy from local packets
#ifdef _OPENMP
#pragma omp parallel for reduction(min:domLoX,domLoY,domLoZ,logR0_) reduction(max:domHiX,domHiY,domHiZ,logR1_)
#endif
  for (IdxType i=0; i<x.size(); i++) {
    if (domLoX > x[i][0]) domLoX = x[i][0];
    if (domLoY > x[i][1]) domLoY = x[i][1];
    if (domLoZ > x[i][2]) domLoZ = x[i][2];
    if (domHiX < x[i][0]) domHiX = x[i][0];
    if (domHiY < x[i][1]) domHiY = x[i][1];
    if (domHiZ < x[i][2]) domHiZ = x[i][2];
    Real logR = log(pd[i].R());
    if (logR0_ > logR) logR0_ = logR;
    if (logR1_ < logR) logR1_ = logR;
  }

  // Now add local sources
#ifdef _OPENMP
#pragma omp parallel for reduction(min:domLoX,domLoY,domLoZ,logR0_) reduction(max:domHiX,domHiY,domHiZ,logR1_)
#endif
  for (IdxType i=0; i<src.size(); i++) {
    if (domLoX > xSrc[i][0]) domLoX = xSrc[i][0];
    if (domLoY > xSrc[i][1]) domLoY = xSrc[i][1];
    if (domLoZ > xSrc[i][2]) domLoZ = xSrc[i][2];
    if (domHiX < xSrc[i][0]) domHiX = xSrc[i][0];
    if (domHiY < xSrc[i][1]) domHiY = xSrc[i][1];
    if (domHiZ < xSrc[i][2]) domHiZ = xSrc[i][2];
    Real lR0 = log(src[i].R0());
    Real lR1 = log(src[i].R1());
    if (logR0_ > lR0) logR0_ = lR0;
    if (logR1_ < lR1) logR1_ = lR1;
  }

  // In an MPI calculation, synchronize this information across all
  // ranks
#ifdef ENABLE_MPI
  MPI_Allreduce(MPI_IN_PLACE, domLo.data(), 3, MPIUtil::MPI_Real, MPI_MIN,
		MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, domHi.data(), 3, MPIUtil::MPI_Real, MPI_MAX,
		MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &logR0, 1, MPIUtil::MPI_Real, MPI_MIN,
		MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &logR1, 1, MPIUtil::MPI_Real, MPI_MAX,
		MPI_COMM_WORLD);
#endif

  // Synchronize total numbers and weights of all packets and sources
  totPacket = pd.size();
  totDeletedPacket = pdDel.size();
  totSrc = src.size();
  totSrcWgt = 0.0;
  totSrcWgtSpec.assign(partTypes::nPartType, 0);
  for (IdxType i = 0; i<src.size(); i++) {
    Real wgt = src[i].wgt(qSamp);
    totSrcWgt += wgt;
    totSrcWgtSpec[src[i].type] += wgt;
  }
#ifdef ENABLE_MPI
  MPI_Allreduce(MPI_IN_PLACE, &totPacket, 1, MPIUtil::MPI_Idx,
		MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &totDeletedPacket, 1, MPIUtil::MPI_Idx,
		MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &totSrc, 1, MPIUtil::MPI_Idx,
		MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &totSrcWgt, 1, MPIUtil::MPI_Real,
		MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, totSrcWgtSpec.data(), partTypes::nPartType,
		MPIUtil::MPI_Real, MPI_SUM, MPI_COMM_WORLD);
#endif

  // Issue warning if sources are present but total weight is zero,
  // since this likely indicates user error
  if (src.size() > 0 && totSrcWgt == 0.0) {
    cout << "CRTree::setupDomain: warning: sources are present but "
	 << "total luminosity of all sources is zero"
	 << endl;
  }

  // Compute energy grid size
  dlogR = (logR1 - logR0) / TREE_RIGIDITY_BINS;
}



void CRTree::buildGlobalTree() {

  // Status message
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: building global domain partition tree" << endl;
  
  // Step 1: determine how many levels will be in the global tree, and
  // allocate the appropriate storage
  globTreeMaxLev = 0;
  while (1 << globTreeMaxLev < MPIUtil::nRank) globTreeMaxLev++;
  globTreeMaxLev += globTreeExtLev;
  globNodes.resize( (1 << (globTreeMaxLev+1)) - 1);

  // Step 2: set up the parts of the root node we can; remainder will
  // be filled in after the local tree is built
  globNodes[rootNode].start = 0;
  globNodes[rootNode].srcStart = 0;
  globNodes[rootNode].count = x.size();
  globNodes[rootNode].srcCount = xSrc.size();
  globNodes[rootNode].box.lo = domLo;
  globNodes[rootNode].box.hi = domHi;
  globNodes[rootNode].owner = -1;
  globNodes[rootNode].splitDim = -1;

  // Step 3: now build the rest of the global tree
  for (IdxType lev = 0; lev < globTreeMaxLev; lev++) {
    for (SignedIdxType i = (1 << lev) - 1; i < (1 << (lev+1)) - 1; i++) {
      
      // Step 3a: compute the way to split this node
      GlobalNode &nd = globNodes[i];
      computeSplit(nd);
      
      // Step 3b: now partition the packets and sources based on the
      // split
      IdxType splitIdx = partitionGlobalNode(nd, nd.start, nd.count, x, pd);
      IdxType srcSplitIdx = partitionGlobalNode(nd, nd.srcStart, nd.srcCount,
						xSrc, src);

      // Step 3c: initialize the index pointers, counts, owners, and
      // bounding boxes of the child nodes
      GlobalNode &l = globNodes[2*i+1];
      GlobalNode &r = globNodes[2*i+2];
      l.start = nd.start;
      l.count = splitIdx - l.start;
      r.start = splitIdx;
      r.count = nd.start + nd.count - splitIdx;
      l.srcStart = nd.srcStart;
      l.srcCount = srcSplitIdx - l.srcStart;
      r.srcStart = srcSplitIdx;
      r.srcCount = nd.srcStart + nd.srcCount - srcSplitIdx;
      l.box = nd.box;
      l.box.hi[nd.splitDim] = nd.splitVal;
      r.box = nd.box;
      r.box.lo[nd.splitDim] = nd.splitVal;
      l.owner = r.owner = -1;
      l.splitDim = r.splitDim = -1;
    }
  }

  // Step 4: assign each leaf to an MPI rank, and record details about
  // which parts of the tree the local MPI rank owns
  globTreeNLeaf = 1 << globTreeMaxLev;
  globTreeLeafPtr = globTreeNLeaf - 1;
  globTreeMyLeafPtr = nullIdx;
  globTreeNOwned = 0;
  IdxType leafPerRank = globTreeNLeaf / MPIUtil::nRank;
  IdxType leafRemainder = globTreeNLeaf - MPIUtil::nRank * leafPerRank;
  IdxType ptr = globTreeLeafPtr;
  int rank = 0;
  while (ptr < globNodes.size()) {
    IdxType nLeafThisRank = leafPerRank;
    if (leafRemainder > 0) {
      nLeafThisRank++;
      leafRemainder--;
    }
    for (IdxType i=0; i<nLeafThisRank; i++) {
      if (rank == MPIUtil::myRank && globTreeMyLeafPtr == nullIdx)
	globTreeMyLeafPtr = ptr;
      else if (rank != MPIUtil::myRank &&
	       globTreeNOwned == 0 &&
	       globTreeMyLeafPtr != nullIdx)
	globTreeNOwned = ptr - globTreeMyLeafPtr;
      globNodes[ptr++].owner = rank;
      if (ptr == globNodes.size()) break;
    }
    rank++;
  }
  if (globTreeNOwned == 0)
    globTreeNOwned = globNodes.size() - globTreeMyLeafPtr;

  // Verbose status message
  if (MPIUtil::IOProc) {
    if (verbosity > 1)
      cout << "CRTree: completed construction of global tree with "
	   << globTreeMaxLev+1 << " levels and " << globTreeNLeaf
	   << " leaves" << endl << flush;
    if (verbosity > 2) {
      cout << "CRTree: full global tree is:" << endl;
      for (IdxType i = 0; i < globNodes.size(); i++) {
	GlobalNode& nd = globNodes[i];
	cout << "   node " << i << ": start = " << nd.start
	     << ", count = " << nd.count
	     << ", srcStart = " << nd.srcStart
	     << ", srcCount = " << nd.srcCount
	     << ", owner = " << nd.owner
	     << ", splitDim = " << nd.splitDim
	     << ", splitVal = " << nd.splitVal
	     << endl;
      }
    }
  }
}


void CRTree::computeSplit(GlobalNode& nd) {

  // Step 1: each MPI rank draws sample points that will be used to
  // produce the histogram used to partition the node; note that we
  // have to be careful to handle the case where we do not have any
  // packets or samples on this rank
  vector<RealVec> xSamp(partitionSamples * MPIUtil::nRank);
  if (nd.count + nd.srcCount == 0) {
    for (int i=0; i<partitionSamples; i++)
      xSamp[MPIUtil::myRank * partitionSamples + i] = maxReal;
  } else {
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i=0; i<partitionSamples; i++) {
      IdxType j = rng.uniformInt(0, nd.count + nd.srcCount - 1);
      if (j < nd.count)
	xSamp[MPIUtil::myRank * partitionSamples + i] = x[j + nd.start];
      else
	xSamp[MPIUtil::myRank * partitionSamples + i] =
	  xSrc[j - nd.count + nd.srcStart];
    }
  }
   
  // Step 2: sample points are shared across MPI ranks
#ifdef ENABLE_MPI
  MPI_Allgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL,
		xSamp.data(), partitionSamples, MPIUtil::MPI_RealVec,
		MPI_COMM_WORLD);
#endif

  // Step 3: discard invalid samples; handle special case where there
  // are no valid samples
  IdxType nValid = 0;
  for (IdxType i=0; i<xSamp.size(); i++) {
    if (xSamp[i][0] != maxReal) {
      if (i != nValid) xSamp[nValid] = xSamp[i];
      nValid++;
    }
  }
  if (nValid == 0) {
    nd.splitDim = 0;
    nd.splitVal = 0.5 * (nd.box.lo[0] + nd.box.hi[0]);
    return;
  }
  xSamp.resize(nValid);

  // Step 4: each rank computes the variance in each dimension; the
  // node will split along the dimension with the largest variances
  RealVec mean, var;
  Real& meanv0 = mean.v[0];
  Real& meanv1 = mean.v[1];
  Real& meanv2 = mean.v[2];
  Real& varv0 = var.v[0];
  Real& varv1 = var.v[1];
  Real& varv2 = var.v[2];
#ifdef _OPENMP
#pragma omp parallel for shared(xSamp) reduction(+:meanv0,meanv1,meanv2)
#endif
  for (IdxType i=0; i<xSamp.size(); i++) {
    meanv0 += xSamp[i][0];
    meanv1 += xSamp[i][1];
    meanv2 += xSamp[i][2];
  }
  mean /= xSamp.size();
#ifdef _OPENMP
#pragma omp parallel for shared(xSamp,mean) reduction(+:varv0,varv1,varv2)
#endif
  for (IdxType i=0; i<xSamp.size(); i++) {
    varv0 += (xSamp[i][0] - mean[0]) * (xSamp[i][0] - mean[0]);
    varv1 += (xSamp[i][1] - mean[1]) * (xSamp[i][1] - mean[1]);
    varv2 += (xSamp[i][2] - mean[2]) * (xSamp[i][2] - mean[2]);
  }
  nd.splitDim = var.argmax();

  // Step 5: now that we know the splitting dimension, we use the
  // sample points in that dimension to define the bins of the
  // histogram we will construct
  vector<Real> xBin(xSamp.size());
  for (IdxType i=0; i<xSamp.size(); i++) xBin[i] = xSamp[i][nd.splitDim];
  sort(xBin.begin(), xBin.end());
  
  // Step 6: make a histogram to count the number of packets in each
  // bin on this rank
  IdxType nHist = MPIUtil::nRank * partitionSamples + 2;
  vector<IdxType> hist(nHist);
#ifdef _OPENMP
  
  // Threaded version -- needs temporary storage to avoid
  // thread-locking when adding to the histogram
  vector<IdxType> histTmp(nHist * omp_get_max_threads()); // Temp storage
#pragma omp parallel shared(hist,histTmp,nHist,x,xBin,nd)
  {
    IdxType off = omp_get_thread_num() * nHist;
#pragma omp for 
    for (IdxType i = nd.start; i < nd.start + nd.count; i++) {
      if (x[i][nd.splitDim] <= xBin[0]) {
	histTmp[off]++;
      } else if (x[i][nd.splitDim] > xBin.back()) {
	histTmp[off + nHist - 1]++;
      } else {
	IdxType l=0, r=xBin.size()-1;
	while (r-l > 1) {
	  IdxType c = (l+r)/2;
	  if (x[i][nd.splitDim] < xBin[c]) r = c;
	  else l = c;
	}
	histTmp[off + r]++;
      }
    }
    // Reduce across temporary storage
    int nThread = omp_get_num_threads();
#pragma omp for
    for (IdxType i = 0; i < nHist; i++) {
      for (int j = 0; j < nThread; j++)
	hist[i] += histTmp[j * hist.size() + i];
    }
    
  }

#else

  // Non-threaded version
  for (IdxType i=nd.start; i<nd.start+nd.count; i++) {
    if (x[i][nd.splitDim] <= xBin[0])
      hist[0]++;
    else if (x[i][nd.splitDim] > xBin.back())
      hist[nHist-1]++;
    else {
      IdxType l=0, r=xBin.size()-1;
      while (r-l > 1) {
	IdxType c = (l+r)/2;
	if (x[i][nd.splitDim] < xBin[c]) r = c;
	else l = c;
      }
      hist[r]++;
    }
  }

#endif
  
  // Step 7: sum the histogram counts across MPI ranks
#ifdef ENABLE_MPI
  MPI_Allreduce(MPI_IN_PLACE, hist.data(), hist.size(),
		MPIUtil::MPI_Idx, MPI_SUM, MPI_COMM_WORLD);
#endif

  // Step 8: choose the split point to be the sample point closest to
  // the median of the histogram
  IdxType nNodeTot = 0;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:nNodeTot)
#endif
  for (IdxType i = 0; i < hist.size(); i++) nNodeTot += hist[i];
  IdxType cumCount = hist[0], ptr = 0;
  while (1) {
    if (cumCount >= nNodeTot/2) break;
    ptr++;
    cumCount += hist[ptr];
  }
  if (ptr == 0) nd.splitVal = xBin[0];
  else if (cumCount - nNodeTot/2 >
	   cumCount - hist[ptr] - nNodeTot/2) nd.splitVal = xBin[ptr-1];
  else nd.splitVal = xBin[ptr];
}


template <class U> 
IdxType CRTree::partitionGlobalNode(const GlobalNode& nd,
				    const IdxType start,
				    const IdxType count,
				    vector<RealVec>& x_,
				    vector<U>& dat) {

  // Handle special case of an empty node; in this case we just return
  // the starting index
  if (count == 0) return start;
  
  // Initialize pointers
  SignedIdxType l = start;
  SignedIdxType r = start + count - 1;

  // Iterate until partition is complete
  while (l < r) {

    // Move left pointer until it points to something that should be
    // on the right side of the partition; do the same with right
    // pointer
    while (x_[l][nd.splitDim] <= nd.splitVal && l < r) l++;
    while (x_[r][nd.splitDim] > nd.splitVal && l < r) r--;

    // If our pointers have converged, we're done
    if (l == r) break;

    // If we are here, exchange the points on the wrong side of the
    // partition
    swap(x_[l], x_[r]);
    swap(dat[l], dat[r]);

    // Move the pointers
    l++;
    r--;
  }

  // Return either the value at which l and r converged, or the one to
  // its immediate right, depending on whether the convergence point
  // belongs on the left or right of the partition
  if (x_[l][nd.splitDim] > nd.splitVal) return l;
  else return l+1;
}


#ifdef ENABLE_MPI
void CRTree::exchange() {
  
  // Verbose status message
  if (verbosity > 1) {
    if (MPIUtil::IOProc)
      cout << "CRTree: starting exchange, current counts are:" << endl;
    for (int rank=0; rank<MPIUtil::nRank; rank++) {
      if (MPIUtil::myRank == rank)
	cout << "   rank " << rank << ": " << nPacketLoc()
	     << " packets, " << nSourceLoc() << " sources" << endl;
      MPIUtil::barrier();
    }
  }

#if 0
  // We do communication here in three main steps. First, we go
  // through the tree and find all the leaves we do not own, and
  // record the locations in memory of the data to be exported to
  // each other rank; we also keep count of exactly how many
  // packets and sources we are sending to each rank for each
  // leaf. Second, we first communicate -- first telling each
  // destination rank how many packets and sources for each
  // leaf we are sending it, then sending the corresponding data.
  // Third, we re-arrange our own packet and source lists to
  // remove the data we have exported, and insert the data we
  // have imported.

  // Step 1: go through our local tree and for each leaf we do
  // not own, record how many packets are to be exported from
  // that leaf and to where, and record where in our lists of
  // packets and sources the data to be exported live. The
  // arrays we define here are to be filled with the following
  // information for each rank:
  // exportCount -- two entries for each leaf in the global
  //    tree owned, with the first giving the number of packets
  //    we will be sending to the rank that owns that leaf,
  //    and the second giving the number of sources we will be
  //    sending
  // rankPtr -- a pointer indicating where each rank's data
  //    in exportCount starts
  // rankCount -- a counter indicating how many entries in
  //    exportCount get sent to each rank
  // pktExportStart, pktExportLength -- one entry for each
  //    leaf in the global tree owned by each rank that
  //    contains any packets; the entry in pktExportStart
  //    give the location of these packets in our x and
  //    pd arrays, and the entry in pktExportBlocksize
  //    gives the number of packets
  // srcExportStart, srcExportLength -- same as pktExportStart
  //    and pktExportLenght, but for packets
  vector<int> exportCount(2 * globTreeNLeaf);
  vector<int> rankPtr(MPIUtil::nRank), rankCount(MPIUtil::nRank);
  vector<vector<int> > pktExportStart(MPIUtil::nRank),
    pktExportCount(MPIUtil::nRank);
  vector<vector<int> > srcExportStart(MPIUtil::nRank),
    srcExportCount(MPIUtil::nRank);

  // Loop over leaves
  int rlast = 0;
  rankPtr[0] = 0;
  for (IdxType i = 0; i < globTreeNLeaf; i++) {
    GlobalNode& nd = globNodes[i + globTreeLeafPtr];

    // Are we starting a new rank? If so, record values for rankPtr
    // and rankCount
    int r = nd.owner;
    if (r != rlast) {
      rankPtr[r] = i;
      rankCount[r-1] = i - rankPtr[r-1];
      rlast = r;
    }

    // If I own this leaf, nothing to export
     if (r == MPIUtil::myRank) continue;

    // I do not own this leaf, so record the counts and memory
    // locations for the node that does own it
    exportCount[r].push_back(nd.count);
    exportCount[r].push_back(nd.srcCount);
    if (nd.count > 0) {
      pktExportStart[r].push_back(nd.start);
      pktExportCount[r].push_back(nd.count);
    }
    if (nd.srcCount > 0) {
      srcExportStart[r].push_back(nd.srcStart);
      srcExportCount[r].push_back(nd.srcCount);
    }
  }

  // Step 2: do an all-to-all in which each rank sends to
  // all other ranks the number of packets and sources that
  // it will be exporting.

#else
  // Communication here takes place in two steps. The first is for
  // every rank to tell every other rank how many packets and sources
  // it will be sending. The second is to send those packets and
  // sources.
  
  // Step 1: create an array for this rank to communicate to each
  // other rank how many packets and sources we will be sending to
  // it. This array has two entries per leaf of the global tree, with
  // one entry containing the number of packets and the other the
  // number of sources. In the process of creating this array, we will
  // also create arrays specifying the number of integers we will be
  // sending to each rank, and the locations of those integers in the
  // send count array. At the same time, we will keep track of (1) the
  // total number of packets and sources that will be dispatched to
  // each rank, and (2) the total number of packets and sources that
  // will be sent to all ranks, since we will need these below to
  // create buffers.
  vector<IdxType> pktSrcSendCount(2*globTreeNLeaf);
  vector<int> sendCounts(MPIUtil::nRank), sendDisp(MPIUtil::nRank);
  vector<int> pktToRank(MPIUtil::nRank), srcToRank(MPIUtil::nRank);
  int nPktSendTot = 0, nSrcSendTot = 0;
  int lastrank = -1;
  for (IdxType i = 0; i < globTreeNLeaf; i++) {
    GlobalNode& nd = globNodes[i + globTreeLeafPtr];
    int r = nd.owner;

    // Record where the data going to this rank starts
    if (r > lastrank) {
      sendDisp[r] = 2*i;
      lastrank = r;
    }

    // If I own this leaf, nothing to export
    if (r == MPIUtil::myRank) continue;

    // For leaves that I do not own, record how many packets and
    // sources we are going to be exporting for the leaf; also record
    // the amount of data we will be sending to each MPI rank
    pktSrcSendCount[2*i] = nd.count;
    pktSrcSendCount[2*i+1] = nd.srcCount;
    sendCounts[r] += 2;
    pktToRank[r] += nd.count;
    srcToRank[r] += nd.srcCount;
    nPktSendTot += nd.count;
    nSrcSendTot += nd.srcCount; 
  }
      
  // Step 2: exchange the counts; each rank expects to receive two
  // numbers from each other MPI rank for each leaf it owns; at the
  // end of this exchange, move the 1D buffer into a nested array just
  // to make things easier to follow in the rest of the code, since
  // the alternative is lots of nasty, easy-to-get-wrong index
  // arithmetic. The end will be a 2D array of size nOwned * nRank,
  // where element [i][j] gives the number of packets or sources we
  // are going to receive on the ith leaf we own from the jth rank.
  vector<int> recvCounts(MPIUtil::nRank), recvDisp(MPIUtil::nRank);
  const IdxType nRecvCount = 2 * MPIUtil::nRank * globTreeNOwned;
  vector<IdxType> pktSrcRecvCount(nRecvCount);
  for (int i = 0; i < MPIUtil::nRank; i++) {
    if (i != MPIUtil::myRank)
      recvCounts[i] = 2 * globTreeNOwned;
    else
      recvCounts[i] = 0;
    recvDisp[i] = 2 * globTreeNOwned * i;      
  }  
  MPI_Alltoallv(pktSrcSendCount.data(), sendCounts.data(),
		sendDisp.data(), MPIUtil::MPI_Idx,
		pktSrcRecvCount.data(), recvCounts.data(),
		recvDisp.data(), MPIUtil::MPI_Idx,
		MPI_COMM_WORLD);
  vector<vector<IdxType> > pktRecvCount(globTreeNOwned),
    srcRecvCount(globTreeNOwned);
  for (IdxType i = 0; i < globTreeNOwned; i++) {
    pktRecvCount[i].resize(MPIUtil::nRank);
    srcRecvCount[i].resize(MPIUtil::nRank);
    for (int r = 0; r < MPIUtil::nRank; r++) {
      pktRecvCount[i][r] = pktSrcRecvCount[2*globTreeNOwned*r + 2*i];
      srcRecvCount[i][r] = pktSrcRecvCount[2*globTreeNOwned*r + 2*i + 1];
    }
  }

  // Verbose status message
  if (verbosity > 1) {
    if (MPIUtil::IOProc)
      cout << "CRTree: import / export counts are:" << endl;
    for (int rank=0; rank<MPIUtil::nRank; rank++) {
      if (MPIUtil::myRank == rank) {
	IdxType nPktRecvTot = 0, nSrcRecvTot = 0;
	for (IdxType i=0; i<pktRecvCount.size(); i++) {
	  for (int r=0; r<MPIUtil::nRank; r++) {
	    nPktRecvTot += pktRecvCount[i][r];
	    nSrcRecvTot += srcRecvCount[i][r];
	  }
	}
	cout << "   rank " << rank << ": import "
	     << nPktRecvTot << " packets, "
	     << nSrcRecvTot << " sources; export "
	     << nPktSendTot << " packets, "
	     << nSrcSendTot << " sources" << endl << flush;
      }
      MPIUtil::barrier();
    }
  }
  
  // Step 3: construct a buffer to hold the data we will be exporting
  // to other ranks; this buffer will hold everything -- packet
  // positions, packet data, source positions, source data -- in a
  // single contiguous memory block, so we can send everything with a
  // single MPI message. When building this buffer, record the amount
  // of data that is going to each MPI rank, and where in the buffer
  // that data begins, since we will need to provie that information
  // to the MPI command.
  IdxType sendBufSize =
    nPktSendTot * (sizeof(RealVec) + sizeof(CRPacket)) +
    nSrcSendTot * (sizeof(RealVec) + sizeof(CRSource));
  vector<unsigned char> sendBuf(sendBufSize);
  IdxType bufPtr = 0;
  lastrank = -1;
  for (int r = 0; r < MPIUtil::nRank; r++)
    sendCounts[r] = 0;  // Initialize count of bytes being sent to
			// each rank
  for (IdxType i = 0; i < globTreeNLeaf; i++) {

    // If we are on to a new destination, record the displacement in
    // the buffer for data going to this rank    
    GlobalNode& nd = globNodes[i + globTreeLeafPtr];
    int r = nd.owner;
    if (r > lastrank) {
      // Record where the data doing to this rank starts
      sendDisp[r] = bufPtr;
      lastrank = r;
    }

    // If we own this leaf, nothing else to do
    if (r == MPIUtil::myRank) continue;

    // For leaves we do not own, copy any packets and sources we have
    // into the send buffer
    IdxType bufPtrSave = bufPtr;
    memcpy(sendBuf.data() + bufPtr, x.data() + nd.start,
	   nd.count * sizeof(RealVec));
    bufPtr += nd.count * sizeof(RealVec);
    memcpy(sendBuf.data() + bufPtr, pd.data() + nd.start,
	   nd.count * sizeof(CRPacket));
    bufPtr += nd.count * sizeof(CRPacket);
    memcpy(sendBuf.data() + bufPtr, xSrc.data() + nd.srcStart,
	   nd.srcCount * sizeof(RealVec));
    bufPtr += nd.srcCount * sizeof(RealVec);
    memcpy(sendBuf.data() + bufPtr, src.data() + nd.srcStart,
	   nd.srcCount * sizeof(CRSource));
    bufPtr += nd.srcCount * sizeof(CRSource);
    sendCounts[r] += bufPtr - bufPtrSave;
  }

  // Step 4: allocate a receive buffer that will receive the data from
  // all other ranks; as with the send buffer, keep track of how much
  // data we are receiving from each rank, and where within the buffer
  // that data will be placed
  IdxType recvBufSize = 0;
  for (int r = 0; r < MPIUtil::nRank; r++) {
    recvCounts[r] = 0;
    recvDisp[r] = recvBufSize;
    for (IdxType j=0; j<globTreeNOwned; j++) {
      IdxType recvCountsSave = recvCounts[r];
      recvCounts[r] += pktRecvCount[j][r] *
	(sizeof(RealVec) + sizeof(CRPacket));
      recvCounts[r] += srcRecvCount[j][r] *
	(sizeof(RealVec) + sizeof(CRSource));
      recvBufSize += recvCounts[r] - recvCountsSave;
    }
  }
  vector<unsigned char> recvBuf(recvBufSize);

  // Step 5: exchange all data; this is done in a single MPI
  // all-to-allv call, facilitated by our copying all data into a
  // single buffer
  MPI_Alltoallv(sendBuf.data(), sendCounts.data(),
		sendDisp.data(), MPI_BYTE,
		recvBuf.data(), recvCounts.data(),
		recvDisp.data(), MPI_BYTE,
		MPI_COMM_WORLD);
  
  // Step 6: figure out the counts and starting points that every leaf
  // in the global tree will have once the exported data is removed,
  // and the imported data is added.
  vector<IdxType> startNew(globTreeNLeaf), srcStartNew(globTreeNLeaf);
  vector<IdxType> countNew(globTreeNLeaf), srcCountNew(globTreeNLeaf);
  IdxType ownLeafPtr = 0;
  for (IdxType i = 0; i < globTreeNLeaf; i++) {
    
    // Store new start value
    if (i != 0) {
      startNew[i] = startNew[i-1] + countNew[i-1];
      srcStartNew[i] = srcStartNew[i-1] + srcCountNew[i-1];
    } else {
      startNew[0] = 0;
      srcStartNew[0] = 0;
    }

    // Next step depends on whether we own this leaf
    GlobalNode& nd = globNodes[i + globTreeLeafPtr];
    if (nd.owner != MPIUtil::myRank) {
      // We do not own it, so set counts to zero
      countNew[i] = srcCountNew[i] = 0;
    } else {
      // We do own this leaf, so set our count equal to the sum of the
      // locally-stored count and the counts in the import buffer
      countNew[i] = nd.count;
      srcCountNew[i] = nd.srcCount;
      for (int r = 0; r < MPIUtil::nRank; r++) {
	countNew[i] += pktRecvCount[ownLeafPtr][r];
	srcCountNew[i] += srcRecvCount[ownLeafPtr][r];
      }
      ownLeafPtr++;
    }
  }

  // Step 7: if our arrays to hold the data are not large enough,
  // expand them now
  IdxType nPacketNew = 0, nSourceNew = 0;
  for (IdxType i = 0; i < globTreeNLeaf; i++) {
    nPacketNew += countNew[i];
    nSourceNew += srcCountNew[i];
  }
  if (nPacketNew > x.size()) {
    x.resize(nPacketNew);
    pd.resize(nPacketNew);
  }
  if (nSourceNew > xSrc.size()) {
    xSrc.resize(nSourceNew);
    src.resize(nSourceNew);
  }

  // Step 8: move the local and imported data packet data into their
  // final places in the x and pd arrays. Since we are doing this
  // in-place, it requires some care, because the local data are
  // already in the array, and we need to make sure not to overwrite
  // anything that needs to be saved. We therefore carry out this data
  // movement in three phases:
  // (a) for any local data that is moving forward in memory (towards
  // the start of the array), move it up, starting from the first leaf
  // we own; this ordering guarantees that we move things out of the
  // way before overwriting them. We do this separately for the packet
  // data and the source data.
  // (b) for any local data that is moving backward in memory (towards
  // the end of the array), move it backwards, starting from the
  // *last* leaf we own; again, this backwards ordering guarantees
  // that we do not overwrite anything before moving it. Again, so
  // this separately for the packet and source data.
  // (c) copy data out of the receive buffer and into its designated
  // locations in the final array.
  
  // Step 8a: forward pass through packet data that is moving forward
  // in memory.
  for (IdxType i = 0; i < globTreeNOwned; i++) {

    // Skip leaves that contain no data or where the data are not
    // moving forward in memory
    GlobalNode& nd = globNodes[i + globTreeMyLeafPtr];
    IdxType start = startNew[i + globTreeMyLeafPtr - globTreeLeafPtr];
    if (nd.count == 0 || start >= nd.start) continue;
    
    // If we're here, data need to be moved forward in the data array,
    // so do so via a copy; note that we do not use memcpy, because
    // the data ranges may overlap.
    copy(x.begin() + nd.start, x.begin() + nd.start + nd.count,
	 x.begin() + start);
    copy(pd.begin() + nd.start, pd.begin() + nd.start + nd.count,
	 pd.begin() + start);
  }

  // Repeat for the source data
  for (IdxType i = 0; i < globTreeNOwned; i++) {

    // Skip leaves that contain no data or where the data are not
    // moving forward in memory
    GlobalNode& nd = globNodes[i + globTreeMyLeafPtr];
    IdxType start = srcStartNew[i + globTreeMyLeafPtr - globTreeLeafPtr];
    if (nd.srcCount == 0 || start >= nd.srcStart) continue;
    
    // If we're here, data need to be moved forward in the data array,
    // so do so via a copy; note that we do not use memcpy, because
    // the data ranges may overlap.
    copy(xSrc.begin() + nd.srcStart, xSrc.begin() + nd.srcStart + nd.srcCount,
	 xSrc.begin() + start);
    copy(src.begin() + nd.srcStart, src.begin() + nd.srcStart + nd.srcCount,
	 src.begin() + start);
  }

  // Step 8b: now loop backwards, starting from the last leaf we own,
  // and move backwards in memory any data that need to be moved back
  for (SignedIdxType i = globTreeNOwned-1; i >= (SignedIdxType) 0;
       i--) {

    // Skip leaves that contain no data or where the data are not
    // moving backward in memory
    GlobalNode& nd = globNodes[i + globTreeMyLeafPtr];
    IdxType start = startNew[i + globTreeMyLeafPtr - globTreeLeafPtr];
    if (nd.count == 0 || start <= nd.start) continue;

    // If we're here, data need to be moved backward in the data
    // array; we do so using a copy_backward, to ensure that we don't
    // overwrite.
    copy_backward(x.begin() + nd.start, x.begin() + nd.start + nd.count,
		  x.begin() + start + nd.count);
    copy_backward(pd.begin() + nd.start, pd.begin() + nd.start + nd.count,
		  pd.begin() + start + nd.count);
  }

  // Repeat for the source data
  for (SignedIdxType i = globTreeNOwned-1; i >= (SignedIdxType) 0;
       i--) {

    // Skip leaves that contain no data or where the data are not
    // moving backward in memory
    GlobalNode& nd = globNodes[i + globTreeMyLeafPtr];
    IdxType start = srcStartNew[i + globTreeMyLeafPtr - globTreeLeafPtr];
    if (nd.srcCount == 0 || start <= nd.srcStart) continue;

    // If we're here, data need to be moved forward in the data array,
    // so do so via a copy; note that we do not use memcpy, because
    // the data ranges may overlap.
    copy_backward(xSrc.begin() + nd.srcStart,
		  xSrc.begin() + nd.srcStart + nd.srcCount,
		  xSrc.begin() + start +
		  nd.srcCount);
    copy_backward(src.begin() + nd.srcStart,
		  src.begin() + nd.srcStart + nd.srcCount,
		  src.begin() + start +
		  nd.srcCount);
  }

  // Step 8c: now that the local data has been moved to its correct
  // position, loop through the receive buffer and insert the imported
  // data. As we finish pasting the data into place for each leaf,
  // update the start and count values for that leaf to their final
  // values.
  ownLeafPtr = 0;
  for (IdxType i = 0; i < globTreeNLeaf; i++) {

    // Check if we own this leaf
    GlobalNode& nd = globNodes[i + globTreeLeafPtr];
    if (nd.owner == MPIUtil::myRank) {

      // For leaves we do own, loop through the data that the other MPI
      // ranks have placed into our receive buffer for this leaf, and
      // copy it into place.
      IdxType ptr = startNew[i] + nd.count;  // End of local data
      IdxType srcPtr = srcStartNew[i] + nd.srcCount;
      for (int r = 0; r < MPIUtil::nRank; r++) {

	// Point to location in receive buffer where the packet and
	// source data are stored; skip to start of data for this leaf
	unsigned char *buf = recvBuf.data() + recvDisp[r];
	for (IdxType k = 0; k < ownLeafPtr; k++) {
	  buf += pktRecvCount[k][r] *
	    (sizeof(RealVec) + sizeof(CRPacket));
	  buf += srcRecvCount[k][r] *
	    (sizeof(RealVec) + sizeof(CRSource));
	}

	// Get number of packets and sources we got from this rank for
	// this leaf in the global tree
	IdxType nPktRecv = pktRecvCount[ownLeafPtr][r];
	IdxType nSrcRecv = srcRecvCount[ownLeafPtr][r];

	// Copy data out of receive buffer
	memcpy(x.data() + ptr, buf, nPktRecv * sizeof(RealVec));
      	buf += nPktRecv * sizeof(RealVec);
      	memcpy(pd.data() + ptr, buf, nPktRecv * sizeof(CRPacket));
	buf += nPktRecv * sizeof(CRPacket);
	ptr += nPktRecv;
	memcpy(xSrc.data() + srcPtr, buf, nSrcRecv * sizeof(RealVec));
	buf += nSrcRecv * sizeof(RealVec);
    	memcpy(src.data() + srcPtr, buf, nSrcRecv * sizeof(CRSource));
	srcPtr += nSrcRecv;
      }

      // Increment count through leaves we own
      ownLeafPtr++;
    }

    // Updated leaf starts and counts to new values
    nd.start = startNew[i];
    nd.count = countNew[i];
    nd.srcStart = srcStartNew[i];
    nd.srcCount = srcCountNew[i];
  }
  
  // Step 9: if the data arrays should be smaller due to removal of
  // exports, shrink them now
  if (nPacketNew < x.size()) {
    x.resize(nPacketNew);
    pd.resize(nPacketNew);
  }
  if (nSourceNew < xSrc.size()) {
    xSrc.resize(nSourceNew);
    src.resize(nSourceNew);
  }

  // Step 10: propagate the new start and count values back up through
  // the higher levels of the global tree
  for (SignedIdxType lev = globTreeMaxLev-1; lev >= 0; lev--) {
    IdxType nNodeLev = 1 << lev;
    IdxType levStart = nNodeLev - 1;
    for (IdxType i = levStart; i < levStart + nNodeLev; i++) {
      IdxType l = 2*i + 1;
      IdxType r = 2*i + 2;
      globNodes[i].start = globNodes[l].start;
      globNodes[i].srcStart = globNodes[l].srcStart;
      globNodes[i].count = globNodes[l].count + globNodes[r].count;
      globNodes[i].srcCount = globNodes[l].srcCount +
	globNodes[r].srcCount;
    }
  }

  // Step 11: in debug mode, run a safety check to ensure all packets are valid
#ifndef NDEBUG
  for (int rank=0; rank<MPIUtil::nRank; rank++) {
    if (MPIUtil::myRank == rank) {
      for (IdxType i = 0; i < nPacketLoc(); i++) {
        if (getPacketData(i).p <= 0.0) {
	  stringstream ss;
	  ss << "CRTree::exchange: MPI rank " << MPIUtil::myRank
	     << " has invalid packet with p = "
	     << getPacketData(i).p << endl;
	  MPIUtil::haltRun(ss.str(), errUnspecified);
        }
      }
    }
    MPIUtil::barrier();
  }
#endif

  // Print status message
  if (verbosity > 1) {
    if (MPIUtil::IOProc)
      cout << flush
	   << "CRTree: exchange complete; updated counts are:" << endl;
    for (int rank=0; rank<MPIUtil::nRank; rank++) {
      if (MPIUtil::myRank == rank)
	cout << "   rank " << rank << ": " << nPacketLoc()
	     << " packets, " << nSourceLoc() << " sources" << endl;
      MPIUtil::barrier();
    }
  }
#endif
}
#endif
// endif ENABLE_MPI


void CRTree::buildLocalTree() {

  // Print status
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: building local tree" << endl;

  // First clone the global tree into the local tree
  nodes.resize(globNodes.size());
  for (IdxType i=0; i<globNodes.size(); i++) {
    nodes[i].start = globNodes[i].start;
    nodes[i].count = globNodes[i].count;
    nodes[i].splitDim = globNodes[i].splitDim;
    nodes[i].splitVal = globNodes[i].splitVal;
    nodes[i].box = globNodes[i].box;
    if (i == 0) nodes[i].parent = nullNode;
    else nodes[i].parent = (i-1)/2;
    if (i >= globNodes.size() - globTreeNLeaf) {
      nodes[i].lchild = nodes[i].rchild = nullNode;
    } else {
      nodes[i].lchild = 2*i + 1;
      nodes[i].rchild = nodes[i].lchild + 1;
    }
    nodes[i].order = nullNode;
  }
  
  // Now compute the properties of the local trees that will be built
  // under each leaf of the global tree that this MPI rank owns
  IdxType locTreeNNode = nodes.size();
  locTreeOffsets.resize(globTreeNOwned);
  locTreeMaxLev.resize(globTreeNOwned);
  locTreeLeafStart.resize(globTreeNOwned);
  locTreeNLeaf.resize(globTreeNOwned);
  for (IdxType i = 0; i < globTreeNOwned; i++) {
    
    // Compute size of local tree needed to manage this leaf of global
    // tree
    LocalNode& nd = nodes[i + globTreeMyLeafPtr];
    IdxType nLeaf = 1;
    IdxType nNode = 1;
    IdxType nLev = globTreeMaxLev;
    for (IdxType n = nd.count; n >= (IdxType) leafSize; n = n >> 1) {
      nLeaf = nLeaf << 1;
      nNode += nLeaf;
      nLev++;
    }

    // Add number of nodes in this local tree to running total; note
    // that we subtract one from the node count to account for the
    // fact that the root entry for this local tree is already stored,
    // as part of the global tree
    nNode--;
    locTreeNNode += nNode;

    // Store details describing where this local tree will reside in
    // the nodes array
    if (i == 0) locTreeOffsets[i] = globNodes.size();
    else locTreeOffsets[i] = locTreeOffsets[i - 1] + nNode;
    locTreeMaxLev[i] = nLev;
    locTreeNLeaf[i] = nLeaf;

  }
  nodes.resize(locTreeNNode);

  // Loop over the leaves of the global tree that we own, and for each
  // leaf build the rest of the tree below it
  for (IdxType i = 0; i < globTreeNOwned; i++) {

    // Print status
    if (verbosity > 2 && MPIUtil::IOProc)
      cout << "CRTree: building local tree under global leaf " << i
	   << endl;

    // Set maximum level of this level tree, and handle special case
    // where this is equal to the maximum level in the global tree, so
    // no local tree exists
    IdxType maxLev = locTreeMaxLev[i];
    if (maxLev == globTreeMaxLev) {
      locTreeLeafStart[i] = i + globTreeMyLeafPtr;
      continue;
    }

    // Set pointers to (1) where the current level of the tree starts
    // in the nodes array, and (2) where the next level of the tree
    // starts; also set number of nodes on this level of the tree
    IdxType offsetThisLev = globTreeMyLeafPtr + i;
    IdxType offsetNextLev = locTreeOffsets[i];
    IdxType nNodeLev = 1;
    
    // Traverse tree breadth-first, starting from the global tree
    // leaf, so that construction can be threaded
    for (IdxType n = globTreeMaxLev; n < maxLev; n++) {

      // Parallelise construction of nodes on this level
#ifdef _OPENMP
#pragma omp parallel for
#endif
      for (IdxType j = 0; j < nNodeLev; j++) {

	// Point to node we're working on and its children
	IdxType nd = offsetThisLev + j;
	IdxType ndLeft = offsetNextLev + 2*j;
	IdxType ndRight = ndLeft + 1;

	// Initialize parent and child pointers
	nodes[ndLeft].parent = nodes[ndRight].parent = nd;
	nodes[nd].lchild = ndLeft;
	nodes[nd].rchild = ndRight;

	// Partition the node
	partitionNode(nodes[nd]);

	// Initialize the child node
	nodes[ndLeft].start = nodes[nd].start;
	nodes[ndLeft].count = (nodes[nd].count+1) / 2;
	nodes[ndRight].start = nodes[ndLeft].start + nodes[ndLeft].count;
	nodes[ndRight].count = nodes[nd].count / 2;
	nodes[ndLeft].order = nodes[ndRight].order = nullNode;
	for (int j=0; j<3; j++) {
	  if (j == nodes[nd].splitDim) {
	    IdxType idx = nodes[ndLeft].start + (nodes[nd].count-1)/2;
	    nodes[ndLeft].box.lo[j] = nodes[nd].box.lo[j];
	    nodes[ndLeft].box.hi[j] = x[idx][j];
	    nodes[ndRight].box.lo[j] = x[idx][j];
	    nodes[ndRight].box.hi[j] = nodes[nd].box.hi[j];
	  } else {
	    nodes[ndLeft].box.lo[j] = nodes[ndRight].box.lo[j]
	      = nodes[nd].box.lo[j];
	    nodes[ndLeft].box.hi[j] = nodes[ndRight].box.hi[j]
	      = nodes[nd].box.hi[j];
	  }
	}

	// If this is the last level in the tree, mark that the
	// children are leaves
	if (n == maxLev - 1) {
	  nodes[ndLeft].splitDim = nodes[ndRight].splitDim = -1;
	  nodes[ndLeft].lchild = nodes[ndRight].lchild =
	    nodes[ndLeft].rchild = nodes[ndRight].rchild = nullNode;
	}

      } // end loop over nodes on this level

      // Move pointers to tree levels
      nNodeLev *= 2;
      offsetThisLev = offsetNextLev;
      offsetNextLev += nNodeLev;
      
    } // end loop over levels

    // Store location where the leaves of this local tree start
    locTreeLeafStart[i] = offsetThisLev;

  } // end loop over global tree leaves

  // Print status
  if (verbosity > 2 && MPIUtil::IOProc) 
    cout << "CRTree: all local trees built" << endl;
  
  // Final step: now that the local tree is built, figure out which
  // leaf each source belongs in; we will use this information later
  // so that we can assign the packets those sources inject to leaves
  srcLeaf.resize(xSrc.size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (IdxType i = 0; i < xSrc.size(); i++) {
    IdxType p = rootNode;
    while (nodes[p].splitDim != -1) {
      LocalNode& nd = nodes[p];
      if (xSrc[i][nd.splitDim] < nd.splitVal) p = nd.lchild;
      else p = nd.rchild;
    }
    srcLeaf[i] = p;
  }

}


void CRTree::partitionNode(LocalNode& nd) {

  // Partition along the longest dimension
  nd.splitDim = nd.box.size().argmax();

  // Initialize values
  IdxType l = 0;
  IdxType r = nd.count - 1;
  IdxType m = (nd.count - 1) >> 1;

  // Iterate until partition is complete
  while (l < r) {

    // Initialize pointers for this iteration
    IdxType lptr = l;
    IdxType rptr = r;

    // Set partition value of this iteration to the value of the
    // median point
    Real splitVal = x[nd.start + (lptr+rptr)/2][nd.splitDim];

    // Move points smaller than the partition value to the left, and
    // points larger to the right
    while (1) {
      while (x[nd.start + lptr][nd.splitDim] < splitVal && lptr < rptr) lptr++;
      while (x[nd.start + rptr][nd.splitDim] > splitVal && lptr <= rptr) rptr--;
      if (lptr >= rptr) break;
      swap(x[nd.start + lptr], x[nd.start + rptr]);
      swap(pd[nd.start + lptr], pd[nd.start + rptr]);
      lptr++;
      rptr--;
    }

    // Set rptr to point to first element larger than the partition
    // value
    if (x[nd.start + rptr][nd.splitDim] > splitVal && lptr <= rptr) rptr--;
    rptr++;

    // Set l and r so that they enclose the part of the list
    // containing the middle index; if the middle index is the only
    // thing left, then we're done
    if (rptr > m) r = rptr - 1;
    else l = rptr;
  }

  // Store the value at which we split the node
  nd.splitVal = x[nd.start + (nd.count-1)/2][nd.splitDim];
}


void CRTree::sumTree() {

  // Print status
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: summing local tree" << endl;

  // Step 1: sum the parts of the local tree that are entirely owned
  // by this MPI rank; this requires that we loop over the leaves in
  // the global tree owned by this MPI rank
  for (IdxType i = 0; i < globTreeNOwned; i++) {

    // Loop over the leaves
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (IdxType j = 0; j < locTreeNLeaf[i]; j++) {

      // Skip if node is empty
      LocalNode& nd = nodes[j + locTreeLeafStart[i]];
      if (nd.count == 0) continue;

      // Get tight bounding box, central position, and covariance
      // matrix for node
      nd.bnd = RealBox(&x[nd.start], nd.count);
      nd.wSum = nd.w2Sum = 0.0;
      nd.cm = zeroVec;
      nd.cov = zeroTensor;
      for (IdxType p = nd.start; p < nd.start + nd.count; p++) {
	nd.wSum += pd[p].w;
	nd.w2Sum += pd[p].w * pd[p].w;
	nd.cm += pd[p].w * x[p];
      }
      nd.cm /= nd.wSum;
      for (IdxType p = nd.start; p < nd.start + nd.count; p++) {
	RealVec dx = x[p] - nd.cm;
	nd.cov += pd[p].w * outer(dx, dx);
      }
      nd.cov /= nd.wSum;

      // Sort the packets in the leaf by rigidity -- this will
      // speed up calculations later
      vector<RealVec> xTmp(nd.count);
      vector<CRPacket> pdTmp(nd.count);
      copy(x.begin() + nd.start, x.begin() + nd.start + nd.count,
	   xTmp.begin());
      copy(pd.begin() + nd.start, pd.begin() + nd.start + nd.count,
	   pdTmp.begin());
      vector<IdxType> idx(nd.count);
      for (IdxType i = 0; i < nd.count; i++) idx[i] = i;
      sort(idx.begin(), idx.end(),
	   [pdTmp](const IdxType& i, const IdxType& j) -> bool
	   { return pdTmp[i].R() < pdTmp[j].R(); }
	   );
      for (IdxType i = 0; i < nd.count; i++) {
	x[nd.start+i] = xTmp[idx[i]];
	pd[nd.start+i] = pdTmp[idx[i]];
      }

      // Loop over rigidity bins and add packet contributions to
      // each; do this starting from the highest rigidity bin,
      // which has the fewest packets contributing to it, and work
      // backward
      SignedIdxType ptr = nd.start + nd.count - 1;
      for (SignedIdxType k = TREE_RIGIDITY_BINS-1;
	   k >= 0;
	   k--) {

	// Initialize this bin
	if (k == TREE_RIGIDITY_BINS-1) {
	  nd.fQ[k].setZero();
	} else {
	  nd.fQ[k] = nd.fQ[k+1];
	}
	if (ptr < static_cast<SignedIdxType>(nd.start)) continue;

	// Add contribution from packets that are above the
	// rigidity threshold for this bin
	Real logRmin = logR0 + k * dlogR;
	while (log(pd[ptr].R()) >= logRmin) {
	  nd.fQ[k].addPacket(pd[ptr], zeroVec, identityTensor);
	  ptr--;
	  if (ptr < static_cast<SignedIdxType>(nd.start)) break;
	}

      }
    }

    // Print status
    if (verbosity > 2 && MPIUtil::IOProc)
      cout << "CRTree: local tree sums completed for all leaves" << endl;

    // Now propagate the sums to the remaining levels of the tree that
    // are stored on this MPI rank
    IdxType offset = locTreeLeafStart[i] - locTreeNLeaf[i]/2;
    IdxType nLev = locTreeNLeaf[i] / 2;
    for (SignedIdxType n = locTreeMaxLev[i]-1;
	 n >= (SignedIdxType) globTreeMaxLev; n--) {
    
      // Loop over nodes at this tree level
#ifdef _OPENMP
#pragma omp parallel for
#endif
      for (IdxType j = 0; j < nLev; j++) {

	// Skip if node is empty
	LocalNode& nd = nodes[j + offset];
	if (nd.count == 0) continue;

	// Grab children of this node
	LocalNode& l = nodes[nd.lchild];
	LocalNode& r = nodes[nd.rchild];

	// Get bounding box from child nodes
	nd.bnd = RealBox(l.bnd, r.bnd);
	  
	// Sum fieldQuantity contributions of child nodes
	for (IdxType k = 0; k < TREE_RIGIDITY_BINS; k++) {
	  nd.fQ[k] = l.fQ[k] + r.fQ[k];
	}

	// Sum other quantities using correct summation rules for
	// each quantity
	nd.wSum = l.wSum + r.wSum;
	nd.w2Sum = l.w2Sum + r.w2Sum;
	nd.cm = (l.wSum * l.cm + r.wSum * r.cm) / nd.wSum;
	nd.cov = (l.wSum * l.cov + r.wSum * r.cov +
		  (l.wSum * r.wSum) / nd.wSum *
		  outer(l.cm - r.cm, l.cm - r.cm)) / nd.wSum;

      } // end loop over nodes at this level of the tree

      // Adjust pointers
      nLev /= 2;
      if (n-1 != (SignedIdxType) globTreeMaxLev) offset -= nLev;
      else offset = i + globTreeMyLeafPtr;
      
    } // end loop over tree levels

  } // end loop over leaves of the global tree

  // Print status
  if (verbosity > 2 && MPIUtil::IOProc)
    cout << "CRTree: local tree sums propagated to global tree" << endl;
  
  // Step 2: now we have filled in the parts of the local tree up to
  // and including the leaves of the global tree that are owned by
  // this MPI rank. At this point we need to exchange between MPI
  // ranks, so each rank has this information for the leaves of the
  // global tree that live on other ranks.
#ifdef ENABLE_MPI
  {

    // Create a buffer to hold the data to be exchanged; we pack all
    // this into a single buffer so that we can exchange all
    // information with a single MPI_Allgather. The data to be
    // exchanged are the bounding boxes bndLo and bndHi, the weight
    // sums, the center of mass position, the covariance matrix, and
    // the field quantity sums
    IdxType leafMemSize = sizeof(int) + 3 * sizeof(Real) + sizeof(RealBox) +
      sizeof(RealVec) + sizeof(RealTensor2) +
      TREE_RIGIDITY_BINS * sizeof(FieldQty);
    IdxType bufSize = globTreeNLeaf * leafMemSize;
    vector<unsigned char> buf(bufSize);
    vector<int> recvCount(MPIUtil::nRank), recvDisp(MPIUtil::nRank);
    int lastRank = -1;
    IdxType ptr = 0;
    for (IdxType i = 0; i < globTreeNLeaf; i++) {

      // Grab this node
      LocalNode& lnd = nodes[i + globTreeLeafPtr];
      GlobalNode& gnd = globNodes[i + globTreeLeafPtr];
      int& rank = gnd.owner;
      
      // If this node belongs to a different rank than the previous
      // node, initialize the displacement and count for this rank
      if (rank != lastRank) {
	lastRank = rank;
	recvDisp[rank] = ptr;
	recvCount[rank] = 0;
      }

      // Increment pointer and count for this rank
      recvCount[rank] += leafMemSize;

      // If we own this node, copy our data into the buffer for
      // transmission to other nodes; if we do not own the buffer,
      // move the pointer, but do not copy
      if (rank == MPIUtil::myRank) {
	memcpy(buf.data() + ptr, &lnd.splitDim, sizeof(int));
	ptr += sizeof(int);
	memcpy(buf.data() + ptr, &lnd.splitVal, sizeof(Real));
	ptr += sizeof(Real);
	memcpy(buf.data() + ptr, &lnd.bnd, sizeof(RealBox));
	ptr += sizeof(RealBox);
	memcpy(buf.data() + ptr, &lnd.wSum, sizeof(Real));
	ptr += sizeof(Real);
	memcpy(buf.data() + ptr, &lnd.w2Sum, sizeof(Real));
	ptr += sizeof(Real);
	memcpy(buf.data() + ptr, &lnd.cm, sizeof(RealVec));
	ptr += sizeof(RealVec);
	memcpy(buf.data() + ptr, &lnd.cov, sizeof(RealTensor2));
	ptr += sizeof(RealTensor2);
	memcpy(buf.data() + ptr, &lnd.fQ,
	       TREE_RIGIDITY_BINS * sizeof(FieldQty));
	ptr += TREE_RIGIDITY_BINS * sizeof(FieldQty);
      } else {
	ptr += leafMemSize;
      }
    }

    // Exchange data
    MPI_Allgatherv(MPI_IN_PLACE, 0, MPI_BYTE,
		   buf.data(), recvCount.data(), recvDisp.data(),
		   MPI_BYTE, MPI_COMM_WORLD);

    // Unpack data from buffer into local nodes
    IdxType leafPtr = 0;
    lastRank = -1;
    while (leafPtr < globTreeNLeaf) {      
      GlobalNode& gnd = globNodes[leafPtr + globTreeLeafPtr];
      LocalNode& lnd = nodes[leafPtr + globTreeLeafPtr];
      int& rank = gnd.owner;
      if (rank != lastRank) {
	lastRank = rank;
	ptr = recvDisp[rank];
      }
      if (rank != MPIUtil::myRank) {
	// I do not own this leaf, so I got data for it from another
	// rank; unpack it
	memcpy(&lnd.splitDim, buf.data() + ptr, sizeof(int));
	ptr += sizeof(int);
	memcpy(&lnd.splitVal, buf.data() + ptr, sizeof(Real));
	ptr += sizeof(Real);
	memcpy(&lnd.bnd, buf.data() + ptr, sizeof(RealBox));
	ptr += sizeof(RealBox);
	memcpy(&lnd.wSum, buf.data() + ptr, sizeof(Real));
	ptr += sizeof(Real);
	memcpy(&lnd.w2Sum, buf.data() + ptr, sizeof(Real));
	ptr += sizeof(Real);
	memcpy(&lnd.cm, buf.data() + ptr, sizeof(RealVec));
	ptr += sizeof(RealVec);
	memcpy(&lnd.cov, buf.data() + ptr, sizeof(RealTensor2));
	ptr += sizeof(RealTensor2);
	memcpy(&lnd.fQ, buf.data() + ptr,
	       TREE_RIGIDITY_BINS * sizeof(FieldQty));
	ptr += TREE_RIGIDITY_BINS * sizeof(FieldQty);
	// To indicate children of this leaf live on another MPI rank,
	// we set the child index to the rank that owns the child,
	// with an added check bit to flag that this is an external node
	lnd.lchild = lnd.rchild = extNode | (IdxType) rank;
      }
      leafPtr++;
    }
  }
#endif

  // Step 3: now sum over part of local tree that corresponds to
  // non-leaf nodes of the global tree
  for (SignedIdxType n = globTreeMaxLev-1; n >= 0; n--) {

    IdxType startNode = (1 << n) - 1;
    IdxType endNode = (1 << (n+1)) - 1;
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (IdxType i = startNode; i < endNode; i++) {
      LocalNode& nd = nodes[i];
      LocalNode& l = nodes[nd.lchild];
      LocalNode& r = nodes[nd.rchild];
      for (IdxType k = 0; k < TREE_RIGIDITY_BINS; k++) {
	nd.fQ[k] = l.fQ[k] + r.fQ[k];
      }
      nd.bnd = RealBox(l.bnd, r.bnd);
      nd.wSum = l.wSum + r.wSum;
      nd.w2Sum = l.w2Sum + r.w2Sum;
      nd.cm = (l.wSum * l.cm + r.wSum * r.cm) / nd.wSum;
      nd.cov = (l.wSum * l.cov + r.wSum * r.cov +
		(l.wSum * r.wSum) / nd.wSum *
		outer(l.cm - r.cm, l.cm - r.cm)) / nd.wSum;
    }
  }

  // Step 4: for convenience, make a list of all the leaves in the
  // local tree that contain packets whose field quantities will need
  // to be filled; conversely, record the order in which the leaves
  // are visited when we traverse the tree by breadth
  IdxType nLeaf = 0;
  vector<IdxType> leafOffset(globTreeNOwned);
  for (IdxType i = 0; i < globTreeNOwned; i++) {
    nLeaf += locTreeNLeaf[i];
    if (i == 0) leafOffset[i] = 0;
    else leafOffset[i] = leafOffset[i - 1] + locTreeNLeaf[i];
  }
  leafList.resize(nLeaf);
  for (IdxType i = 0; i < globTreeNOwned; i++) {
    for (IdxType j = 0; j < locTreeNLeaf[i]; j++) {
      leafList[leafOffset[i] + j] = locTreeLeafStart[i] + j;
      nodes[locTreeLeafStart[i] + j].order = leafOffset[i] + j;
    }
  }

  // Step 5: for each leaf in the local tree, determine the bandwidth
  // that will be used to compute the field quantities for packets in
  // that leaf
  h2InvLeaf.resize(nLeaf);
  hScaleLeaf.resize(nLeaf);
  if (needFQG) h2InvGradLeaf.resize(nLeaf);
  for (IdxType n = 0; n < locTreeLeafStart.size(); n++) {
    
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (IdxType i = 0; i < locTreeNLeaf[n]; i++) {

      // Point to element in h2InvLeaf and related arrays we will be
      // computing, and to the leaf we're working on
      IdxType p = leafOffset[n] + i;
      IdxType ptr = locTreeLeafStart[n] + i;

      // If this leaf is empty, skip it
      if (nodes[ptr].count == 0) continue;

      // Climb the tree until we reach the requested effective
      // neighbor number target
      Real nEff = 0.0;
      while (true) {
	nEff = nodes[ptr].wSum * nodes[ptr].wSum / nodes[ptr].w2Sum;
	if (nEff >= nNgbEff || ptr == rootNode) break;
	ptr = nodes[ptr].parent;
      }

      // Next compute the inverse-squared bandwidth matrix, which is given
      // by a scale factor (which is different for the field quantities
      // and their derivatives) multiplied by the inverse covariance
      // matrix.
      RealTensor2 covInv = nodes[ptr].cov.inv();
      h2InvLeaf[p] = pow(4 / (5*nEff), -2.0/7.0) * covInv;
      if (needFQG)
	h2InvGradLeaf[p] = pow(4 / (7*nEff), -2.0/9.0) * covInv;

      // Now compute scale factor, which is used for error
      // normalization and time step estimation. By default we use the
      // geometric mean of the eigenvalues. However, we make an
      // exception if the matrix condition number is very large, which
      // can occur if the CR packets are highly anisotropic (e.g., all
      // strung out along a single field line), in which case the
      // small eigenvalues may not be reliable numerically, and, more
      // importantly, doing so would give us a time step constrained
      // by the dynamics in the numerically-short direction. If this
      // situation is detected, we use the largest eigenvalue instead.
      RealVec hEVal = h2InvLeaf[p].eVal();
      for (int j=0; j<3; j++) hEVal[j] = 1.0 / sqrt(hEVal[j]);
      Real hMax = hEVal[0], hMin = hEVal[0], hProd = hEVal[0];
      for (int j=1; j<3; j++) {
	if (hEVal[j] < hMin) hMin = hEVal[j];
	if (hEVal[j] > hMax) hMax = hEVal[j];
	hProd *= hEVal[j];
      }
      hScaleLeaf[p] = (hMax / hMin < 100.0) ? pow(hProd, 1./3.) : hMax;

      // Apply the computed scale factor to every packet in the leaf
      IdxType nd = locTreeLeafStart[n] + i;
      for (IdxType j = nodes[nd].start;
	   j < nodes[nd].start + nodes[nd].count;
	   j++) h[j] = hScaleLeaf[p];
    }
  }
}


void CRTree::fillFieldQty(vector<IdxType>& start,
			  vector<IdxType>& count,
			  vector<IdxType>& leafList_) {

  // If no quantities to compute, just return
  if (!needFQ) return;

  // Print status
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: computing field quantities" << endl;

#ifdef ENABLE_MPI
  comm.initCycle();
#endif
  
  // Loop through leaves
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic)
#endif
  for (IdxType i = 0; i < leafList_.size(); i++) {

    // Grab node information for this leaf
    IdxType nd = leafList_[i];
    RealTensor2& h2Inv = h2InvLeaf[nodes[nd].order];

    // Compute rigidities of all packets in the leaf
    vector<Real> R(count[i]);
    for (IdxType j = 0; j < count[i]; j++)
      R[j] = getPacketData(j + start[i]).R();

    // Fill field quantities for this leaf
    if (!needFQG) {
      fillFieldQtyRange(start[i],
			count[i],
			&getPacketPos(start[i]),
			R.data(),
			h2Inv,
			zeroTensor,
			0.0,
			&getPacketQty(start[i]),
			&getPacketQtyGrad(start[i]));
    } else {
      RealTensor2& h2InvGrad = h2InvGradLeaf[nodes[nd].order];
      Real& hScale = hScaleLeaf[nodes[nd].order];
      fillFieldQtyRange(start[i],
			count[i],
			&getPacketPos(start[i]),
			R.data(),
			h2Inv,
			h2InvGrad,
			hScale,
			&getPacketQty(start[i]),
			&getPacketQtyGrad(start[i]));
    }
  }
    
  // In an MPI calculation, some of the leaves may have been deferred
  // because they require data from other ranks, and some other ranks
  // may need data from us; now loop to handle that
#ifdef ENABLE_MPI

  // Dispatch our requests to other ranks
  comm.sendRequests();

  // Loop until done
#ifdef _OPENMP
#pragma omp master
#endif
  while (!comm.allDone()) {

    // Receive requests and responses from other ranks
    comm.recv();

    // Counter to ensure we check communication frequently enough
    int nproc = 0;

    // Process requests from other ranks
    while (true) {
      if (nproc++ > MPI_COMM_CYCLE_SIZE) break;

      // Grab next request in queue
      pair<MPIUtil::fillRequest*, int> reqRank = comm.getFillRequest();
      if (!reqRank.first) break;
#ifdef _OPENMP
#pragma omp task firstprivate(reqRank)
#endif
      {
	MPIUtil::fillRequest& req = *reqRank.first;
	int& rank = reqRank.second;
	MPIUtil::fillResponse resp;
	resp.count = req.count;
	resp.idx = req.idx;
	fillFieldQtyRange(nullPacket,
			  req.count,
			  req.x.data(),
			  req.R.data(),
			  req.h2Inv,
			  req.h2InvGrad,
			  req.hScale,
			  resp.q.data(),
			  resp.qGrad.data(),
			  req.nrank,
			  req.qMin.data());

	// Queue response to remote rank
	comm.registerResponse(rank, resp);
      }
    }

    // Process responses from other ranks
    while (true) {
      if (nproc++ > MPI_COMM_CYCLE_SIZE) break;

      // Grab next response in queue
      MPIUtil::fillResponse *resp = comm.getFillResponse();
      if (!resp) break;
#ifdef _OPENMP
#pragma omp task firstprivate(resp)
#endif
      {
	// Add the contributions from remote ranks in this response
	for (IdxType i = 0; i < resp->count; i++) {
	  FieldQty& q = getPacketQty(resp->idx[i]);
	  q += resp->q[i];
	  if (needFQG) {
	    FieldQtyGrad& qGrad = getPacketQtyGrad(resp->idx[i]);
	    qGrad += resp->qGrad[i];
	  }
	}
      }
    }

    // Send responses
    comm.sendResponses();
    
  } // end while (!comm.allDone())
    
  // De-allocate all communication buffers
  comm.finalizeCycle();
#endif
}


void CRTree::fillFieldQtyRange(const IdxType start,
			       const IdxType count,
			       const RealVec x_[],
			       const Real R[],
			       const RealTensor2& h2Inv,
			       const RealTensor2& h2InvGrad,
			       const Real& hScale,
			       FieldQty q[],
			       FieldQtyGrad qGrad[]
#ifdef ENABLE_MPI
			       , const int nrank,
			       const FieldQty* qOwner
#endif
			       ) {

  // Catch the special case where the tree contains zero packets; this
  // is possible in a situation where we are in the first time step of
  // a simulation, and in the initial state there are only sources
  // present, no packets. In this case the sources will inject packets
  // during the first time step, but we cannot compute field
  // quantities for them because there were no CR packets present at
  // the start of the time step. We just return immediately in this
  // case.
  if (nodes[rootNode].count == 0) return;

  // Catch special case of a tree with only a single node (i.e., the
  // tree root is also a leaf), in which case we just compute the
  // answer directly
  if (nodes[rootNode].splitDim == -1) {
    addNodeToField(rootNode, start, count, x_, R, h2Inv, h2InvGrad,
		   q, nullptr, qGrad, nullptr);
    return;
  }

  // Create the list we will use to keep track of which nodes in the
  // tree we need to examine. The accounting here is slightly
  // complicated, and to facilitate it we define a struct representing
  // an entry in the list, which contains the following fields:
  // node -- the index of a non-leaf node in the tree
  // qNode -- estimated contribution to each field quantity from that node
  // qErr -- error on qNode; the true value of the contribution from
  //         each node is guaranteed to lie in the range qNode +- qErr
  // qGradNode, qGradErr -- same as corresponding quantities without
  //                        the Grad, but for gradients of field
  //                        quantities
  typedef struct {
    IdxType node;
    vector<FieldQty> qNode, qErr;
    vector<FieldQtyGrad> qGradNode, qGradErr;
  } nodeData;
  list<nodeData> nodeList;
  
  // In an MPI calculation, we also have
  // qExt -- estimated contribution to field quantities coming from
  //         nodes that cannot be opened because their child live on
  //         another MPI rank
  // qExtErr -- error on qExt; the true contribution from nodes on
  //            external ranks is guaranteed to lie in the range qExt
  //            +- qExtErr
  // qGradExt, qGradExtErr -- same as the corresponding qExt and
  //                          qExtErr, but for gradients
  // extRanks -- list of external ranks that hold data corresponding to
  //             nodes we have tried to open; this list is used to
  //             determine where to send MPI requests if this rank is
  //             unable to converge due to missing data that lives on
  //             other ranks
#ifdef ENABLE_MPI
  vector<FieldQty> qExt(count), qExtErr(count);
  vector<FieldQtyGrad> qGradExt(count), qGradExtErr(count);
  vector<int> extRanks;
#endif

  // Initialize the stack; this happens differently depending on
  // whether we are working on packets we own, or on packets owned by
  // another MPI rank. If we are working on packets we own, we want to
  // compute the contribution to the field quantities from all ranks,
  // and we therefore initialize the tree starting from the root
  // node. On the other hand, if we are working on packets that
  // another rank owns, we only want to calculat the contribution from
  // this rank. We therefore iniaitlize the stack by pushing onto the
  // stack all the nodes that correspond to nodes of the global tree
  // that this rank owns; this excludes contributions from any other
  // ranks.
#ifdef ENABLE_MPI
  if (start != nullPacket) {
#endif
    
    // Case where we own the packet, and therefore the first nodeList
    // entry corresponds to the root node
    nodeList.emplace_back();
    nodeData& nd = nodeList.back();
    nd.node = rootNode;
    nd.qNode.resize(count);
    nd.qErr.resize(count);
    if (needFQG) {
      nd.qGradNode.resize(count);
      nd.qGradErr.resize(count);
    }      

    // Compute estimates for root node
    addNodeToField(rootNode, start, count, x_, R, h2Inv, h2InvGrad,
		   nd.qNode.data(),
		   nd.qErr.data(),
		   nd.qGradNode.data(),
		   nd.qGradErr.data());

#ifdef ENABLE_MPI
  } else {

    // Case where we do not own the packet, and therefore must start
    // from the leaves of the global tree that we own
    for (IdxType i = globTreeMyLeafPtr;
	 i < globTreeMyLeafPtr + globTreeNOwned;
	 i++) {

      // Is this node a leaf?
      if (nodes[i].splitDim == -1) {

	// This node is a leaf; add directly to the final accumulators
	addNodeToField(i, start, count, x_, R, h2Inv, h2InvGrad,
		       q, nullptr, qGrad, nullptr);

      } else {

	// This node is not a leaf, so push onto the node stack
	nodeList.emplace_back();
	nodeData& nd = nodeList.back();
	nd.node = i;
	nd.qNode.resize(count);
	nd.qErr.resize(count);
	if (needFQG) {
	  nd.qGradNode.resize(count);
	  nd.qGradErr.resize(count);
	}

	// Add this node's contribution
	addNodeToField(i, start, count, x_, R, h2Inv, h2InvGrad,
		       nd.qNode.data(),
		       nd.qErr.data(),
		       nd.qGradNode.data(),
		       nd.qGradErr.data());

      }
    }
  }
#endif
  
  // Begin main iteration loop
  while (!nodeList.empty()) {

    // Step 1: set accuracy goal for this step; this is kdeTol divided
    // by the number of ranks contributing data
#ifdef ENABLE_MPI
    Real tol = kdeTol / (1 + nrank + extRanks.size());
#else
    Real tol = kdeTol;
#endif

    // Step 2: compute the sum of the uncertainties from all nodes in
    // the node stack
    vector<FieldQty> qErrTot(count);
    vector<FieldQtyGrad> qGradErrTot(count);
    for (auto it = nodeList.begin(); it != nodeList.end(); ++it) {
      for (IdxType i = 0; i < count; i++) {
	qErrTot[i] = (*it).qErr[i];
	if (needFQG) qGradErrTot[i] += (*it).qGradErr[i];
      }
    }

    // Step 3: compute the current lower limit on q for all packets
    // and quantities; this lower limit is given by the sum of the
    // leaves of the local tree, the lower limits derived from each
    // node in the node list (equal to qNode - qErr), the lower
    // limits derived from any external ranks (qExt - qExtErr), and,
    // if we do not own this packet, the minimum value qOwner
    // determined by the rank that does own it.
    vector<FieldQty> qMin(count);
    for (IdxType i = 0; i < count; i++) qMin[i] = q[i];
    for (auto it = nodeList.begin(); it != nodeList.end(); ++it) {
      for (IdxType i = 0; i < count; i++) {
	qMin[i] += (*it).qNode[i] - (*it).qErr[i];
      }
    }
#ifdef ENABLE_MPI
    if (start == nullPacket) {
      for (IdxType i = 0; i < count; i++) qMin[i] += qOwner[i];
    }
    for (IdxType i = 0; i < count; i++) {
      qMin[i] += qExt[i] - qExtErr[i];
    }
#endif
    for (IdxType i = 0; i < count; i++) {
      for (int j = 0; j < nFieldQty; j++) {
	if (qMin[i][j] == 0.0) qMin[i][j] = eps; // Avoid divide by zero
      }
    }

    // Step 3: compute the relative error and the *local* relative
    // error for each quantity and packet. The relative error is
    // defined as
    //
    // RE = qErr / qMin       RE = |qGradErr| * hMax / qMin
    //
    // for field quantities and gradients of field quantities,
    // respectively; here qErr = the sum of the errors on q from all
    // nodes in the node list plus (stored in qErrTot) any errors
    // contributed by nodes that live on external ranks (stored in
    // qExtErr). The local relative error is the same, except that the
    // sum does not include the contribution from external ranks.
    vector<array<Real, nFieldQty> > re(count);
    vector<array<Real, nFieldQty> > reGrad(count);
#ifdef ENABLE_MPI
    vector<array<Real, nFieldQty> > reLoc(count);
    vector<array<Real, nFieldQty> > reGradLoc(count);
#endif
    for (IdxType i = 0; i < count; i++) {
      re[i] = qErrTot[i] / qMin[i];
      if (needFQG) {
	for (int j = 0; j < nFieldQty; j++) {
	  reGrad[i][j] = qGradErrTot[i][j].mag() * hScale / qMin[i][j];
	}
      }
#ifdef ENABLE_MPI
      reLoc[i] = re[i];
      if (needFQG) reGradLoc[i] = reGrad[i];
#endif
    }
#ifdef ENABLE_MPI
    for (IdxType i = 0; i < count; i++) {
      for (int j = 0; j < nFieldQty; j++) {
	re[i][j] += qExtErr[i][j] / qMin[i][j];
	if (needFQG) {
	  reGrad[i][j] += qGradExtErr[i][j].mag() * hScale / qMin[i][j];
	}
      }
    }
#endif

    // Step 4: loop over all packets and check if any should be
    // deferred; the criterion for deferring a packet is that (1) the
    // *local* relative error for that packet is < tol for all
    // quantities (and gradients), but (2) the total relative error
    // *includuing the external contribution* is > tol for at least
    // one field quantity. Note that we can skip this step for packets
    // that we do not own, since for them we have only local errors,
    // and thus the local and total relative errors are always the
    // same.
#ifdef ENABLE_MPI
    vector<bool> defer(count, false);
    bool anyDefer = false;
    if (start != nullPacket) {
      for (IdxType i = 0; i < count; i++) {
	Real maxRE = 0, maxRELoc = 0;
	for (int j = 0; j < nFieldQty; j++) {
	  if (re[i][j] > maxRE) maxRE = re[i][j];
	  if (reLoc[i][j] > maxRELoc) maxRELoc = reLoc[i][j];
	  if (needFQG) {
	    if (reGrad[i][j] > maxRE) maxRE = reGrad[i][j];
	    if (reGradLoc[i][j] > maxRELoc) maxRELoc = reGradLoc[i][j];
	  }
	}
	if (maxRELoc < tol && maxRE >= tol) {
	  defer[i] = true;
	  anyDefer = true;
	  continue;
	}
      }
    }
#endif

    // Step 5: for packets that are not deferred, determine (1) the
    // largest relative error across all packets and quantities, and
    // (2) for which packet and quantity this error occurs. In this
    // step we use the total, rather tahn the local, error estimates.
    Real maxErr = 0;
    IdxType maxErrPacket = 0;
    int maxErrQty = 0;
    int maxGradErrQty = 0;
    for (IdxType i = 0; i < count; i++) {
#ifdef ENABLE_MPI
      if (defer[i]) continue;
#endif
      for (int j = 0; j < nFieldQty; j++) {
	if (re[i][j] > maxErr) {
	  maxErr = re[i][j];
	  maxErrPacket = i;
	  maxErrQty = j;
	  maxGradErrQty = -1;
	}
	if (reGrad[i][j] > maxErr) {
	  maxErr = reGrad[i][j];
	  maxErrPacket = i;
	  maxErrQty = -1;
	  maxGradErrQty = j;
	}
      }
    }

    // Step 6: determine if the maximum error is below our tolerance;
    // if so we can stop iterating here. If we do stop iterating, we
    // do the following: (1) we add qErrTot to q in order to produce a
    // central estimate of q including the contribution from local
    // nodes that we did not open because we did not have to do so to
    // reach the required tolerance; (2) for packets that we are not
    // going to defer, we also add qExtErr to q, to give a simlar
    // estimate of the contribution from external ranks that we are
    // not opening; (3) for packets that we are deferring, we subtract
    // (qExt - qExtErr) from qMin, so that qMin becomes the minimum
    // value of q excluding any contribution from the external ranks
    // that we are about to send MPI requests to examine; (4) for
    // packets that we are deferring, we then register the MPI request
    // for other ranks to compute their contributions to the field
    // quantities.
    if (maxErr < tol) {
      for (IdxType i = 0; i < count; i++) {
	q[i] += qErrTot[i];
	if (needFQG) qGrad[i] += qGradErrTot[i];
#ifdef ENABLE_MPI
	if (defer[i]) {
	  qMin[i] -= (qExt[i] - qExtErr[i]);
	} else {
	  q[i] += qExt[i];
	}
#endif
      }
#ifdef ENABLE_MPI
      if (anyDefer) {
	comm.registerRequest(start, count, defer, extRanks,
			     h2Inv, h2InvGrad, hScale, qMin);
      }
#endif
      break;
    }

    // Step 7: if we are here, then we have not yet reached the
    // required tolerance. Go through the node list and find the node that
    // makes the largest contribution to the relative error budget for
    // the packet and quantity for which the relative error is
    // largest.
    Real maxAbsErr = 0.0;
    auto nodeListPtr = nodeList.begin();
    for (auto it = nodeList.begin(); it != nodeList.end(); ++it) {
      Real err = maxErrQty != -1 ?
	(*it).qErr[maxErrPacket][maxErrQty] :
	(*it).qGradErr[maxErrPacket][maxGradErrQty].mag() * hScale;
      if (err > maxAbsErr) {
	maxAbsErr = err;
	nodeListPtr = it;
      }
    }

    // Step 8: pop the node that contributes the most error off the
    // node list so that we can process it
    nodeData nd = (*nodeListPtr);
    nodeList.erase(nodeListPtr);
    
    // Step 9: next step depends on what kind of children this node
    // has; the possibilities are:
    // (1) children are leaves
    // (2) children are non-leaf nodes that live on this MPI rank
    // (3) children live on anotehr MPI rank
    IdxType nodeIdx = nd.node;
#ifdef ENABLE_MPI
    if (nodes[nodeIdx].ext()) {
      
      // Children live on another rank

      // Add contribution of this node to the running total from
      // "external" nodes
      for (IdxType i = 0; i < count; i++) {
	qExt[i] += nd.qNode[i];
	qExtErr[i] += nd.qErr[i];
	if (needFQG) {
	  qGradExt[i] += nd.qGradNode[i];
	  qGradExtErr[i] += nd.qGradErr[i];
	}
      }

      // Record the rank of this node in the list of ranks we might
      // have to examine
      auto it = find(extRanks.begin(), extRanks.end(),
		     nodes[nodeIdx].rank());
      if (it == extRanks.end()) extRanks.push_back(nodes[nodeIdx].rank());

    } else {
#endif

      // Children live on this MPI rank; grab their indices
      IdxType l = nodes[nodeIdx].lchild;
      IdxType r = nodes[nodeIdx].rchild;

      // Check if the children are leaves or not; note that since the
      // tree is balanced, either both are leaves or neither are
      if (nodes[l].splitDim == -1) {

	// Children are leaves, so add their contribution to the
	// running totals
	addNodeToField(l, start, count, x_, R, h2Inv, h2InvGrad,
		       q, nullptr, qGrad, nullptr);
	addNodeToField(r, start, count, x_, R, h2Inv, h2InvGrad,
		       q, nullptr, qGrad, nullptr);

      } else {

	// Children are not leaves, so create entries in the node
	// list for them and compute their contribution to field
	// quantities

	// Left child
	nodeList.emplace_back();
	nodeData& lch = nodeList.back();
	lch.node = l;
	lch.qNode.resize(count);
	lch.qErr.resize(count);
	if (needFQG) {
	  lch.qGradNode.resize(count);
	  lch.qGradErr.resize(count);
	}
	addNodeToField(l, start, count, x_, R, h2Inv, h2InvGrad,
		       lch.qNode.data(),
		       lch.qErr.data(),
		       lch.qGradNode.data(),
		       lch.qGradErr.data());
	  
	// Right child
	nodeList.emplace_back();
	nodeData& rch = nodeList.back();
	rch.node = r;
	rch.qNode.resize(count);
	rch.qErr.resize(count);
	if (needFQG) {
	  rch.qGradNode.resize(count);
	  rch.qGradErr.resize(count);
	}
	addNodeToField(r, start, count, x_, R, h2Inv, h2InvGrad,
		       rch.qNode.data(),
		       rch.qErr.data(),
		       rch.qGradNode.data(),
		       rch.qGradErr.data());
	
      }
#ifdef ENABLE_MPI
    }
#endif
  }

  // Final step: properly normalize everything by dividing by (2
  // pi)^(3/2) det(h)
  Real norm = 2.0 * M_PI * sqrt(2.0 * M_PI) / sqrt(h2Inv.det());
  Real normGrad = needFQG ?
    2.0 * M_PI * sqrt(2.0 * M_PI) / sqrt(h2InvGrad.det()) :
    1.0;
  for (IdxType i = 0; i < count; i++) {
    q[i] /= norm;
    if (needFQG) qGrad[i] /= normGrad;
  }
}


void CRTree::addNodeToField(const IdxType& nodeIdx,
			    const IdxType& start,
			    const IdxType& count,
			    const RealVec x_[],
			    const Real R[],
			    const RealTensor2& h2Inv,
			    const RealTensor2& h2InvGrad,
			    FieldQty q[],
			    FieldQty qErr[],
			    FieldQtyGrad qGrad[],
			    FieldQtyGrad qGradErr[]) {

  // Action depends on whether this node is a leaf or not
  LocalNode& nd = nodes[nodeIdx];
  if (nd.splitDim == -1) {

    // Node is a leaf

    // Loop over packets within leaf, adding contribution from each.
    // Note that for this loop we take advantage of the fact that both
    // the packets we're working on and the packets in the leaf we're
    // examining are sorted by rigidity, so we don't need to examine
    // every packet versus every other packet; instead, we start with
    // the highest-rigidity packets and work down to the lower
    // rigidity ones.
    SignedIdxType rIdx = nd.count;
    for (SignedIdxType i = count - 1; i >= 0; i--) {

      // Move the pointer so that it points at the first packet in the
      // leaf with rigidity >= rigidity of the target packet
      while (1) {
	if (rIdx == 0) break;
	if (pd[nd.start + rIdx - 1].R() < R[i]) break;
	rIdx--;
      }

      // Loop over packets with rigidities high enough to contribute
      for (IdxType j = rIdx; j < nd.count; j++) {

	// Suppress self-contribution
	if (start == nd.start && i == (SignedIdxType) j) continue;

	// Grab packet that is contributing
	RealVec& xAdd = x[nd.start + j];
	CRPacket& pdAdd = pd[nd.start + j];

	// Add contribution
	RealVec dx = geom.disp(x_[i], xAdd);
	q[i].addPacket(pdAdd, dx, h2Inv);
	if (needFQG) qGrad[i].addPacket(pdAdd, dx, h2InvGrad);
      }
    }

  } else {

    // Node is not a leaf

    // Loop over the packets in the target leaf
    for (IdxType i = 0; i < count; i++) {

      // Find the rigidity bin to use for this packet; the bin we
      // choose corresponds to the largest rigidity that is <= the
      // rigidity of the target packet; handle special case where
      // rigidity of packet is larger than that of any packet in the
      // tree (can happen when dealing with newly-added packets in the
      // buffer).
      Real logR = log(R[i]);
      int rIdx, rIdxP;
      if (logR > logR1) {
	// Packet is above maximum rigidity, so contribution is zero
	qErr[i].setZero();
	if (needFQG) qGradErr[i].setZero();
	continue;
      }	else if (logR == logR1) {
	// Packet is at largest rigidity; handle manually to avoid
	// roundoff issues
	rIdx = rIdxP = TREE_RIGIDITY_BINS - 1;
      } else if (logR < logR0) {
	// Packet is below minimum rigidity, so set both pointers to
	// the lowest rigidity bin, since all packets in the node
	// contribute
	rIdx = rIdxP = 0;
      } else {
	// Packet is properly bracketed	
	rIdx = static_cast<int>((logR - logR0) / dlogR);
	if (rIdx != TREE_RIGIDITY_BINS-1) rIdxP = rIdx + 1;
	else rIdxP = TREE_RIGIDITY_BINS;
      }

      // Get the minimum and maximum of the weight and weight gradient
      // (if required) functions over the node
      Real wMin, wMax;
      RealVec wGradMin, wGradMax;
      nodeWgtLimits(x_[i], nd.bnd, h2Inv, h2InvGrad,
		    wMin, wMax, wGradMin, wGradMax);

      // Add the mean of the min and max contributions to the running
      // totals, and half their difference to the error. Note that the
      // code here assumes that wMin, wMax, and fQ are
      // positive-definite quantities.
      FieldQty fQMin = wMin * nd.fQ[rIdxP];
      FieldQty fQMax = wMax * nd.fQ[rIdx];
      q[i] = 0.5 * (fQMin + fQMax);
      qErr[i] = 0.5 * (fQMax - fQMin);

      // Repeat for gradients if required; these require a bit more
      // care about signs, because wGradMin and wGradMax are not
      // positive-definite quantities.
      if (needFQG) {
	FieldQtyGrad fQg1, fQg2, fQg3, fQg4;
	for (int n=0; n<nFieldQty; n++) {
	  fQg1[n] = wGradMin * nd.fQ[rIdx][n];
	  fQg2[n] = wGradMin * nd.fQ[rIdxP][n];
	  fQg3[n] = wGradMax * nd.fQ[rIdx][n];
	  fQg4[n] = wGradMax * nd.fQ[rIdxP][n];
	}
	FieldQtyGrad fQgMin = min(min(min(fQg1, fQg2), fQg3), fQg4);
	FieldQtyGrad fQgMax = max(max(max(fQg1, fQg2), fQg3), fQg4);
	qGrad[i] = 0.5 * (fQgMin + fQgMax);
	qGradErr[i] = 0.5 * (fQgMax - fQgMin);
      }
    }
  }
}


void CRTree::nodeWgtLimits(const RealVec& x_,
			   const RealBox& bnd,
			   const RealTensor2& h2Inv,
			   const RealTensor2& h2InvGrad,
			   Real& wMin,
			   Real& wMax,
			   RealVec& wGradMin,
			   RealVec& wGradMax) const {
  
  // Get box in reference frame centered on target packet
  RealBox box = bnd - x_;

  // Do periodic loop; if the box in this reference frame loops around
  // the periodic boundaries, this will break it into sub-boxes that
  // go up to but not across the boundary
  vector<RealBox> vrb = geom.periodicLoopBox(box, true);

  // If the periodic loop broke our box up into multiple sub-boxes,
  // call this routine recursively on each of them, then combine the
  // results and return that
  if (vrb.size() > 1) {
    wMin = maxReal;
    wMax = -maxReal;
    if (needFQG) {
      wGradMin = maxReal;
      wGradMax = -maxReal;
    }
    for (auto rb : vrb) {
      Real wMin_, wMax_;
      RealVec wGradMin_, wGradMax_;
      nodeWgtLimits(x_, rb, h2Inv, h2InvGrad,
		    wMin_, wMax_, wGradMin_, wGradMax_);
      if (wMin_ < wMin) wMin = wMin_;
      if (wMax_ > wMax) wMax = wMax_;
      if (needFQG) {
	wGradMin = min(wGradMin, wGradMin_);
	wGradMax = max(wGradMax, wGradMax_);
      }
    }
    return;
  }

  // If we're here, we have only a single box, which we have now
  // converted to coordinates centered on the packet; we'll work with
  // that
  box = vrb[0];

  // Get extrema of weight function on the box
  box.gaussExtrema(h2Inv, wMin, wMax);

  // If we do not need the extrema of the gradient weight function, we
  // are now down
  if (!needFQG) return;

  // We now need to find the maximum and minimum of the gradient
  // weight function
  // dw / dx_i = -h2inv_ij x_j w,
  // where
  // w = exp( -h2inv_ij x_i x_j / 2)
  // is the weight function, and we note here that the h2inv value
  // used here is not the same as the one used to compute the field
  // quantities themselves. The extrema are defined by the condition
  // d/dx_j (dw / dx_i) = (x_l x_k h_ik h_jl - h_ij) w = 0.
  // Unfortunately, solving this equation in the box interior, and
  // constrained on box faces and edges, as we do for w, results in
  // solutions that are extraordinarily long and intractable. Instead,
  // we use a weaker but much more easily computable strategy to bound
  // dw / dx_i, by noting that its extrema are bounded by the extrema
  // on w multiplied by the extrema on -h2inv_ij x_j. That is,
  // max( dw / dx_i ) <= max(w) * max( -h2inv_ij x_j ),
  // and similarly for the minima. Since h2inv_ij x_j is a linear
  // function of position, and thus has no local maxima or minima,
  // implying that the maximum and minimum values always occur on box
  // corners.

  // Find extrema of -h2inv_ij x_j
  array<RealVec,8> corners = box.corners();
  RealVec cMin = -h2InvGrad * corners[0];
  RealVec cMax = cMin;
  for (int i=1; i<8; i++) {
    RealVec c = -h2InvGrad * corners[i];
    cMin = min(cMin, c);
    cMax = max(cMax, c);
  }

  // Find exterma of w -- note that these are different than the wMin
  // and wMax found above, because they use a different bandwidth
  Real wGMin, wGMax;
  box.gaussExtrema(h2InvGrad, wGMin, wGMax);

  // Get min and max of dw / dx_i; note that this requires some care
  // about signs, since we handle things differently dependng on
  // whether dw / dx_i is positive or negative
  for (int i=0; i<3; i++) {
    wGradMin[i] = cMin[i] < 0 ? cMin[i] * wGMax : cMin[i] * wGMin;
    wGradMax[i] = cMax[i] > 0 ? cMax[i] * wGMax : cMax[i] * wGMin;
  }
}  


void CRTree::injectPackets(const Real tStart,
			   const Real tStop) {

  // Make sure we have a valid packet rate if sources are present
  if (src.size() > 0 && !(packetRate > 0))
    MPIUtil::haltRun("CRTree::injectPackets: packetRate must be "
		     "set in input file if sources are present",
		     errBadInputFile);

  // Figure out number of packets to draw for each source; this is
  // done differently depending on the sampling strategy
  vector<IdxType> nDraw(src.size());
  IdxType nDrawTot = 0;
  if (equalSampling) {

    // For equal sampling, packet rate is divided equally between
    // injected species
    IdxType nSpec = 0;
    for (IdxType i = 0; i < totSrcWgtSpec.size(); i++)
      if (totSrcWgtSpec[i] > 0.0) nSpec++;
#ifdef _OPENMP
#pragma omp parallel for reduction(+:nDrawTot)
#endif
    for (IdxType i = 0; i < src.size(); i++) {
      Real wgt = src[i].wgt(qSamp);
      if (wgt > 0.0) {
	nDraw[i] = lround( (tStop - tStart) * packetRate / nSpec *
			   wgt /
			   totSrcWgtSpec[src[i].type]);
	nDrawTot += nDraw[i];
      }
    }

  } else if (samplingRatio.size() > 0) {

    // Manually-specified ratio of packet injection rates between
    // species
    Real specNorm = 0.0;
    for (IdxType i = 0; i < totSrcWgtSpec.size(); i++)
      if (totSrcWgtSpec[i] > 0.0) specNorm += samplingRatio[i];
#ifdef _OPENMP
#pragma omp parallel for reduction(+:nDrawTot)
#endif
    for (IdxType i = 0; i < src.size(); i++) {
      Real wgt = src[i].wgt(qSamp);
      if (wgt > 0.0) {
	nDraw[i] = lround( (tStop - tStart) * packetRate *
			   samplingRatio[i] / specNorm *
			   wgt /
			   totSrcWgtSpec[src[i].type]);
	nDrawTot += nDraw[i];
      }
    }    
    
  } else {
    
    // For proportional sampling, treat all injected species the same
#ifdef _OPENMP
#pragma omp parallel for reduction(+:nDrawTot)
#endif
    for (IdxType i = 0; i < src.size(); i++) {
      nDraw[i] = lround( (tStop - tStart) * packetRate *
			 src[i].wgt(qSamp) / totSrcWgt);
      nDrawTot += nDraw[i];
    }
    
  }

  // Allocate storage
  vector<RealVec> xNew(nDrawTot);
  vector<CRPacket> pdNew(nDrawTot);
  vector<IdxType> leaf, leafStart;
  if (needFQ) {
    leaf.resize(src.size());
    leafStart.resize(src.size()+1);
  }

  // Now draw packets; note that this loop is not threaded, because
  // the drawing procedure within each source is threaded
  for (IdxType i = 0, ptr = 0; i < src.size(); i++) {

    // Record which leaf these packets will go into
    if (needFQ) {
      leaf[i] = srcLeaf[i];
      leafStart[i] = ptr;
    }

    // Draw packets and drop into correct location in storage array
    const vector<CRPacket> pdTmp =
      src[i].draw(nDraw[i], tStart, tStop, qSamp, rng);
    copy(pdTmp.begin(), pdTmp.end(), pdNew.begin() + ptr);

    // Set positions; note that, in cases where hDither is
    // non-zero, we dither the packet positions by assigning them to
    // be at a uniformly-distributed random distance from the
    // source, rather than placing them all exactly at the source
    // position. The reason for this is to ensure that, in propagation
    // models where the direction of the gradient matters, the
    // gradient of the newly-added packets points radially outward
    // from the source.
    if (hDither > 0.0) {
#ifdef _OPENMP
#pragma omp parallel for
#endif
      for (IdxType j = 0; j < nDraw[i]; j++) {
	Real r = rng.uniform(0, hDither);
	Real mu = rng.uniform(-1.0, 1.0);
	Real phi = rng.uniform(0.0, 2.0*M_PI);
	xNew[j+ptr][0] = xSrc[i][0] + r * sqrt(1.0 - mu*mu) * cos(phi);
	xNew[j+ptr][1] = xSrc[i][1] + r * sqrt(1.0 - mu*mu) * sin(phi);
	xNew[j+ptr][2] = xSrc[i][2] + r * mu;
	Real dt = pdTmp[j].tInj - tStart;
	xNew[j+ptr] += src[i].v * dt + 0.5 * src[i].a * dt * dt;
      }
    } else {
#ifdef _OPENMP
#pragma omp parallel for
#endif
      for (IdxType j = 0; j < nDraw[i]; j++) {
	Real dt = pdTmp[j].tInj - tStart;	
	xNew[j+ptr] = xSrc[i] + src[i].v * dt + 0.5 * src[i].a * dt * dt;
      }
    }

    // Increment pointer
    ptr += nDraw[i];
  }
  if (needFQ) leafStart.back() = xNew.size();
    
  // Add new packets to the buffer
  IdxType off = xBuf.size();
  addPackets(xNew, pdNew);

  // Fill field quantities for the packets we just injected; this
  // requires that we group the packets by leaf, but since we may have
  // thousands of packets added that all belong to the same leaf, this
  // inhibits efficient threaded processing, since parallelism is done
  // by leaf. To avoid this problem, we break up long lists of packets
  // that all belong to the same leaf into pseudo-leaves of no more
  // than leafSize elements before calling the fillFieldQty
  // routine. Also note that we cannot fill field quantities for the
  // special case where there are no packets in the tree at the start
  // of the time step; if that is the case, we simply skip filling the
  // field quantities this time step.
  if (needFQ && nPacket() > 0) {
    vector<IdxType> start, count, leaf_;
    if (xNew.size() > 0) {
      IdxType pPtr = off, leafPtr = 0;
      while (pPtr < xBuf.size()) {
	start.push_back(pPtr + x.size());
	leaf_.push_back(leaf[leafPtr]);
	pPtr += leafSize;
	if (pPtr - off >= leafStart[leafPtr+1]) {
	  pPtr = leafStart[leafPtr+1] - off;
	  leafPtr++;
	}
      }
      count.resize(start.size());
      for (IdxType i = 0; i < start.size()-1; i++)
	count[i] = start[i+1] - start[i];
      count.back() = xBuf.size() - start.back() + x.size();
    }
    fillFieldQty(start, count, leaf_);
  }
}


Real CRTree::advancePackets(const Real tStart, const Real tStop) {

  // Save advance time and time step mean
  vector<Real> t(nPacketLoc());

  // Initialize recorders
  Real dtMin = maxReal;
  Real logDtSum = 0.0;
  IdxType nDel = 0;

  // Print status
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: starting advance loop, tStart = " << tStart
	 << ", tStop = " << tStop << ", dt = "
	 << tStop - tStart << "..." << endl;

  // Outer loop -- iterate until all packets are processed, including
  // secondaries created during this update
  IdxType start = 0;
  IdxType end = nPacketLoc();
  while (1) {

    // Storage for secondaries created during this iteration
#ifdef _OPENMP
    int nth = omp_get_max_threads();
#else
    int nth = 1;
#endif
    vector<vector<RealVec> > secondaryPositions(nth);
    vector<vector<CRPacket> > secondaryPackets(nth);
    vector<vector<FieldQty> > secondaryQty(nth);
    vector<vector<FieldQtyGrad> > secondaryQtyGrad(nth);

    // Print status
    if (verbosity > 2 && MPIUtil::IOProc)
      cout << "CRTree: advance loop pass with "
	   << end-start << " packets, dt = " << tStop-tStart
	   << "..." << endl;
    
    // Loop over packets
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) reduction(min:dtMin) reduction(+:logDtSum) reduction(+:nDel)
#endif
    for (IdxType i=start; i<end; i++) {

      // Grab pointer to data for this packet
      RealVec& x_ = getPacketPos(i);
      CRPacket& pd_ = getPacketData(i);
      const FieldQty& q_ = getPacketQty(i);
      const FieldQtyGrad& qGrad_ = getPacketQtyGrad(i);
      const Real& h_ = getPacketScale(i);

      // Grab pointer to secondary positions and packets lists that is
      // private to this thread
#ifdef _OPENMP
      int th = omp_get_thread_num();
#else
      int th = 0;
#endif
      vector<RealVec>& secondaryPositions_ = secondaryPositions[th];
      vector<CRPacket>& secondaryPackets_ = secondaryPackets[th];
      IdxType nSec = secondaryPositions_.size();

      // Advance the packet
      Real dt;
      t[i] = advancePacket(tStart, tStop, geom, gasBG, prop, loss, rng,
			   q_, qGrad_, cStep, errTol, wFracMin,
			   pMin, TMin, packetDtMin, h_, x_, pd_, dt,
			   secondaryPositions_, secondaryPackets_);

      // Update global time step data if we have field quantities; the
      // global time step is only meaningful if field quantities exist
      if (needFQ) {
	if (dt < dtMin) dtMin = dt;
	logDtSum += log(dt);
      }

      // Record if this packet is going to be deleted
      if (pd_.w < 0) nDel++;

      // If secondaries were created by this packet, copy field
      // quantities from primary to secondary
      if (secondaryPositions_.size() > nSec && needFQ) {
	secondaryQty[th].insert(secondaryQty[th].end(),
				secondaryPositions_.size() - nSec,
				q_);
	if (needFQG) {
	  secondaryQtyGrad[th].insert(secondaryQtyGrad[th].end(),
				      secondaryPositions_.size() - nSec,
				      qGrad_);
	}
      }
    }	

    // Count total number of secondaries created, and if no
    // secondaries are created *on any MPI rank* exit loop -- we
    // require that there be no secondaries on any rank to exit
    // because if any rank creates secondaries, then *all* ranks must
    // call addPackets below, since this step involve an MPI
    // synchronization to ensure that that the global packet counter
    // remains in sync between ranks
    vector<IdxType> nSec(nth+1);
    nSec[0] = 0;
    for (int i = 0; i < nth; i++)
      nSec[i+1] = nSec[i] + secondaryPackets[i].size();
    IdxType nSecMax = nSec.back();
#ifdef ENABLE_MPI
    MPI_Allreduce(MPI_IN_PLACE, &nSecMax, 1, MPIUtil::MPI_Idx, MPI_MAX,
                  MPI_COMM_WORLD);
#endif
    if (nSecMax == 0) break;

    // Print if sufficiently verbose
    if (verbosity > 2 && MPIUtil::IOProc) {
      cout << "CRTree: rank 0: advanced " << end-start
	   << " packets, created " << nSec.back()
	   << " secondaries; advancing secondaries..."
	   << endl;
    }

    // If we are here, secondaries exist; merge the individual thread
    // lists in preparation for adding to main buffer
    vector<RealVec> secPositions(nSec.back());
    vector<CRPacket> secPackets(nSec.back());
    vector<FieldQty> secQty(nSec.back());
    vector<FieldQtyGrad> secQtyGrad(nSec.back());
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i = 0; i < nth; i++) {
      copy(secondaryPositions[i].begin(),
	   secondaryPositions[i].end(),
	   secPositions.begin() + nSec[i]);
      copy(secondaryPackets[i].begin(),
	   secondaryPackets[i].end(),
	   secPackets.begin() + nSec[i]);
      if (needFQ) {
	copy(secondaryQty[i].begin(),
	     secondaryQty[i].end(),
	     secQty.begin() + nSec[i]);
      }
      if (needFQG) {
	copy(secondaryQtyGrad[i].begin(),
	     secondaryQtyGrad[i].end(),
	     secQtyGrad.begin() + nSec[i]);
      }
    }

    // Add secondary packets to main packet list and fill field quantites
    addPackets(secPositions, secPackets);
    if (needFQ) {
      for (IdxType i = 0; i < secQty.size(); i++)
	getPacketQty(i + start) = secQty[i];
    }
    if (needFQG) {
      for (IdxType i = 0; i < secQtyGrad.size(); i++)
	getPacketQtyGrad(i + start) = secQtyGrad[i];
    }

    // Expand list of times
    t.resize(t.size() + nSec.back());

    // Update start and end pointers to process the next set of secondaries
    start = end;
    end += nSec.back();
  }
  
  // Print status
  if (verbosity > 1 && MPIUtil::IOProc)
    cout << "CRTree: advance loop complete, computing next time step"
	 << endl;

  // Compute next time step; if we do not have field quantities this
  // is meaningless so we return 0
  Real dtNext = 0.0;
  if (needFQ) {
    
    // Reduce time step data across ranks
    IdxType nPacketTot = x.size() + xBuf.size();
#ifdef ENABLE_MPI
    MPI_Allreduce(MPI_IN_PLACE, &dtMin, 1, MPIUtil::MPI_Real, MPI_MIN,
                  MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &logDtSum, 1, MPIUtil::MPI_Real, MPI_SUM,
                  MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &nPacketTot, 1, MPIUtil::MPI_Idx, MPI_SUM,
                  MPI_COMM_WORLD);
#endif
    Real dtMean = exp(logDtSum / nPacketTot);

    // Set new time global step estimate
    dtNext = pow(dtMin, cMinWgt) * pow(dtMean, 1 - cMinWgt);
  }

  // Move packets from packet list that have been marked for deletion
  // to the deleted list; note that this procedure does not retain
  // packet order, but this doesn't matter because we will be
  // re-building the tree in the next advance anyway
  if (nDel > 0) {

    // Set some pointers
    IdxType delPtr = xDel.size();
    IdxType bufPtr = x.size();
    
    // Expand deleted packet holders
    xDel.resize(xDel.size() + nDel);
    pdDel.resize(pdDel.size() + nDel);
    tDel.resize(tDel.size() + nDel);

    // Loop over packet list, starting from the back; for each entry,
    // if it is to be deleted, we copy it to the deleted list, then
    // overwrite it with the last element in the list
    for (SignedIdxType ptr = x.size()-1; ptr >= 0; ptr--) {

      // Check for deletion flag
      if (pd[ptr].w <= 0.0) {
	
	// Copy to deleted list; flip weight in deleted list back to
	// positive
	xDel[delPtr] = x[ptr];
	pdDel[delPtr] = pd[ptr];
	tDel[delPtr] = t[ptr];
	pdDel[delPtr].w = -pdDel[delPtr].w;

	// Overwrite with current last element
	x[ptr] = x.back();
	pd[ptr] = pd.back();
	if (needFQ) {
	  qty[ptr] = qty.back();
	  h[ptr] = h.back();
	  if (needFQG) qtyGrad[ptr] = qtyGrad.back();
	}

	// Pop last element of packet lists
	x.pop_back();
	pd.pop_back();
	if (needFQ) {
	  qty.pop_back();
	  h.pop_back();
	  if (needFQG) qtyGrad.pop_back();
	}

	// Increment pointer in deleted list
	delPtr++;
      }
    }

    // Repeat all the steps above for the buffer
    for (SignedIdxType ptr = xBuf.size()-1; ptr >= 0; ptr--) {
      if (pdBuf[ptr].w <= 0.0) {
	xDel[delPtr] = xBuf[ptr];
	pdDel[delPtr] = pdBuf[ptr];
	tDel[delPtr] = t[ptr + bufPtr];
	pdDel[delPtr].w = -pdDel[delPtr].w;
	xBuf[ptr] = xBuf.back();
	pdBuf[ptr] = pdBuf.back();
	if (needFQ) {
	  qtyBuf[ptr] = qtyBuf.back();
	  hBuf[ptr] = hBuf.back();
	  if (needFQG) qtyGradBuf[ptr] = qtyGradBuf.back();
	}
	xBuf.pop_back();
	pdBuf.pop_back();
	if (needFQ) {
	  qtyBuf.pop_back();
	  hBuf.pop_back();
	  if (needFQG) qtyGradBuf.pop_back();
	}
	delPtr++;
      }
    }

  }

  // Return estimate of next global time step
  return dtNext;
}



vector<Real> CRTree::photLumSummary(const Real t,
				    const vector<Real>& en,
				    const int rank) const {

  // Allocate memory to hold result; if running in threaded mode,
  // allocate one block per thread to avoid thread-locking, and reduce
  // across threads at the end
  const IdxType blockSize = lossMech::nMech * en.size();
  const IdxType outSize = partTypes::nPartType * 2 * blockSize;
#ifdef _OPENMP
  vector<Real> dLdE(outSize * omp_get_max_threads());
#else
  vector<Real> dLdE(outSize);
#endif
  for (IdxType i = 0; i < dLdE.size(); i++) dLdE[i] = 0.0;
  
  // Loop over packets, adding contribution to each
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (IdxType i = 0; i < pd.size(); i++) {

    // Point to location in output block where this packet's
    // contribution should be added
    const int ptr = pd[i].isSecondary() + 2 * ((int) pd[i].type);
    IdxType offset = blockSize * ptr;
#ifdef _OPENMP
    offset += omp_get_thread_num() * outSize;
#endif
    
    // Compute emission for this packet
    gas::GasData gd = gasBG.gasData(x[i], t);
    vector<Real> dLdEPacket = loss.photLum(gd, pd[i], en);

    // Add contribution from this packet to running total
    for (IdxType j = 0; j < blockSize; j++)
      dLdE[offset + j] += dLdEPacket[j];
    
  }

  // Now reduce across threads
#ifdef _OPENMP
#pragma omp parallel for
  for (IdxType i = 0; i < outSize; i++) {
    for (int j = 1; j < omp_get_max_threads(); j++)
      dLdE[i] += dLdE[i + j * outSize];
  }
  dLdE.resize(outSize);
#endif

  // Final reduction across MPI ranks
#ifdef ENABLE_MPI
  if (rank == -1) {
    MPI_Allreduce(MPI_IN_PLACE, dLdE.data(), outSize, MPIUtil::MPI_Real,
		  MPI_SUM, MPI_COMM_WORLD);
  } else if (rank > 0) {
    if (rank == MPIUtil::myRank)
      MPI_Reduce(MPI_IN_PLACE, dLdE.data(), outSize, MPIUtil::MPI_Real,
		 MPI_SUM, rank, MPI_COMM_WORLD);
    else
      MPI_Reduce(dLdE.data(), nullptr, outSize, MPIUtil::MPI_Real,
		 MPI_SUM, rank, MPI_COMM_WORLD);
  }
#endif

  // Return
  return dLdE;
}

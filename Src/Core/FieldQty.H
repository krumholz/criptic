/**
 * @file FieldQty.H
 * @brief Define classes for field quantities and their gradients.
 *
 * @details
 * In criptic, "field quantities" are quantities that can be computed
 * from the CR field, for example the CR number density, energy
 * density, or pressure. This module provides two classes,
 * FieldQty and FieldQtyGrad, which define field quantities and their
 * gradients.
 */

#ifndef _FIELDQTY_H_
#define _FIELDQTY_H_

#include <array>
#include <string>
#include <vector>
#include "CRPacket.H"
#include "../Definitions.H"
#include "../Utils/Constants.H"
#include "../Utils/RealTensor2.H"
#include "../Utils/Types.H"
#include "../Utils/Units.H"
#include "../Utils/Vec3.H"

namespace criptic {

  /**
   * @brief Enum of field quantities
   */
  typedef enum {
    ndenIdx = 0,  /**< Number density */
    presIdx,      /**< Pressure */
    edenIdx,      /**< Kinetic energy density */
    nFieldQty     /**< Total number of field quantities */ 
  } FieldQtyType;
  
  //////////////////////////////////////////////////////////////
  // FieldQty class
  //////////////////////////////////////////////////////////////

  /**
   * @class FieldQty
   * @brief Class to hold and compute field quantities
   * @details
   * In criptic, "field quantities" are quantities that can be computed
   * from the CR field, for example the CR energy density or
   * pressure. This class holds the field quantities and provides methods
   * for computing them. Note that the algorithm used to compute field
   * quantities assumes they are positive definite.
   */
  class FieldQty {

  public:

    std::array<Real,nFieldQty> q;  /**< Array of field quantities */

    // Names and units of field quantities -- used in ASCII output
    static constexpr int qtyNameLen = 8; /**< Maximum length of a 
					    quantity name */
    static constexpr std::array<char[qtyNameLen],nFieldQty> qtyNames =
      {{ "nden", "pres", "eden" }};  /**< Names of field quantities
 				          used in ASCII output */
    static constexpr std::array<Real,nFieldQty> qtyUnits = {
      1.0, constants::mp_c2 / constants::kB, constants::mp_c2 / units::eV
    };  /**< Unit conversions from internal to ASCII output units */

    /**
     * @brief Construct a field quantity filled with zeros
     */
    FieldQty() {
      for (int i = 0; i < nFieldQty; i++) q[i] = 0;
    }
    
    /**
     * @brief Set all field quantities to zero
     */
    void setZero() {
      for (int i=0; i<nFieldQty; i++) q[i] = 0.0;
    }

    /**
     * @brief Get weight with which packet contributes to field quantities
     * @param pkt Packet whose weight is being computed
     * @returns Weight of the packet in all quantities
     * @details
     * The weights returned are the quantities that are multiplied by
     * the kernel density weight to estimate the field quantities at a
     * given point.
     */
    std::array<Real,nFieldQty> getWgt(const CRPacket& pkt) const {
      std::array<Real,nFieldQty> w;
      w[ndenIdx] = pkt.w;
      w[presIdx] = pkt.w * pkt.v() * pkt.p;
      w[edenIdx] = pkt.w * pkt.T();
      return w;
    }
    
    /**
     * @brief Add contribution of packet to field quantities at target point
     * @param pkt Packet being added
     * @param r Vector from packet to target point
     * @param h2Inv Squared inverse of bandwidth matrix
     * @details
     * The addition done in this routine is non-normalized, so once
     * all packets have been added, divide by (2 pi)^(3/2) det(h) to
     * get properly normalized quantities.
     */
    void addPacket(const CRPacket& pkt,
		   const RealVec& r,
		   const RealTensor2& h2Inv) {
      Real k = std::exp( -0.5 * h2Inv.contract(r, r) );
      q[ndenIdx] += k * pkt.w;
      q[presIdx] += k * pkt.w * pkt.v() * pkt.p;
      q[edenIdx] += k * pkt.w * pkt.T();
    }

    /**
     * @fn criptic::FieldQty::operator[](int)
     * @brief Return an element of the FieldQty
     * @param i The index of the quantity to return
     * @return The value of quantity t
     */
    Real& operator [] (int i) {
      return q[i];
    }
    /**
     * @fn criptic::FieldQty::operator[](int) const
     * @brief Return an element of the FieldQty
     * @param i The index of the quantity to return
     * @return The value of quantity t
     */
    const Real& operator [] (int i) const {
      return q[i];
    }
    
    // Binary operations with two FieldQty objects
    
    /**
     * @fn criptic::FieldQty::operator+(const FieldQty&) const
     * @brief Add two FieldQty objects
     * @param a The FieldQty to add
     * @return A FieldQty containing the elementwise sum
     */
    FieldQty operator+(const FieldQty& a) const {
      FieldQty fq;
      for (int i=0; i<nFieldQty; i++) fq.q[i] = q[i] + a.q[i];
      return fq;
    }
    /**
     * @fn criptic::FieldQty::operator-(const FieldQty&) const
     * @brief Subtract two FieldQty objects
     * @param a The FieldQty to subtract
     * @return A FieldQty containing the elementwise difference
     */
    FieldQty operator-(const FieldQty& a) const {
      FieldQty fq;
      for (int i=0; i<nFieldQty; i++) fq.q[i] = q[i] - a.q[i];
      return fq;
    }
    /**
     * @fn criptic::FieldQty::operator/(const FieldQty&) const
     * @brief Divide two FieldQty objects
     * @param a The FieldQty by which to divide
     * @return An array containing the elementwise division
     */
    std::array<Real,nFieldQty> operator/(const FieldQty& a) const {
      std::array<Real,nFieldQty> arr;
      for (int i=0; i<nFieldQty; i++) arr[i] = q[i] / a.q[i];
      return arr;
    }

    // Binary operations with scalars
    /**
     * @fn criptic::FieldQty::operator*(const Real) const
     * @brief Multiply a FieldQty by a scalar
     * @param a The scalar by which to multiply
     * @return A FieldQty containing the elementwise product
     */
    FieldQty operator*(const Real a) const {
      FieldQty fq;
      for (int i=0; i<nFieldQty; i++) fq.q[i] = q[i] * a;
      return fq;
    }
    /**
     * @fn criptic::FieldQty::operator/(const Real) const
     * @brief Divide a FieldQty by a scalar
     * @param a The scalar by which to divide
     * @return A FieldQty containing the elementwise dividend
     */
    FieldQty operator/(const Real a) const {
      FieldQty fq;
      for (int i=0; i<nFieldQty; i++) fq.q[i] = q[i] / a;
      return fq;
    }
    
    // Unary operations with scalars
    /**
     * @fn criptic::FieldQty::operator*=(const Real&)
     * @brief Multiply a FieldQty by a scalar
     * @param a The scalar by which to multiply
     * @return This FieldQty multiplied by a
     */
    FieldQty& operator*=(const Real& a) {
      for (int i=0; i<nFieldQty; i++) q[i] *= a;
      return *this;
    }
    /**
     * @fn criptic::FieldQty::operator/=(const Real&)
     * @brief Divide a FieldQty by a scalar
     * @param a The scalar by which to divide
     * @return This FieldQty divided by a
     */
    FieldQty& operator/=(const Real& a) {
      for (int i=0; i<nFieldQty; i++) q[i] /= a;
      return *this;
    }    
    
    // Unary operations with field quantities
    /**
     * @fn criptic::FieldQty::operator+=(const FieldQty&)
     * @brief Add a FieldQty object to this one
     * @param a The FieldQty to add
     * @return This FieldQty
     */
    FieldQty& operator+=(const FieldQty& a) {
      for (int i=0; i<nFieldQty; i++) q[i] += a.q[i];
      return *this;
    }
    /**
     * @fn criptic::FieldQty::operator-=(const FieldQty&)
     * @brief Subtract a FieldQty object from this one
     * @param a The FieldQty to subtract
     * @return This FieldQty
     */
    FieldQty& operator-=(const FieldQty& a) {
      for (int i=0; i<nFieldQty; i++) q[i] -= a.q[i];
      return *this;
    }

  }; // end class FieldQty
  
  /**
   * @brief Multiply a scalar by a FieldQty elementwise
   * @param a The scalar
   * @param fq The FieldQty
   * @return A FieldQty containing the elementwise product a*q
   */
  inline criptic::FieldQty operator*(const criptic::Real a,
				     const criptic::FieldQty& fq) {
    FieldQty f;
    for (int i=0; i<nFieldQty; i++) f.q[i] = a * fq.q[i];
    return f;
  }

  /**
   * @brief Take elementwise minimum of two FieldQty objects
   * @param q1 The first FieldQty
   * @param q2 The second FieldQty
   * @return The elementwise minimum of q1 and q2
   */
  inline criptic::FieldQty min(const criptic::FieldQty& q1,
			       const criptic::FieldQty& q2) {
    FieldQty f;
    for (int i=0; i<nFieldQty; i++)
      f.q[i] = std::min(q1.q[i], q2.q[i]);
    return f;
  }

  /**
   * @brief Take elementwise maximum of two FieldQty objects
   * @param q1 The first FieldQty
   * @param q2 The second FieldQty
   * @return The elementwise maximum of q1 and q2
   */
  inline criptic::FieldQty max(const criptic::FieldQty& q1,
			       const criptic::FieldQty& q2) {
    FieldQty f;
    for (int i=0; i<nFieldQty; i++)
      f.q[i] = std::max(q1.q[i], q2.q[i]);
    return f;
  }
  
  //////////////////////////////////////////////////////////////
  // FieldQtyGrad class
  //////////////////////////////////////////////////////////////

  /**
   * @class FieldQtyGrad
   * @brief Class to hold and compute gradients of field quantities
   */
  class FieldQtyGrad {

  public:
    
    std::array<RealVec,nFieldQty> qGrad; /**<Array of field quantity 
					    gradients */

    /**
     * @brief Construct a field quantity gradient filled with zeros
     */
    FieldQtyGrad() {
      for (int i = 0; i < nFieldQty; i++) qGrad[i] = 0;
    }

    /**
     * @brief Set all field quantities to zero
     */
    void setZero() {
      for (int i=0; i<nFieldQty; i++) qGrad[i] = 0.0;
    }

    /**
     * @brief Add contribution of packet at target point
     * @param pkt Packet being added
     * @param r Vector from packet to target point
     * @param h2Inv Squared inverse of bandwidth matrix
     * @details
     * The addition done in this routine is non-normalized, so once
     * all packets have been added, divide by (2 pi)^(3/2) det(h) to
     * get properly normalized quantities.
     */
    void addPacket(const CRPacket& pkt,
		   const RealVec& r,
		   const RealTensor2& h2Inv) {
      RealVec hr = h2Inv * r;
      RealVec dk = -hr * std::exp( -hr.dot(r) / 2);
      qGrad[ndenIdx] += dk * pkt.w;
      qGrad[presIdx] += dk * pkt.w * pkt.v() * pkt.p;
      qGrad[edenIdx] += dk * pkt.w * pkt.T();
    }
    
    /**
     * @fn criptic::FieldQtyGrad::operator[](int)
     * @brief Return an element of the FieldQtyGrad
     * @param i The index of the quantity gradient to return
     * @return The value of quantity gradient t
     */
    RealVec& operator [] (int i) {
      return qGrad[i];
    }
    /**
     * @fn criptic::FieldQtyGrad::operator[](int) const
     * @brief Return an element of the FieldQtyGrad
     * @param i The index of the quantity gradient to return
     * @return The value of quantity gradient t
     */
    const RealVec& operator [] (int i) const {
      return qGrad[i];
    }

    // Binary operations involving two FieldQtyGrad objects
    /**
     * @fn criptic::FieldQtyGrad::operator+(const FieldQtyGrad&) const
     * @brief Add two FieldQtyGrad objects
     * @param a The FieldQtyGrad to add
     * @return A FieldQtyGrad containing the elementwise sum
     */
    FieldQtyGrad operator+(const FieldQtyGrad& a) const {
      FieldQtyGrad fqg;
      for (int i=0; i<nFieldQty; i++)
	fqg.qGrad[i] = qGrad[i] + a.qGrad[i];
      return fqg;
    }
    /**
     * @fn criptic::FieldQtyGrad::operator-(const FieldQtyGrad&) const
     * @brief Subtract two FieldQtyGrad objects
     * @param a The FieldQtyGrad to subtract
     * @return A FieldQtyGrad containing the elementwise difference
     */
    FieldQtyGrad operator-(const FieldQtyGrad& a) const {
      FieldQtyGrad fqg;
      for (int i=0; i<nFieldQty; i++)
	fqg.qGrad[i] = qGrad[i] - a.qGrad[i];
      return fqg;
    }
    /**
     * @fn criptic::FieldQtyGrad::operator/(const FieldQtyGrad&) const
     * @brief Divide two FieldQtyGrad objects
     * @param a The FieldQtyGrad by which to divide
     * @return An array containing the elementwise division
     */
    std::array<RealVec,nFieldQty> operator/(const FieldQtyGrad& a) const {
      std::array<RealVec,nFieldQty> arr;
      for (int i=0; i<nFieldQty; i++) arr[i] = qGrad[i] / a.qGrad[i];
      return arr;
    }

    // Binary operations with scalars
    /**
     * @fn criptic::FieldQtyGrad::operator*(const Real) const
     * @brief Multiply a FieldQtyGrad by a scalar
     * @param a The scalar by which to multiply
     * @return A FieldQtyGrad containing the elementwise product
     */
    FieldQtyGrad operator*(const Real a) const {
      FieldQtyGrad fqg;
      for (int i=0; i<nFieldQty; i++)
	fqg.qGrad[i] = qGrad[i] * a;
      return fqg;
    }
    /**
     * @fn criptic::FieldQtyGrad::operator/(const Real) const
     * @brief Divide a FieldQtyGrad by a scalar
     * @param a The scalar by which to divide
     * @return A FieldQtyGrad containing the elementwise dividend
     */
    FieldQtyGrad operator/(const Real a) const {
      FieldQtyGrad fqg;
      for (int i=0; i<nFieldQty; i++)
	fqg.qGrad[i] = qGrad[i] / a;
      return fqg;
    }

    // Binary operations with vectors
    /**
     * @fn criptic::FieldQtyGrad::operator*(const RealVec&) const
     * @brief Multiply a FieldQtyGrad by a scalar
     * @param a The scalar by which to multiply
     * @return A FieldQtyGrad containing the elementwise product
     */
    FieldQtyGrad operator*(const RealVec& a) const {
      FieldQtyGrad fqg;
      for (int i=0; i<nFieldQty; i++)
	fqg.qGrad[i] = qGrad[i] * a;
      return fqg;
    }
    
    // Unary operations with scalars
    /**
     * @fn criptic::FieldQtyGrad::operator*=(const Real&)
     * @brief Multiply a FieldQtyGrad by a scalar
     * @param a The scalar by which to multiply
     * @return This FieldQtyGrad multiplied by a
     */
    FieldQtyGrad& operator*=(const Real& a) {
      for (int i=0; i<nFieldQty; i++) qGrad[i] *= a;
      return *this;
    }
    /**
     * @fn criptic::FieldQtyGrad::operator/=(const Real&)
     * @brief Divide a FieldQty by a scalar
     * @param a The scalar by which to divide
     * @return This FieldQty divided by a
     */
    FieldQtyGrad& operator/=(const Real& a) {
      for (int i=0; i<nFieldQty; i++) qGrad[i] /= a;
      return *this;
    }
    
    // Unary operations with field quantity gradients
    /**
     * @fn criptic::FieldQtyGrad::operator+=(const FieldQtyGrad&)
     * @brief Add a FieldQtyGrad object to this one
     * @param a The FieldQtyGrad to add
     * @return This FieldQtyGrad
     */
    FieldQtyGrad& operator+=(const FieldQtyGrad& a) {
      for (int i=0; i<nFieldQty; i++) qGrad[i] += a.qGrad[i];
      return *this;
    }
    /**
     * @fn criptic::FieldQtyGrad::operator-=(const FieldQtyGrad&)
     * @brief Subtract a FieldQtyGrad object from this one
     * @param a The FieldQtyGrd to subtract
     * @return This FieldQtyGrad
     */
    FieldQtyGrad& operator-=(const FieldQtyGrad& a) {
      for (int i=0; i<nFieldQty; i++) qGrad[i] -= a.qGrad[i];
      return *this;
    }
    
  }; // end FieldQtyGrad

  /**
   * @brief Multiply a scalar by a FieldQtyGrad elementwise
   * @param a The scalar
   * @param fqg The FieldQtyGrad
   * @return A FieldQty containing the elementwise product a*q
   */
  inline criptic::FieldQtyGrad operator*(const criptic::Real a,
					 const criptic::FieldQtyGrad& fqg) {
    FieldQtyGrad f;
    for (int i=0; i<nFieldQty; i++) f.qGrad[i] = a * fqg.qGrad[i];
    return f;
  }

  /**
   * @brief Multiply a RealVec by a FieldQtyGrad elementwise
   * @param a The RealVec
   * @param fqg The FieldQtyGrad
   * @return A FieldQty containing the elementwise product a*q
   */
  inline criptic::FieldQtyGrad operator*(const criptic::RealVec& a,
					 const criptic::FieldQtyGrad& fqg) {
    FieldQtyGrad f;
    for (int i=0; i<nFieldQty; i++) f.qGrad[i] = a * fqg.qGrad[i];
    return f;
  }
  
  /**
   * @brief Take elementwise minimum of two FieldQtyGrad objects
   * @param q1 The first FieldQtyGrad
   * @param q2 The second FieldQtyGrad
   * @return The elementwise minimum of q1 and q2
   */
  inline criptic::FieldQtyGrad min(const criptic::FieldQtyGrad& q1,
				   const criptic::FieldQtyGrad& q2) {
    FieldQtyGrad f;
    for (int i=0; i<nFieldQty; i++)
      f[i] = min(q1[i], q2[i]);
    return f;
  }

  /**
   * @brief Take elementwise maximum of two FieldQtyGrad objects
   * @param q1 The first FieldQtyGrad
   * @param q2 The second FieldQtyGrad
   * @return The elementwise maximum of q1 and q2
   */
  inline criptic::FieldQtyGrad max(const criptic::FieldQtyGrad& q1,
				   const criptic::FieldQtyGrad& q2) {
    FieldQtyGrad f;
    for (int i=0; i<nFieldQty; i++)
      f[i] = max(q1[i], q2[i]);
    return f;
  }
  
} // end namespace criptic



// Overload the >> and << operators to make it easy to do IO on field
// quantities
/**
 * @fn operator>>(std::istream&, criptic::FieldQty&)
 * @brief ASCII-formatted read of field quantities from stream
 * @param is The stream to read from
 * @param fQ The FieldQty object in which to store the result
 * @return The processed input stream
 */
std::istream& operator>>(std::istream& is,
			 criptic::FieldQty& fQ);

/**
 * @fn operator>>(std::istream&, criptic::FieldQtyGrad&)
 * @brief ASCII-formatted read of field quantities from stream
 * @param is The stream to read from
 * @param fQG The FieldQtyGrad object in which to store the result
 * @return The processed input stream
 */
std::istream& operator>>(std::istream& is,
			 criptic::FieldQtyGrad& fQG);

/**
 * @fn operator<<(std::ostream&, const criptic::FieldQty&)
 * @brief ASCII-formatted write of field quantities to stream
 * @param os The stream to write to
 * @param fQ The FieldQty object to be written
 * @return The output stream written to
 */
std::ostream& operator<<(std::ostream& os,
			 const criptic::FieldQty& fQ);

/**
 * @fn operator<<(std::ostream&, const criptic::FieldQtyGrad&)
 * @brief ASCII-formatted write of field quantities to stream
 * @param os The stream to write to
 * @param fQG The FieldQtyGrad object to be written
 * @return The output stream written to
 */
std::ostream& operator<<(std::ostream& os,
			 const criptic::FieldQtyGrad& fQG);

#endif
// _FIELDQTY_H_

// Implementation of the FieldQty class

#include <iostream>
#include <iomanip>
#include <ostream>
#include <sstream>
#include <string>
#include <type_traits>
#include "FieldQty.H"
#include "../Utils/Constants.H"
#include "../Utils/Units.H"

using namespace std;
using namespace criptic;

// Overloaded >> operator
istream& operator>>(istream& is,
		    FieldQty& fQ) {
  for (IdxType i=0; i<nFieldQty; i++) {
    is >> fQ.q[i];
    fQ.q[i] /= FieldQty::qtyUnits[i];
  }
  return is;
}

// Overloaded >> operator
istream& operator>>(istream& is,
		    FieldQtyGrad& fQG) {
  for (IdxType i=0; i<nFieldQty; i++) {
    is >> fQG.qGrad[i];
    fQG.qGrad[i] /= FieldQty::qtyUnits[i];
  }
  return is;
}

// Overloaded << operator
ostream& operator<<(ostream& os,
		    const FieldQty& fQ) {
  for (IdxType i=0; i<nFieldQty; i++)
    os << setw(ASCIIWIDTH) << fQ.q[i] * FieldQty::qtyUnits[i];
  return os;
}

// Overloaded << operator
ostream& operator<<(ostream& os,
		    const FieldQtyGrad& fQG) {
  for (IdxType i=0; i<nFieldQty; i++)
    os << setw(ASCIIWIDTH) << fQG.qGrad[i] * FieldQty::qtyUnits[i];
  return os;
}

#ifdef ENABLE_MPI

#include "CRTreeCommunicator.H"
#include "../Core/CRTree.H"
#include <algorithm>

using namespace std;
using namespace criptic;
using namespace criptic::MPIUtil;

CRTreeCommunicator::CRTreeCommunicator(const CRTree* tree_) :
  tree(tree_)
{

  // Initialize outgoing buffers to correct size
  outReqBuf.resize(nRank);
  outRespBuf.resize(nRank);

  // Initialize openMP infrastructure
#ifdef _OPENMP
  outReqBufLock.resize(nRank);
  outRespBufLock.resize(nRank);
  for (int i = 0; i < nRank; i++) {
    omp_init_lock(&outReqBufLock[i]);
    omp_init_lock(&outRespBufLock[i]);
  }
#endif
  
}


CRTreeCommunicator::~CRTreeCommunicator() {
  
  // Take down openMP infrastructure
#ifdef _OPENMP
  for (int i = 0; i < nRank; i++) {
    omp_destroy_lock(&outReqBufLock[i]);
    omp_destroy_lock(&outRespBufLock[i]);
  }
#endif
}

/////////////////////////////////////////////////////////
// Status methods
/////////////////////////////////////////////////////////

void CRTreeCommunicator::initCycle() {

  // Initialize the all done message handles and buffer
  rankDone.assign(nRank, false);
  doneRecvHandle.assign(nRank, MPI_REQUEST_NULL);
  doneSendHandle.assign(nRank, MPI_REQUEST_NULL);
  
  // Post non-blocking receives to receive the "all done" messages
  // from other ranks that will conclude this cycle
  for (int i = 0; i < nRank; i++) {
    if (i != myRank) {
      MPI_Irecv(&rankDone[i], 1, MPI_INT, i, tagWorkDone,
		MPI_COMM_WORLD, &doneRecvHandle[i]);
    }
  }

  // Initialize the request and response handles
  outReqHandle.assign(nRank, MPI_REQUEST_NULL);
  outRespHandle.assign(nRank, vector<MPI_Request>());

  // Initialize the outgoing response buffer to an empty vector of
  // size 1
  outRespBuf.assign(nRank, list<vector<fillResponse> >(1));
  
  // Set all read/write pointers to zero
  inReqPtr = inRespWritePtr = inRespProcPtr = 0;
}

void CRTreeCommunicator::finalizeCycle() {

  // Clear all buffers
  for (int i = 0; i < nRank; i++) {
    outReqBuf[i].clear();
    outRespBuf[i].clear();
  }
  inReqBuf.clear();
  inReqRank.clear();
  inRespBuf.clear();
}

bool CRTreeCommunicator::allDone() {

  // Check if we have already marked outselves as done; if we have, we
  // don't need to test anything except the all done messages
  if (!rankDone[myRank]) {

    // Test if all our outgoing requests have finished transmitting; if
    // not, we are not done
    int flag;
    MPI_Testall(nRank, outReqHandle.data(), &flag, MPI_STATUSES_IGNORE);

    // Test if all our outgoing responses have finished transmitting; if
    // not, we are not done
    for (int i = 0; i < nRank; i++) {
      if (outRespHandle[i].empty()) continue;
      int flagRank;
      MPI_Testall(outRespHandle[i].size(), outRespHandle[i].data(), &flagRank,
		  MPI_STATUSES_IGNORE);
      flag = flag && flagRank;
    }
    if (!flag) return false;

    // Check if we have received all the incoming responses we expect to
    // receive, and if we have processes all of those; if not, we are
    // not done
    if (inRespProcPtr != inRespWritePtr ||
	inRespWritePtr != inRespBuf.size()) return false;
  
    // If we are here, we are done; send our "all done" message to all
    // other ranks if we have not yet done so
    rankDone[myRank] = 1;
    for (int i = 0; i < nRank; i++) {
      if (i == myRank) continue;
      MPI_Isend(rankDone.data() + myRank, 1, MPI_INT, i, tagWorkDone,
		MPI_COMM_WORLD, &doneSendHandle[i]);
    }
    if (tree->verbosity > 2)
      cout << "CRTreeCommunicator: rank " << myRank
	   << ": cycle complete" << endl;
  }
  
  // Test if our sends and receives for "all done" messages have
  // completed; we are done only if all have completed
  int flag;
  MPI_Testall(nRank, doneSendHandle.data(), &flag, MPI_STATUSES_IGNORE);
  if (!flag) return false;
  MPI_Testall(nRank, doneRecvHandle.data(), &flag, MPI_STATUSES_IGNORE);
  if (!flag) return false;
 
  // If we are here, all ranks report they are done, so we can report
  // all done
  return true;
}

/////////////////////////////////////////////////////////
// Registration methods
/////////////////////////////////////////////////////////

void
CRTreeCommunicator::
registerRequest(const IdxType& start,
		const IdxType& count,
		const vector<bool>& defer,
		const vector<int>& rank,
		const RealTensor2& h2Inv,
		const RealTensor2& h2InvGrad,
		const Real& hScale,
		const vector<FieldQty>& qMin) {

  // Create the MPI request
  fillRequest req;
  req.h2Inv = h2Inv;
  req.h2InvGrad = h2InvGrad;
  req.hScale = hScale;
  req.count = 0;
  req.nrank = 1 + rank.size();
  for (IdxType j = 0; j < count; j++) {
    if (defer[j]) {
      req.idx[req.count] = start + j;
      req.x[req.count] = tree->getPacketPos(start + j);
      req.R[req.count] = tree->getPacketData(start + j).R();
      req.qMin[req.count] = qMin[j];
      req.count++;
    }
  }

  // Push the fill requests into the outgoing request buffer
  for (IdxType i = 0; i < rank.size(); i++) {
#ifdef _OPENMP
    omp_set_lock(&outReqBufLock[rank[i]]);
#endif
    outReqBuf[rank[i]].push_back(req);
#ifdef _OPENMP
    omp_unset_lock(&outReqBufLock[rank[i]]);
#endif
  }
}


void
CRTreeCommunicator::
registerResponse(const int& rank,
		 const fillResponse& resp) {
  // Push onto the outgoing response buffer for the appropriate rank
#ifdef _OPENMP
  omp_set_lock(&outRespBufLock[rank]);
#endif
  outRespBuf[rank].back().push_back(resp);
#ifdef _OPENMP
  omp_unset_lock(&outRespBufLock[rank]);
#endif  
}

/////////////////////////////////////////////////////////
// Communication methods
/////////////////////////////////////////////////////////

void CRTreeCommunicator::recv() {

  // Loop until no incoming messages found
  while (true) {

    // Probe for messages
    int flag;
    MPI_Status stat;
    MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &stat);

    // If no messages found, end loop
    if (!flag) break;

    // If we did find a message, first use the tag to determine
    // whether it contains a request or a response
    if (stat.MPI_TAG & tagFillReq) {

      // This is a request; make room in the incoming request buffer
      // and receive it
      int count;
      MPI_Get_count(&stat, MPI_FillReq, &count);
      IdxType ptr = inReqBuf.size();
      inReqBuf.resize(inReqBuf.size() + count);
      if (tree->verbosity > 2)
	cout << "CRTreeCommunicator: rank " << myRank
	     << " receiving " << count
	     << " packet fill requests from rank " << stat.MPI_SOURCE
	     << endl;
      MPI_Recv(inReqBuf.data() + ptr, count, MPI_FillReq,
	       stat.MPI_SOURCE, stat.MPI_TAG, MPI_COMM_WORLD,
	       MPI_STATUS_IGNORE);

      // Record the rank of the process that made the request
      inReqRank.insert(inReqRank.end(), count, stat.MPI_SOURCE);      

    } else {

      // This is a response; place it in the appropriate part of the
      // response buffer and move the write pointer
      int count;
      MPI_Get_count(&stat, MPI_FillResp, &count);
      if (tree->verbosity > 2)
	cout << "CRTreeCommunicator: rank " << myRank
	     << " receiving " << count
	     << " packet fill responses from rank " << stat.MPI_SOURCE
	     << endl;
      MPI_Recv(inRespBuf.data() + inRespWritePtr, count, MPI_FillResp,
	       stat.MPI_SOURCE, stat.MPI_TAG, MPI_COMM_WORLD,
	       MPI_STATUS_IGNORE);
      inRespWritePtr += count;
    }
  }
}


void CRTreeCommunicator::sendRequests() {
  
  // Send requests to all ranks where the buffer is non-empty with
  // non-blocking communication
  IdxType nReq = 0;
  for (int i = 0; i < nRank; i++) {
    if (!outReqBuf[i].empty()) {
      if (tree->verbosity > 2)
	cout << "CRTreeCommunicator: rank " << myRank
	     << " sending " << outReqBuf[i].size()
	     << " field quantity fill requests to rank " << i
	     << endl;
      MPI_Isend(outReqBuf[i].data(), outReqBuf[i].size(), MPI_FillReq, i,
		tagFillReq, MPI_COMM_WORLD, &outReqHandle[i]);
      nReq += outReqBuf[i].size();
    } else {
      outReqHandle[i] = MPI_REQUEST_NULL;
    }
  }

  // Allocate an incoming response buffer to hold the responses we
  // expect to receive, and initialize the writing and processing
  // pointers ot the start of that buffer
  inRespBuf.resize(nReq);
  inRespWritePtr = inRespProcPtr = 0;
}

void
CRTreeCommunicator::sendResponses() {

  // Loop over ranks
  for (int i = 0; i < nRank; i++) {

#ifdef _OPENMP
    // Lock the buffer for this rank
    omp_set_lock(&outRespBufLock[i]);
#endif

    // If the buffer is empty, release the lock and go to the next
    // rank
    if (outRespBuf[i].back().empty()) {
#ifdef _OPENMP
      omp_unset_lock(&outRespBufLock[i]);
#endif
      continue;
    }

    // If we're here, the buffer is non-empty; grab a pointer to the
    // data for the buffer that we're going to send, and push an empty
    // vector onto the back of the buffer list to hold any subsequent
    // data
    vector<fillResponse>& buf = outRespBuf[i].back();
    outRespBuf[i].emplace_back(vector<fillResponse>());

    // Release lock
#ifdef _OPENMP
    omp_unset_lock(&outRespBufLock[i]);
#endif
    
    // Dispatch the MPI send, capturing the handle
    if (tree->verbosity > 2)
      cout << "CRTreeCommunicator: rank " << myRank
	   << " sending " << buf.size()
	   << " packet fill responses to rank " << i
	   << endl;
    MPI_Request req;
    MPI_Isend(buf.data(), buf.size(), MPI_FillResp, i,
	      tagFillResp | (int) outRespHandle[i].size(),
	      MPI_COMM_WORLD, &req);
    outRespHandle[i].push_back(req);
  }
}

#endif
// ENABLE_MPI

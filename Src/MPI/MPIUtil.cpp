// Implementation of the MPIUtil namespace

#include <unistd.h>
#include "MPIUtil.H"
#include "../Core/CRTree.H"
#include "../Core/FieldQty.H"
#include "../Losses/Losses.H"
#ifdef _OPENMP
#   include <omp.h>
#endif

namespace criptic {
  namespace MPIUtil {
    
    // Declare global variables
    int nRank;         // Total number of MPI ranks
    int myRank;        // My rank number
    bool IOProc;       // Is this the processor that does I/O?
    int log2nRank;     // log_2 (nRank)
#ifdef ENABLE_MPI
    // MPI data types
    MPI_Datatype MPI_Real;
    MPI_Datatype MPI_RealVec;
    MPI_Datatype MPI_RealTensor2;
    MPI_Datatype MPI_Idx;
    MPI_Datatype MPI_CRPacket;
    MPI_Datatype MPI_CRSource;
    MPI_Datatype MPI_FieldQty;
    MPI_Datatype MPI_FieldQtyGrad;
    MPI_Datatype MPI_RealMechArr;
    MPI_Datatype MPI_FillReq;
    MPI_Datatype MPI_FillResp;
#endif
  }
}

using namespace criptic;

// Method to set up an MPI run
void MPIUtil::setup(int *argc, char **argv[]) {

#ifdef ENABLE_MPI
  // Version if MPI is turned on
    
  // Call the MPI library setup method
#ifdef _OPENMP
  int threadSupportLev;
  MPI_Init_thread(argc, argv, MPI_THREAD_FUNNELED, &threadSupportLev);
  if (threadSupportLev != MPI_THREAD_FUNNELED) {
    std::cerr << "MPIUtil::setup: this MPI implementation does not appear to "
	      << "support funneled threads!" << std::endl;
    exit(2);
  }
#else
  MPI_Init(argc, argv);
#endif

  // Set the global variables
  MPI_Comm_size(MPI_COMM_WORLD, &nRank);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  IOProc = (myRank == IORank);
  for (log2nRank = 0;
       nRank > (1 << log2nRank);
       log2nRank++) { };

  // Define MPI data types corresponding to criptic::Real and
  // criptic::IdxType; these must be set based on compile-time
  // ifdef's, since they depend on compilation options and system
  // sizes for different types
#ifdef CRIPTIC_PRECISION_SINGLE
  MPI_Real = MPI_FLOAT;
#elif defined(CRIPTIC_PRECISION_DOUBLE)
  MPI_Real = MPI_DOUBLE;
#endif
#if SIZE_MAX == UCHAR_MAX
  MPI_Idx = MPI_UNSIGNED_CHAR;
#elif SIZE_MAX == USHRT_MAX
  MPI_Idx = MPI_UNSIGNED_SHORT;
#elif SIZE_MAX == UINT_MAX
  MPI_Idx = MPI_UNSIGNED;
#elif SIZE_MAX == ULONG_MAX
  MPI_Idx = MPI_UNSIGNED_LONG;
#elif SIZE_MAX == ULLONG_MAX
  MPI_Idx = MPI_UNSIGNED_LONG_LONG;
#else
#error "Unable to determine number of bits in size_t!"
#endif


  // Define the MPI data types we will need to pass things around
  {
    // RealVec data type
    int nBlock = 1;
    int blockLengths[1] = { 3 };
    MPI_Datatype types[1] = { MPI_Real };
    MPI_Aint offsets[1] = { offsetof(RealVec, v) };
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_RealVec);
    MPI_Type_commit(&MPI_RealVec);
  }
  
  {
    // RealTensor2 data type
    int nBlock = 1;
    int blockLengths[1] = { 9 };
    MPI_Datatype types[1] = { MPI_Real };
    MPI_Aint offsets[1] = { offsetof(RealTensor2, t) };
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_RealTensor2);
    MPI_Type_commit(&MPI_RealTensor2);
  }
  
  {
    // CR packet data type
#ifndef TRACK_PITCH_ANGLE
    constexpr int nBlock = 8;
    int blockLengths[nBlock] = { 1, 1, 1, 1, 1, 1, 1, 1 };
    MPI_Datatype types[nBlock] = { MPI_INT, MPI_Idx, MPI_Idx,
				   MPI_Real, MPI_Real, MPI_Real,
				   MPI_Real, MPI_Real };
#else
    constexpr int nBlock = 9;
    int blockLengths[nBlock] = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    MPI_Datatype types[nBlock] = { MPI_INT, MPI_Idx, MPI_Idx,
				   MPI_Real, MPI_Real, MPI_Real,
				   MPI_Real, MPI_Real, MPI_Real };
#endif
    MPI_Aint offsets[nBlock];    
    offsets[0] = offsetof(CRPacket, type);
    offsets[1] = offsetof(CRPacket, src);
    offsets[2] = offsetof(CRPacket, uniqueID);
    offsets[3] = offsetof(CRPacket, tInj);
    offsets[4] = offsetof(CRPacket, wInj);
    offsets[5] = offsetof(CRPacket, p);
    offsets[6] = offsetof(CRPacket, w);
    offsets[7] = offsetof(CRPacket, gr);
#ifdef TRACK_PITCH_ANGLE
    offsets[8] = offsetof(CRPacket, mu);
#endif
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_CRPacket);
    MPI_Type_commit(&MPI_CRPacket);
  }

  {
    // CR source data type
#ifndef TRACK_PITCH_ANGLE
    constexpr int nBlock = 8;
    int blockLengths[nBlock] = { 1, 1, 1, 1, 1, 1, 1, 1 };
    MPI_Datatype types[nBlock] = { MPI_INT, MPI_Idx, MPI_Real,
                                   MPI_Real, MPI_Real, MPI_Real,
                                   MPI_RealVec, MPI_RealVec };
#else
    constexpr int nBlock = 10;
    int blockLengths[nBlock] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    MPI_Datatype types[nBlock] = { MPI_INT, MPI_Idx, MPI_Real,
                                   MPI_Real, MPI_Real, MPI_Real,
                                   MPI_Real, MPI_Real,
                                   MPI_RealVec, MPI_RealVec };
#endif
    MPI_Aint offsets[nBlock];
    offsets[0] = offsetof(CRSource, type);
    offsets[1] = offsetof(CRSource, uniqueID);
    offsets[2] = offsetof(CRSource, p0);
    offsets[3] = offsetof(CRSource, p1);
    offsets[4] = offsetof(CRSource, q);
    offsets[5] = offsetof(CRSource, k);
#ifdef TRACK_PITCH_ANGLE
    offsets[6] = offsetof(CRSource, mu0);
    offsets[7] = offsetof(CRSource, mu1);
    offsets[8] = offsetof(CRSource, v);
    offsets[9] = offsetof(CRSource, a);
#else
    offsets[6] = offsetof(CRSource, v);
    offsets[7] = offsetof(CRSource, a);
#endif
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_CRSource);
    MPI_Type_commit(&MPI_CRSource);
  }

  {
    // FieldQty data type
    int nBlock = 1;
    int blockLengths[1] = { nFieldQty };
    MPI_Datatype types[1] = { MPI_Real };
    MPI_Aint offsets[1];
    offsets[0] = offsetof(FieldQty, q);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_FieldQty);
    MPI_Type_commit(&MPI_FieldQty);
  }
    
  {
    // FieldQtyGrad data type
    int nBlock = 1;
    int blockLengths[1] = { nFieldQty };
    MPI_Datatype types[1] = { MPI_RealVec };
    MPI_Aint offsets[1];
    offsets[0] = offsetof(FieldQtyGrad, qGrad);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_FieldQtyGrad);
    MPI_Type_commit(&MPI_FieldQtyGrad);
  }

  {
    // RealMechArr data type
    int nBlock = 1;
    int blockLengths[1] = { lossMech::nMech };
    MPI_Datatype types[1] = { MPI_Real };
    MPI_Aint offsets[1];
    offsets[0] = 0;
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_RealMechArr);
    MPI_Type_commit(&MPI_RealMechArr);
  }

  {
    // fillReq type
    int nBlock = 9;
    int blockLengths[9] = { 1, 1, 1, 1, 1,
			    TREE_LEAF_SIZE,
			    TREE_LEAF_SIZE,
			    TREE_LEAF_SIZE,
			    TREE_LEAF_SIZE };
    MPI_Datatype types[9] = {
      MPI_Idx, MPI_RealTensor2, MPI_RealTensor2, MPI_Real,
      MPI_INT, MPI_Idx, MPI_RealVec, MPI_Real, MPI_FieldQty };
    MPI_Aint offsets[9];
    offsets[0] = offsetof(fillRequest, count);
    offsets[1] = offsetof(fillRequest, h2Inv);
    offsets[2] = offsetof(fillRequest, h2InvGrad);
    offsets[3] = offsetof(fillRequest, hScale);
    offsets[4] = offsetof(fillRequest, nrank);
    offsets[5] = offsetof(fillRequest, idx);
    offsets[6] = offsetof(fillRequest, x);
    offsets[7] = offsetof(fillRequest, R);
    offsets[8] = offsetof(fillRequest, qMin);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_FillReq);
    MPI_Type_commit(&MPI_FillReq);
  }

  {
    // fillResp type
    int nBlock = 4;
    int blockLengths[4] = { 1, TREE_LEAF_SIZE,
			    TREE_LEAF_SIZE, TREE_LEAF_SIZE };
    MPI_Datatype types[4] = { MPI_Idx, MPI_Idx,
			      MPI_FieldQty, MPI_FieldQtyGrad };
    MPI_Aint offsets[4];
    offsets[0] = offsetof(fillResponse, count);
    offsets[1] = offsetof(fillResponse, idx);
    offsets[2] = offsetof(fillResponse, q);
    offsets[3] = offsetof(fillResponse, qGrad);
    MPI_Type_create_struct(nBlock, blockLengths, offsets, types,
			   &MPI_FillResp);
    MPI_Type_commit(&MPI_FillResp);
  }

  // If running in debug mode, and unless told otherwise, pause the
  // run at this point and print out the process IDs of all processes
  // to facilitate gdb attachment
#if !defined(NDEBUG) && !defined(NO_DEBUG_PAUSE)
  {
    volatile int i = 0;
    char hostname[256];
    gethostname(hostname, sizeof(hostname));
    std::cout << "PID " << getpid() << " on " << hostname
	      << " ready for attach" << std::endl << std::flush;
    while (0 == i)
        sleep(5);
  }
#endif

#else

  // Version if MPI is turned off
  nRank = 1;
  myRank = 0;
  log2nRank = 0;
  IOProc = true;
#endif
}

// Method to halt a run
void MPIUtil::haltRun(std::string  msg, errCodes errcode) {
#ifdef ENABLE_MPI
  std::cerr << "CRIPTIC rank " << MPIUtil::myRank << ": ";
#endif
  std::cerr << msg << std::endl;
  std::cerr << "Exiting with error code " << errcode
	    << ", " << errCodeStrings[errcode].data() << std::endl;
  std::cerr << std::flush;
#ifdef ENABLE_MPI
  MPI_Abort(MPI_COMM_WORLD, errcode);
#endif
  exit(errcode);
}

// MPI barrier method
void MPIUtil::barrier() {
#ifdef ENABLE_MPI
  MPI_Barrier(MPI_COMM_WORLD);
#endif
}

// Method to finalize
void MPIUtil::shutdown() {
#ifdef ENABLE_MPI
  MPI_Finalize();
#endif
}

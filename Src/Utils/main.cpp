#include <iostream>
#include <vector>
#include "RealVec.H"
#include "RealTensor2.H"
#include "SR.H"

using namespace criptic;
using namespace std;

int main() {
  Real data[]{1,1,1,2,2,2};
  RealVec v(data);
  RealVec v1(data+3);
  RealVec v2 = v + v1;
  cout << v.mag() << " " << v1.mag() << " " << v2.mag() << endl;
}

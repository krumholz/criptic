/**
 * @file Interp1D.H
 * @brief A utility class to handle 1D interpolation
 *
 * @author Mark Krumholz
 */

#ifndef _INTERP1D_H_
#define _INTERP1D_H_

#include <cmath>
#include <gsl/gsl_interp.h>
#include "ErrCodes.H"
#include "ThreadVec.H"
#include "Types.H"

namespace criptic {

  /**
   * @brief Enum describing different ways to handle extrapolation
   */
  typedef enum {
    extrapError,   /**< Throw error if asked to extrapolate */
    extrapLinear,  /**< Extrapolate linearly */
    extrapConst,   /**< Return final table value if asked to
		      extrapolate */
    extrapFixedVal /**< return a fixed value if asked to extraplate
		    */
  } extrapType;
  
  /**
   * @class Interp1D
   * @brief A class to handle interpolation on 1D tables
   * @details
   * This class wraps the GSL interpolation routines and provides
   * interpolation on 1D tables. It automatically handles things like
   * take logarithms when the tables are logarithmic, extrapolating
   * where necessary, etc. Note that, because this is intended to wrap
   * the GSL, all Real numbers here are double precision, even if the
   * rest of the code operates in single precision.
   */
  class Interp1D {

  public:

    /**
     * @brief Constructor
     * @param x_ Tabular data in the independent variable
     * @param y_ Tabular data in the dependent variable
     * @param n_ Number of entries in table
     * @param extrapLo_ Type of extrapolation to use in low direction
     * @param extrapHi_ Type of extrapolation to use in high direction
     * @param xLog_ If true, the values in x are log_10(x)
     * @param yLog_ If true, the values in y are log_10(y)
     * @param yLo_ Value for off-table low extrapolation
     * @param yHi_ Value for off-table high extrapolation
     * @details
     * This class does not make any copies of the underlying data
     * pointed to by x_ or y_, so it is the responsibility of the user
     * not to modify or deallocate them. The parameters yLo_ and yHi_
     * are only used if extrapLo_ or extrapHi_ is set to
     * extrapFixedVal, in which case they specify the fixed value that
     * is returned.
     */
    Interp1D(const double* x_, const double* y_, const size_t n_,
	     const extrapType extrapLo_ = extrapError,
	     const extrapType extrapHi_ = extrapError,
	     const bool xLog_ = false, const bool yLog_ = false,
	     const double yLo_ = 0.0, const double yHi_ = 0.0) :
      x(x_),
      y(y_),
      n(n_),
      extrapLo(extrapLo_),
      extrapHi(extrapHi_),
      xLog(xLog_),
      yLog(yLog_),
      yLo(yLo_),
      yHi(yHi_)
    {

      // Allocate and initialize GSL objects
      interp = gsl_interp_alloc(gsl_interp_steffen, n);
      gsl_interp_init(interp, x, y, n);
      for (IdxType i = 0; i < acc.size(); i++)
	acc[i] = gsl_interp_accel_alloc();

      // Store slopes
      if (extrapLo == extrapLinear)
	mLo = (y[1] - y[0]) / (x[1] - x[0]);
      if (extrapHi == extrapLinear)
	mHi = (y[n-1] - y[n-2]) / (x[n-1] - x[n-2]);
    }      

    /**
     * @brief Destructor
     */
    ~Interp1D() {
      // Free memory
      gsl_interp_free(interp);
      for (IdxType i = 0; i < acc.size(); i++)
	gsl_interp_accel_free(acc[i]);
    }

    /**
     * @brief Return lower limit of table
     * @returns Minimum value of x in table
     */
    double xMin() const { return xLog ? std::pow(10., x[0]) : x[0]; }
    
    /**
     * @brief Return upper limit of table
     * @returns Maximum value of x in table
     */
    double xMax() const { return xLog ? std::pow(10., x[n-1]) : x[n-1]; }

    /**
     * @fn criptic::Interp1D::operator()(const double& x) const
     * @brief Caclulate an interpolated value from the table
     * @param x_ The value to which to interpolate
     * @returns Interpolated value at x_
     */
    double operator()(const Real& x_) const {

      // Take log if requested, and catch case where taking the log
      // and then its inverse causes a value of x that should be at
      // the edge of the table to move slighlty off it; to handle
      // this, if xLog is set, we check for near-equality between x1
      // and the table limits, and coerce x1 back onto the table if it
      // is off by less than some tolerance.
      double x1 = xLog ? std::log10(x_) : x_;
      if (xLog) {
	if (x1 < x[0] && x[0] - x1 < 1.0e-10) x1 = x[0];
	else if (!(x1 < x[n-1]) && x1 - x[n-1] < tol) x1 = x[n-1];
      }

      // Compute interpolation
      double y1 = 0.0;
      if (x1 < x[0]) {
	// Off table low
	switch (extrapLo) {
	case extrapLinear: {
	  y1 = y[0] + mLo * (x1 - x[0]);
	  break;
	}
	case extrapFixedVal: {
	  return yLo;
	}
	case extrapConst: {
	  y1 = y[0];
	  break;
	}
	case extrapError: {
	  throw(errBadXLoExtrapolation);
	}
	}
      } else if (x1 > x[n-1]) {
	// Off table high
	switch (extrapHi) {
	case extrapLinear: {
	  y1 = y[n-1] + mHi * (x1 - x[n-1]);
	  break;
	}
	case extrapFixedVal: {
	  return yHi;
	}
	case extrapConst: {
	  y1 = y[n-1];
	  break;
	}
	case extrapError: {
	  throw(errBadXHiExtrapolation);
	}
	}	
      } else {
	// Within table
	y1 = gsl_interp_eval(interp, x, y, x1, acc());
      }
      if (yLog) {
	return std::pow(10., y1);
      } else {
	return y1;
      }
    }

  private:

    const double *x;     /**< x values in table */
    const double *y;     /**< y values in table */
    const size_t n;      /**< Number of entries in table */
    const extrapType extrapLo; /**< Type of extrapolation in low direction */
    const extrapType extrapHi; /**< Type of extrapolation in high direction */
    const bool xLog;     /**< Are the x values logarithmic? */
    const bool yLog;     /**< Are the y values logarithmic? */
    const double yLo;    /**< Off-table low value */
    const double yHi;    /**< Off-table high value */
    double mLo;          /**< Slope for extrapolation in low direction */
    double mHi;          /**< Slope for extrapolation in high direction */
    static constexpr double tol = 1.0e-8;  /**< Tolerance for off-table
					      values */

    // GSL machinery
    gsl_interp *interp;                /**< GSL interpolator */
    ThreadVec<gsl_interp_accel *> acc; /**< GSL accelerator */
    
  };

}

#endif
// _INTERP1D_H_

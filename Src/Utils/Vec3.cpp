#include "Vec3.H"

using namespace std;
using namespace criptic;

template<class T>
istream& operator>>(istream& is,
		    Vec3<T>& v) {
  is >> v[0] >> v[1] >> v[2];
  return is;
}

template<class T>
ostream& operator<<(ostream& os,
		    const Vec3<T>& v) {
  os << v[0] << " " << v[1] << " " << v[2];
  return os;
}

// Force instantiations
template ostream& operator<<(ostream&, const RealVec&);
template istream& operator>>(istream&, RealVec&);
template ostream& operator<<(ostream&, const IdxVec&);
template istream& operator>>(istream&, IdxVec&);
template ostream& operator<<(ostream&, const SIdxVec&);
template istream& operator>>(istream&, SIdxVec&);
template ostream& operator<<(ostream&, const BoolVec&);
template istream& operator>>(istream&, BoolVec&);


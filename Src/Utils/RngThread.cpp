// Implementation of the RngThread class

#include "RngThread.H"

using namespace std;
using namespace criptic;

// Constructor
RngThread::RngThread(const ParmParser &pp) {

  // Check if we have been told to initialize with a fixed seed
  int fixedSeed = 0;
  pp.query("rng.fixed_seed", fixedSeed);
  
  // Loop over thread copies
  for (IdxType i = 0; i < r.size(); i++) {

    if (!fixedSeed) {
      
      // Default initialization -- set random device
      pcg_extras::seed_seq_from<std::random_device> seed_source;
    
      // Set up generator for this thread using random device for seed
      r[i] = new pcg64(seed_source);

    } else {

      // Initialize all streams using PCG's default seed
      r[i] = new pcg64();

    }
  }
}


// Destructor
RngThread::~RngThread() {
  
  // Loop over thread copies
  for (IdxType i = 0; i < r.size(); i++) {
    delete r[i];
  }
}

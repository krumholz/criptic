#include "RealBox.H"

using namespace std;
using namespace criptic;

istream& operator>>(istream& is,
		    RealBox& b) {
  char c;
  is >> b.lo;
  is.get(c); // process the comma
  is >> b.hi;
  return is;
}

ostream& operator<<(ostream& os,
		    const RealBox& b) {
  os << b.lo << ", " << b.hi;
  return os;
}

// Function to find extrema of a Gaussian over the box
void RealBox::gaussExtrema(const RealTensor2& eta,
			   Real& gMin,
			   Real& gMax) const {

  // First find the minimum of the Gaussian; this is straightforward
  // because the Gaussian is a strictly decreasing function of
  // distance from the origin, and the point in a box that is furthest
  // from the origin is always one of the corners. Thus we just need
  // to check the corners.
  gMin = maxReal;
  array<RealVec,8> crnr = corners();
  for (auto c : crnr) {
    Real g = exp( -eta.contract(c, c) / 2 );
    if (g < gMin) gMin = g;
  }

  // Next look for the maximum.  Unlike the minimum, the
  // maximum could be located on the volume interior, on one of the
  // faces, on one of the edges, or at one of the corners. Multiple
  // maxima may exist on multiple faces or edges. We must therefore
  // check each possibility in turn.

  // Check box interior first; if it contains x = 0, then we have
  // found the global maximum of the Guassian, and we are done.
  if (contains(zeroVec)) {
    gMax = 1.0;
    return;
  }

  // If we are here, we need to check faces, edges, and corners. To
  // minimize the number of exponentials we need to evaluate, rather
  // than finding the maximum of the Gaussian, we will find the
  // minimum of d2 = eta_ij x_i x_j, and then use that to evaluate the
  // Gaussian.
  Real d2 = maxReal;

  // First check faces. Faces lie in planes of constant x_i (where i =
  // 0, 1, or 2), and the minimum of d2 on a given plane occurs at the
  // location satisfying
  // d(d2) / dx_j = dw / dx_k = 0,
  // where i, j, and k are related by cyclic permutation, such that j
  // = i + 1 % 3 and k = i + 2 % 3. This is a linear system, whose
  // solutions are given by
  // x_j = x_i ( eta_jk eta_ki - eta_ji eta_kk ) /
  //                ( eta_ii eta_jj - eta_ij eta_ji )
  // x_k = x_i ( eta_ik eta_ji - eta_ii eta_jk ) /
  //                ( eta_ii eta_jj - eta_ij eta_ji ).
  // We solve for x_j and x_k, and check if that point falls within
  // the bounds of the rectangle that defines each face, and, if it
  // does, we evaluate d2 there.
  for (int n=0; n<3; n++) {
    int i = n % 3;
    int j = (n+1) % 3;
    int k = (n+2) % 3;

    // Loop over low and high faces
    RealVec p;
    for (int m=0; m<2; m++) {
      p[i] = (m == 0) ? lo[i] : hi[i];
      p[j] = p[i] *
	( eta(j,k) * eta(k,i) - eta(j,i) * eta(k,k) ) /
	( eta(j,j) * eta(k,k) - eta(j,k) * eta(k,j) );
      p[k] = p[i] *
	( eta(j,i) * eta(k,j) - eta(j,j) * eta(k,i) ) /
	( eta(j,j) * eta(k,k) - eta(j,k) * eta(k,j) );
      if (contains(p)) {
	Real eta_pp = eta.contract(p, p);
	if (eta_pp < d2) d2 = eta_pp;
      }
    }
  }

  // Next look for minima of d2 on box edges; an edge is a line of
  // constant (x_i, x_j), and along such a line the maximum of d2
  // occurs at the location that satisfies
  // d(d2) / dx_k = eta_ki x_i + eta_kj x_j + eta_kk x_k = 0,
  // which has the solution
  // x_k = - ( eta_ki x_i + eta_kj x_j) / eta_kk.
  // We check if this solution falls along the line segment that
  // defines our box edges, and, if so, evaluate d2 there.
  for (int n=0; n<3; n++) {
    int i = n % 3;
    int j = (n+1) % 3;
    int k = (n+2) % 3;

    // Loop over four edes in this direction
    RealVec p;
    for (int m=0; m<2; m++) {
      p[i] = (m == 0) ? lo[i] : hi[i];
      for (int l=0; l<2; l++) {
	p[j] = (l == 0) ? lo[j] : hi[j];
	p[k] = -( p[i] * eta(k,i) + p[j] * eta(k,j) )
	  / eta(k,k);
	if (contains(p)) {
	  Real eta_pp = eta.contract(p, p);
	  if (eta_pp < d2) d2 = eta_pp;
	}
      }
    }
  }

  // Finally, check corners
  for (auto c : crnr) {
    Real eta_cc = eta.contract(c, c);
    if (eta_cc < d2) d2 = eta_cc;
  }

  // Now evaluate using minimum value of d2
  gMax = exp( -d2 / 2 );
}

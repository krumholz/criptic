/**
 * @file Propagation.H
 * @brief Interface used to describe a model for CR propagation parameters
 *
 * @details
 * This file defines a struct and a class that are used to provide
 * information about CR propagation to the remainder of the code. The
 * class is an abstract base class that can be specialized by users to
 * provide their own physics models for how CRs propagate.
 *
 * @author Mark Krumholz
 */

#ifndef _PROPAGATION_H_
#define _PROPAGATION_H_

#include "../Core/CRPacket.H"
#include "../Core/FieldQty.H"
#include "../Gas/Gas.H"
#include "../Utils/Types.H"
#include "../Utils/Vec3.H"

namespace criptic {

  /**
   * @brief Enum of types of field quantities to be computed
   */
  typedef enum {
    noFieldQty = 0,   /**< No field quantities needed */
    needFieldQty,     /**< Field quantities needed */
    needFieldQtyGrad  /**< Gradients of field quantities needed */
  } FieldQtyNeedType;
  
  /**
   * @brief A namespace to hold quantities related to CR propagation
   */
  namespace propagation {
    
    /**
     * @brief Structure contain the requires propagation parameters
     * @details
     * This struct defines the set of propagation parameters that
     * criptic requires in order to evolve CR packets.
     */
    typedef struct {
      Real kPar;         /**< Parallel diffusion coefficient */
      Real kPerp;        /**< Perpendicular diffusion coefficient */
      Real kPP;          /**< Momentum diffusion coefficient */
#ifdef TRACK_PITCH_ANGLE
      Real kMu;          /**< Pitch angle diffusion coefficient */
#else
      Real vStr;         /**< Streaming velocity */
#endif
      Real dkPP_dp;      /**< Derivative of momentum diffusion with
	  		      respect to momentum */
      Real dvStr_dp;     /**< Derivative of streaming velocity with
			      respect to momentum */
      RealVec kParGrad;  /**< Gradient of parallel diffusion
			      coefficient */
      RealVec kPerpGrad; /**< Gradient of perpendicular diffusion
			      coefficient */
#ifdef TRACK_PITCH_ANGLE
      Real dkMudMu;      /**< Derivative of kMu with respect to mu */
#else
      RealVec vStrGrad;  /**< Gradient of streaming velocity */
#endif      
    } PropagationData;

    /**
     * @class Propagation
     * @brief Interface to describe the CR propagation model
     * @details
     * This is a virtual base class that defines and interface through
     * which the user describes the CR propagation model to
     * criptic. Users can describe arbitrary propagation models by
     * deriving a specialized class from this interface.
     */
    class Propagation {

    public:
      
      /**
       * @brief Empty virtual destructor
       */
      virtual ~Propagation() { };
	
      /**
       * @brief Access to field quantities needed?
       * @return Level of field quantity information required
       * @details
       * This is a pure virtual function that is left to be
       * implemented in derived classes. The function can return
       * noFieldQty, needFieldQty, or needFieldQtyGrad, which
       * indicates that the propagation model does not need any field
       * quantities, needs field quantities, or needs field quantities
       * and their gradients, respectively.
       */
      virtual FieldQtyNeedType fieldQtyNeed() const = 0;
      
      /**
       * @fn criptic::propagation::Propagation::operator()(const
       * criptic::RealVec&, const criptic::Real, const
       * criptic::gas::Gas&, const criptic::CRPacket&, const
       * criptic::Real *) const
       * @brief Return the propagation data
       * @param x Position
       * @param t Time
       * @param gd Data on background gas state
       * @param packet Packet data
       * @param qty Field quantities at the specified (x,t)
       * @param qtyGrad Gradients of field quantities at specified (x,t)
       * @return The propagation data for the specified CR and point
       */
      virtual PropagationData
      operator()(const RealVec& x,
		 const Real t,
		 const gas::GasData& gd,
		 const CRPacket& packet,
		 const FieldQty& qty,
		 const FieldQtyGrad& qtyGrad) const = 0;

    };
  }
}

#endif
// _PROPAGATION_H_

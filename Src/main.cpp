// Main

#include <chrono>
#include <iostream>
#include "Core/CRPacket.H"
#include "Core/CRSource.H"
#include "Core/CRTree.H"
#include "Core/Geometry.H"
#include "Gas/Gas.H"
#include "IO/CheckpointManager.H"
#include "IO/ParmParser.H"
#include "MPI/MPIUtil.H"
#include "Prob/Prob.H"
#include "Propagation/Propagation.H"
#include "Utils/RngThread.H"
#ifdef _OPENMP
#   include "omp.h"
#endif

#include <unistd.h>

using namespace std;
using namespace criptic;

int main(int argc, char *argv[]) {

  // Initialize MPI
  MPIUtil::setup(&argc, &argv);

  // Get start time on root rank
  auto start_timestamp = chrono::steady_clock::now();
  
  // Read the input deck
  ParmParser pp(argc, argv);

  // Get verbosity level and print status if sufficiently verbose
  int verbosity = 1;
  pp.query("verbosity", verbosity);
  if (verbosity > 0 && MPIUtil::IOProc) {
    cout << "CRIPTIC: starting up: ";
#ifdef _OPENMP
    cout << omp_get_max_threads();
    if (omp_get_max_threads() > 1)
      cout << " threads, ";
    else
      cout << " thread, ";
#endif
    cout << MPIUtil::nRank;
    if (MPIUtil::nRank > 1)
      cout << " MPI ranks" << endl;
    else
      cout << " MPI rank" << endl;
  }

  // Intitialize the random number generator
  RngThread rng(pp);

  // Set up the problem geometry
  Geometry geom(pp);

  // Call the user problem setup and any user-defined startup work
  Prob *prob = initProb(pp, geom, rng);
  prob->userSetup();

  // Initialize the background gas
  gas::Gas *gasBG = prob->initGas();
  
  // Initialize the CR propagation model
  propagation::Propagation *prop = prob->initProp();

  // Initialize the loss mechanisms
  Losses loss(pp, rng);

  // Start up the checkpoint manager
  CheckpointManager chk(pp, *prop, loss);

  // Initialize an empty tree
  CRTree tree(pp, geom, *gasBG, *prop, loss, rng);
  
  // Variables to hold time, time step, and step number
  int step;
  Real t, dt;
  
  // Add the initial set of packets and sources to the tree
  if (!pp.isRestart()) {

    // For non-restarts, initialize using user-provided init functions
    vector<RealVec> x, xSrc;
    vector<CRPacket> packets;
    vector<CRSource> sources;
    prob->initPackets(x, packets);
    prob->initSources(xSrc, sources);

    // Add initial packets and sources to tree
    tree.addPacketsAndSources(x, packets, xSrc, sources);

    // Set initial time, step number, and time step
    step = 0;
    t = 0.0;
    pp.get("dt_init", dt);

  } else {

    // For restarts, read time stepping and tree data directly from
    // checkpoint
    chk.readCheckpoint(pp.restartFile(), step, t, dt, tree, prob);

  }

  // Read control parameters
  int maxStep = maxInt;
  pp.query("max_step", maxStep);
  Real maxTime = maxReal;
  pp.query("max_time", maxTime);
  Real maxDtIncrease = 1.1;
  pp.query("max_dt_increase", maxDtIncrease);
  Real minDt = eps;
  pp.query("min_dt", minDt);

  // Issue warning about unused keywords if necessary
  pp.warnUnusedKeywords();
  
  // Build the tree and compute starting field quantities
  tree.rebuild();
  tree.fillFieldQty();

  // For non-restart runs, write a checkpoint containing the initial
  // state
  if (!pp.isRestart())
    chk.writeCheckpoint(0, 0.0, dt, tree, prob);

  // Print status
  auto main_start_timestamp = chrono::steady_clock::now();
  if (verbosity > 0 && MPIUtil::IOProc) {
    chrono::duration<double> start_time =
      main_start_timestamp - start_timestamp;
    cout << "CRIPTIC: setup complete in " << start_time.count()
	 << "s, starting main loop" << endl;
  }

  // Start main simulation loop
  bool writeCheckpoint = false;
  while (step < maxStep && t < maxTime) {

    // Save the current time step estimate, in case it gets throttled
    // below; we want to base our maximum next time step off the one
    // set by physics, not one imposed by one of those conditions
    Real dtSave = dt;

    // Update the gas state to the new time, and throttle time step if
    // needed
    Real tNext = t + dt, tNextSave = t + dt;
    gasBG->updateState(t, tNext);
    if (tNext != tNextSave) dt = tNext - t;

    // Throttle the time step to make sure we do not overshoot either
    // the maximum time or the next time at which a checkpoint must be
    // written
    if (tNext > maxTime) {
      tNext = maxTime;
      dt = tNext - t;
    }
    if (tNext >= chk.nextTime()) {
      tNext = chk.nextTime();
      dt = tNext - t;
      writeCheckpoint = true;  // Flag that we need to write a
			       // checkpoint
    }

    // Inject new packets, and compute field quantities for them
    tree.injectPackets(t, tNext);

    // Advance packets, getting an estimate for the next global time
    // step in the process
    Real dtNewEst = tree.advancePackets(t, tNext);
    
    // Increment time and step number
    step++;
    t = tNext;
    
    // Limit time step increase
    if (dtNewEst / dtSave > maxDtIncrease)
      dtNewEst = dtSave * maxDtIncrease;
    
    // Call the user work function, which allows the user to perform
    // arbitrary modifications of the system (e.g., moving or changing
    // the properties of sources, changing the background gas state,
    // etc.)
    prob->userWork(t, dtNewEst, *gasBG, *prop, tree);
    
    // Rebuild the tree
    tree.rebuild();

    // Fill field quantities for next time step
    tree.fillFieldQty();
    
    // Print status if verbose
    if (verbosity > 0 && MPIUtil::IOProc) {
      cout << "CRIPTIC: completed step " << step
	   << ": t = " << t << ", dt_done = " << dt
	   << ", dt_next = "
	   << (dtNewEst > 0.0 ? dtNewEst : dtSave)
	   << ", n_packet = " << tree.nPacket()
	   << endl;
    }

    // Set next global time step, and bail out if it is too
    // small; note that dtNewEst will be zero if we do not have field
    // quantities, in which case the global time step is meaningless;
    // in this case we just leave dt unchanged
    if (dtNewEst > 0.0) {
      dt = dtNewEst;
      if (dt < minDt) break;
    } else {
      dt = dtSave;
    }
    
    // Write checkpoint if necessary
    if (writeCheckpoint) {
      chk.writeCheckpoint(step, t, dt, tree, prob);
      tree.clearDeletedPackets();  // Clear the deleted packet list
      writeCheckpoint = false;
    }

  }

  // Get time of completion
  auto main_end_timestamp = chrono::steady_clock::now();

  // Print status
  if (MPIUtil::IOProc && verbosity > 0) {
    chrono::duration<double> run_time =
      main_end_timestamp - main_start_timestamp;
    int hr = run_time.count() / 3600;
    int min = (run_time.count() - 3600*hr) / 60;
    double sec = run_time.count() - 3600*hr - 60*min;
    cout << "CRIPTIC: run complete in "
	 << hr << "h " << min << "m " << sec  << "s: ";
    if (step == maxStep)
      cout << "reached maximum step number = " << step << endl;
    else if (t >= maxTime)
      cout << "reached maximum time = " << t << endl;
    else if (dt < minDt)
      cout << "time step dt = " << dt << " below minimum dt = "
	   << minDt << endl;
  }  

  // Save final state if we didn't just write it
  if (t != chk.lastTime() && step != chk.lastStep())
    chk.writeCheckpoint(step, t, dt, tree, prob);

  // Free memory
  delete prop;
  delete gasBG;

  // Finalize MPI
  MPIUtil::shutdown();
}

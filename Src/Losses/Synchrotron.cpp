#include "Synchrotron.H"
#include <cmath>

using namespace std;
using namespace criptic;

// Constructor
Synchrotron::Synchrotron() :
  FSyncInterp(logw,
	      logF,
	      FSyncNTab,
	      extrapLinear, // Extrapolate linearly below minimum w/w_cut
	      extrapLinear, // Extrapolate linearly above maximum w/w_cut
	      true,         // w/w_cut values are logarithmic
	      true),        // F(w/w_cut) values are logarithmic	      
  FSyncPAInterp(logw,
		logFPA,
		FSyncNTab,
		extrapLinear, // Extrapolate linearly below minimum w/w_cut
		extrapLinear, // Extrapolate linearly above maximum w/w_cut
		true,         // w/w_cut values are logarithmic
		true)         // F(w/w_cut) values are logarithmic	      
{ }

// Synchrotron emission function
vector<Real> Synchrotron::photLum(const gas::GasData& gd,
				  const CRPacket& packet,
				  const vector<Real>& en,
				  const vector<RealVec> dir
#ifdef TRACK_PITCH_ANGLE
				  , const Real hmu
#endif
				  ) const {

  // Prepare output holder
  vector<Real> specLum(en.size() * dir.size());

  // Get some quantities we need from the packet and the gas
  Real beta = packet.v();
  Real gamma = packet.gamma();
  Real B = gd.B.mag();

  // Compute gyration angular frequency and cutoff frequency for pitch
  // angle alpha = 90 degrees
#ifdef CRIPTIC_UNITS_CGS
  Real wB = constants::e * B / (gamma * constants::me * constants::c);
#else
  Real wB = constants::e * B / (gamma * constants::me);
#endif
  Real wc = 1.5 * gamma*gamma*gamma / beta * wB;
  
  // Prefactor in synchrotron function; note that we convert this to
  // units of energy / time / GeV, instead of the usual energy / time
  // / Hz
#ifdef CRIPTIC_UNITS_CGS
  Real fac = sqrt(3) * constants::e3 * B /
    constants::me_c2 * units::GeV / constants::h;
#else
  Real fac = 10 * sqrt(3) * constants::e3 * B /
    constants::me * units::GeV / constants::h;
#endif
  fac *= packet.w;

  // Loop over directions
  for (IdxType j = 0; j < dir.size(); j++) {

    // Separate directional and non-directional cases
    if (dir[j].mag2() == 0.0) {

      // Case where we are computing isotropic emission

#ifdef TRACK_PITCH_ANGLE
      // Case where CR packets are at particular pitch angles; use
      // equation 4.57 of Blumenthal & Gould 1970, but treating the
      // distribution of pitch angles N(alpha) in that equation as a
      // delta function

      // Get pitch angle of this CR packet; avoid divide by zero
      // errors by making sure it is not exactly zero
      Real sin_pa = std::max(std::sqrt(1.0 - packet.mu*packet.mu), eps);

      // Loop over energies
      for (IdxType i = 0; i < en.size(); i++) {
	Real x = en[i] * units::GeV /
	  (constants::hBar * wc * sin_pa); // w/wc
	try {
	  specLum[i + en.size()*j] = 0.5 * fac * sin_pa * FSyncPAInterp(x);
	} catch (errCodes e) {
	  MPIUtil::haltRun("Synchrotron::photLum "
			   "illegal extrapolation!",
			   e);
	}
      }

#else

      // Case where CR packets are isotropic and emission is
      // isotropic; use equation 4.57 of Blumenthal & Gould 1970

      // Loop over energies
      for (IdxType i = 0; i < en.size(); i++) {
	Real x = en[i] * units::GeV / (constants::hBar * wc); // w / wc
	try {
	  specLum[i + en.size()*j] = fac * FSyncInterp(x);
	} catch (errCodes e) {
	  MPIUtil::haltRun("Synchrotron::photLum "
			   "illegal extrapolation!",
			   e);
	}
      }

#endif

    } else {

      // Case where we are computing emission per solid angle in a
      // particular direction
      
      // Compute pitch angle between direction vector and magnetic
      // field vector
      Real cos_pa = gd.B.dot(dir[j]) / (B * dir[j].mag());
      Real sin_pa = std::max(std::sqrt(1 - cos_pa*cos_pa), eps);
           
#ifdef TRACK_PITCH_ANGLE
      // Case where CR packets are at particular pitch angles; in this
      // case we only expect emission if the CR pitch angle matches
      // the direction pitch angle; however, to avoid numerical noise,
      // we artificially broaden the CR pitch angle distribution by
      // taking it to be a Gaussian of bandwidth hmu instead of a
      // delta function. We then use equation 4.57 of Blumenthal &
      // Gould 1970, except that we do not integrate over pitch angle
      Real fmu = (packet.mu - cos_pa) / hmu;
      Real wmu = 1 / (2 * M_PI * std::sqrt(M_PI) * hmu) * exp(-fmu*fmu);

#else
      // Case where CR packets are isotropic in pitch angle; in this
      // case the emission is isotropic, so the weight factor wmu is
      // just 1/4 pi.
      constexpr Real wmu = 1 / (4*M_PI);
#endif

      // Loop over energies
      for (IdxType i = 0; i < en.size(); i++) {
	Real x = en[i] * units::GeV /
	  (constants::hBar * wc * sin_pa); // w/wc
	try {
	  specLum[i + en.size()*j] = fac * sin_pa * FSyncPAInterp(x) * wmu;
	} catch (errCodes e) {
	  MPIUtil::haltRun("Synchrotron::photLum "
			   "illegal extrapolation!",
			   e);
	}
      }
      
    }

  }
    
  return specLum;
}

// Sychrotron function table; note that we give two versions of the
// synchrotron function F, one that is the usual sychrotron F and
// applies to a single pitch angle, and another that has been averaged
// over pitch angle.
const double Synchrotron::logw[] =
  {-3., -2.98, -2.96, -2.94, -2.92, -2.9, -2.88, -2.86, -2.84, -2.82, 
   -2.8, -2.78, -2.76, -2.74, -2.72, -2.7, -2.68, -2.66, -2.64, -2.62, 
   -2.6, -2.58, -2.56, -2.54, -2.52, -2.5, -2.48, -2.46, -2.44, -2.42, 
   -2.4, -2.38, -2.36, -2.34, -2.32, -2.3, -2.28, -2.26, -2.24, -2.22, 
   -2.2, -2.18, -2.16, -2.14, -2.12, -2.1, -2.08, -2.06, -2.04, -2.02, 
   -2., -1.98, -1.96, -1.94, -1.92, -1.9, -1.88, -1.86, -1.84, -1.82, 
   -1.8, -1.78, -1.76, -1.74, -1.72, -1.7, -1.68, -1.66, -1.64, -1.62, 
   -1.6, -1.58, -1.56, -1.54, -1.52, -1.5, -1.48, -1.46, -1.44, -1.42, 
   -1.4, -1.38, -1.36, -1.34, -1.32, -1.3, -1.28, -1.26, -1.24, -1.22, 
   -1.2, -1.18, -1.16, -1.14, -1.12, -1.1, -1.08, -1.06, -1.04, -1.02, 
   -1., -0.98, -0.96, -0.94, -0.92, -0.9, -0.88, -0.86, -0.84, -0.82, 
   -0.8, -0.78, -0.76, -0.74, -0.72, -0.7, -0.68, -0.66, -0.64, -0.62, 
   -0.6, -0.58, -0.56, -0.54, -0.52, -0.5, -0.48, -0.46, -0.44, -0.42, 
   -0.4, -0.38, -0.36, -0.34, -0.32, -0.3, -0.28, -0.26, -0.24, -0.22, 
   -0.2, -0.18, -0.16, -0.14, -0.12, -0.1, -0.08, -0.06, -0.04, -0.02, 
   0., 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2, 0.22, 
   0.24, 0.26, 0.28, 0.3, 0.32, 0.34, 0.36, 0.38, 0.4, 0.42, 0.44, 0.46, 
   0.48, 0.5, 0.52, 0.54, 0.56, 0.58, 0.6, 0.62, 0.64, 0.66, 0.68, 0.7, 
   0.72, 0.74, 0.76, 0.78, 0.8, 0.82, 0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 
   0.96, 0.98, 1.};
// Classical synchrotron function for single pitch angle, defined by
// x \int_x^infty K_{5/3}(xi) dxi
const double Synchrotron::logFPA[] = {
  -0.671337, -0.664786, -0.658238, -0.651694, -0.645153, -0.638617,	
  -0.632085, -0.625557, -0.619034, -0.612515, -0.606, -0.599491,	
  -0.592986, -0.586487, -0.579993, -0.573504, -0.567021, -0.560543,	
  -0.554072, -0.547607, -0.541148, -0.534696, -0.52825, -0.521812,	
  -0.515381, -0.508957, -0.502541, -0.496133, -0.489733, -0.483342,	
  -0.47696, -0.470586, -0.464222, -0.457867, -0.451523, -0.445189,	
  -0.438865, -0.432552, -0.426251, -0.419961, -0.413684, -0.407419,	
  -0.401166, -0.394927, -0.388702, -0.382491, -0.376295, -0.370114,	
  -0.363949, -0.357799, -0.351667, -0.345552, -0.339454, -0.333375,	
  -0.327316, -0.321276, -0.315256, -0.309257, -0.303281, -0.297327,	
  -0.291396, -0.285489, -0.279607, -0.273751, -0.267922, -0.262121,	
  -0.256348, -0.250604, -0.244891, -0.23921, -0.233562, -0.227947,	
  -0.222368, -0.216825, -0.21132, -0.205854, -0.200428, -0.195044,	
  -0.189703, -0.184407, -0.179158, -0.173957, -0.168806, -0.163707,	
  -0.158662, -0.153672, -0.14874, -0.143868, -0.139059, -0.134314,	
  -0.129635, -0.125026, -0.12049, -0.116028, -0.111644, -0.10734,	
  -0.103121, -0.0989884, -0.0949464, -0.0909984, -0.0871482,		
  -0.0833996, -0.0797566, -0.0762234, -0.0728045, -0.0695044,		
  -0.0663279, -0.0632802, -0.0603664, -0.057592, -0.0549627,		
  -0.0524847, -0.0501641, -0.0480077, -0.0460221, -0.0442148,		
  -0.0425931, -0.0411651, -0.0399391, -0.0389236, -0.0381279,		
  -0.0375614, -0.0372341, -0.0371565, -0.0373395, -0.0377948,		
  -0.0385342, -0.0395706, -0.0409171, -0.0425876, -0.0445969,		
  -0.0469601, -0.0496933, -0.0528134, -0.0563381, -0.060286,		
  -0.0646764, -0.0695299, -0.0748678, -0.0807127, -0.0870883,		
  -0.0940191, -0.101531, -0.109652, -0.118411, -0.127836, -0.137961,	
  -0.148818, -0.160442, -0.172868, -0.186137, -0.200287, -0.215361,	
  -0.231404, -0.248461, -0.26658, -0.285815, -0.306216, -0.327842,	
  -0.350751, -0.375004, -0.400667, -0.427807, -0.456495, -0.486807,	
  -0.518821, -0.552618, -0.588285, -0.625912, -0.665593, -0.707429,	
  -0.751521, -0.79798, -0.846919, -0.898458, -0.952722, -1.00984,	
  -1.06996, -1.13321, -1.19975, -1.26973, -1.34333, -1.42071, -1.50205, 
  -1.58756, -1.67742, -1.77184, -1.87104, -1.97526, -2.08472, -2.19968, 
  -2.32041, -2.44717, -2.58026, -2.71998, -2.86663, -3.02056, -3.18211, 
  -3.35163, -3.52952, -3.71616
};  
// Pitch angle-averaged synchrotron function, defined by
// (x/2) int_0^pi sin(alpha) int_{x/sin(alpha)}^infty K_{5/3}(xi)
// dxi dalpha
const double Synchrotron::logF[] = {
  -0.747079, -0.740549, -0.734024, -0.727503, -0.720987, -0.714476, 
  -0.70797, -0.701468, -0.694972, -0.688481, -0.681996, -0.675516, 
  -0.669043, -0.662575, -0.656114, -0.649659, -0.643211, -0.63677, 
  -0.630336, -0.62391, -0.617491, -0.61108, -0.604677, -0.598283, 
  -0.591897, -0.58552, -0.579152, -0.572794, -0.566445, -0.560107, 
  -0.55378, -0.547463, -0.541157, -0.534863, -0.528581, -0.522311, 
  -0.516053, -0.509809, -0.503579, -0.497362, -0.49116, -0.484973, 
  -0.478802, -0.472646, -0.466507, -0.460385, -0.45428, -0.448194, 
  -0.442127, -0.436079, -0.430051, -0.424044, -0.418058, -0.412095, 
  -0.406155, -0.400238, -0.394346, -0.388479, -0.382639, -0.376826, 
  -0.37104, -0.365284, -0.359558, -0.353864, -0.348201, -0.342571, 
  -0.336977, -0.331417, -0.325895, -0.320411, -0.314967, -0.309563, 
  -0.304202, -0.298885, -0.293614, -0.288389, -0.283214, -0.278089, 
  -0.273017, -0.267998, -0.263037, -0.258133, -0.25329, -0.24851, 
  -0.243795, -0.239147, -0.234569, -0.230064, -0.225634, -0.221282, 
  -0.217011, -0.212825, -0.208725, -0.204716, -0.200801, -0.196983, 
  -0.193267, -0.189657, -0.186155, -0.182767, -0.179497, -0.17635, 
  -0.17333, -0.170442, -0.167692, -0.165085, -0.162627, -0.160324, 
  -0.158181, -0.156206, -0.154405, -0.152785, -0.151354, -0.150118, 
  -0.149087, -0.148268, -0.147671, -0.147304, -0.147177, -0.1473, 
  -0.147683, -0.148337, -0.149274, -0.150505, -0.152043, -0.153899, 
  -0.156091, -0.158629, -0.161529, -0.164807, -0.168479, -0.172562, 
  -0.177073, -0.182031, -0.187455, -0.193366, -0.199784, -0.206732, 
  -0.214233, -0.222311, -0.230992, -0.240301, -0.250267, -0.260919, 
  -0.272286, -0.284401, -0.297297, -0.311008, -0.325571, -0.341024, 
  -0.357407, -0.37476, -0.393129, -0.412558, -0.433096, -0.454791, 
  -0.477697, -0.501868, -0.527362, -0.554237, -0.582558, -0.61239, 
  -0.643801, -0.676863, -0.711652, -0.748247, -0.78673, -0.82719, 
  -0.869711, -0.914394, -0.961336, -1.01064, -1.06242, -1.11678, 
  -1.17385, -1.23374, -1.2966, -1.36256, -1.43175, -1.50434, -1.58047, 
  -1.66031, -1.74404, -1.83184, -1.92388, -2.02038, -2.12154, -2.22758, 
  -2.33871, -2.45519, -2.57726, -2.70518, -2.83922, -2.97967, -3.12683, 
  -3.28102, -3.44256, -3.61179, -3.78908, -3.97481, -4.16936
};



// Implementation of the Losses class

#include "Bremsstrahlung.H"
#include "Coulomb.H"
#include "Losses.H"
#include "NucInelastic.H"
#include "InverseCompton.H"
#include "Ionization.H"
#include "NucInelastic.H"
#include "PositronAnnihilation.H"
#include "Synchrotron.H"
#include <cmath>

using namespace std;
using namespace criptic;

/////////////////////////////////////
// Constructor and destructor, and related functions
/////////////////////////////////////

Losses::Losses(const ParmParser& pp,
	       const RngThread& rng_) :
  brem(rng_),
  ic(rng_),
  nucInel(pp, rng_)
{
  // Store loss mechanisms in holder
  mech[lossMech::brem] = &brem;
  mech[lossMech::coul] = &coul;
  mech[lossMech::ic] = &ic;
  mech[lossMech::ion] = &ion;
  mech[lossMech::nucInel] = &nucInel;
  mech[lossMech::posAnn] = &posAnn;
  mech[lossMech::sync] = &sync;
  
  // Set fSec
  fSec = 1.0/log(10);
  pp.query("losses.f_sec", fSec);

  // Set defaults for which mechanisms cause catastrophic and
  // continuous losses, and produce photons
  for (int i = 0; i < lossMech::nMech; i++) {
    for (int j = 0; j < partTypes::nPartType; j++) {
      ctsLoss[i][j] = mech[i]->ctsLoss((partTypes::pType) j);
      catLoss[i][j] = mech[i]->catLoss((partTypes::pType) j);
      makesPhotons[i][j] = mech[i]->photEmission((partTypes::pType) j);
    }
  }

  // Check for disable flag
  array<bool, lossMech::nMech> mechOn;
  int disable = 0;
  pp.query("losses.disable_all", disable);
  if (!disable)
    for (int i = 0; i < lossMech::nMech; i++) mechOn[i] = true;
  else
    for (int i = 0; i < lossMech::nMech; i++) mechOn[i] = false;

  // Check for commands to enable or disable specific mechanisms
  int flag;
  if (pp.query("losses.enable_brem", flag)) {
    if (flag) mechOn[lossMech::brem] = true;
    else mechOn[lossMech::brem] = false;
  }
  if (pp.query("losses.enable_coulomb", flag)) {
    if (flag) mechOn[lossMech::coul] = true;
    else mechOn[lossMech::coul] = false;
  }
  if (pp.query("losses.enable_IC", flag)) {
    if (flag) mechOn[lossMech::ic] = true;
    else mechOn[lossMech::ic] = false;
  }
  if (pp.query("losses.enable_ionization", flag)) {
    if (flag) mechOn[lossMech::ion] = true;
    else mechOn[lossMech::ion] = false;
  }
  if (pp.query("losses.enable_nuc_inelastic", flag)) {
    if (flag) mechOn[lossMech::nucInel] = true;
    else mechOn[lossMech::nucInel] = false;
  }
  if (pp.query("losses.enable_positron_annihilation", flag)) {
    if (flag) mechOn[lossMech::posAnn] = true;
    else mechOn[lossMech::posAnn] = false;
  }
  if (pp.query("losses.enable_synchrotron", flag)) {
    if (flag) mechOn[lossMech::sync] = true;
    else mechOn[lossMech::sync] = false;
  }

  // Apply on-off switches to matrices
  for (int i = 0; i < lossMech::nMech; i++) {
    for (int j = 0; j < partTypes::nPartType; j++) {
      ctsLoss[i][j] = ctsLoss[i][j] && mechOn[i];
      catLoss[i][j] = catLoss[i][j] && mechOn[i];
      makesPhotons[i][j] = makesPhotons[i][j] && mechOn[i];
    }
  }
}


/////////////////////////////////////
// Loss and draw functions
/////////////////////////////////////


void Losses::computeLoss(const gas::GasData& gd,
			 const CRPacket& packet,
			 Real& dpdtCts,
			 Real& lossRate,
			 Real& wSec,
			 MechMatrix<Real>& pSec) const {

  // Initialize all quantities
  dpdtCts = lossRate = 0.0;
  Real pSecSum = 0.0;
  for (IdxType i = 0; i < lossMech::nMech; i++) {
    for (IdxType j = 0; j < partTypes::nPartType; j++) {
      pSec[i][j] = 0.0;
    }
  }

  // Loop over continuous loss mechanisms
  for (int i = 0; i < lossMech::nMech; i++) {
    if (ctsLoss[i][packet.type]) dpdtCts += mech[i]->dpdt(gd, packet);
  }

  // Loop over catastrophic loss mechanisms
  for (int i = 0; i < lossMech::nMech; i++) {
    if (catLoss[i][packet.type]) {
      Real lr = mech[i]->lossRate(gd, packet);
      auto mult = mech[i]->secMult(gd, packet);
      lossRate += lr;
      for (int j = 0; j < partTypes::nPartType; j++) {
	pSec[i][j] = lr * mult[j];
	pSecSum += pSec[i][j];
      }
    }
  }

  // Normalize pSec and compute wSec
  wSec = pSecSum / lossRate / fSec;
  if (pSecSum > 0.0) {
    for (int i = 0; i < lossMech::nMech; i++) {
      for (int j = 0; j < partTypes::nPartType; j++) {
	pSec[i][j] *= fSec / pSecSum;
      }
    }
  }
}

Losses::MechArr<Real> Losses::dpdtMech(const gas::GasData& gd,
				       const CRPacket& packet) const {

  // Initialize output holder
  MechArr<Real> dpdt;

  // Loop over mechanisms
  for (int i = 0; i < lossMech::nMech; i++) {
    if (ctsLoss[i][packet.type]) dpdt[i] = mech[i]->dpdt(gd, packet);
    else dpdt[i] = 0;
  }

  // Return
  return dpdt;
}


Losses::MechArr<Real> Losses::lossRateMech(const gas::GasData& gd,
					   const CRPacket& packet) const {
  // Initialize output holder
  MechArr<Real> lr;

  // Loop over mechanisms
  for (int i = 0; i < lossMech::nMech; i++) {
    if (catLoss[i][packet.type]) lr[i] = mech[i]->lossRate(gd, packet);
    else lr[i] = 0;
  }

  // Return
  return lr;
}


CRPacket Losses::drawSecondary(const gas::GasData& gd,
			       const CRPacket& packet,
			       const Real& t,
			       const Real& wSec,
			       const lossMech::mechType& m,
			       const partTypes::pType& type) const {

  // Set secondary packet properties
  CRPacket s;
  s.type = type;
  s.src = packet.src;
  if (s.type != packet.type) s.src = s.src | CRPacket::secondaryFlag;
  s.tInj = t;
  s.wInj = s.w = wSec * packet.w;
  s.gr = 0.0;
  s.p = mech[m]->secMomentum(gd, packet, type);
#ifdef TRACK_PITCH_ANGLE
  // Set secondary pitch angle equal to primary pitch angle. This is
  // only approximately true, since the interaction should perturb the
  // pitch angle, but I have not thus far tried to implement a model
  // for this perturbation (which would almost certainly be different
  // for different loss mechanisms).
  s.mu = packet.mu;
#endif
  
  // Return
  return s;
}

// Photon emission function
vector<Real> Losses::photLum(const gas::GasData& gd,
			     const CRPacket& packet,
			     const vector<Real>& en,
			     const vector<RealVec> dir
#ifdef TRACK_PITCH_ANGLE
			     , const Real hmu
#endif
			     ) const {

  // Initialize output holder
  vector<Real> out;
  out.reserve(en.size() * dir.size() * lossMech::nMech);

  // Loop over mechanisms
  for (int i = 0; i < lossMech::nMech; i++) {
    if (makesPhotons[i][packet.type]) {
      std::vector<Real> dLdE
	= mech[i]->photLum(gd, packet, en, dir
#ifdef TRACK_PITCH_ANGLE
			   , hmu
#endif
			   );
      out.insert(out.end(), dLdE.begin(), dLdE.end());
    } else {
      out.insert(out.end(), en.size() * dir.size(), 0);
    }
  }
  // Return
  return out;
}


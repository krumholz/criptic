/**
 * @file Synchrotron.H
 * @brief Class to describe synchrotron losses
 *
 * This class is derived from LossMechanism, and represents losses due
 * to synchrotron radiation.
 */

#ifndef _SYNCHROTRON_H_
#define _SYNCHROTRON_H_

#include "LossMechanism.H"
#include "../Utils/Constants.H"
#include "../Utils/Interp1D.H"
#include "../Utils/PartTypes.H"
#include "../Utils/ThreadVec.H"
#include "../Utils/Units.H"

namespace criptic {

  /**
   * @class Synchrotron
   * @brief A class that computes synchrotron losses and emission
   */
  class Synchrotron : public LossMechanism {

  public:

    /**
     * @brief Constructor
     */
    Synchrotron();

    /**
     * @brief Mechanism causes continuous loss to a given type?
     * @param type Type of particle
     * @return Whether this mechanism causes continuous loss
     */
    virtual bool ctsLoss(const partTypes::pType type) const override {
      return (type == partTypes::electron) ||
	(type == partTypes::positron);
    }
    
    /**
     * @brief Mechanism causes photon emission from given particle type?
     * @param type Type of particle
     * @return Whether this mechanism causes photon emission
     */
    virtual bool photEmission(const partTypes::pType type) const override {
      return (type == partTypes::electron) ||
	(type == partTypes::positron);
    }

    /**
     * @brief Compute the continuous loss rate due to a process
     * @param gd Background gas data
     * @param packet Cosmic ray packet whose losses are being computed
     * @returns Rate of continuous momentum loss dp/dt
     * @details
     * The quantity returned must be in code units, so momentum is
     * measured in units of m_p * c.
     */
    virtual Real dpdt(const gas::GasData& gd,
		      const CRPacket& packet) const override {
      Real bg = packet.v() * packet.gamma();
      Real dEdt = constants::sigmaT * constants::c *
#ifdef TRACK_PITCH_ANGLE
	2 * (1.0 - packet.mu * packet.mu) *    // Pitch angle-dependent
#else
	4./3. *                                // Pitch angle-averaged
#endif
	bg * bg * gd.UB() / constants::mp_c2;  // de/dt in code units
      return dEdt / packet.dT_dp();
    };
        
    /**
     * @brief Compute the photon emission spectrum from a packet
     * @param gd Background gas data
     * @param packet Cosmic ray packet
     * @param en Photon energies at which to compute emission rate
     * @param dir Vector of directions in which to calculate emission
     * @param muSoft Relativistic beaming softening parameter
     * @returns Specific luminosity for this mechanism
     * @details
     * This routine computes the specific luminosity or emissivity
     * of synchrotron emission as a function of photon
     * energy en. The meaning of the quantity that this routine returns
     * depends on the value of the direction dir. If a particular
     * entry in dir is zeroVec i.e., the vector is (0,0,0), then the
     * quantity computed is the specific luminosity dL/dE emitted
     * averaged over all directions. For entries in dir that are
     * non-zero, the quantity returned is d^2L / dE dOmega, the energy
     * per unit time per unit photon energy per unit solid angle
     * emitted in the direction specified by the vector dir. The
     * quantity thetaSoft is a softening angle, used to soften the
     * relativistic beaming of CR packets in calculated where pitch
     * angle tracking is enabled; the CRs in a particular packet mu
     * are interpreted as having a Gaussian distribution of pitch
     * angles of bandwidth hmu. Unlike other functions, this one operates
     * in physical units. Specifically, en is the photon energy in
     * GeV, and dL/dE is the specific luminosity in erg/s/GeV (if the
     * code was compiled in CGS mode; J/s/GeV is compiled in MKS
     * mode). The data are packed into a single vector of size number
     * of energies * number of directions, ordered with photon energy
     * as the fastest-varying dimension.
     */
    virtual std::vector<Real>
    photLum(const gas::GasData& gd,
	    const CRPacket& packet,
	    const std::vector<Real>& en,
	    const std::vector<RealVec> dir = { zeroVec }
#ifdef TRACK_PITCH_ANGLE
	    , const Real hmu = 0.01
#endif
	    ) const override;

  private:

    // Interpolator
    Interp1D FSyncInterp;    /**< Pitch angle-averaged F_sync function */
    Interp1D FSyncPAInterp;  /**< Pitch angle-dependent F_sync function */
    
    // Tabulated data
    static constexpr size_t FSyncNTab = 201; /**< Size of synchrotron
						function table */
    static const double logw[FSyncNTab];   /**< Table of log(w/w_cut) values */
    static const double logF[FSyncNTab];   /**< Table of log F(w/w_c) values */
    static const double logFPA[FSyncNTab]; /**< Table of log F_PA(w/w_c) values */
  };

}
#endif
// _SYNCHROTRON_H_

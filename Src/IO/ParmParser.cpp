// Implementation of the parmParser class

#include "ParmParser.H"

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <typeinfo>
#include "../MPI/MPIUtil.H"
#include "../Utils/Constants.H"

using namespace std;
using namespace criptic;

#ifdef USE_BOOST
// Boost is available as an alternative to the c++17 standard
#include <boost/variant/get.hpp>
using namespace boost;

// Define our own version of holds_alternative
template <typename T, typename... Ts>
bool holds_alternative(const boost::variant<Ts...>& v) noexcept
{
    return boost::get<T>(&v) != nullptr;
}

// Use #define's to specify which versions of GET and INDEX
#define GET         boost::get
#define VARIDX(X)   ((X).which())

#else

#define GET         std::get
#define VARIDX(X)   ((X).index())

#endif

////////////////////////////////////////////////////////////////////////
// The constructor
////////////////////////////////////////////////////////////////////////

ParmParser::ParmParser(int argc, char **argv)
{
  // Loop over arguments
  string paramFileName;
  int argptr = 1;
  restart = false;
  while (argptr < argc) {

    // Read next argument and see if it begins with -, indicating a
    // command flag, or not, indicating that this is a potential
    // argument
    string arg(argv[argptr++]);
    if (arg.find("-") == 0) {

      // Flag string, so decide what to do next based on which flag it
      // is
      if (!arg.compare("-h") || !arg.compare("--help")) {
	
	// Help flag; just print usage information and exit
	printUsage();
	MPIUtil::shutdown();
	exit(0);
	
      } else if (!arg.compare("-r") || !arg.compare("--restart")) {

	// Flag to specify to restart from a checkpoint
	restart = true;

	// If this is a restart, and the argument after it is a file
	// name, read that
	if (argptr < argc) {
	  string nextarg(argv[argptr]);
	  restartFileName = string(argv[argptr++]);
	} else {
	  MPIUtil::haltRun("restart requested, but no restart file "
			   "name provided", errBadCommandLine);
	}

      } else {

	// Unknown flag; bail out
	printUsage();
	stringstream ss;
	ss << "unknown option: " << arg;
	MPIUtil::haltRun(ss.str(), errBadCommandLine);

      }

    } else {

      // No flag, so this must be the name of our input file
      if (paramFileName.length() != 0) {
	printUsage();
	MPIUtil::haltRun("found unexpected argument after input file name",
			 errBadCommandLine);
      }
      paramFileName = arg;

    }
  }

  // Make sure we got an input file name, and bail out if not
  if (paramFileName.length() == 0) {
    printUsage();
    MPIUtil::haltRun("missing input file name", errBadCommandLine);
  }

  // Open parameter file
  ifstream paramFile;
  paramFile.open(paramFileName.c_str(), ios::in);
  if (!paramFile.is_open()) {
    string errMsg = "unable to open file " + paramFileName;
    MPIUtil::haltRun(errMsg, errBadInputFile);
  }

  // Parse parameter file
  parseFile(paramFile);

  // Close the file
  paramFile.close();
}

////////////////////////////////////////////////////////////////////////
// Methods to get the value of a keyword
////////////////////////////////////////////////////////////////////////

void ParmParser::get(const std::string& name, int& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  accessed.at(name) = true;  // Flag that keyword has been accessed
  if (holds_alternative<int>(k)) {
    val = GET<int>(k);
  } else if (VARIDX(k) == 1) {
    // Real --> int
    val = static_cast<int>(GET<Real>(k));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const std::string& name, Real& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  accessed.at(name) = true;  // Flag that keyword has been accessed
  if (holds_alternative<Real>(k)) {
    val = GET<Real>(k);
  } else if (VARIDX(k) == 0) {
    // int --> Real
    val = static_cast<Real>(GET<int>(k));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const std::string& name, std::string& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  accessed.at(name) = true;  // Flag that keyword has been accessed
  if (holds_alternative<string>(k)) {
    val = GET<string>(k);
  } else if (VARIDX(k) == 0) {
    // int --> string
    val = to_string(GET<int>(k));
  } else if (VARIDX(k) == 1) {
    // Real --> string
    val = to_string(GET<Real>(k));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const std::string& name,
		     std::vector<int>& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  accessed.at(name) = true;  // Flag that keyword has been accessed
  if (holds_alternative<vector<int> >(k)) {
    val = GET<vector<int> >(k);
  } else if (VARIDX(k) == 0) {
    // int --> vector<int>
    val.clear();
    val.push_back(GET<int>(k));
  } else if (VARIDX(k) == 1) {
    // Real --> vector<int>
    val.clear();
    val.push_back(static_cast<int>(GET<Real>(k)));
  } else if (VARIDX(k) == 4) {
    // vector<Real> --> vector<int>
    val.clear();
    vector<Real> v = GET<vector<Real> >(k);
    for (vector<Real>::size_type i=0; i<v.size(); i++)
      val.push_back(static_cast<int>(v[i]));
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const std::string& name,
		     std::vector<Real>& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  accessed.at(name) = true;  // Flag that keyword has been accessed
  if (holds_alternative<vector<Real> >(k)) {
    val = GET<vector<Real> >(k);
  } else if (VARIDX(k) == 0) {
    // int --> vector<Real>
    val.clear();
    val.push_back(GET<int>(k));
  } else if (VARIDX(k) == 1) {
    // Real --> vector<Real>
    val.clear();
    val.push_back(GET<Real>(k));
  } else if (VARIDX(k) == 3) {
    // vector<int> --> vector<Real>
    val.clear();
    vector<int> v = GET<vector<int> >(k);
    for (vector<Real>::size_type i=0; i<v.size(); i++)
      val.push_back(v[i]);
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}

void ParmParser::get(const std::string& name,
		     std::vector<std::string>& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  accessed.at(name) = true;  // Flag that keyword has been accessed
  if (holds_alternative<vector<string> >(k)) {
    val = GET<vector<string> >(k);
  } else if (VARIDX(k) == 0) {
    // int --> vector<string>
    val.clear();
    val.push_back(to_string(GET<int>(k)));
  } else if (VARIDX(k) == 1) {
    // Real --> vector<string>
    val.clear();
    val.push_back(to_string(GET<Real>(k)));
  } else if (VARIDX(k) == 2) {
    // string --> vector<string>
    val.clear();
    val.push_back(GET<string>(k));
  } else if (VARIDX(k) == 3) {
    // vector<int> --> vector<string>
    val.clear();
    vector<int> v = GET<vector<int> >(k);
    for (vector<int>::size_type i=0; i<v.size(); i++)
      val.push_back(to_string(v[i]));
  } else if (VARIDX(k) == 4) {
    // vector<Real> --> vector<string>
    val.clear();
    vector<Real> v = GET<vector<Real> >(k);
    for (vector<Real>::size_type i=0; i<v.size(); i++)
      val.push_back(to_string(v[i]));
  }
}

template<class T>
void ParmParser::get(const std::string& name,
		     Vec3<T>& val) const {
  if (keywords.find(name) == keywords.end()) missingKeyError(name);
  const keyword& k = keywords.at(name);
  accessed.at(name) = true;  // Flag that keyword has been accessed
  if (holds_alternative<vector<Real> >(k)) {
    // vector<Real> --> Vec3
    vector<Real> v = GET<vector<Real> >(k);
    // Make sure vector is of correct length; if so, return it, if not
    // throw error
    if (v.size() == 3) val = v;
    else typeError(name);
  } else if (VARIDX(k) == 3) {
    // vector<int> --> Vec3
    vector<int> v = GET<vector<int> >(k);
    // Make sure vector is of correct length; if so, return it, if not
    // throw error
    if (v.size() == 3) val = v;
    else typeError(name);
  } else {
    // If we're here, we don't know how to do this type conversion, so
    // throw an error
    typeError(name);
  }
}


template <class T> bool
ParmParser::query(const std::string &name, T& val) const {
  if (keywords.find(name) == keywords.end()) return false;
  else {
    this->get(name, val);
    return true;
  }
}

// Instatiate various versions of the query member function
template bool ParmParser::query(const string&, int&) const;
template bool ParmParser::query(const string&, Real&) const;
template bool ParmParser::query(const string&, string&) const;
template bool ParmParser::query(const string&, vector<int>&) const;
template bool ParmParser::query(const string&, vector<Real>&) const;
template bool ParmParser::query(const string&, vector<string>&) const;
template bool ParmParser::query(const string&, RealVec&) const;
template bool ParmParser::query(const string&, IdxVec&) const;


////////////////////////////////////////////////////////////////////////
// Method to write out all parameter values
////////////////////////////////////////////////////////////////////////

void ParmParser::dumpParams(ostream &outFile) {

  if (MPIUtil::IOProc) {
    // Loop over all key, value pairs
    for (auto it = keywords.begin(); it != keywords.end(); ++it) {
      string k = it->first;
      vector<string> v;
      this->get(k, v);
      outFile << k << "  ";
      for (vector<string>::size_type i=0; i<v.size(); i++)
	outFile << "  " << v[i];
      outFile << endl;
    }
  }
}

////////////////////////////////////////////////////////////////////////
// Method to warn about unused keywords
////////////////////////////////////////////////////////////////////////

void ParmParser::warnUnusedKeywords() const {
  if (MPIUtil::IOProc) {
    for (auto it = accessed.begin(); it != accessed.end(); ++it) {
      if (!it->second) {
	cout << "CRIPTIC: Warning: input file keyword "
	     << it->first
	     << " set but not used"
	     << endl;
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////
// Method to print a usage message
////////////////////////////////////////////////////////////////////////

void ParmParser::printUsage() {
  if (MPIUtil::IOProc) {
    cout << "Usage: criptic criptic.in" << endl;
    cout << "       criptic [-r or --restart CHECKPOINTFILE] criptic.in "
	 << endl;
    cout << "       criptic [-h or --help]" << endl;
    cout << "       Options: " << endl;
    cout << "       --restart : restart from CHECKPOINTFILE" << endl;
    cout << "       --help : print this message and exit" << endl;
  }
}

////////////////////////////////////////////////////////////////////////
// Method to parse an open parameter file
////////////////////////////////////////////////////////////////////////

void ParmParser::parseFile(ifstream &paramFile) {
  string line;
  while (!(paramFile.eof())) {

    // Read a line, and save a copy of the original
    getline(paramFile, line);
    string origLine(line);

    // Trim leading and trailing whitespace
    const std::string wschars = "\t\n\v\f\r ";
    line.erase(0, line.find_first_not_of(wschars));
    line.erase(line.find_last_not_of(wschars)+1);

    // Trim anything after a #
    while (true) {
      size_t commentPos = line.find_last_of("#");
      if (commentPos == string::npos) break;
      line.erase(commentPos);
    }

    // If line is empty, continue
    if (line.length() == 0) continue;

    // Break line up into whitespace-separated tokens
    vector<string> tokens;
    istringstream iss(line);
    copy(istream_iterator<string>(iss),
	 istream_iterator<string>(),
	 back_inserter(tokens));

    // Issue warning if we get duplicate tokens
    if (keywords.find(tokens[0]) != keywords.end()) {
      if (MPIUtil::IOProc)
	cout << "ParmParser: WARNING: found duplicate keyword "
	     << tokens[0]
	     << " in parameter file; the value used will be the"
	     << " value given by the last occurrence"
	     << endl;
    }
	
    // Proceed based on number of tokens
    if (tokens.size() < 2) {
      
      // Need at least two tokens, so this is an error
      parseError(origLine);

    } else if (tokens.size() == 2) {

      // Exactly two tokens, so try to parse this first as an integer,
      // then if that fails as a double, then if that fails as a
      // string; note that this it written using C-style rather than
      // C++-style strong parsing, because for some insane reason
      // certain versions of mpiCC seem to break the usual try {
      // stoi(...) } catch (...) { ... } paradigm. I have no idea why,
      // but it's easier to just rewrite this C-style than to try to
      // figure out what the compiler is doing.
      
      // Try parsing as an integer first
      char *p;
      const char *tok = tokens[1].c_str();
      int ival = strtol(tok, &p, 10);
      if (p - tok == (long) tokens[1].size()) {
	keywords[tokens[0]] = ival;
	accessed[tokens[0]] = false;
      } else {
	// Try parsing as a result number
	Real rval = strtod(tok, &p);
	if (p - tok == (long) tokens[1].size()) {
	  keywords[tokens[0]] = rval;
	  accessed[tokens[0]] = false;
	} else {
	  // If we're here, this didn't parse as in int or a real, so
	  // just leave as a string
	  keywords[tokens[0]] = tokens[1];
	  accessed[tokens[0]] = false;
	}
      }

    } else {
    
      // For two or more arguments, this is going to be a vector of
      // something; proceed to determine the type of that something
      // exactly as in the single argument case
      char *p;

      // Try parsing as integers
      vector<int> ival;
      for (IdxType i = 1; i < tokens.size(); i++) {
	const char *tok = tokens[i].c_str();
	int iv = strtol(tok, &p, 10);
	if (p - tok == (long) tokens[i].size()) {
	  ival.push_back(iv);
	} else {
	  break;
	}
      }
      if (ival.size() == tokens.size() - 1) {
	keywords[tokens[0]] = ival;
	accessed[tokens[0]] = false;
      } else {
	// Try parsing as reals
	vector<Real> rval;
	for (IdxType i = 1; i < tokens.size(); i++) {
	  const char *tok = tokens[i].c_str();
	  Real rv = strtod(tok, &p);
	  if (p - tok == (long) tokens[i].size()) {
	    rval.push_back(rv);
	  } else {
	    break;
	  }
	}
	if (rval.size() == tokens.size() - 1) {
	  keywords[tokens[0]] = rval;
	  accessed[tokens[0]] = false;
	} else {
	  // If we're here, this didn't parse as ints or reals, so
	  // just make it a vector of strings
	  vector<string> sval;
	  for (vector<string>::size_type i=1; i<tokens.size(); i++)
	    sval.push_back(tokens[i]);
	  keywords[tokens[0]] = sval;
	  accessed[tokens[0]] = false;
	}
      }
    }
  }
}
    
////////////////////////////////////////////////////////////////////////
// Methods to throw error and exit
////////////////////////////////////////////////////////////////////////

void ParmParser::parseError(const string& line) const {
  string err = "unable to parse input file line: " + line;
  MPIUtil::haltRun(err, errBadInputFile);
}

void ParmParser::typeError(const string& name) const {
  string err = "for keyword " + name + ", unable to interpret value";
  vector<string> kOut;
  get(name, kOut);
  for (auto k : kOut) err += "  " + k;
  MPIUtil::haltRun(err, errBadInputFile);
}

void ParmParser::missingKeyError(const string& name) const {
  string err = "keyword " + name + " not found in input file";
  MPIUtil::haltRun(err, errBadInputFile);
}

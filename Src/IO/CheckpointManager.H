/**
 * @file CheckpointManager.H
 * @brief Class to manage checkpoints
 *
 * @details
 * This file provide the CheckpointManager class, which is a class to
 * manage writing to and reading from checkpoint files.
 */

#ifndef _CHECKPOINTMANAGER_H_
#define _CHECKPOINTMANAGER_H_

#include <string>
#include "ParmParser.H"
#include "../Core/CRTree.H"
#include "../Losses/Losses.H"
#include "../Prob/Prob.H"
#include "../Propagation/Propagation.H"
#include "../Utils/Types.H"
#include "hdf5.h"

namespace criptic {

  /**
   * @class CheckpointManager
   * @brief A class to write and read checkpoint files
   * @details
   * This class provides methods to write and read checkpoint files,
   * and carry out related tasks such as keeping track of checkpoint
   * numbers and times
   */
  class CheckpointManager {

  public:

    /**
     * @brief Start up the checkpoint manager
     * @param pp The ParmParser containing the input deck
     * @param prop CR propagation model
     * @param loss CR loss model
     */
    CheckpointManager(const ParmParser& pp,
		      const propagation::Propagation& prop,
		      const Losses& loss);

    /**
     * @brief Function to write a checkpoint
     * @param step The step number at which the checkpoint is written
     * @param t The time at which the checkpoint is written
     * @param dt The size of the next time step
     * @param tree The CRTree containing the current state
     * @param prob Problem setup class; provides problem-specific IO
     */
    void writeCheckpoint(const int step,
			 const Real t,
			 const Real dt,
			 const CRTree& tree,
			 Prob* prob);

    /**
     * @brief Function to read a checkpoint
     * @param filename Name of checkpoint file to read
     * @param step Step number stored in checkpoint
     * @param t Time stored in checkpoint
     * @param dt Time step stored in checkpoint
     * @param tree CRTree to fill with checkpoint data
     * @param prob Problem setup class; provides problem-specific IO
     * @details
     * When run in parallel, the data to be read are divided between
     * MPI ranks; if the number of ranks is equal to the number
     * used in the checkpoint, the data will be divided between ranks
     * exactly as it was when the checkpoint was written. Otherwies,
     * this routine will divide the data between ranks as evenly as
     * possible given the number of ranks present in the checkpoint.
     */
    void readCheckpoint(const std::string& filename,
			int& step,
			Real& t,
			Real& dt,
			CRTree& tree,
			Prob* prob);

    /**
     * @brief Step number at which the last checkpoint was written
     * @returns Step number at which the last checkpoint was written
     */
    int lastStep() const { return lastChkStep; }
    
    /**
     * @brief Step number at which the next checkpoint will be written
     * @returns Step number at which the next checkpoint will be written
     */
    int nextStep() const { return nextChkStep; }
    
    /**
     * @brief Time at which the last checkpoint was written
     * @returns Time at which the last checkpoint was written
     */
    Real lastTime() const { return lastChkTime; }
    
    /**
     * @brief Time at which the next checkpoint will be written
     * @returns Time at which the next checkpoint will be written
     */
    Real nextTime() const { return nextChkTime; }

    static const int fWidth = 15;  /**< Field width in ASCII output */

  private:

    /**
     * @brief Sync packet, souce, and deleted packet counts to IO rank
     * @param nPkt Number of packets on each rank
     * @param nSrc Number of sources on each rank
     * @param nDel Number of deleted packets on each rank
     * @param tree The CRTree containing the current state
     * @details
     * This is a collective method that is to be invoked by all MPI
     * ranks. On the IO rank, it has the effect of setting nPkt, nSrc,
     * and nDel to vectors of MPIUtil::nRank elements, with each
     * element containing the number of packets, sources, and deleted
     * packets stored on each MPI rank. On all other ranks the arrays
     * nPkt, nSrc, and nDel are unchanged and can be ignored. Note
     * that, if this routine is invoked in a run where MPI is
     * disabled, its effect is just to set nPkt, nSrc, and nDel to
     * vectors of length 1 containing the packet, source, and deleted
     * packets counts on the single MPI rank running.
     */
    void syncCounts(std::vector<IdxType>& nPkt,
		    std::vector<IdxType>& nSrc,
		    std::vector<IdxType>& nDel,
		    const CRTree& tree) const;
    
#ifdef ENABLE_MPI
    /**
     * @brief Send checkpoint data to be written to the rank doing IO
     * @param t Current simulation time
     * @param tree The CRTree containing the current state
     */
     void sendCheckpointData(const Real& t,
			     const CRTree& tree) const;
#endif

    /**
     * @brief Write packet data to file
     * @param fileID The HDF5 handle for the output file
     * @param nPkt Number of packets on each rank
     * @param step Current simulation step number
     * @param t Current simulation time
     * @param dt Current time step
     * @param tree The CRTree containing the current state
     */
    void writePackets(hid_t& fileID,
		      const std::vector<IdxType>& nPkt,
		      const int& step,
		      const Real& t,
		      const Real& dt,
		      const CRTree& tree) const;

    /**
     * @brief Write source data to file
     * @param fileID The HDF5 handle for the output file
     * @param nSrc Number of sources on each rank
     * @param tree The CRTree containing the current state
     */
    void writeSources(hid_t& fileID,
		      const std::vector<IdxType>& nSrc,
		      const CRTree& tree) const;
    
    /**
     * @brief Write deleted packet data to file
     * @param fileID The HDF5 handle for the output file
     * @param nDel Number of deleted packets on each rank
     * @param tree The CRTree containing the current state
     */
    void writeDelPackets(hid_t& fileID,
			 const std::vector<IdxType>& nDel,
			 const CRTree& tree) const;
    
    /**
     * @brief Write field quantity data to file
     * @param fileID The HDF5 handle for the output file
     * @param nPkt Number of packets on each rank
     * @param tree The CRTree containing the current state
     */
    void writeFieldQty(hid_t& fileID,
		       const std::vector<IdxType>& nPkt,
		       const CRTree& tree) const;
    
    /**
     * @brief Write loss rate data to file
     * @param fileID The HDF5 handle for the output file
     * @param nPkt Number of packets on each rank
     * @param t Current simulation time
     * @param tree The CRTree containing the current state
     */
    void writeLoss(hid_t& fileID,
		   const std::vector<IdxType>& nPkt,
		   const Real& t,
		   const CRTree& tree) const;

    /**
     * @brief Write packet-by-packet photon emission data to file
     * @param fileID The HDF5 handle for the output file
     * @param photEn Photon energies at which emission is calculated
     * @param nPkt Number of packets on each rank
     * @param t Current simulation time
     * @param tree The CRTree containing the current state
     */
    void writePhotonsFull(hid_t& fileID,
			  const std::vector<Real>& photEn,
			  const std::vector<IdxType>& nPkt,
			  const Real& t,
			  const CRTree& tree) const;
    
    /**
     * @brief Write summary photon emission data to file
     * @param fileID The HDF5 handle for the output file
     * @param photEn Photon energies at which emission is calculated
     * @param t Current simulation time
     * @param tree The CRTree containing the current state
     */
    void writePhotonsSummary(hid_t& fileID,
			     const std::vector<Real>& photEn,
			     const Real t, 
			     const CRTree& tree) const;
    
    /**
     * @brief Write ionization rate data to file
     * @param fileID The HDF5 handle for the output file
     * @param nPkt Number of packets on each rank
     * @param t Current simulation time
     * @param tree The CRTree containing the current state
     */
    void writeIonization(hid_t& fileID,
			 const std::vector<IdxType>& nPkt,
			 const Real& t,
			 const CRTree& tree) const;

    // Internal data
    std::string baseChkName; /**< Base name for checkpoint files */
    bool needFQ;             /**< Field quantities needed for this run? */
    bool needFQG;            /**< Field qty gradients needed for this run? */
    bool writeDelPackets_;   /**< Write deleted packets to checkpoints? */
    bool writeFieldQty_;     /**< Write field quantities to checkpoints? */
    bool writeLoss_;         /**< Write packet loss rates to checkpoints? */
    bool writePhotons_;      /**< Write photon emission to checkpoints? */
    bool writePhotonsFull_;  /**< Write packet-by-packet photon data? */
    bool writeIonization_;   /**< Write out ionization rates? */
    std::vector<Real> photEn; /**< Energies for photon emission
				 calculation (always in GeV) */
    int verbosity;           /**< Level of verbosity */
    int chkNum;              /**< Checkpoint number counter */
    int chkInt;              /**< Step interval between checkpoints */
    int lastChkStep;         /**< Step at which last checkpoint was
				written */
    int nextChkStep;         /**< Step at which next checkpoint will
				be written */
    Real chkDt;              /**< Time interval between checkpoints */
    Real lastChkTime;        /**< Time at which last checkpoint was
				written */
    Real nextChkTime;        /**< Time at which next checkpoint will
				be written */
    hid_t RealVec_HDF5;      /**< HDF5 type corresponding to RealVec */
    hid_t RealTensor2_HDF5;  /**< HDF5 type corresponding to RealTensor2 */
    hid_t CRPacket_HDF5;     /**< HDF5 type corresponding to CRPacket */
    hid_t CRSource_HDF5;     /**< HDF5 type corresponding to CRSource */
    hid_t FieldQty_HDF5;     /**< HDF5 type corresponding to FieldQty */
    hid_t FieldQtyGrad_HDF5; /**< HDF5 type corresponding to FieldQtyGrad */
  };

}

// _CHECKPOINTMANAGER_H_
#endif

// Implementation of the CheckpointManager class

#include "CheckpointManager.H"
#include "../Definitions.H"
#include "../Core/FieldQty.H"
#include "../MPI/MPIUtil.H"
#include "../Utils/Constants.H"
#include "../Utils/Units.H"
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cmath>
#include <type_traits>
#if 1
#include <arpa/inet.h>
#endif
#if __cplusplus >= 201703L
#   include <filesystem>
#elif defined(USE_BOOST)
#   include <boost/filesystem.hpp>
using namespace boost;
#endif
#ifdef _OPENMP
#   include "omp.h"
#endif

// Namespaces
using namespace std;
using namespace criptic;
using namespace MPIUtil;

// A little function to tokenize strings, trimming whitespace as we do
inline vector<string> tokenize(string& str, char delim = '=') {
  const std::string wschars = "\t\n\v\f\r ";
  IdxType start;
  IdxType end = 0;
  vector<string> out;
  while ((start = str.find_first_not_of(delim, end)) != string::npos) {
    end = str.find(delim, start);
    string tok = str.substr(start, end-start);
    tok.erase(0, tok.find_first_not_of(wschars));
    tok.erase(tok.find_last_not_of(wschars)+1);
    out.push_back(tok);
  }
  return out;
}


CheckpointManager::CheckpointManager(const ParmParser &pp,
				     const propagation::Propagation& prop,
				     const Losses& loss)
{

  // Get base name for checkpoints
  verbosity = 0;
  pp.query("verbosity", verbosity);
  baseChkName = "criptic_";
  pp.query("output.chk_name", baseChkName);

  // Record whether we are using field quantities or their gradients
  needFQ = prop.fieldQtyNeed() >= needFieldQty;
  needFQG = prop.fieldQtyNeed() == needFieldQtyGrad;

  // Get checkpoint intervals in time and step number
  if (!pp.query("output.chk_int", chkInt)) chkInt = 0;
  if (!pp.query("output.chk_dt", chkDt)) chkDt = 0.0;

  // Initialize counters assuming this is not a restart; if it is a
  // restart, these values will be overwritten when readCheckpoint is
  // called.
  chkNum = 0;
  if (chkInt > 0) nextChkStep = chkInt; else nextChkStep = maxInt;
  if (chkDt > 0.0) nextChkTime = chkDt; else nextChkTime = maxReal;

  // Look for directives changing the defaults on which quantities are
  // written to checkpoints files
  int writeDelPacketsI;
  if (!pp.query("output.deleted_packets", writeDelPacketsI))
    writeDelPackets_ = true;
  else
    writeDelPackets_ = writeDelPacketsI != 0;
  int writeFieldQtyI;
  if (needFQ) {
    if (!pp.query("output.field_quantities", writeFieldQtyI))
      writeFieldQty_ = true;
    else
      writeFieldQty_ = writeFieldQtyI != 0;
  } else {
    writeFieldQty_ = false;
  }
  int writeLossI;
  if (!pp.query("output.loss_rates", writeLossI))
    writeLoss_ = true;
  else
    writeLoss_ = writeLossI != 0;
  int writePhotonsI;
  if (!pp.query("output.photon_emission", writePhotonsI))
    writePhotons_ = true;
  else
    writePhotons_ = writePhotonsI != 0;
  int writePhotonsFullI;
  if (!pp.query("output.photon_emission_full", writePhotonsFullI))
    writePhotonsFull_ = false;
  else
    writePhotonsFull_ = writePhotonsFullI != 0;
  int writeIonizationI;
  if (!pp.query("output.ionization", writeIonizationI))
    writeIonization_ = true;
  else
    writeIonization_ = writeIonizationI != 0;

  // Read energies at which to compute photon emission; users can
  // specify this in two ways:
  // 1) The keyword output.photon_energy followed by a list of photon
  // energies.
  // 2) The keyword output.photon_energy_span_N where N = 1, 2, 3,
  // ...; each such keyword is followed by numbers giving the minimum
  // energy, the maximum energy, and the number of points in between
  // the min and max (sampled uniformly in log) at which to
  // output. Any number of such spans can appear.
  //
  // For each keyword output.photon_energy or
  // output.photon_energy_span_N, the user also has the option to
  // specify a unit, by adding the corresponding keywords
  // output.photon_energy_unit or output.photon_energy_span_N_unit,
  // respectively. If unspecified, units are assumed to be in GeV, but
  // users can specify one of the following strings to specify an
  // alternate unit: Hz, kHz, MHz, GHz, eV, keV, MeV, GeV.
  // 
  if (writePhotons_) {

    // Direct specification of energies; read energies then check for
    // unit conversion instructions
    pp.query("output.photon_energies", photEn);
    string unitStr;
    if (pp.query("output.photon_energies_unit", unitStr)) {
      Real fac = 1;
      if (unitStr == "Hz") fac = constants::h / units::GeV;
      else if (unitStr == "kHz") fac = 1e3 * constants::h / units::GeV;
      else if (unitStr == "MHz") fac = 1e6 * constants::h / units::GeV;
      else if (unitStr == "GHz") fac = 1e9 * constants::h / units::GeV;
      else if (unitStr == "eV") fac = 1e-9;
      else if (unitStr == "keV") fac = 1e-6;
      else if (unitStr == "MeV") fac = 1e-3;
      else if (unitStr == "GeV") fac = 1;
      else
	MPIUtil::haltRun("CheckpointManager: expected "
			 "output.photon_energies_unit to be one of "
			 "the following: Hz, kHz, MHz, GHz, eV, keV, "
			 "MeV, GeV", errBadInputFile);
      for (IdxType i = 0; i < photEn.size(); i++) photEn[i] *= fac;
    }

    // Read energy spans
    string base = "output.photon_energy_span_";
    int spanNum = 0;
    while (true) {

      // Read span statement and make sure it is valid
      spanNum++;
      stringstream ss;
      ss << base << spanNum;
      vector<Real> en_span;
      if (!pp.query(ss.str(), en_span)) break;
      if (en_span.size() != 3) {
	MPIUtil::haltRun("CheckpointManager: expected "
			 "output.photon_energy_span_N to "
			 "provide three quantities", errBadInputFile);
      }
      Real enLo = en_span[0];
      Real enHi = en_span[1];
      SignedIdxType nEn = static_cast<IdxType>(en_span[2]);
      if (enLo <= 0.0 || enHi <= 0.0 || nEn <= 0) {
	MPIUtil::haltRun("CheckpointManager: expected "
			 "output.photon_energy_span_N to "
			 "provide positive quantities", errBadInputFile);
      }

      // Read corresponding unit conversion
      Real fac = 1;
      ss << "_unit";
      if (pp.query(ss.str(), unitStr)) {
	if (unitStr == "Hz") fac = constants::h / units::GeV;
	else if (unitStr == "kHz") fac = 1e3 * constants::h / units::GeV;
	else if (unitStr == "MHz") fac = 1e6 * constants::h / units::GeV;
	else if (unitStr == "GHz") fac = 1e9 * constants::h / units::GeV;
	else if (unitStr == "eV") fac = 1e-9;
	else if (unitStr == "keV") fac = 1e-6;
	else if (unitStr == "MeV") fac = 1e-3;
	else if (unitStr == "GeV") fac = 1;
	else {
	  stringstream ss1;
	  ss1 << "CheckpointManager: expected " << ss.str()
	      << " to be one of Hz, kHZ, MHz, GHz, eV, keV, MeV, GeV";
	  MPIUtil::haltRun(ss1.str(), errBadInputFile);
	}
      }
	
      // Insert span into list
      for (SignedIdxType i = 0; i < nEn; i++)
	photEn.push_back(fac * enLo *
			 pow(enHi/enLo, static_cast<Real>(i)/(nEn-1)));

    }

    // See if we have photon energies, and, if so, sort output energy list
    if (photEn.size() == 0) writePhotons_ = false;
    else sort(photEn.begin(), photEn.end());
  }
  
  // Create HDF5 compound datatypes corresponding to various
  // structures used in the code
  {
    // RealVec and RealTensor2
    hsize_t dims[] = { 3 };
    RealVec_HDF5 = H5Tarray_create(H5T_NATIVE_REAL, 1, dims);
    dims[0] = 9;
    RealTensor2_HDF5 = H5Tarray_create(H5T_NATIVE_REAL, 1, dims);
  }
  
  {
    // CRPacket
    CRPacket_HDF5 = H5Tcreate(H5T_COMPOUND, sizeof(CRPacket));
    H5Tinsert(CRPacket_HDF5, "type", HOFFSET(CRPacket, type),
	      H5T_NATIVE_INT);
    H5Tinsert(CRPacket_HDF5, "src", HOFFSET(CRPacket, src),
	      H5T_NATIVE_ULLONG);
    H5Tinsert(CRPacket_HDF5, "uniqueID", HOFFSET(CRPacket, uniqueID),
	      H5T_NATIVE_ULLONG);
    H5Tinsert(CRPacket_HDF5, "tInj", HOFFSET(CRPacket, tInj),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRPacket_HDF5, "wInj", HOFFSET(CRPacket, wInj),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRPacket_HDF5, "p", HOFFSET(CRPacket, p),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRPacket_HDF5, "w", HOFFSET(CRPacket, w),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRPacket_HDF5, "gr", HOFFSET(CRPacket, gr),
	      H5T_NATIVE_REAL);
#ifdef TRACK_PITCH_ANGLE
    H5Tinsert(CRPacket_HDF5, "mu", HOFFSET(CRPacket, mu),
	      H5T_NATIVE_REAL);
#endif
  }

  {
    // CRSource
    CRSource_HDF5 = H5Tcreate(H5T_COMPOUND, sizeof(CRSource));
    H5Tinsert(CRSource_HDF5, "type", HOFFSET(CRSource, type),
	      H5T_NATIVE_INT);
    H5Tinsert(CRSource_HDF5, "uniqueID", HOFFSET(CRSource, uniqueID),
	      H5T_NATIVE_ULLONG);
    H5Tinsert(CRSource_HDF5, "p0", HOFFSET(CRSource, p0),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRSource_HDF5, "p1", HOFFSET(CRSource, p1),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRSource_HDF5, "q", HOFFSET(CRSource, q),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRSource_HDF5, "k", HOFFSET(CRSource, k),
	      H5T_NATIVE_REAL);
#ifdef TRACK_PITCH_ANGLE
    H5Tinsert(CRSource_HDF5, "mu0", HOFFSET(CRSource, mu0),
	      H5T_NATIVE_REAL);
    H5Tinsert(CRSource_HDF5, "mu1", HOFFSET(CRSource, mu1),
	      H5T_NATIVE_REAL);
#endif
    H5Tinsert(CRSource_HDF5, "v", HOFFSET(CRSource, v),
	      RealVec_HDF5);
    H5Tinsert(CRSource_HDF5, "a", HOFFSET(CRSource, a),
	      RealVec_HDF5);
  }

  if (writeFieldQty_) {
    // FieldQty and FieldQtyGrad
    hsize_t dims[] = { nFieldQty };
    FieldQty_HDF5 = H5Tarray_create(H5T_NATIVE_REAL, 1, dims);
    FieldQtyGrad_HDF5 = H5Tarray_create(RealVec_HDF5, 1, dims);
  }
}


void CheckpointManager::writeCheckpoint(const int step,
					const Real t,
					const Real dt,
					const CRTree& tree,
					Prob* prob) {
  // Construct output file name
  stringstream ss;
  ss << baseChkName << setfill('0') << setw(CHKDIGITS)
     << chkNum << ".hdf5";
  string name = ss.str();

  // Print status if verbose
  if (verbosity > 0 && MPIUtil::IOProc)
    cout << "CheckpointManager: writing checkpoint "
	 << ss.str() << " at step = " << step
	 << ", t = " << t << endl;
  
  // Save time and step number of this checkpoint
  lastChkStep = step;
  lastChkTime = t;

  // Record time and step number of this checkpoint, and compute time
  // and step number when next checkpoint should be written
  if (chkInt > 0)
    nextChkStep = chkInt * (step / chkInt + 1);
  if (chkDt > 0.0)
    nextChkTime = chkDt * (round(t / chkDt) + 1.0);
  
  // Exchange information on how many packets, sources, and deleted
  // packets live on each MPI rank
  vector<IdxType> nPkt, nSrc, nDel;
  syncCounts(nPkt, nSrc, nDel, tree);

  // At this point the IO and non-IO ranks split up
#ifdef ENABLE_MPI
  if (!MPIUtil::IOProc) {
    // The non-IO ranks just send their data to the IO rank
    sendCheckpointData(t, tree);
  } else {
#endif
    // The IO rank writes the outputs to file
    
    // Open HDF5 file
    hid_t fileID = H5Fcreate(name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
			     H5P_DEFAULT);

    // Write out CR packets
    writePackets(fileID, nPkt, step, t, dt, tree);

    // Write out sources
    writeSources(fileID, nSrc, tree);

    // Write deleted packets
    if (writeDelPackets_) writeDelPackets(fileID, nDel, tree);

    // Write field quantities
    if (writeFieldQty_) writeFieldQty(fileID, nPkt, tree);

    // Write loss rates
    if (writeLoss_) writeLoss(fileID, nPkt, t, tree);

    // Write photon emission summary or full photon emission
    if (writePhotons_) {
      if (writePhotonsFull_) writePhotonsFull(fileID, photEn, nPkt, t, tree);
      else writePhotonsSummary(fileID, photEn, t, tree);
    }
  
    // Write ionization data
    if (writeIonization_) writeIonization(fileID, nPkt, t, tree);
  
    // Flush to disk and close file
    H5Fflush(fileID, H5F_SCOPE_LOCAL);
    H5Fclose(fileID);
#ifdef ENABLE_MPI
  }
#endif

  // Call the user write function to do custom writing
  prob->userWrite(step, t, dt, chkNum, baseChkName, tree); 

  // Increment checkpoint counter
  chkNum++;

  // Print status
  if (verbosity > 2 && MPIUtil::IOProc)
    cout << "CheckpointManager: completed writing checkpoint "
	 << name << endl;    
}
  

void CheckpointManager::readCheckpoint(const std::string& filename,
				       int& step,
				       Real& t,
				       Real& dt,
				       CRTree& tree,
				       Prob* prob) {

  // Print status if verbose
  if (verbosity > 0 && MPIUtil::IOProc)
    cout << "CheckpointManager: reading checkpoint "
	 << filename << endl;

  // Error string if needed
  string errStr = "CheckpointManager::readCheckpoint: invalid restart file ";
  
  // Open file
  hid_t fileID = H5Fopen(filename.c_str(), H5F_ACC_RDONLY,
			 H5P_DEFAULT);
  if (fileID == H5I_INVALID_HID)
    MPIUtil::haltRun(errStr, errBadCheckpoint);

  // Open CR packet group
  hid_t CRPacket_grp;
  if ((CRPacket_grp = H5Gopen(fileID, "CRPacket", H5P_DEFAULT)) ==
      H5I_INVALID_HID)
    MPIUtil::haltRun(errStr, errBadCheckpoint);
    
  // Extract metadata for full simulation -- t, dt, step, uniqueID,
  // unit system
  {
    hid_t t_id = H5Aopen_by_name(CRPacket_grp, ".", "t",
				 H5P_DEFAULT, H5P_DEFAULT);
    if (t_id < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);
    if (H5Aread(t_id, H5T_NATIVE_REAL, &t) < 0)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    hid_t dt_id = H5Aopen_by_name(CRPacket_grp, ".", "dt",
				  H5P_DEFAULT, H5P_DEFAULT);
    if (dt_id < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);
    if (H5Aread(dt_id, H5T_NATIVE_REAL, &dt) < 0)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    hid_t step_id = H5Aopen_by_name(CRPacket_grp, ".", "step",
				    H5P_DEFAULT, H5P_DEFAULT);
    if (step_id < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);
    if (H5Aread(step_id, H5T_NATIVE_INT, &step) < 0)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    hid_t uid_id = H5Aopen_by_name(CRPacket_grp, ".", "uniqueID",
				   H5P_DEFAULT, H5P_DEFAULT);
    if (uid_id < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);
    IdxType uniqueID;
    if (H5Aread(uid_id, H5T_NATIVE_ULLONG, &uniqueID) < 0)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    tree.setUniqueID(uniqueID);
    hid_t units_id = H5Aopen_by_name(CRPacket_grp, ".", "units",
				     H5P_DEFAULT, H5P_DEFAULT);
    if (units_id < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);
    int units;
    if (H5Aread(units_id, H5T_NATIVE_INT, &units) < 0)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
#ifdef CRIPTIC_UNITS_MKS
    constexpr int unit_system = 1;
#else
    constexpr int unit_system = 0;
#endif
    if (units != unit_system)
      MPIUtil::haltRun("CheckpointManager::readCheckpoint: criptic "
		       "has been compiled in MKS mode, but the "
		       "checkpoint file appears to use CGS units",
		       errBadCheckpoint);
  }

  // Read CR packet position and property data
  {
    // Open the data sets
    hid_t CRPacket_pos_dataset = H5Dopen2(CRPacket_grp, "pos", H5P_DEFAULT);
    hid_t CRPacket_data_dataset = H5Dopen2(CRPacket_grp, "data", H5P_DEFAULT);
    if (CRPacket_pos_dataset == H5I_INVALID_HID ||
	CRPacket_data_dataset == H5I_INVALID_HID)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
      
    // Read the dimensionality of the data
    hid_t CRPacket_pos_dataspace = H5Dget_space(CRPacket_pos_dataset);
    hid_t CRPacket_data_dataspace = H5Dget_space(CRPacket_data_dataset);
    if (CRPacket_pos_dataspace == H5I_INVALID_HID ||
	CRPacket_data_dataspace == H5I_INVALID_HID)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    if (H5Sget_simple_extent_ndims(CRPacket_pos_dataspace) != 1 ||
	H5Sget_simple_extent_ndims(CRPacket_data_dataspace) != 1)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    hsize_t npos, ndata;
    H5Sget_simple_extent_dims(CRPacket_pos_dataspace, &npos, nullptr);
    H5Sget_simple_extent_dims(CRPacket_data_dataspace, &ndata, nullptr);
    if (npos != ndata) MPIUtil::haltRun(errStr, errBadCheckpoint);

    // Compute packet count and offset to each MPI rank
    vector<IdxType> nPkt(MPIUtil::nRank);
    vector<IdxType> nPktCum(MPIUtil::nRank);
    nPktCum[0] = 0;
    for (int i = 0; i < MPIUtil::nRank; i++) {
      nPkt[i] = npos / MPIUtil::nRank;
      if ((IdxType) i < npos - nPkt[i] * MPIUtil::nRank) nPkt[i]++;
      if (i > 0) nPktCum[i] = nPktCum[i-1] + nPkt[i];
    }

    // Allocate storage to hold packet data
    vector<RealVec>& x = tree.getPacketPos();
    vector<CRPacket>& pd = tree.getPacketData();
    x.resize(nPkt[MPIUtil::myRank]);
    pd.resize(nPkt[MPIUtil::myRank]);

    // Select the hyperslab within the data that we are going to read
    // on the MPI rank
    hsize_t count[1] = { nPkt[MPIUtil::myRank] };
    hsize_t offset[1] = { nPktCum[MPIUtil::myRank] };
    H5Sselect_hyperslab(CRPacket_pos_dataspace, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    H5Sselect_hyperslab(CRPacket_data_dataspace, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Make memory space descriptor for data to be read
    hid_t memspace = H5Screate_simple(1, count, nullptr);

    // Read packet data
    herr_t stat = H5Dread(CRPacket_pos_dataset, RealVec_HDF5, memspace,
			  CRPacket_pos_dataspace, H5P_DEFAULT,
			  x.data());
    if (stat < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);
    stat = H5Dread(CRPacket_data_dataset, CRPacket_HDF5, memspace,
		   CRPacket_data_dataspace, H5P_DEFAULT,
		   pd.data());
    if (stat < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);

    // Free resources
    H5Sclose(CRPacket_pos_dataspace);
    H5Sclose(CRPacket_data_dataspace);
    H5Sclose(memspace);
    H5Dclose(CRPacket_pos_dataset);
    H5Dclose(CRPacket_data_dataset);
    H5Gclose(CRPacket_grp);
  }
    
  // Read CR source data if present
  if (H5Lexists(fileID, "CRSource", H5P_DEFAULT) > 0) {

    // Open group
    hid_t CRSource_grp;
    if ((CRSource_grp = H5Gopen(fileID, "CRSource", H5P_DEFAULT)) ==
	H5I_INVALID_HID)
      MPIUtil::haltRun(errStr, errBadCheckpoint);

    // Open data set
    hid_t CRSource_pos_dataset = H5Dopen2(CRSource_grp, "pos", H5P_DEFAULT);
    hid_t CRSource_data_dataset = H5Dopen2(CRSource_grp, "data", H5P_DEFAULT);
    if (CRSource_pos_dataset == H5I_INVALID_HID ||
	CRSource_data_dataset == H5I_INVALID_HID)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
      
    // Read number of sources present
    hid_t CRSource_pos_dataspace = H5Dget_space(CRSource_pos_dataset);
    hid_t CRSource_data_dataspace = H5Dget_space(CRSource_data_dataset);
    if (CRSource_pos_dataspace == H5I_INVALID_HID ||
	CRSource_data_dataspace == H5I_INVALID_HID)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    if (H5Sget_simple_extent_ndims(CRSource_pos_dataspace) != 1 ||
	H5Sget_simple_extent_ndims(CRSource_data_dataspace) != 1)
      MPIUtil::haltRun(errStr, errBadCheckpoint);
    hsize_t npos, ndata;
    H5Sget_simple_extent_dims(CRSource_pos_dataspace, &npos, nullptr);
    H5Sget_simple_extent_dims(CRSource_data_dataspace, &ndata, nullptr);
    if (npos != ndata) MPIUtil::haltRun(errStr, errBadCheckpoint);

    // Compute source count and offset to each MPI rank
    vector<IdxType> nSrc(MPIUtil::nRank);
    vector<IdxType> nSrcCum(MPIUtil::nRank);
    nSrcCum[0] = 0;
    for (int i = 0; i < MPIUtil::nRank; i++) {
      nSrc[i] = npos / MPIUtil::nRank;
      if ((IdxType) i < npos - nSrc[i] * MPIUtil::nRank) nSrc[i]++;
      if (i > 0) nSrcCum[i] = nSrcCum[i-1] + nSrc[i];
    }

    // Allocate storage to hold source data
    vector<RealVec>& xSrc = tree.getSourcePos();
    vector<CRSource>& src = tree.getSourceData();
    xSrc.resize(nSrc[MPIUtil::myRank]);
    src.resize(nSrc[MPIUtil::myRank]);

    // Select the hyperslab within the data that we are going to read
    // on the MPI rank
    hsize_t count[1] = { nSrc[MPIUtil::myRank] };
    hsize_t offset[1] = { nSrcCum[MPIUtil::myRank] };
    H5Sselect_hyperslab(CRSource_pos_dataspace, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    H5Sselect_hyperslab(CRSource_data_dataspace, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Make memory space descriptor for data to be read
    hid_t memspace = H5Screate_simple(1, count, nullptr);

    // Read data
    herr_t stat = H5Dread(CRSource_pos_dataset, RealVec_HDF5, memspace,
			  CRSource_pos_dataspace, H5P_DEFAULT,
			  xSrc.data());
    if (stat < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);
    stat = H5Dread(CRSource_data_dataset, CRSource_HDF5, memspace,
		   CRSource_data_dataspace, H5P_DEFAULT,
		   src.data());
    if (stat < 0) MPIUtil::haltRun(errStr, errBadCheckpoint);

    // Free HDF5 resources
    H5Sclose(CRSource_pos_dataspace);
    H5Sclose(CRSource_data_dataspace);
    H5Sclose(memspace);
    H5Dclose(CRSource_pos_dataset);
    H5Dclose(CRSource_data_dataset);
    H5Gclose(CRSource_grp);      
  }

  // Close file
  H5Fclose(fileID);
  
  // Set internal counters to match this checkpoint file
  lastChkStep = step;
  lastChkTime = t;
  chkNum = stoi(filename.substr(filename.length() - CHKDIGITS - 4)) + 1;

  // Compute time and step number when next checkpoint should be written
  if (chkInt > 0)
    nextChkStep = chkInt * (step / chkInt + 1);
  if (chkDt > 0.0)
    nextChkTime = chkDt * (round(t / chkDt) + 1.0);

  // Call the user function
  prob->userRead(filename, step, t, dt, tree);
}


void CheckpointManager::syncCounts(vector<IdxType>& nPkt,
				   vector<IdxType>& nSrc,
				   vector<IdxType>& nDel,
				   const CRTree& tree) const {

#ifdef ENABLE_MPI
  // MPI version
  
  // Fill send buffer
  vector<IdxType> sendBuf(3);
  sendBuf[0] = tree.getPacketPos().size();
  sendBuf[1] = tree.getSourcePos().size();
  sendBuf[2] = tree.getDeletedPacketPos().size();

  // Actions on different ranks
  if (MPIUtil::IOProc) {

    // Allocate receive buffer on IO rank
    vector<IdxType> recvBuf(3 * MPIUtil::nRank);

    // Do MPI exchange
    MPI_Gather(sendBuf.data(), 3, MPIUtil::MPI_Idx,
	       recvBuf.data(), 3, MPIUtil::MPI_Idx,
	       MPIUtil::IORank, MPI_COMM_WORLD);

    // Unpack data into output arrays
    nPkt.resize(MPIUtil::nRank);
    nSrc.resize(MPIUtil::nRank);
    nDel.resize(MPIUtil::nRank);
    for (int i = 0; i < MPIUtil::nRank; i++) {
      nPkt[i] = recvBuf[3*i];
      nSrc[i] = recvBuf[3*i+1];
      nDel[i] = recvBuf[3*i+2];
    }

  } else {

    // Do MPI exchange on all ranks except the IO one
    MPI_Gather(sendBuf.data(), 3, MPIUtil::MPI_Idx,
	       nullptr, 0, MPIUtil::MPI_Idx,
	       MPIUtil::IORank, MPI_COMM_WORLD);
    
  }
  
#else

  // Non-MPI version is trivial
  nPkt.resize(1);
  nSrc.resize(1);
  nDel.resize(1);
  nPkt[0] = tree.nPacket();
  nSrc[0] = tree.nSource();
  nDel[0] = tree.nDeletedPacket();

#endif
  
}


#ifdef ENABLE_MPI
void CheckpointManager::sendCheckpointData(const Real& t,
					   const CRTree& tree) const {

  // Convenience aliases
  IdxType nPkt = tree.getPacketPos().size();
  IdxType nSrc = tree.getSourcePos().size();
  IdxType nDel = tree.getDeletedPacketPos().size();

  // Send packets
  if (nPkt > 0) {
    MPI_Send(tree.getPacketPos().data(), nPkt, MPIUtil::MPI_RealVec,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
    MPI_Send(tree.getPacketData().data(), nPkt, MPIUtil::MPI_CRPacket,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
  }

  // Send sources
  if (nSrc > 0) {
    MPI_Send(tree.getSourcePos().data(), nSrc, MPIUtil::MPI_RealVec,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
    MPI_Send(tree.getSourceData().data(), nSrc, MPIUtil::MPI_CRSource,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
  }
  
  // Send deleted packets
  if (nDel > 0 && writeDelPackets_) {
    MPI_Send(tree.getDeletedPacketPos().data(), nDel, MPIUtil::MPI_RealVec,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
    MPI_Send(tree.getDeletedPacketData().data(), nDel, MPIUtil::MPI_CRPacket,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
    MPI_Send(tree.getDeletedPacketTime().data(), nDel, MPIUtil::MPI_Real,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
  }

  // Send field quantities
  if (nPkt > 0 && writeFieldQty_) {
    MPI_Send(tree.getPacketQty().data(), nPkt, MPIUtil::MPI_FieldQty,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
  }

  // Send gradients of field quantities
  if (nPkt > 0 && needFQG && writeFieldQty_) {
    MPI_Send(tree.getPacketQtyGrad().data(), nPkt, MPIUtil::MPI_FieldQtyGrad,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
  }

  // Send loss rates; note that these have to be computed first
  if (nPkt > 0 && writeLoss_) {
    vector<Losses::MechArr<Real> > lossRate, dpdt;
    tree.getLossRatesMech(t, dpdt, lossRate);
    MPI_Send(lossRate.data(), nPkt, MPIUtil::MPI_RealMechArr,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
    MPI_Send(dpdt.data(), nPkt, MPIUtil::MPI_RealMechArr,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
  }

  // Handle photons; this has two cases, one for writing them out
  // packet-by-packet and one for writing a summary
  if (writePhotons_) {
    if (writePhotonsFull_) {
      // Packet-by-packet case
      if (nPkt > 0) {
	vector<Real> dLdE = tree.photLum(t, photEn);
	MPI_Send(dLdE.data(), dLdE.size(), MPIUtil::MPI_Real,
		 IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
      }
    } else {
      // Summary case; note a subtlety here is that we compute the
      // luminosity and do an MPI_Reduce even if we have no local
      // packets, as long as ANY rank has packets, because we need to
      // reduce across all ranks
      if (tree.nPacket() > 0) {
	vector<Real> dLdE = tree.photLumSummary(t, photEn);
	MPI_Reduce(dLdE.data(), nullptr, dLdE.size(), MPIUtil::MPI_Real,
		   MPI_SUM, IORank, MPI_COMM_WORLD);
      }
    }
  }

  // Compute and send ionization rate
  if (nPkt > 0 && writeIonization_) {
    vector<Real> ionRate = tree.ionRate(t);
    MPI_Send(ionRate.data(), ionRate.size(), MPIUtil::MPI_Real,
	     IORank, MPIUtil::tagHDF5IO, MPI_COMM_WORLD);
  }
}
#endif


void CheckpointManager::writePackets(hid_t& fileID,
				     const vector<IdxType>& nPkt,
				     const int& step,
				     const Real& t,
				     const Real& dt,
				     const CRTree& tree) const {

  // Create CR packet group
  hid_t CRPacket_grp = H5Gcreate(fileID, "CRPacket", H5P_DEFAULT,
				 H5P_DEFAULT, H5P_DEFAULT);
  
  // Metadata for the simulation as a whole is attached to this group
  {
    hsize_t dims = 1;
    hid_t dataspace_id = H5Screate_simple(1, &dims, nullptr);
    {
      // time
      hid_t t_id = H5Acreate(CRPacket_grp, "t", H5T_NATIVE_REAL,
			     dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(t_id, H5T_NATIVE_REAL, &t);
      H5Aclose(t_id);
    }
    {
      // time step
      hid_t dt_id = H5Acreate(CRPacket_grp, "dt", H5T_NATIVE_REAL,
			      dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(dt_id, H5T_NATIVE_REAL, &dt);
      H5Aclose(dt_id);
    }
    {
      // step number
      hid_t step_id = H5Acreate(CRPacket_grp, "step", H5T_NATIVE_INT,
				dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(step_id, H5T_NATIVE_INT, &step);
      H5Aclose(step_id);
    }
    {
      // unique ID counter
      hid_t uid_id = H5Acreate(CRPacket_grp, "uniqueID", H5T_NATIVE_ULLONG,
			       dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
      IdxType uid = tree.getUniqueID();
      H5Awrite(uid_id, H5T_NATIVE_ULLONG, &uid);
      H5Aclose(uid_id);
    }
    {
      // unit system
      hid_t units_id = H5Acreate(CRPacket_grp, "units", H5T_NATIVE_INT,
				 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
#ifdef CRIPTIC_UNITS_MKS
      int units = 1;
#else
      int units = 0;
#endif
      H5Awrite(units_id, H5T_NATIVE_INT, &units);
      H5Aclose(units_id);
    }
    {
      // Names of particle types
      dims = partTypes::nPartType;
      hid_t partName_dataspace_id = H5Screate_simple(1, &dims, nullptr);
      hid_t strtype = H5Tcopy(H5T_C_S1);
      IdxType partNameLen = 0;
      for (auto it = partTypes::names.begin();
	   it != partTypes::names.end();
	   ++it)
	partNameLen = partNameLen < (*it).size() ?
				    (*it).size() : partNameLen;
      H5Tset_size(strtype, partNameLen);
      hid_t partName_id = H5Acreate(CRPacket_grp, "ParticleNames",
				    strtype, partName_dataspace_id,
				    H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(partName_id, strtype, partTypes::names.data());
      H5Aclose(partName_id);
      H5Sclose(partName_dataspace_id);
    }
    if (writeLoss_ || writePhotons_) {
      // Names of loss mechanisms
      dims = lossMech::nMech;
      hid_t mechName_dataspace_id = H5Screate_simple(1, &dims, nullptr);
      hid_t strtype = H5Tcopy(H5T_C_S1);
      H5Tset_size(strtype, lossMech::mechNameLen);
      hid_t mechName_id = H5Acreate(CRPacket_grp, "LossMechanisms", strtype,
				    mechName_dataspace_id, H5P_DEFAULT,
				    H5P_DEFAULT);
      H5Awrite(mechName_id, strtype, lossMech::names.data());
      H5Aclose(mechName_id);
      H5Sclose(mechName_dataspace_id);
    }
    H5Sclose(dataspace_id);
#ifdef ENABLE_MPI
    {
      // Counts of packets on each MPI rank
      dims = MPIUtil::nRank;
      hid_t nPkt_dataspace_id = H5Screate_simple(1, &dims, nullptr);
      hid_t nPkt_id = H5Acreate(CRPacket_grp, "MPICounts",
				H5T_NATIVE_ULLONG, nPkt_dataspace_id,
				H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(nPkt_id, H5T_NATIVE_ULLONG, nPkt.data());
      H5Aclose(nPkt_id);
      H5Sclose(nPkt_dataspace_id);
    }
#endif
  }

  // Create data space for CR packets
  hsize_t CRPacket_dims = tree.nPacket();
  hid_t CRPacket_dataspace = H5Screate_simple(1, &CRPacket_dims,
					      nullptr);
    
  // Create data sets corresponding to CR packet positions and data
  hid_t CRPacket_pos_dataset = H5Dcreate2(fileID,
					  "/CRPacket/pos",
					  RealVec_HDF5,
					  CRPacket_dataspace,
					  H5P_DEFAULT,
					  H5P_DEFAULT,
					  H5P_DEFAULT);
  hid_t CRPacket_data_dataset = H5Dcreate2(fileID,
					   "/CRPacket/data",
					   CRPacket_HDF5,
					   CRPacket_dataspace,
					   H5P_DEFAULT,
					   H5P_DEFAULT,
					   H5P_DEFAULT);
  hid_t CRPacket_pos_dataspace_id = H5Dget_space(CRPacket_pos_dataset);
  hid_t CRPacket_data_dataspace_id = H5Dget_space(CRPacket_data_dataset);

  // Loop over MPI ranks
  IdxType pktPtr = 0;
  for (int i = 0; i < MPIUtil::nRank; i++) {

    // Skip if no packets on this rank
    if (nPkt[i] == 0) continue;

    // Grab the correct hyperslab into which to write
    hsize_t count[1] = { nPkt[i] };
    hsize_t offset[1] = { pktPtr };
    H5Sselect_hyperslab(CRPacket_pos_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    H5Sselect_hyperslab(CRPacket_data_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Create the memory data space to specify the layout of the data
    // in memory
    hid_t memspace = H5Screate_simple(1, count, nullptr);

    // Action depends on whether we own these data
    if (i == MPIUtil::myRank) {

      // We own these data, so write directly
      H5Dwrite(CRPacket_pos_dataset, RealVec_HDF5, memspace,
	       CRPacket_pos_dataspace_id, H5P_DEFAULT,
	       tree.getPacketPos().data());
      H5Dwrite(CRPacket_data_dataset, CRPacket_HDF5, memspace,
	       CRPacket_data_dataspace_id, H5P_DEFAULT,
	       tree.getPacketData().data());
    }
#ifdef ENABLE_MPI
    else {

      // Data must be imported from a remote rank      
      MPI_Status stat;
      vector<RealVec> pktPos(nPkt[i]);
      vector<CRPacket> pktData(nPkt[i]);
      MPI_Recv(pktPos.data(), nPkt[i], MPIUtil::MPI_RealVec, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);
      MPI_Recv(pktData.data(), nPkt[i], MPIUtil::MPI_CRPacket, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);

      // Write imported data
      H5Dwrite(CRPacket_pos_dataset, RealVec_HDF5, memspace,
	       CRPacket_pos_dataspace_id, H5P_DEFAULT,
	       pktPos.data());
      H5Dwrite(CRPacket_data_dataset, CRPacket_HDF5, memspace,
	       CRPacket_data_dataspace_id, H5P_DEFAULT,
	       pktData.data());

    }
#endif

    // Free memory space specifier
    H5Sclose(memspace);
    
    // Increment counter
    pktPtr += nPkt[i];
  }

  // Free all HDF5 infrastructure
  H5Sclose(CRPacket_pos_dataspace_id);
  H5Sclose(CRPacket_data_dataspace_id);
  H5Sclose(CRPacket_dataspace);
  H5Dclose(CRPacket_pos_dataset);
  H5Dclose(CRPacket_data_dataset);
  H5Gclose(CRPacket_grp);
}


void CheckpointManager::writeSources(hid_t& fileID,
				     const vector<IdxType>& nSrc,
				     const CRTree& tree) const {

  // If no sources present, do nothing
  if (tree.nSource() == 0) return;

  // Create group
  hid_t CRSource_grp = H5Gcreate(fileID, "CRSource", H5P_DEFAULT,
				 H5P_DEFAULT, H5P_DEFAULT);

  // Write an attribute giving the source counts by MPI rank
#ifdef ENABLE_MPI
  {
    hsize_t dims = MPIUtil::nRank;
    hid_t nSrc_dataspace_id = H5Screate_simple(1, &dims, nullptr);
    hid_t nSrc_id = H5Acreate(CRSource_grp, "MPICounts",
			      H5T_NATIVE_ULLONG, nSrc_dataspace_id,
			      H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(nSrc_id, H5T_NATIVE_ULLONG, nSrc.data());
    H5Aclose(nSrc_id);
    H5Sclose(nSrc_dataspace_id);
  }
#endif

  // Create data spaces for CR sources
  hsize_t CRSource_dims = tree.nSource();
  hid_t CRSource_dataspace = H5Screate_simple(1, &CRSource_dims,
					      nullptr);

  // Create data sets for CR sources
  hid_t CRSource_pos_dataset = H5Dcreate2(fileID,
					  "/CRSource/pos",
					  RealVec_HDF5,
					  CRSource_dataspace,
					  H5P_DEFAULT,
					  H5P_DEFAULT,
					  H5P_DEFAULT);
  hid_t CRSource_data_dataset = H5Dcreate2(fileID,
					   "/CRSource/data",
					   CRSource_HDF5,
					   CRSource_dataspace,
					   H5P_DEFAULT,
					   H5P_DEFAULT,
					   H5P_DEFAULT);
  hid_t CRSource_pos_dataspace_id = H5Dget_space(CRSource_pos_dataset);
  hid_t CRSource_data_dataspace_id = H5Dget_space(CRSource_data_dataset);

  // Loop over MPI ranks
  IdxType srcPtr = 0;
  for (int i = 0; i < MPIUtil::nRank; i++) {
    
    // Skip if no sources on this rank
    if (nSrc[i] == 0) continue;
    
    // Grab the correct hyperslab into which to write
    hsize_t count[1] = { nSrc[i] };
    hsize_t offset[1] = { srcPtr };
    H5Sselect_hyperslab(CRSource_pos_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    H5Sselect_hyperslab(CRSource_data_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Create the memory data space to specify the layout of the data
    // in memory
    hid_t memspace = H5Screate_simple(1, count, nullptr);
    
    // Action depends on whether we own these data
    if (i == MPIUtil::myRank) {

      // We own these data, so write directly
      H5Dwrite(CRSource_pos_dataset, RealVec_HDF5, memspace,
	       CRSource_pos_dataspace_id, H5P_DEFAULT,
	       tree.getSourcePos().data());
      H5Dwrite(CRSource_data_dataset, CRSource_HDF5, memspace,
	       CRSource_data_dataspace_id, H5P_DEFAULT,
	       tree.getSourceData().data());
    }
    
#ifdef ENABLE_MPI
    else {

      // Data must be imported from a remote rank
      MPI_Status stat;
      vector<RealVec> srcPos(nSrc[i]);
      vector<CRSource> srcData(nSrc[i]);
      MPI_Recv(srcPos.data(), nSrc[i], MPIUtil::MPI_RealVec, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);
      MPI_Recv(srcData.data(), nSrc[i], MPIUtil::MPI_CRSource, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);

      // Write imported data
      H5Dwrite(CRSource_pos_dataset, RealVec_HDF5, memspace,
	       CRSource_pos_dataspace_id, H5P_DEFAULT,
	       srcPos.data());
      H5Dwrite(CRSource_data_dataset, CRSource_HDF5, memspace,
	       CRSource_data_dataspace_id, H5P_DEFAULT,
	       srcData.data());
    }
#endif

    // Free memory space specifier
    H5Sclose(memspace);
    
    // Increment counter
    srcPtr += nSrc[i];
  }

  // Free all HDF5 infrastructure
  H5Sclose(CRSource_pos_dataspace_id);
  H5Sclose(CRSource_data_dataspace_id);
  H5Sclose(CRSource_dataspace);
  H5Dclose(CRSource_pos_dataset);
  H5Dclose(CRSource_data_dataset);
  H5Gclose(CRSource_grp);
}


void CheckpointManager::writeDelPackets(hid_t& fileID,
					const vector<IdxType>& nDel,
					const CRTree& tree) const {

  // If no deleted packets exist, do nothing
  if (tree.nDeletedPacket() == 0) return;

  // Create group
  hid_t CRPacketDel_grp = H5Gcreate(fileID, "CRPacketDel",
				    H5P_DEFAULT,
				    H5P_DEFAULT,
				    H5P_DEFAULT);

  // Create dataspace
  hsize_t CRPacketDel_dims = tree.nDeletedPacket();
  hid_t CRPacketDel_dataspace = H5Screate_simple(1, &CRPacketDel_dims,
						 nullptr);

  // Create data sets
  hid_t CRPacketDel_pos_dataset = H5Dcreate2(fileID,
					     "/CRPacketDel/pos",
					     RealVec_HDF5,
					     CRPacketDel_dataspace,
					     H5P_DEFAULT,
					     H5P_DEFAULT,
					     H5P_DEFAULT);
  hid_t CRPacketDel_data_dataset = H5Dcreate2(fileID,
					      "/CRPacketDel/data",
					      CRPacket_HDF5,
					      CRPacketDel_dataspace,
					      H5P_DEFAULT,
					      H5P_DEFAULT,
					      H5P_DEFAULT);
  hid_t CRPacketDel_tdel_dataset = H5Dcreate2(fileID,
					      "/CRPacketDel/tdel",
					      H5T_NATIVE_REAL,
					      CRPacketDel_dataspace,
					      H5P_DEFAULT,
					      H5P_DEFAULT,
					      H5P_DEFAULT);
  hid_t CRPacketDel_pos_dataspace_id = H5Dget_space(CRPacketDel_pos_dataset);
  hid_t CRPacketDel_data_dataspace_id = H5Dget_space(CRPacketDel_data_dataset);
  hid_t CRPacketDel_tdel_dataspace_id = H5Dget_space(CRPacketDel_tdel_dataset);

  // Loop over MPI ranks
  IdxType delPtr = 0;
  for (int i = 0; i < MPIUtil::nRank; i++) {
    
    // Skip if no sources on this rank
    if (nDel[i] == 0) continue;
    
    // Grab the correct hyperslab into which to write
    hsize_t count[1] = { nDel[i] };
    hsize_t offset[1] = { delPtr };
    H5Sselect_hyperslab(CRPacketDel_pos_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    H5Sselect_hyperslab(CRPacketDel_data_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    H5Sselect_hyperslab(CRPacketDel_tdel_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Create the memory data space to specify the layout of the data
    // in memory
    hid_t memspace = H5Screate_simple(1, count, nullptr);
    
    // Action depends on whether we own these data
    if (i == MPIUtil::myRank) {

      // We own these data, so write directly
      H5Dwrite(CRPacketDel_pos_dataset, RealVec_HDF5, memspace,
	       CRPacketDel_pos_dataspace_id, H5P_DEFAULT,
	       tree.getDeletedPacketPos().data());
      H5Dwrite(CRPacketDel_data_dataset, CRPacket_HDF5, memspace,
	       CRPacketDel_data_dataspace_id, H5P_DEFAULT,
	       tree.getDeletedPacketData().data());
      H5Dwrite(CRPacketDel_tdel_dataset, H5T_NATIVE_REAL, memspace,
	       CRPacketDel_data_dataspace_id, H5P_DEFAULT,
	       tree.getDeletedPacketTime().data());
    }
    
#ifdef ENABLE_MPI
    else {

      // Data must be imported from a remote rank      
      MPI_Status stat;
      vector<RealVec> delPos(nDel[i]);
      vector<CRSource> delData(nDel[i]);
      vector<Real> delTime(nDel[i]);
      MPI_Recv(delPos.data(), nDel[i], MPIUtil::MPI_RealVec, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);
      MPI_Recv(delData.data(), nDel[i], MPIUtil::MPI_CRPacket, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);
      MPI_Recv(delTime.data(), nDel[i], MPIUtil::MPI_Real, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);

      // Write imported data
      H5Dwrite(CRPacketDel_pos_dataset, RealVec_HDF5, memspace,
	       CRPacketDel_pos_dataspace_id, H5P_DEFAULT,
	       delPos.data());
      H5Dwrite(CRPacketDel_data_dataset, CRPacket_HDF5, memspace,
	       CRPacketDel_data_dataspace_id, H5P_DEFAULT,
	       delData.data());
      H5Dwrite(CRPacketDel_tdel_dataset, H5T_NATIVE_REAL, memspace,
	       CRPacketDel_data_dataspace_id, H5P_DEFAULT,
	       delTime.data());
    }
#endif

    // Free memory space specifier
    H5Sclose(memspace);
    
    // Increment counter
    delPtr += nDel[i];
  }

  // Free all HDF5 infrastructure
  H5Sclose(CRPacketDel_pos_dataspace_id);
  H5Sclose(CRPacketDel_data_dataspace_id);
  H5Sclose(CRPacketDel_tdel_dataspace_id);
  H5Sclose(CRPacketDel_dataspace);
  H5Dclose(CRPacketDel_pos_dataset);
  H5Dclose(CRPacketDel_data_dataset);
  H5Gclose(CRPacketDel_grp);  
}


void CheckpointManager::writeFieldQty(hid_t& fileID,
				      const vector<IdxType>& nPkt,
				      const CRTree& tree) const {

  // If no packets, do nothing
  if (tree.nPacket() == 0) return;

  // Create group
  hid_t FieldQty_grp = H5Gcreate(fileID, "FieldQty", H5P_DEFAULT,
				 H5P_DEFAULT, H5P_DEFAULT);

  // Write an attribute of the group containing the names of the field
  // quantities
  {
    hsize_t dims = nFieldQty;
    hid_t dataspace_id = H5Screate_simple(1, &dims, nullptr);
    hid_t strtype = H5Tcopy(H5T_C_S1);
    H5Tset_size(strtype, FieldQty::qtyNameLen);
    hid_t qtyName_id = H5Acreate(FieldQty_grp, "names", strtype,
				 dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(qtyName_id, strtype, FieldQty::qtyNames.data());
    H5Aclose(qtyName_id);
    H5Sclose(dataspace_id);
  }
    
  // Create dataspace
  hsize_t CRPacket_dims = tree.nPacket();
  hid_t FieldQty_dataspace = H5Screate_simple(1, &CRPacket_dims,
					      nullptr);

  // Create data sets
  hid_t FieldQty_dataset = H5Dcreate2(fileID,
				      "/FieldQty/qty",
				      FieldQty_HDF5,
				      FieldQty_dataspace,
				      H5P_DEFAULT,
				      H5P_DEFAULT,
				      H5P_DEFAULT);
  hid_t FieldQty_dataspace_id = H5Dget_space(FieldQty_dataset);
  hid_t FieldQtyGrad_dataset = 0;
  hid_t FieldQtyGrad_dataspace_id = 0;
  if (needFQG) {
    FieldQtyGrad_dataset = H5Dcreate2(fileID,
				      "/FieldQty/grad",
				      FieldQtyGrad_HDF5,
				      FieldQty_dataspace,
				      H5P_DEFAULT,
				      H5P_DEFAULT,
				      H5P_DEFAULT);
    FieldQtyGrad_dataspace_id = H5Dget_space(FieldQtyGrad_dataset);
  }

  // Loop over MPI ranks
  IdxType pktPtr = 0;
  for (int i = 0; i < MPIUtil::nRank; i++) {

    // Skip if no packets on this rank
    if (nPkt[i] == 0) continue;

    // Grab the correct hyperslab into which to write
    hsize_t count[1] = { nPkt[i] };
    hsize_t offset[1] = { pktPtr };
    H5Sselect_hyperslab(FieldQty_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    if (needFQG)
      H5Sselect_hyperslab(FieldQtyGrad_dataspace_id, H5S_SELECT_SET,
			  offset, nullptr, count, nullptr);

    // Create the memory data space to specify the layout of the data
    // in memory
    hid_t memspace = H5Screate_simple(1, count, nullptr);
    
    // Action depends on whether we own these data
    if (i == MPIUtil::myRank) {

      // We own these data, so write directly
      H5Dwrite(FieldQty_dataset, FieldQty_HDF5, memspace,
	       FieldQty_dataspace_id, H5P_DEFAULT,
	       tree.getPacketQty().data());
      if (needFQG)
	H5Dwrite(FieldQtyGrad_dataset, FieldQtyGrad_HDF5, memspace,
		 FieldQtyGrad_dataspace_id, H5P_DEFAULT,
		 tree.getPacketQtyGrad().data());
    }
#ifdef ENABLE_MPI
    else {

      // Data must be imported from a remote rank      
      MPI_Status stat;
      vector<FieldQty> fld(nPkt[i]);
      vector<FieldQtyGrad> fldGrad;
      MPI_Recv(fld.data(), nPkt[i], MPIUtil::MPI_FieldQty, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);
      if (needFQG) {
	fldGrad.resize(nPkt[i]);
	MPI_Recv(fldGrad.data(), nPkt[i], MPIUtil::MPI_FieldQtyGrad, i,
		 MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);
      }

      // Write imported data
      H5Dwrite(FieldQty_dataset, FieldQty_HDF5, memspace,
	       FieldQty_dataspace_id, H5P_DEFAULT,
	       fld.data());
      if (needFQG)
	H5Dwrite(FieldQtyGrad_dataset, FieldQtyGrad_HDF5, memspace,
		 FieldQtyGrad_dataspace_id, H5P_DEFAULT,
		 fldGrad.data());
    }
#endif

    // Free memory space specifier
    H5Sclose(memspace);
    
    // Increment counter
    pktPtr += nPkt[i];
  }

  // Free all HDF5 infrastructure
  H5Sclose(FieldQty_dataspace_id);
  if (needFQG) H5Sclose(FieldQtyGrad_dataspace_id);
  H5Sclose(FieldQty_dataspace);
  H5Dclose(FieldQty_dataset);
  if (needFQG) H5Dclose(FieldQtyGrad_dataset);
  H5Gclose(FieldQty_grp);
}


void CheckpointManager::writeLoss(hid_t& fileID,
				  const vector<IdxType>& nPkt,
				  const Real& t,
				  const CRTree& tree) const {

  // If no packets, do nothing
  if (tree.nPacket() == 0) return;
  
  // Create group
  hid_t Loss_grp = H5Gcreate(fileID, "Losses", H5P_DEFAULT,
			     H5P_DEFAULT, H5P_DEFAULT);

  // Create data space
  hsize_t dims[2] = { tree.nPacket(), lossMech::nMech };
  hid_t Loss_dataspace = H5Screate_simple(2, dims, nullptr);

  // Create data sets
  hid_t Loss_lossRate_dataset = H5Dcreate2(fileID,
					   "/Losses/lossRate",
					   H5T_NATIVE_REAL,
					   Loss_dataspace,
					   H5P_DEFAULT,
					   H5P_DEFAULT,
					   H5P_DEFAULT);
  hid_t Loss_dpdt_dataset = H5Dcreate2(fileID,
				       "/Losses/dpdt",
				       H5T_NATIVE_REAL,
				       Loss_dataspace,
				       H5P_DEFAULT,
				       H5P_DEFAULT,
				       H5P_DEFAULT);
  hid_t Loss_lossRate_dataspace_id = H5Dget_space(Loss_lossRate_dataset);
  hid_t Loss_dpdt_dataspace_id = H5Dget_space(Loss_dpdt_dataset);

  // Loop over MPI ranks
  IdxType pktPtr = 0;
  for (int i = 0; i < MPIUtil::nRank; i++) {

    // Skip if no packets on this rank
    if (nPkt[i] == 0) continue;

    // Grab the correct hyperslab into which to write
    hsize_t count[2] = { nPkt[i], lossMech::nMech };
    hsize_t offset[2] = { pktPtr, 0 };
    H5Sselect_hyperslab(Loss_lossRate_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);
    H5Sselect_hyperslab(Loss_dpdt_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Create the memory data space to specify the layout of the data
    // in memory
    hid_t memspace = H5Screate_simple(2, count, nullptr);
    
    // Action depends on whether we own these data
    if (i == MPIUtil::myRank) {

      // We own these data, so calculate loss rate and write to file
      vector<Losses::MechArr<Real> > lossRate;
      vector<Losses::MechArr<Real> > dpdt;
      tree.getLossRatesMech(t, dpdt, lossRate);
      H5Dwrite(Loss_lossRate_dataset, H5T_NATIVE_REAL, memspace,
	       Loss_lossRate_dataspace_id, H5P_DEFAULT,
	       lossRate.data());
      H5Dwrite(Loss_dpdt_dataset, H5T_NATIVE_REAL, memspace,
	       Loss_dpdt_dataspace_id, H5P_DEFAULT,
	       dpdt.data());
    }
#ifdef ENABLE_MPI
    else {

      // Data must be imported from a remote rank      
      MPI_Status stat;
      vector<Losses::MechArr<Real> > lossRate(nPkt[i]);
      vector<Losses::MechArr<Real> > dpdt(nPkt[i]);
      MPI_Recv(lossRate.data(), nPkt[i], MPIUtil::MPI_RealMechArr, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);
      MPI_Recv(dpdt.data(), nPkt[i], MPIUtil::MPI_RealMechArr, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);

      // Write imported data
      H5Dwrite(Loss_lossRate_dataset, H5T_NATIVE_REAL, memspace,
	       Loss_lossRate_dataspace_id, H5P_DEFAULT,
	       lossRate.data());
      H5Dwrite(Loss_dpdt_dataset, H5T_NATIVE_REAL, memspace,
	       Loss_dpdt_dataspace_id, H5P_DEFAULT,
	       dpdt.data());
    }
#endif

    // Free memory space specifier
    H5Sclose(memspace);
    
    // Increment counter
    pktPtr += nPkt[i];
  }

  // Free all HDF5 infrastructure
  H5Sclose(Loss_lossRate_dataspace_id);
  H5Sclose(Loss_dpdt_dataspace_id);
  H5Sclose(Loss_dataspace);
  H5Dclose(Loss_lossRate_dataset);
  H5Dclose(Loss_dpdt_dataset);
  H5Gclose(Loss_grp);
}


void CheckpointManager::writePhotonsFull(hid_t& fileID,
					 const vector<Real>& photEn,
					 const vector<IdxType>& nPkt,
					 const Real& t,
					 const CRTree& tree) const {

  // If no packets, do nothing
  if (tree.nPacket() == 0) return;

  // Create photon group
  hid_t Photon_grp = H5Gcreate(fileID, "PhotonEmission", H5P_DEFAULT,
			       H5P_DEFAULT, H5P_DEFAULT);

  // Add an attribute to store the set of photon energies at which the
  // spectrum is being computed
  {
    hsize_t dims = photEn.size();
    hid_t dataspace_id = H5Screate_simple(1, &dims, nullptr);
    hid_t photEn_id = H5Acreate(Photon_grp, "photEn", H5T_NATIVE_REAL,
				dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(photEn_id, H5T_NATIVE_REAL, photEn.data());
    H5Aclose(photEn_id);
    H5Sclose(dataspace_id);
  }

  // Create data space
  hsize_t dims[3] = { tree.nPacket(), lossMech::nMech, photEn.size() };
  hid_t Photon_dataspace = H5Screate_simple(3, dims, nullptr);

  // Create data set
  hid_t Photon_dLdE_dataset = H5Dcreate2(fileID,
					 "/PhotonEmission/dLdE",
					 H5T_NATIVE_REAL,
					 Photon_dataspace,
					 H5P_DEFAULT,
					 H5P_DEFAULT,
					 H5P_DEFAULT);
  hid_t Photon_dLdE_dataspace_id = H5Dget_space(Photon_dLdE_dataset);

  // Loop over MPI ranks
  IdxType pktPtr = 0;
  for (int i = 0; i < MPIUtil::nRank; i++) {

    // Skip if no packets on this rank
    if (nPkt[i] == 0) continue;

    // Grab the correct hyperslab into which to write
    hsize_t count[3] = { nPkt[i], lossMech::nMech, photEn.size() };
    hsize_t offset[3] = { pktPtr, 0, 0 };
    H5Sselect_hyperslab(Photon_dLdE_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Create the memory data space to specify the layout of the data
    // in memory
    hid_t memspace = H5Screate_simple(3, count, nullptr);
    
    // Action depends on whether we own these data
    if (i == MPIUtil::myRank) {

      // We own these data, so calculate emission rate and write to file
      H5Dwrite(Photon_dLdE_dataset, H5T_NATIVE_REAL, memspace,
	       Photon_dLdE_dataspace_id, H5P_DEFAULT,
	       tree.photLum(t, photEn).data());
    }
#ifdef ENABLE_MPI
    else {

      // Data must be imported from a remote rank      
      MPI_Status stat;
      vector<Real> photLum(nPkt[i] * lossMech::nMech * photEn.size());
      MPI_Recv(photLum.data(), photLum.size(), MPIUtil::MPI_Real, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);

      // Write imported data
      H5Dwrite(Photon_dLdE_dataset, H5T_NATIVE_REAL, memspace,
	       Photon_dLdE_dataspace_id, H5P_DEFAULT,
	       photLum.data());
    }
#endif

    // Free memory space specifier
    H5Sclose(memspace);
    
    // Increment counter
    pktPtr += nPkt[i];
  }

  // Free all HDF5 infrastructure
  H5Sclose(Photon_dLdE_dataspace_id);
  H5Sclose(Photon_dataspace);
  H5Dclose(Photon_dLdE_dataset);
  H5Gclose(Photon_grp);
}


void CheckpointManager::
writePhotonsSummary(hid_t& fileID,
		    const vector<Real>& photEn,
		    const Real t,
		    const CRTree& tree) const {

  // If no packets, do nothing
  if (tree.nPacket() == 0) return;

  // Compute photon luminosity due to packets in local memory
  if (verbosity > 1) {
    cout << "CheckpointManager: computing photon emission" << endl;
  }
  vector<Real> photLumSummary = tree.photLumSummary(t, photEn);
  
  // Sum photon luminosities across MPI ranks
#ifdef ENABLE_MPI
  MPI_Reduce(MPI_IN_PLACE, photLumSummary.data(), photLumSummary.size(),
	     MPIUtil::MPI_Real, MPI_SUM, IORank, MPI_COMM_WORLD);
#endif
  
  // Create photon group
  hid_t Photon_grp = H5Gcreate(fileID, "PhotonEmission", H5P_DEFAULT,
			       H5P_DEFAULT, H5P_DEFAULT);

  // Add an attribute to store the set of photon energies at which the
  // spectrum is being computed
  {
    hsize_t dims = photEn.size();
    hid_t dataspace_id = H5Screate_simple(1, &dims, nullptr);
    hid_t photEn_id = H5Acreate(Photon_grp, "photEn", H5T_NATIVE_REAL,
				dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(photEn_id, H5T_NATIVE_REAL, photEn.data());
    H5Aclose(photEn_id);
    H5Sclose(dataspace_id);
  }

  // Create data space
  hsize_t dims[4] = { partTypes::nPartType, 2, lossMech::nMech,
    photEn.size() };
  hid_t Photon_dataspace = H5Screate_simple(4, dims, nullptr);

  // Create data set
  hid_t Photon_dLdE_dataset = H5Dcreate2(fileID,
					 "/PhotonEmission/dLdEsummary",
					 H5T_NATIVE_REAL,
					 Photon_dataspace,
					 H5P_DEFAULT,
					 H5P_DEFAULT,
					 H5P_DEFAULT);
  hid_t Photon_dLdE_dataspace_id = H5Dget_space(Photon_dLdE_dataset);

  // Write summary data
  H5Dwrite(Photon_dLdE_dataset, H5T_NATIVE_REAL, H5S_ALL,
	   Photon_dLdE_dataspace_id, H5P_DEFAULT,
	   photLumSummary.data());

  // Free all HDF5 infrastructure
  H5Sclose(Photon_dLdE_dataspace_id);
  H5Sclose(Photon_dataspace);
  H5Dclose(Photon_dLdE_dataset);
  H5Gclose(Photon_grp);
}


void CheckpointManager::writeIonization(hid_t& fileID,
					const vector<IdxType>& nPkt,
					const Real& t,
					const CRTree& tree) const {

  // If no packets, do nothing
  if (tree.nPacket() == 0) return;
  
  // Create group
  hid_t Ion_grp = H5Gcreate(fileID, "Ionization", H5P_DEFAULT,
			    H5P_DEFAULT, H5P_DEFAULT);

  // Write attributes giving names of ionization targets
  {
    hsize_t dims = ionData::nTarget;
    hid_t targetName_dataspace_id = H5Screate_simple(1, &dims, nullptr);
    hid_t strtype = H5Tcopy(H5T_C_S1);
    H5Tset_size(strtype, 3);
    hid_t targetName_id = H5Acreate(Ion_grp, "TargetNames",
				    strtype, targetName_dataspace_id,
				    H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(targetName_id, strtype, ionData::targetNames.data());
    H5Aclose(targetName_id);
    H5Sclose(targetName_dataspace_id);
  }
  
  // Create data space
  hsize_t dims[2] = { tree.nPacket(), ionData::nTarget };
  hid_t Ion_dataspace = H5Screate_simple(2, dims, nullptr);

  // Create data set
  hid_t Ion_dataset = H5Dcreate2(fileID,
				 "Ionization/ionRate",
				 H5T_NATIVE_REAL,
				 Ion_dataspace,
				 H5P_DEFAULT,
				 H5P_DEFAULT,
				 H5P_DEFAULT);
  hid_t Ion_dataspace_id = H5Dget_space(Ion_dataset);

  // Loop over MPI ranks
  IdxType pktPtr = 0;
  for (int i = 0; i < MPIUtil::nRank; i++) {

    // Skip if no packets on this rank
    if (nPkt[i] == 0) continue;

    // Grab the correct hyperslab into which to write
    hsize_t count[2] = { nPkt[i], ionData::nTarget };
    hsize_t offset[2] = { pktPtr, 0 };
    H5Sselect_hyperslab(Ion_dataspace_id, H5S_SELECT_SET,
			offset, nullptr, count, nullptr);

    // Create the memory data space to specify the layout of the data
    // in memory
    hid_t memspace = H5Screate_simple(2, count, nullptr);
    
    // Action depends on whether we own these data
    if (i == MPIUtil::myRank) {

      // We own these data, so calculate ionization rate and write to file
      H5Dwrite(Ion_dataset, H5T_NATIVE_REAL, memspace,
	       Ion_dataspace_id, H5P_DEFAULT,
	       tree.ionRate(t).data());
    }
#ifdef ENABLE_MPI
    else {

      // Data must be imported from a remote rank      
      MPI_Status stat;
      vector<Real> ionRate(nPkt[i] * ionData::nTarget);
      MPI_Recv(ionRate.data(), ionRate.size(), MPIUtil::MPI_Real, i,
	       MPIUtil::tagHDF5IO, MPI_COMM_WORLD, &stat);

      // Write imported data
      H5Dwrite(Ion_dataset, H5T_NATIVE_REAL, memspace,
	       Ion_dataspace_id, H5P_DEFAULT,
	       ionRate.data());
    }
#endif

    // Free memory space specifier
    H5Sclose(memspace);
    
    // Increment counter
    pktPtr += nPkt[i];
  }

  // Free all HDF5 infrastructure
  H5Sclose(Ion_dataspace);
  H5Sclose(Ion_dataspace_id);
  H5Dclose(Ion_dataset);
  H5Gclose(Ion_grp);
}
